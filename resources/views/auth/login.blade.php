<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
    <link rel="icon" type="image/png" href="{{ asset('logindesign/images/icons/favicon.ico') }}"/>

<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/logindesign/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/logindesign/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/logindesign/vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="/logindesign/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/logindesign/vendor/select2/select2.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/logindesign/css/util.css">
    <link rel="stylesheet" type="text/css" href="/logindesign/css/main.css">
<!--===============================================================================================-->
</head>
<body>
    
    <div class="limiter">
        <div class="container-login100" style="background:  url('{{ URL::asset('assets/bg/rt2.jpg') }}'); background-size: cover; background-repeat:no-repeat;  background-position: center center;  height: 100vh;">
                     
                  
                    <div class="fixed-top titleclass">
                        <span  style="color: #21A014 ; font-weight: bold;font-family: Calibri;">
                            e-Compendium for Asi@Connect
                        </span>
                    </div>
                  
                  <div class="test">
                    <form class="login100-form validate-form" method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                            <input class="input100" type="text" name="email" placeholder="Email" value="{{ old('email') }}">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </span>
                        </div>

                        <div class="wrap-input100 validate-input" data-validate = "Password is required">
                            <input class="input100" type="password" name="password" placeholder="Password">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-lock" aria-hidden="true"></i>
                            </span>
                        </div>
                        
                        <div class="container-login100-form-btn">
                            <button class="login100-form-btn">
                                Sign In 
                            </button>
                        </div>


                        <div class="container-login100-form-btn">
                            <a class="login100-form-btn" href="{{ route('guestview') }}" >Guest View</a>
                        </div>

                        <div class="text-center" style="margin-top: 10px">
                            <a class="txt2" href="{{ route('register') }}" style=" font-weight: bold;font-size: 16px; color:#57b846;">
                                Create your Account
                                <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                            </a>
                        </div>
                    </form>
                </div>

        
        </div>
    </div>
    
    

    
<!--===============================================================================================-->  
    <script src="/logindesign/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
    <script src="/logindesign/vendor/bootstrap/js/popper.js"></script>
    <script src="/logindesign/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
    <script src="/logindesign/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
    <script src="/logindesign/vendor/tilt/tilt.jquery.min.js"></script>
    <script >
        $('.js-tilt').tilt({
            scale: 1.1
        })
    </script>
<!--===============================================================================================-->
    <script src="/logindesign/js/main.js"></script>

</body>
</html>