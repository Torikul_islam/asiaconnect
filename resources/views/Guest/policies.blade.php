@extends('Guest.guestlayout.app')



@push('after-styles')
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/> -->

    <link rel="stylesheet" href="{{ asset('dist/css/bootstrap-multiselect.css') }}">

@endpush




@section('content')
    <!-- main content start -->
    <div class="main-content">

        <!-- content -->
        <section class="container-fluid content-top-gap">


            <!-- forms -->
            <section class="forms">
                <!-- forms 1 -->
                <div class="card card_border py-2 mb-4">

                    <div class="card-body">

                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://www.bdren.net.bd/" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.bdren.net.bd/" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.bdren.net.bd/" title="Asiaconnect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>
                        </div>

                        <form method="POST" action="{{route('policy',['slug'=>request('slug')])}}" enctype="multipart/form-data">
                            @csrf

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group ">
                                            <label class="input readonly__label text-primary">Please Select the
                                                Organization: </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <!-- <label>Organization:</label> -->
                                            <select class="form-control form-control-sm required" name="ren_id[]"
                                                    multiple="multiple" id="example-selectAllValue" required>
                                                @foreach($users as $U_data)
                                                    <option value="{{ $U_data->id}}">{{ $U_data->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-12 d-flex justify-content-center">
                                        <button type="submit" class="btn btn-primary">Find Result</button>
                                    </div>

                                </div>
                            </fieldset>

                        </form>

                        @if(count($labelData??[]) >0)
                            <div style="width: 80%; height: 100%; margin: 0 auto;">
                                <canvas id="myChart"></canvas>
                            </div>
                        @endif

                        @if(count($barvalue??[]) >0)
                            <div style="width: 80%; height: 100%; margin: 0 auto;">
                                <canvas id="barChart"></canvas>
                            </div>
                        @endif

                        @if(count($technical??[]) >0)
                        <!-- <div style="width: 80%; height: 480px; margin: 0 auto;">
                            <canvas id="EmployeesbarChart"></canvas>
                        </div> -->
                            <div class="row">
                                <div class="col-md-8">
                                    <canvas id="EmployeesbarChart"></canvas>
                                </div>
                                <div>
                                    <canvas id="pieoftechnicalandnontechnical"></canvas>
                                </div>
                            </div>
                        @endif

                        @if(count($emp_permanent??[]) >0)
                        <!-- <div style="width: 80%; height: 480px; margin: 0 auto;">
                            <canvas id="EmployeesbarChart"></canvas>
                        </div> -->
                            <div class="row">
                                <div class="col-md-8">
                                    <canvas id="EmpStatusbarChart"></canvas>
                                </div>
                                <div>
                                    <canvas id="jobstatus"></canvas>
                                </div>
                            </div>
                        @endif

                        @if(count($own_fund??[]) >0)
                            <div style="width: 80%; height: 100%; margin: 0 auto;">
                                <canvas id="financegraph"></canvas>
                            </div>
                        @endif

                        @if(count($operation_key??[]) >0)
                            <div class="row">
                                <div class="col-sm-6">
                                    <div style="overflow-x:auto;">
                                        <table id="myFileTable" class="table table-striped table-bordered" style="text-align: center;">
                                            <thead class="text-capitalize " style="text-align: center;" >
                                                <tr>
                                                    <th >RENS Name</th>
                                                    <th >Network Operation</th>
                                                    <th>Network Maintenance</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($test as $data)
                                                    <tr>
                                                        <td>{{$data['user_name']}}</td>
                                                        <td >{{$data['network_operation']}}</td>
                                                        <td>{{$data['network_maintenance']}}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <canvas id="networkOperation"></canvas>
                                </div>
                                <div class="col-md-3">
                                    <canvas id="networkMaintenance"></canvas>
                                </div>
                            </div>
                        @endif

                        @if(count($less100??[]) >0)
                        <div style="width: 80%; height: 100%; margin: 0 auto;">
                            <canvas id="linkcapacity"></canvas>
                        </div>
                        @endif

                    </div>
                </div>
            </section>
        </section>
        <!-- //content -->
    </div>
    <!-- main content end-->
@endsection


@push('script')
    <script type="text/javascript">
        /*$(document).ready(function () {
            $('.select2').select2({width: '100%'});
        });
*/
        $(document).ready(function () {
            $('#example-selectAllValue').multiselect({
                includeSelectAllOption: true,
                selectAllValue: 'select-all-value',
                maxHeight: 400
            });
        });

        var ctx = $("#myChart");

        var myChart = new Chart(ctx, {
            type: 'matrix',
            data: {
                datasets: [{
                    label: 'My Matrix',
                    data: @json($graph??[]),
                    backgroundColor(ctx) {
                        return ctx.chart.data.datasets[0].data.map(function (value, index) {
                            return value.v === 0 ? '#ecebeb' : '#00b000';
                        });
                    },
                    borderColor(ctx) {
                        return "rgb(20,20,20)";
                    },
                    borderWidth: 1,
                    width: 35,
                    height: 25,
                }]
            },
            options: {
                plugins: {
                    legend: false,
                    title: {
                        display: true,
                        text: @json($title??[])
                    },
                    tooltip: {
                        callbacks: {
                            label(context) {
                                const v = context.dataset.data[context.dataIndex];
                                return ['x: ' + v.x, 'y: ' + v.y, 'v: ' + v.v];
                            }
                        }
                    }
                },
                scales: {
                    x: {
                        type: 'category',
                        labels: @json($selectedUsers??[]),
                        ticks: {
                            display: true,
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        },
                        grid: {
                            display: false
                        }
                    },
                    y: {
                        type: 'category',
                        labels: @json($labelData??[]),
                        offset: true,
                        ticks: {
                            display: true
                        },
                        grid: {
                            display: false
                        }
                    }
                }
            }
        });

        $(function () {

            //get the bar chart canvas
            var ctx = $("#barChart");

            //bar chart data
            var data = {
                labels: @json($selectedUsers??[]),
                datasets: [
                    {
                        data: @json($barvalue??[]),

                        backgroundColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderWidth: 1,
                        datalabels:{
                            color: 'blue',
                            anchor: 'end',
                            align: 'top'
                        }
                    }
                ]
            };

            //options
            var options = {
                responsive: true,
                plugins: {
                    title: {
                        display: true,
                        text: @json($titletext??[])
                    },
                    legend: {
                        display: false
                    }
                },
                scales: {
                    x: {
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    },
                    y: {
                        type:'logarithmic'
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

        //Employees Based on profession
        $(function () {

            //get the bar chart canvas
            var ctx = $("#EmployeesbarChart");

            //bar chart data
            var data = {
                labels: @json($selectedUsers??[]),
                datasets: [
                    {
                        label: @json($labeltitle[0]??[]),
                        data: @json($technical??[]),

                        backgroundColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderWidth: 1
                    },
                    {
                        label: @json($labeltitle[1]??[]),
                        data: @json($non_technical??[]),

                        backgroundColor: [
                            "rgba(211, 223, 84, 0.8)",
                        ],
                        borderColor: [
                            "rgba(211, 223, 84, 0.8)",
                        ],
                        borderWidth: 1
                    }
                ]
            };

            //options
            var options = {
                responsive: true,
                title: {
                    display: true,
                    position: "top",
                    text: "Bar Graph",
                    fontSize: 18,
                    fontColor: "#111"
                },
                legend: {
                    display: true,
                    position: "bottom",
                    labels: {
                        fontColor: "#333",
                        fontSize: 16

                    }
                },
                scales: {
                    x: {
                        stacked: true,
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    },
                    y: {
                        stacked: true,
                        type:'logarithmic'
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });


        //for technical and non-technical pie chart
        $(function () {

            //get the bar chart canvas
            var ctx = $("#pieoftechnicalandnontechnical");

            //bar chart data
            var data = {
                labels: @json($labeltitle??[]),
                datasets: [
                    {
                        label: " ",
                        data: @json($tech_non_techData??[]),

                        backgroundColor: [
                            'rgba(0,0,255,0.3)',
                            'rgba(211, 223, 84, 0.8)'
                        ],
                        hoverOffset: 12
                    }
                ]
            };

            //options
            var options = {
                plugins: {
                    title: {
                        display: true,
                        text: @json($titletext??[])
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });


        //Employees Based on Job Status
        $(function () {

            //get the bar chart canvas
            var ctx = $("#EmpStatusbarChart");

            //bar chart data
            var data = {
                labels: @json($selectedUsers??[]),
                datasets: [
                    {
                        label: "Permanent",
                        data: @json($emp_permanent??[]),

                        backgroundColor: [
                            "rgba(211, 223, 84, 0.8)",
                        ],
                        borderColor: [
                            "rgba(211, 223, 84, 0.8)",
                        ],
                        borderWidth: 1
                    },
                    {
                        label: "Contract",
                        data: @json($emp_contract??[]),

                        backgroundColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderWidth: 1
                    },
                    {
                        label: "Outsourced",
                        data: @json($emp_outsourced??[]),

                        backgroundColor: [
                            "rgba(200, 240, 16, 0.8)",
                        ],
                        borderColor: [
                            "rgba(200, 240, 16, 0.8)",
                        ],
                        borderWidth: 1
                    }
                ]
            };

            //options
            var options = {
                responsive: true,
                title: {
                    display: true,
                    position: "top",
                    text: "Bar Graph",
                    fontSize: 18,
                    fontColor: "#111"
                },
                legend: {
                    display: true,
                    position: "bottom",
                    labels: {
                        fontColor: "#333",
                        fontSize: 16

                    }
                },
                scales: {
                    x: {
                        stacked: true,
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    },
                    y: {
                        stacked: true,
                        type:'logarithmic'
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });


        //for job status pie chart
        $(function () {

            //get the bar chart canvas
            var ctx = $("#jobstatus");

            //bar chart data
            var data = {
                labels: ['Permanent', 'Contractual', 'Outsourced'],
                datasets: [
                    {
                        label: " ",
                        data: @json($job_status??[]),

                        backgroundColor: [
                            "rgba(211, 223, 84, 0.8)",
                            "rgba(0,0,255,0.3)",
                            "rgba(200, 240, 16, 0.8)",
                        ],
                        hoverOffset: 12
                    }
                ]
            };

            //options
            var options = {
                plugins: {
                    title: {
                        display: true,
                        text: @json($titletext??[])
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

        //For Finance Horizontal stacked graph
        $(function () {

            //get the bar chart canvas
            var ctx = $("#financegraph");

            //bar chart data
            var data = {
                labels: @json($selectedUsers??[]),
                datasets: [
                    {
                        label: @json($labeltitle[0]??[]),
                        data: @json($govt_fund??[]).map(item=>item == 0 ? ' ' :item),

                        backgroundColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderWidth: 1
                    },
                    {
                        label: @json($labeltitle[1]??[]),
                        data: @json($own_fund??[]).map(item=>item == 0 ? ' ' :item),

                        backgroundColor: [
                            "rgba(211, 223, 84, 0.8)",
                        ],
                        borderColor: [
                            "rgba(211, 223, 84, 0.8)",
                        ],
                        borderWidth: 1
                    },
                    {
                        label: @json($labeltitle[2]??[]),
                        data: @json($donation??[]).map(item=>item == 0 ? ' ' :item),

                        backgroundColor: [
                            "rgba(178, 92, 136, 0.24)",
                        ],
                        borderColor: [
                            "rgba(178, 92, 136, 0.24)",
                        ],
                        borderWidth: 1
                    },
                    {
                        label: @json($labeltitle[3]??[]),
                        data: @json($others??[]).map(item=>item == 0 ? ' ' :item),

                        backgroundColor: [
                            "#f1c40f",
                        ],
                        borderColor: [
                            "#f1c40f",
                        ],
                        borderWidth: 1
                    }
                ]
            };

            //options
            var options = {
                
                responsive: true,
                plugins: {
                    legend: {
                        display: true,
                        position: "top",
                        labels: {
                            fontColor: "#333",
                            fontSize: 16

                        }
                    },
                    title: {
                        display: true,
                        text: @json($titletext??[])
                    }
                },

                scales: {
                    x: {
                        stacked: true,
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    },
                    y: {
                        stacked: true
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

        //Network Operation
        $(function () {

            //get the bar chart canvas
            var ctx = $("#networkOperation");

            //bar chart data
            var data = {
                labels: @json($operation_key??[]),
                datasets: [
                    {
                        label: " ",
                        data: @json($operation_value??[]),

                        backgroundColor: [
                            'rgba(0,0,255,0.3)',
                            'rgba(211, 223, 84, 0.8)',
                            'rgba(206, 180, 194, 0.8)'
                        ],
                        hoverOffset: 12
                    }
                ]
            };

            //options
            var options = {
                plugins: {
                    title: {
                        display: true,
                        text: 'Network Operations'
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

        //Network Maintenance
        $(function () {

            //get the bar chart canvas
            var ctx = $("#networkMaintenance");

            //bar chart data
            var data = {
                labels: @json($maintenance_key??[]),
                datasets: [
                    {
                        label: " ",
                        data: @json($maintenance_value??[]),

                        backgroundColor: [
                            'rgba(0,0,255,0.3)',
                            'rgba(211, 223, 84, 0.8)',
                            'rgba(206, 180, 194, 0.8)'
                        ],
                        hoverOffset: 12
                    }
                ]
            };

            //options
            var options = {
                plugins: {
                    title: {
                        display: true,
                        text: 'Network Maintenance'
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

        // Link Capacity University
        $(function () {

            //get the bar chart canvas
            var ctx = $("#linkcapacity");

            //bar chart data
            var data = {
                labels: @json($selectedUsers??[]),
                datasets: [
                    {
                        label: @json($leveltext[0]??[]),
                        data: @json($less100??[]),

                        backgroundColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderWidth: 1
                    },
                    {
                        label: @json($leveltext[1]??[]),
                        data: @json($e100??[]),

                        backgroundColor: [
                            "rgba(211, 223, 84, 0.8)",
                        ],
                        borderColor: [
                            "rgba(211, 223, 84, 0.8)",
                        ],
                        borderWidth: 1
                    },
                    {
                        label: @json($leveltext[2]??[]),
                        data: @json($t1G??[]),

                        backgroundColor: [
                            "rgba(178, 92, 136, 0.24)",
                        ],
                        borderColor: [
                            "rgba(178, 92, 136, 0.24)",
                        ],
                        borderWidth: 1
                    },
                    {
                        label: @json($leveltext[3]??[]),
                        data: @json($t10G??[]),

                        backgroundColor: [
                            "rgba(.8, .3, .5, 0.2)",
                        ],
                        borderColor: [
                            "rgba(.8, .3, .5, 0.2)",
                        ],
                        borderWidth: 1
                    },
                    {
                        label: @json($leveltext[4]??[]),
                        data: @json($t40G??[]),

                        backgroundColor: [
                            "rgba(844, 114, 116, 0.8)",
                        ],
                        borderColor: [
                            "rgba(844, 114, 116, 0.8)",
                        ],
                        borderWidth: 1
                    }
                ]
            };

            //options
            var options = {
                
                responsive: true,
                plugins: {
                      legend: {
                                display: true,
                                position: "top",
                                labels: {
                                    fontColor: "#333",
                                    fontSize: 16

                                }
                                },
                      title: {
                        display: true,
                        text: @json($titletext??[])
                      }
                    },

                scales: {
                    x: {
                    stacked: true,
                    ticks: {
                            autoSkip: true,
                            maxRotation: 90,
                            minRotation: 90
                        }  
                  },
                  y: {
                    stacked: true,
                    type:'logarithmic'
                    
                  }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

    </script>
@endpush
