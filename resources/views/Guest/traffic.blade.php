@extends('Guest.guestlayout.app')


@push('after-styles')
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/> -->

    <link rel="stylesheet" href="{{ asset('dist/css/bootstrap-multiselect.css') }}">

@endpush

@section('content')
    <!-- main content start -->
    <div class="main-content">

        <!-- content -->
        <section class="container-fluid content-top-gap">


            <!-- forms -->
            <section class="forms">
                <!-- forms 1 -->
                <div class="card card_border py-2 mb-4">

                    <div class="card-body">

                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://www.bdren.net.bd/" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.bdren.net.bd/" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.bdren.net.bd/" title="Asiaconnect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>
                        </div>

                        <form required method="POST"
                              action="{{route('traffic',['slug'=>request('slug')])}}" enctype="multipart/form-data">
                            @csrf

                            <fieldset class="form-group">

                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group ">
                                            <label class="input readonly__label text-primary">Please Select the Organization</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <!-- <label>Organization:</label> -->
                                            <select class="form-control form-control-sm required" name="ren_id[]"
                                                    multiple="multiple" id="example-selectAllValue" required>

                                                @foreach($users as $U_data)
                                                    <option value="{{ $U_data->id}}"
                                                            @if($ren_id  == $U_data->id) selected @endif >{{ $U_data->name }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                    

                                    <div class="col-12 d-flex justify-content-center">
                                        <button type="submit" class="btn btn-primary">Find Result</button>
                                    </div>
                                </div>
                            </fieldset>

                        </form>

                        @if(count($labelData??[]) >0)
                            <div style="width: 80%;height: 100%;margin: 0 auto;">
                                <canvas id="myChart"></canvas>
                            </div>
                        @endif

                        @if(count($data2019??[]) >0)
                            <div style="width: 80%;height: 680px;margin: 0 auto;">
                                <canvas id="traffic"></canvas>
                            </div>
                        @endif

                         @if(count($key??[]) >0)
                         <div class="row">
                             <div class="col-sm-6">
                                 <div style="overflow-x:auto;">
                                        <table id="myFileTable" class="table table-striped table-bordered" style="text-align: center;">
                                            <thead class="text-capitalize " style="text-align: center;" >
                                                <tr>
                                                    <th >RENS Name</th>
                                                    <th >NOC Availability</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($noccollect as $data)
                                                    <tr>
                                                        <td>{{$data['user_name']}}</td>
                                                        <td >{{$data['NOC']}}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        
                                        </table>
                                    </div>
                            </div>
                            <div class="col-sm-4">
                                <canvas id="nocservice"></canvas>
                            </div>
                         </div>  
                        @endif

                        @if(count($key1??[]) >0)
                        <div class="row">
                                <div class="col-sm-6">
                                    <div style="overflow-x:auto;">
                                        <table id="myFileTable" class="table table-striped table-bordered" style="text-align: center;">
                                            <thead class="text-capitalize " style="text-align: center;" >
                                                <tr>
                                                    <th >RENS Name</th>
                                                    <th >SLA</th>
                                                    <th>Presence of Financial Demurrage</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($sla_compensation as $data)
                                                    <tr>
                                                        <td>{{$data['user_name']}}</td>
                                                        <td >{{$data['SLA']}}</td>
                                                        <td>{{$data['option_of_compensation']}}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        
                                        </table>
                                    </div>
                                </div>
                            <div class="col-md-3">
                                <canvas id="sla"></canvas>
                            </div>
                            <div class="col-md-3">
                                <canvas id="demurage"></canvas>
                            </div>
                        </div>
                        @endif

                        @if(count($doublegraph??[]) >0)
                            <div style="width: 80%;height: 100%;margin: 0 auto;">
                                <canvas id="toolschart"></canvas>
                            </div>
                        @endif

                    </div>
                </div>
            </section>
        </section>
        <!-- //content -->
    </div>
    <!-- main content end-->
@endsection


@push('script')
    <script>

        $(document).ready(function () {
            $('#example-selectAllValue').multiselect({
                includeSelectAllOption: true,
                selectAllValue: 'select-all-value',
                maxHeight: 400
            });

        });

        var ctx = $("#myChart");

        var myChart = new Chart(ctx, {
            type: 'matrix',
            data: {
                datasets: [{
                    label: 'My Matrix',
                    data: @json($graph??[]),
                    backgroundColor(ctx) {
                        return ctx.chart.data.datasets[0].data.map(function (value, index) {
                            return value.v === 0 ? '#ecebeb' : '#00b000';
                        });
                    },
                    borderColor(ctx) {
                        return "rgb(20,20,20)";
                    },
                    borderWidth: 1,
                    width: 35,
                    height: 25,
                }]
            },
            options: {
                plugins: {
                    legend: false,
                    title: {
                        display: true,
                        text: @json($title??[])
                    },
                    tooltip: {
                        callbacks: {
                            label(context) {
                                const v = context.dataset.data[context.dataIndex];
                                return ['x: ' + v.x, 'y: ' + v.y, 'v: ' + v.v];
                            }
                        }
                    }
                },
                scales: {
                    x: {
                        type: 'category',
                        labels: @json($selectedUsers??[]),
                        ticks: {
                            display: true,
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        },
                        grid: {
                            display: false
                        }
                    },
                    y: {
                        type: 'category',
                        labels: @json($labelData??[]),
                        offset: true,
                        ticks: {
                            display: true
                        },
                        grid: {
                            display: false
                        }
                    }
                }
            }
        });


        $(function () {
            //get the bar chart canvas
            var ctx = $("#traffic");

            //bar chart data
            var data = {
                labels: @json($ren_name??[]),
                datasets: [
                    {
                        label: @json($level[0]??[]),
                        data: @json($data2019??[]).map(item=>item == 0 ? ' ' :item),

                        backgroundColor: [
                            "rgba(14, 231, 28, 0.8)", 
                        ],
                        borderColor: [
                            "rgba(14, 231, 28, 0.8)",
                        ],
                        borderWidth: 1,
                        datalabels:{
                            color: 'blue',
                            rotation: '270'

                        }
                    },
                    {
                        label:@json($level[1]??[]),
                        data: @json($data2018??[]).map(item=>item == 0 ? ' ' :item),

                        backgroundColor: [
                            "rgba(81, 37, 132, 0.62)",
                        ],
                        borderColor: [
                            "rgba(81, 37, 132, 0.62)",
                        ],
                        borderWidth: 1,
                        datalabels:{
                            color: 'blue',
                            rotation: '270'

                        }
                    }
                ]
            };

            //options
            var options = {
               
                responsive: true,
                elements: {
                  bar: {
                    borderWidth: 2,
                  }
                },
                plugins: {
                  legend: {
                    position: 'top',
                  },
                  title: {
                    display: true,
                    text: @json($title??[])
                  }
                },
                scales: {
                    x: {
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    },
                    y: {
                        type:'logarithmic'
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

        //Noc Services 24*7
        $(function () {

            //get the bar chart canvas
            var ctx = $("#nocservice");

            //bar chart data
            var data = {
                labels: @json($key??[]),
                datasets: [
                    {
                        label: " ",
                        data: @json($value??[]),

                        backgroundColor: [
                          'rgba(0,0,255,0.3)',
                          'rgba(211, 223, 84, 0.8)'
                        ],
                         hoverOffset: 12
                    }
                ]
            };

            //options
            var options = {
                plugins: {
                    title: {
                        display: true,
                        text: @json($title??[])
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

        //sla
        $(function () {

            //get the bar chart canvas
            var ctx = $("#sla");

            //bar chart data
            var data = {
                labels: @json($key1??[]),
                datasets: [
                    {
                        label: " ",
                        data: @json($value1??[]),

                        backgroundColor: [
                          'rgba(0,0,255,0.3)',
                          'rgba(211, 223, 84, 0.8)'
                        ],
                         hoverOffset: 12
                    }
                ]
            };

            //options
            var options = {
                plugins: {
                    title: {
                        display: true,
                        text: @json($title1??[])
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

        //demurage
        $(function () {

            //get the bar chart canvas
            var ctx = $("#demurage");

            //bar chart data
            var data = {
                labels: @json($key2??[]),
                datasets: [
                    {
                        label: " ",
                        data: @json($value2??[]),

                        backgroundColor: [
                          'rgba(0,0,255,0.3)',
                          'rgba(211, 223, 84, 0.8)'
                        ],
                         hoverOffset: 12
                    }
                ]
            };

            //options
            var options = {
                plugins: {
                    title: {
                        display: true,
                        text: @json($title2??[])
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });


        $(function () {
            //get the bar chart canvas
            var ctx = $("#toolschart");

            //bar chart data
            var data = {
                labels: @json($doublegraph??[]),
                datasets: [
                    {
                        label:@json($datasetlevel??[]),
                        data: @json($doublegraphvalue??[]),

                        backgroundColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderWidth: 1
                    }
                ]
            };

            //options
            var options = {
                responsive: true,
                plugins: {
                    title: {
                        display: true,
                        text: @json($title??[])
                    },
                    legend: {
                        display: false
                    }
                },
                scales: {
                    x: {
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

    </script>


@endpush
