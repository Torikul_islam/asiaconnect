@extends('Guest.guestlayout.app')


<?php
function class_generator($key, $value)
{
    $k = explode('-', $key)[1];

    if ($k == 'service_demand' && $value == 1) {
        return 'bg-success';
    } elseif ($k == 'service_implemented' && $value == 1) {
        return 'bg-danger';
    } elseif ($k == 'service_pipeline' && $value == 1) {
        return 'bg-info';
    } else {
        return 'bg-light';
    }
}
?>
@section('content')
    <!-- main content start -->
    <div class="main-content">

        <!-- content -->
        <section class="container-fluid content-top-gap">
            <div class="table-responsive">
                <table class="table table-sm">
                    @foreach($tableData as $rowKey => $rowValue)
                        <tr>
                            <td width="{{100/(count($rowValue)+1)}}%;">
                                {{$rowKey}}
                            </td>

                            @foreach ($rowValue as $key => $value)
                                <td class="{{class_generator($key, $value)}}">
                                    &nbsp;
                                </td>
                            @endforeach
                        </tr>
                    @endforeach

                    <tr>
                        <td></td>
                        <?php
                        $ranName = "";

                        foreach ($tableData[array_key_first($tableData)] as $key => $value) {
                            $temp = explode('-', $key)[1];
                            if ($temp != $ranName) {
                                $ranName = $temp;
                                echo "<td  align='center'>" .
                                    str_replace("service_", "", $temp) . "</td>";
                            }
                        }

                        ?>
                    </tr>

                    <tr>
                        <td></td>
                        <?php
                        $ranName = "";
                        foreach ($tableData[array_key_first($tableData)] as $key => $value) {
                            $temp = explode('-', $key)[0];
                            if ($temp != $ranName) {
                                $ranName = $temp;
                                echo "<td colspan='3' align='center'>" . $ranName . "</td>";

                            }
                        }

                        ?>
                    </tr>
                </table>
            </div>
        </section>
    </div>


@endsection
