@extends('Guest.guestlayout.app')


@push('after-styles')
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/> -->

    <link rel="stylesheet" href="{{ asset('dist/css/bootstrap-multiselect.css') }}">

@endpush

@section('content')
    <!-- main content start -->
    <div class="main-content">

        <!-- content -->
        <section class="container-fluid content-top-gap">


            <!-- forms -->
            <section class="forms">
                <!-- forms 1 -->
                <div class="card card_border py-2 mb-4">

                    <div class="card-body">

                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://www.bdren.net.bd/" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.bdren.net.bd/" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.bdren.net.bd/" title="Asiaconnect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>
                        </div>

                        <form required method="POST"
                              action="{{route('landscape',['slug'=>request('slug')])}}" enctype="multipart/form-data">
                            @csrf

                            <fieldset class="form-group">

                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group ">
                                            <label class="input readonly__label text-primary">Please Select the Organization</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <!-- <label>Organization:</label> -->
                                            <select class="form-control form-control-sm required" name="ren_id[]"
                                                    multiple="multiple" id="example-selectAllValue" required>

                                                @foreach($users as $U_data)
                                                    <option value="{{ $U_data->id}}"
                                                            @if($ren_id  == $U_data->id) selected @endif >{{ $U_data->name }}</option>
                                                @endforeach


                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group ">
                                            <label class="input readonly__label text-primary">Type of Institution: </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <!-- <label>Service Platform</label> -->
                                            <select class="form-control form-control-sm required"
                                                    name="institution_type[]"
                                                    multiple="multiple" id="institute_type" required="required">

                                                <option value="Institutions_connected_Universities">Universities
                                                </option>
                                                <option value="Institutions_connected_RI">Research Institutes
                                                </option>
                                                <option value="Institutions_connected_govt">Government Organizations
                                                </option>
                                                <option value="Institutions_connected_school">Schools and Colleges
                                                </option>
                                                <option value="Institutions_connected_other">Others(Hospitals/Medical
                                                    Colleges)
                                                </option>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-12 d-flex justify-content-center">
                                        <button type="submit" class="btn btn-primary">Find Result</button>
                                    </div>
                                </div>
                            </fieldset>

                        </form>

                        @if(count($mainColl??[]) >0)
                            <div style="width: 80%;height: 680px;margin: 0 auto;">
                                <canvas id="myChart"></canvas>
                            </div>
                        @endif

                    </div>
                </div>
            </section>
        </section>
        <!-- //content -->
    </div>
    <!-- main content end-->
@endsection


@push('script')
    <script>

        $(document).ready(function () {
            $('#example-selectAllValue').multiselect({
                includeSelectAllOption: true,
                selectAllValue: 'select-all-value',
                maxHeight: 400
            });

            $('#institute_type').multiselect({
                includeSelectAllOption: true,
                selectAllValue: 'select-all-value',
                maxHeight: 400
            });
        });

        $(function () {
            //get the bar chart canvas
            var ctx = $("#myChart");

            var main_data = @json($mainColl??[]).
            reduce((group, product) => {
                const {ren_name} = product;
                group[ren_name] = group[ren_name] || [];
                group[ren_name].push(product);
                return group;
            }, {});

            // array row to column

            var reverseArr = {}
            var arrayData = @json($mainColl??[]).
            map(function (item) {
                for (var key in item) {
                    if (item.hasOwnProperty(key)) {
                        if (reverseArr[key] === undefined) {
                            reverseArr[key] = [];
                        }
                        reverseArr[key].push(item[key]);
                    }
                }
            });

            if (reverseArr.hasOwnProperty('ren_name')) {
                delete reverseArr['ren_name'];
            }

            var colorPattern = ['#00a65a', '#f39c12', '#f56954', '#00c0ef', '#3c8dbc', '#d2d6de'];


            //bar chart data
            var data = {
                labels: Object.keys(main_data),
                datasets: [
                    ...Object.keys(reverseArr).map(function (key, i) {
                        console.log("Key", key)
                        return {
                            label: key,
                            data: reverseArr[key].map(item=>item == 0 ? ' ' :item),
                            backgroundColor: colorPattern[i] + '99',
                            borderColor: colorPattern[i],
                            borderWidth: 1,
                            datalabels:{
                            color: 'blue',
                            anchor: 'end',
                            align: 'top',
                            rotation: '270'
                        }
                        }
                    })
                ]
            };


            //options
            var options = {
                responsive: true,
                title: {
                    display: true,
                    position: "top",
                    text: "Bar Graph",
                    fontSize: 18,
                    fontColor: "#111"
                },
                legend: {
                    display: false,
                    position: "bottom",
                    labels: {
                        fontColor: "#333",
                        fontSize: 16
                    }
                },
                scales: {
                    yAxes: {
                        type: 'logarithmic'
                    },
                    x: {
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

    </script>


@endpush
