@extends('Guest.guestlayout.app')

@section('content')
    <!-- main content start -->
    <div class="main-content">

        <!-- content -->
        <section class="container-fluid content-top-gap">


            <!-- forms -->
            <section class="forms">
                <!-- forms 1 -->
                <div class="card card_border py-2 mb-4">

                    <div class="card-body">
                        <form method="POST" action="{{route('guest.store')}}" enctype="multipart/form-data">
                            @csrf


                            <div class="cards__heading">
                                <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                                <div class="text-center">
                                    <a href="https://www.bdren.net.bd/" title="EU"><img
                                            src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                            style="height:80px;"> </a>
                                    <a href="https://www.bdren.net.bd/" title="TEIN"><img
                                            src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                            style="height:80px;"> </a>
                                    <a href="https://www.bdren.net.bd/" title="Asiaconnect"><img
                                            src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                            style="height:80px;"> </a>
                                </div>
                            </div>


                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">Please Select the Organization</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Organization:</label>
                                            <select class="form-control form-control-sm" name="ren_id">
                                                @foreach($users as $U_data)
                                                    <option value="{{ $U_data->id}}" @if($ren_id  == $U_data->id) selected @endif >{{ $U_data->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <div class="row">
                                <div class="col-sm-5"></div>
                                <div class="col-sm-4">
                                    <button type="submit" class="btn btn-primary btn-style mt-4" id="result">Find
                                        Result
                                    </button>
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- //forms 1 -->


                @if(count($dataCollect['contact']??[]) >0)
                    <div class="card card_border py-2 mb-4" id="Response">

                    <div class="card-body">
                        @foreach($dataCollect['user_name']??[] as $ren)
                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">Asi@Connect Compendium Response of {{$ren->name}}</h4>
                        </div>
                        @endforeach

                        @foreach($dataCollect['contact']??[] as $contact)
                            <div class="cards__heading">
                                <h3>NREN Contacts<span></span></h3>
                            </div>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">2.Administrative Contact [Governer in
                                        Asi@Connect]</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Name: </label>
                                    <div class="col-sm-4">
                                        
                                        <input readonly type="text" name="ad_con_designation" class="form-control input readonly-style"
                                               value="{{ $contact->ad_con_name}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Designation: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="ad_con_designation" class="form-control input readonly-style"
                                               value="{{ $contact->ad_con_designation}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Email Address: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="email" name="ad_con_email" class="form-control input readonly-style"
                                               value="{{ $contact->ad_con_email}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Phone Number: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="ad_con_phone" class="form-control input readonly-style"
                                               value="{{ $contact->ad_con_phone}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">3.Technical Contact [Governer in Asi@Connect]</label>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Name: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="tec_con_name" class="form-control input readonly-style"
                                               value="{{ $contact->tec_con_name}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Designation: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="tec_con_designation" class="form-control input readonly-style"
                                               value="{{ $contact->tec_con_designation}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Email Address: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="email" name="tec_con_email" class="form-control input readonly-style"
                                               value="{{ $contact->tec_con_email}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Phone Number: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="tec_con_phone" class="form-control input readonly-style"
                                               value="{{ $contact->tec_con_phone}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">4.Visiblity Contact [Governer in Asi@Connect]</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Name: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="visi_con_name" class="form-control input readonly-style"
                                               value="{{ $contact->visi_con_name}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Designation: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="visi_con_designation" class="form-control input readonly-style"
                                               value="{{ $contact->visi_con_designation}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Email Address: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="email" name="visi_con_email" class="form-control input readonly-style"
                                               value="{{ $contact->visi_con_email}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Phone Number: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="visi_con_phone" class="form-control input readonly-style"
                                               value="{{ $contact->visi_con_phone}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                        @endforeach

                        @foreach($dataCollect['structure_and_financing'] ??[] as $structure_and_financing)

                            <div class="cards__heading">
                                <h3>NREN Organization Structure and Financing<span></span></h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">7. Which Strategical Policies are available in your
                                        organization ?</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">

                                        @php
                                            $days = array("Usage Policy","Connectivity Policy", "Work from Home Policy","Environmental Policy","Procurement Policy","Vehicle Usage Policy");
                                $Strapolicies_data = json_decode($structure_and_financing->Strapolicies);

                                 if($Strapolicies_data === null){
                                    $Strapolicies_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check" >
                                            @foreach($days as $value)
                                                <label class="form-check-label" >
                                                    {!! Form::checkbox('Strapolicies[]', $value, in_array($value, $Strapolicies_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">8. How many employees were working in your organizaton
                                        under the following distict categories ??</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Technical: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="emp_technical" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->emp_technical}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Non- Technical: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="emp_non_technical" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->emp_non_technical}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Permanent/Full Time: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="emp_permanent" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->emp_permanent}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Contractual/Part-time: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="emp_contract" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->emp_contract}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Outsourced/Honorary: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="emp_outsourced" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->emp_outsourced}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">9. How many Female employees were working in your
                                        organizaton under the following distict categories ??</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Technical: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="emp_female_technical" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->emp_female_technical}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Non- Technical: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="emp_fem_non_technical" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->emp_fem_non_technical}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Permanent/Full Time: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="emp_fem_permanent" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->emp_fem_permanent}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Contractual/Part-time: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="emp_fem_contract" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->emp_fem_contract}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Outsourced/Honorary: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="emp_fem_outsourced" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->emp_fem_outsourced}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">10. How is your Capital Expenditure Financed (in %)
                                        ??</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Own Fund: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="capital_ownfund" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->capital_ownfund}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Government Fund: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="capital_government" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->capital_government}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Donation: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="capital_donation" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->capital_donation}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Others: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="capital_others" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->capital_others}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">11. How is your Operating Expenditure Financed (in %)
                                        ??</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Own Fund: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="operating_ownfund" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->operating_ownfund}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Government Fund: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="operating_government" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->operating_government}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Donation: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="operating_donation" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->operating_donation}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Others: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="operating_others" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->operating_others}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">12. How do you distribute the operating Expenditure
                                        among the following categories (% of OPex) ??</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Network Operations: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="distribute_netoperation"
                                               class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->distribute_netoperation}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Network Maintenance: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="distribute_netmaintenance"
                                               class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->distribute_netmaintenance}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Administration: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="distribute_administration"
                                               class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->distribute_administration}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Others: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="distribute_other" class="form-control input readonly-style"
                                               value="{{ $structure_and_financing->distribute_other}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">13. How do you perform your "Network Operations"
                                                ?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="network_operation"
                                                   value="Fully Self" <?php if ($structure_and_financing->network_operation == 'Fully Self') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Fully Self
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="network_operation"
                                                   value="Hybrid"<?php if ($structure_and_financing->network_operation == 'Hybrid') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Hybrid
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="network_operation"
                                                   value="Fully Outsourced" <?php if ($structure_and_financing->network_operation == 'Fully Outsourced') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Fully Outsourced
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">14. How do you perform your "Network
                                                Maintenance" ?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="network_maintenance"
                                                   value="Fully Self" <?php if ($structure_and_financing->network_maintenance == 'Fully Self') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Fully Self
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="network_maintenance"
                                                   value="Hybrid" <?php if ($structure_and_financing->network_maintenance == 'Hybrid') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Hybrid
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="network_maintenance"
                                                   value="Fully Outsourced" <?php if ($structure_and_financing->network_maintenance == 'Fully Outsourced') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Fully Outsourced
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">15. How do you arrange your connectivity
                                                [Backbone] "Node-to-Node" [Fiber Optic Infrastructure] ?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="backbone_connectivity"
                                                   value="Short Term Lease" <?php if ($structure_and_financing->backbone_connectivity == 'Short Term Lease') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Short Term Lease
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="backbone_connectivity"
                                                   value="Long Term Lease" <?php if ($structure_and_financing->backbone_connectivity == 'Long Term lease') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Long Term Lease
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="backbone_connectivity"
                                                   value="Self Owned" <?php if ($structure_and_financing->backbone_connectivity == 'Self Owned') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Self Owned
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="backbone_connectivity"
                                                   value="Blended" <?php if ($structure_and_financing->backbone_connectivity == 'Blended') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Blended
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">16. How do you arrange your connectivity
                                                [Access] "Node-to-Node" [Fiber Optic Infrastructure] ?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="access_connectivity"
                                                   value="Short Term Lease" <?php if ($structure_and_financing->access_connectivity == 'Short Term Lease') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Short Term Lease
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="access_connectivity"
                                                   value="Long Term Lease"<?php if ($structure_and_financing->access_connectivity == 'Long Term Lease') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Long Term Lease
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="access_connectivity"
                                                   value="Self Owned"<?php if ($structure_and_financing->access_connectivity == 'Self Owned') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Self Owned
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="access_connectivity"
                                                   value="Blended"<?php if ($structure_and_financing->access_connectivity == 'Blended') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Blended
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">17. How do you maintain your Backbone Fiber
                                                ?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="backbone_fiber"
                                                   value="Short Term Lease" <?php if ($structure_and_financing->backbone_fiber == 'Short Term Lease') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Short Term Lease
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="backbone_fiber"
                                                   value="Long Term Lease" <?php if ($structure_and_financing->backbone_fiber == 'Long Term lease') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Long Term Lease
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="backbone_fiber"
                                                   value="Self" <?php if ($structure_and_financing->backbone_fiber == 'Self') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Self
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="backbone_fiber"
                                                   value="Blended" <?php if ($structure_and_financing->backbone_fiber == 'Blended') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Blended
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">18. How do you maintain your Access Fiber
                                                ?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="access_fiber"
                                                   value="Short Term Lease" <?php if ($structure_and_financing->access_fiber == 'Short Term Lease') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Short Term Lease
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="access_fiber"
                                                   value="Long Term Lease" <?php if ($structure_and_financing->access_fiber == 'Long Term lease') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Long Term Lease
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="access_fiber"
                                                   value="Self" <?php if ($structure_and_financing->access_fiber == 'Self') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Self
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="access_fiber"
                                                   value="Blended" <?php if ($structure_and_financing->access_fiber == 'Blended') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Blended
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">19. How do you charge the Membership fees?</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">
                                        @php
                                            $days = array("Free of Charge","Fixed/Flat Charge", "pay per user");
                                $membership_fees_data = json_decode($structure_and_financing->membership_fees);

                                 if($membership_fees_data === null){
                                    $membership_fees_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($days as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('membership_fees[]', $value, in_array($value, $membership_fees_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">20. How do you charge the "Bandwidth" Service ?</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">
                                        @php
                                            $bds = array("Free of Charge or Paid by the Government","Fixed/Flat Charge", "Included in the Membership Fees","Charged on Usage");
                                $bandwidth_service_data = json_decode($structure_and_financing->bandwidth_service);

                                 if($bandwidth_service_data === null){
                                    $bandwidth_service_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($bds as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('bandwidth_service[]', $value, in_array($value, $bandwidth_service_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">21. How do you charge the "Other" Services ?</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-18">
                                        @php
                                            $ods = array("Free of Charge or Paid by the Government","Fixed/Flat Charge", "Included in the Membership Fees","Charged on Usage");
                                $other_service_data = json_decode($structure_and_financing->other_service);

                                 if($other_service_data === null){
                                    $other_service_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($ods as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('other_service[]', $value, in_array($value, $other_service_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        @endforeach

                        @foreach($dataCollect['landscapeAndMarketShare'] ??[] as $landscapeAndMarketShare)

                            <div class="cards__heading">
                                <h3>NREN Landscape and Market share<span></span></h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">22. Which category of Institutions/members are currently
                                        connected to your network? ?</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">

                                        @php
                                            $days = array("Universities","Research Institutes", "Government Organisations","Schools and Colleges","Museums/Libraries","Hospitals/Medical Colleges","Others");
                                $Institutions_category_data = json_decode($landscapeAndMarketShare->Institutions_category);

                                 if($Institutions_category_data === null){
                                    $Institutions_category_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($days as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('Institutions_category[]', $value, in_array($value, $Institutions_category_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">23. How many Institutions/members were connected to your
                                        network [end of 2019]?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Universities: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Institutions_connected_Universities"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->Institutions_connected_Universities}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Non- Research
                                        Institutes: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Institutions_connected_RI"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->Institutions_connected_RI}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Government
                                        Organizations: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Institutions_connected_govt"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->Institutions_connected_govt}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Schools and Colleges: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Institutions_connected_school"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->Institutions_connected_school}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Others(Museums, Libraries,
                                        Hospitals/Medical Colleges, Industry, etc): </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Institutions_connected_other"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->Institutions_connected_other}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">24. How many Institutions/members were in your Target to
                                        connect to your network [in 2019 only] ??</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Universities: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Institutions_Target_Universities"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->Institutions_Target_Universities}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Non- Research
                                        Institutes: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Institutions_Target_RI"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->Institutions_Target_RI}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Government
                                        Organizations: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Institutions_Target_govt"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->Institutions_Target_govt}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Schools and Colleges: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Institutions_Target_school"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->Institutions_Target_school}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Others(Museums, Libraries,
                                        Hospitals/Medical Colleges, Industry, etc): </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Institutions_Target_other"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->Institutions_Target_other}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">25. How many Institutions are on your target to connect
                                        [in 2020 only]?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Universities: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Institutions_Target_Universities_now"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->Institutions_Target_Universities_now}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Non- Research
                                        Institutes: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Institutions_Target_RI_now"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->Institutions_Target_RI_now}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Government
                                        Organizations: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Institutions_Target_govt_now"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->Institutions_Target_govt_now}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Schools and Colleges: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Institutions_Target_school_now"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->Institutions_Target_school_now}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Others(Museums, Libraries,
                                        Hospitals/Medical Colleges, Industry, etc): </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Institutions_Target_other_now"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->Institutions_Target_other_now}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">26. Approximately how many users were using your
                                        network/services under the following institutes ??</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Universities: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="user_Universities" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->user_Universities}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Non- Research
                                        Institutes: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="user_RI" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->user_RI}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Government
                                        Organizations: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="user_govt" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->user_govt}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Schools and Colleges: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="user_school" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->user_school}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Others(Museums, Libraries,
                                        Hospitals/Medical Colleges, Industry, etc): </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="user_other" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->user_other}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">27) How many universities are there in Total in your
                                        country?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Total Universities: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="total_Universities" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->total_Universities}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">28. How many different types of Institutions are
                                        connected with less than Fast Ethernet(FE)
                                        (100Mbps) physical link capacity?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Universities: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="lessthan_FE_Universities"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->lessthan_FE_Universities}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Non- Research
                                        Institutes: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="lessthan_FE_RI" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->lessthan_FE_RI}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Government
                                        Organizations: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="lessthan_FE_govt" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->lessthan_FE_govt}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Schools and Colleges: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="lessthan_FE_school" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->lessthan_FE_school}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Others(Museums, Libraries,
                                        Hospitals/Medical Colleges, Industry, etc): </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="lessthan_FE_other" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->lessthan_FE_other}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">29) How many different types of Institutions are
                                        connected with Fast Ethernet(FE) or equivalent (STM1/T4)
                                        physical link capacity?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Universities: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="FE_Universities" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->FE_Universities}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Non- Research
                                        Institutes: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="FE_RI" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->FE_RI}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Government
                                        Organizations: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="FE_govt" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->FE_govt}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Schools and Colleges: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="FE_school" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->FE_school}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Others(Museums, Libraries,
                                        Hospitals/Medical Colleges, Industry, etc): </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="FE_other" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->FE_other}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">30) How many different types of Institutions are
                                        connected with 1G or equivalent (STM4/STM16) physical
                                        link capacity?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Universities: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="1G_Universities" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->t1G_Universities}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Non- Research
                                        Institutes: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="1G_RI" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->t1G_RI}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Government
                                        Organizations: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="1G_govt" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->t1G_govt}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Schools and Colleges: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="1G_school" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->t1G_school}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Others(Museums, Libraries,
                                        Hospitals/Medical Colleges, Industry, etc): </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="1G_other" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->t1G_other}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">31) How many different types of Institutions are
                                        connected with 10G physical link capacity?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Universities: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="10G_Universities" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->t10G_Universities}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Non- Research
                                        Institutes: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="10G_RI" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->t10G_RI}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Government
                                        Organizations: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="10G_govt" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->t10G_govt}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Schools and Colleges: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="10G_school" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->t10G_school}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Others(Museums, Libraries,
                                        Hospitals/Medical Colleges, Industry, etc): </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="10G_other" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->t10G_other}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">32) How many different types of Institutions are
                                        connected with 40G/100G physical link capacity?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Universities: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="40G_Universities" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->t40G_Universities}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Non- Research
                                        Institutes: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="40G_RI" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->t40G_RI}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Government
                                        Organizations: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="40G_govt" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->t40G_govt}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Schools and Colleges: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="40G_school" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->t40G_school}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Others(Museums, Libraries,
                                        Hospitals/Medical Colleges, Industry, etc): </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="40G_other" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->t40G_other}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">33) What is the highest Throughput (in Mbps)
                                        institutions are subscribing?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Universities: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="hightest_throughput_Universities"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->hightest_throughput_Universities}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Non- Research
                                        Institutes: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="hightest_throughput_RI"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->hightest_throughput_RI}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Government
                                        Organizations: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="hightest_throughput_govt"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->hightest_throughput_govt}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Schools and Colleges: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="hightest_throughput_school"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->hightest_throughput_school}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Others(Museums, Libraries,
                                        Hospitals/Medical Colleges, Industry, etc): </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="hightest_throughput_other"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->hightest_throughput_other}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">34) What is the Average Throughput (in Mbps)
                                        institutions are subscribing?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Universities: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="average_throughput_Universities"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->average_throughput_Universities}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Non- Research
                                        Institutes: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="average_throughput_RI" class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->average_throughput_RI}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Government
                                        Organizations: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="average_throughput_govt"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->average_throughput_govt}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Schools and Colleges: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="average_throughput_school"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->average_throughput_school}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Others(Museums, Libraries,
                                        Hospitals/Medical Colleges, Industry, etc): </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="average_throughput_other"
                                               class="form-control input readonly-style"
                                               value="{{ $landscapeAndMarketShare->average_throughput_other}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                        @endforeach

                        @foreach($dataCollect['network_trafficand_monitoring'] ??[] as $network_trafficand_monitoring)

                            <div class="cards__heading">
                                <h3>NREN Network Traffic and Monitoring<span></span></h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">35) What type of traffic is/are carried through your
                                        NREN network/cloud? </label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">

                                        @php
                                            $days35 = array("Research Traffic","Commodity Traffic", "CDN Traffic","Domestic Internet Exchange Traffic");
                                $type_of_traffic_data = json_decode($network_trafficand_monitoring->type_of_traffic);

                                 if($type_of_traffic_data === null){
                                    $type_of_traffic_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($days35 as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('type_of_traffic[]', $value, in_array($value, $type_of_traffic_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">36) Approximate what percentage of total traffic is used
                                        for Research and Education purpose?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Total Universities: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="total_trafficfor_RE" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->total_trafficfor_RE}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">37) Your Total Upstream [Download] Traffic Capacity,
                                        Mbps [in 2018]:</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Commodity: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Upstream_commodity_2018"
                                               class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->Upstream_commodity_2018}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Research and Education:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Upstream_RE_2018" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->Upstream_RE_2018}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Content Data Network
                                        [CDN]:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Upstream_CDN_2018" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->Upstream_CDN_2018}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Internet eXchange Points
                                        (IXPs):</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Upstream_IXP_2018" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->Upstream_IXP_2018}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">38) Your Total Upstream [Download] Traffic Capacity,
                                        Mbps [in 2019]:</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Commodity: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Upstream_commodity_2019"
                                               class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->Upstream_commodity_2019}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Research and Education:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Upstream_RE_2019" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->Upstream_RE_2019}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Content Data Network
                                        [CDN]:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Upstream_CDN_2019" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->Upstream_CDN_2019}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Internet eXchange Points
                                        (IXPs):</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Upstream_IXP_2019" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->Upstream_IXP_2019}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">39) Approximate Ratio of Download vs Upload [Place in
                                        the format of ratio Download:Upload] Traffic:</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">For Commodity Traffic: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="ratio_DU_commodity_traffic"
                                               class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->ratio_DU_commodity_traffic}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">For Research Traffic:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="ratio_DU_research_traffic"
                                               class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->ratio_DU_research_traffic}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">40) Your Total Yearly Traffic Volume [Throughput] in
                                        2019, TB:</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Commodity: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="yearly_traffic_commodity"
                                               class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->yearly_traffic_commodity}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Research and Education:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="yearly_traffic_RE" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->yearly_traffic_RE}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Content Data Network
                                        [CDN]:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="yearly_traffic_CDN" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->yearly_traffic_CDN}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Internet eXchange Points
                                        (IXPs):</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="yearly_traffic_IXP" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->yearly_traffic_IXP}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>


                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">41) URL to your MRTG [2019 and 2018, if
                                        possible]?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Commodity Internet:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="MRTG_commodity" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->MRTG_commodity}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Credentials to Access, if
                                        required:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="MRTG_credentilas" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->MRTG_credentilas}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Research Traffic:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="MRTG_ResearchTraffic" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->MRTG_ResearchTraffic}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Credentials to Access, if
                                        required:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="MRTG_CreAccess" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->MRTG_CreAccess}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">CDN Traffic: </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="MRTG_CDN" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->MRTG_CDN}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Credentials to Access, if
                                        required:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="MRTG_credentilas_2" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->MRTG_credentilas_2}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Domestic Internet Exchange
                                        Traffic:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="MRTG_domestic" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->MRTG_domestic}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Credentials to Access, if
                                        required:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="MRTG_credentilas_3" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->MRTG_credentilas_3}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">42) What is the percentage of your IPv6 Traffic in ratio
                                        with Total Traffic?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="ipv6_traffic" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->ipv6_traffic}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">43) How many IPv4 BGP Peers [in 2018]?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Upstream (Gateways): </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="ipv4_bgp_upstream_2018"
                                               class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->ipv4_bgp_upstream_2018}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Downstream (Clients):</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="ipv4_bgp_downstream_2018"
                                               class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->ipv4_bgp_downstream_2018}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Domestic IXPs:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="ipv4_bgp_domestic_2018"
                                               class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->ipv4_bgp_domestic_2018}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">CDNs & Others:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="ipv4_bgp_cdn_2018" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->ipv4_bgp_cdn_2018}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">44) How many IPv4 BGP Peers [in 2019]?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Upstream (Gateways): </label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="ipv4_bgp_upstream_2019"
                                               class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->ipv4_bgp_upstream_2019}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Downstream (Clients):</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="ipv4_bgp_downstream_2019"
                                               class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->ipv4_bgp_downstream_2019}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Domestic IXPs:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="ipv4_bgp_domestic_2019"
                                               class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->ipv4_bgp_domestic_2019}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">CDNs & Others:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="ipv4_bgp_cdn_2019" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->ipv4_bgp_cdn_2019}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">45) Percentage of your upstream link availability =>
                                        Commodity Traffic [in 2019]</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Availability considering planned
                                        maintenance as non-outage</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="upstream_non_outage" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->upstream_non_outage}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Availability considering planned
                                        maintenance as outage</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="upstream_outage" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->upstream_outage}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">46) Percentage of your downstream [Member
                                        Institutions'/Clients'] link availability (on Average) => [in2019]</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Availability considering planned
                                        maintenance as non-outage</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="downstream_non_outage" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->downstream_non_outage}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Availability considering planned
                                        maintenance as outage</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="downstream_outage" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->downstream_outage}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">47) URL to your Network Diagram:</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">URL:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="network_diagram_url" class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->network_diagram_url}}">

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Credentials to Access, if
                                        required:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="network_diagram_credentilas"
                                               class="form-control input readonly-style"
                                               value="{{ $network_trafficand_monitoring->network_diagram_credentilas}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">48) Do you maintain 7*24 NOC (Network Operation
                                                Centre) service?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="NOC"
                                                   value="Yes" <?php if ($network_trafficand_monitoring->NOC == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="NOC"
                                                   value="No" <?php if ($network_trafficand_monitoring->NOC == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">49) Which tools do you operate for traffic monitoring
                                        and measurement? (Check all that apply)</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">

                                        @php
                                            $days49 = array("Cacti","MRTG", "Nagios","Libre NMS","SolarWinds","PerfSONAR","SmokePing","Weathermap");
                                $tools_TMM_data = json_decode($network_trafficand_monitoring->tools_TMM);

                                 if($tools_TMM_data === null){
                                    $tools_TMM_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($days49 as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('tools_TMM[]', $value, in_array($value, $tools_TMM_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">50) What kind of statistical data do you provide to your
                                        members? (Check all that apply)</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">

                                        @php
                                            $days50 = array("Real Time Traffic Data - MRTG","Email", "Real Time Fault Report","Monthly Usage Data","Fault/Outage Data (Fault Frequency, Response and Restoration Time)","Meetings/Seminars","Meetings/Seminars","Monthly Availability");
                                $statistical_data_data = json_decode($network_trafficand_monitoring->statistical_data);

                                 if($statistical_data_data === null){
                                    $statistical_data_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($days50 as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('statistical_data[]', $value, in_array($value, $statistical_data_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">51) How do you communicate with your
                                        members/Institutions?</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">

                                        @php
                                            $days = array("Realtime through URL","Email", "Direct Phone Call","SMS","Social Media","Meetings/Seminars","Meetings/Seminars","Newsletter/Leaflets");
                                $communication_way_data = json_decode($network_trafficand_monitoring->communication_way);

                                 if($communication_way_data === null){
                                    $communication_way_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($days as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('communication_way[]', $value, in_array($value, $communication_way_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">52) Do you have any SLA (Service Level
                                                Agreement)?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="SLA"
                                                   value="Yes" <?php if ($network_trafficand_monitoring->SLA == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="SLA"
                                                   value="No" <?php if ($network_trafficand_monitoring->SLA == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">53) Does your SLA contain the option of
                                                compensating clients with Financial Penalty?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="option_of_compensation"
                                                   value="Yes"<?php if ($network_trafficand_monitoring->option_of_compensation == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="option_of_compensation"
                                                   value="No"<?php if ($network_trafficand_monitoring->option_of_compensation == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        @endforeach

                        @foreach($dataCollect['service_platform'] ??[] as $service_platform)

                            <div class="cards__heading">
                                <h3>NRENS' Service Platform<span></span></h3>
                            </div>

                            <fieldset class="form-group">

                                <div class="form-group">
                                    <label class="input readonly__label">54. Which network services are you currently
                                        facilitating to REN Community ??</label>
                                </div>
                                <div class="form-group row">

                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">

                                        @php
                                            $SO_Network = array("IPv4 Connectivity","IPv6 Connectivity", "Network Monitoring","Virtual Circuit/VPN","Multicast","Network Troubleshooting","NetFlow Tool", "VehiclOptical Wavelength", "Quality of Service", "Managed Router Services", "Remote Access VPN Services", "PERT", "SDN","Open Lightpath Exchange","Disaster Recovery Services");

                                $network_service_SO_data = json_decode($service_platform->network_service_SO);
                                 if($network_service_SO_data === null){
                                    $network_service_SO_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($SO_Network as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('network_service_SO[]', $value, in_array($value, $network_service_SO_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">55. Which security services are you offering ??</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">
                                        @php
                                            $SO_SS = array("CERT/CSIRT","DDoS Mitigation", "Network Troubleshooting","Anti-spam Solution","Vulnerability Scanning","Firewall-on-demand", "Intrusion Detection", "Identifier Registry", "Security Auditing", "Web Filtering", "PGP Key Server", "Online Payment");

                                $security_service_SO_data = json_decode($service_platform->security_service_SO);
                                 if($security_service_SO_data === null){
                                    $security_service_SO_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($SO_SS as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('security_service_SO[]', $value, in_array($value, $security_service_SO_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>

                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">56. Which Identity services have you already deployed
                                        ??</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">
                                        @php
                                            $SO_identity = array("Eduroam","Federated Services", "EduGAIN","GovRoam Services","Hosted Campus AAI");

                                $identity_service_SO_data = json_decode($service_platform->identity_service_SO);
                                if($identity_service_SO_data === null){
                                    $identity_service_SO_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($SO_identity as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('identity_service_SO[]', $value, in_array($value, $identity_service_SO_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">57. Which Collaboration services are you currently
                                        promoting ??</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">
                                        @php
                                            $SO_colla = array("Mailing Lists","Email Server Hosting", "Journal Access","Research Gateway"," VoIP","Project Collaboration Tools","CMS Services","Database Services","Survey-Polling Tool","Instant Messages","Scheduling Tools","SMS Messaging","Plagiarism","Learning Management Services","ePortfolios","Class Registration Services","Video Conferencing Services","SIS( Students Information Service)");

                                $collaboration_service_SO_data = json_decode($service_platform->collaboration_service_SO);
                                 if($collaboration_service_SO_data === null){
                                    $collaboration_service_SO_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($SO_colla as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('collaboration_service_SO[]', $value, in_array($value, $collaboration_service_SO_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">58. Do you cover any of the following Multimedia
                                        Services to your REN Community ??</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">
                                        @php
                                            $SO_multimedia = array("Web/Desktop Conferencing","Video Streaming (E-Lesson)", "Event Recording / Streaming","TV / Radio Streaming","Media Post Production","Multicast");

                                $multimedia_service_SO_data = json_decode($service_platform->multimedia_service_SO);
                                 if($multimedia_service_SO_data === null){
                                    $multimedia_service_SO_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($SO_multimedia as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('multimedia_service_SO[]', $value, in_array($value, $multimedia_service_SO_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">59. Do you offer the following Storage and Hosting
                                        Services ??</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">
                                        @php
                                            $SO_storage = array("DNS Hosting","File Sender", "Virtual Machines","Cloud Storage","Housing/Colocation","Web Hosting","SaaS","Content Delivery","Disaster Recovery","Hot-Standby","Netnews");

                                $storage_service_SO_data = json_decode($service_platform->storage_service_SO);
                                 if($storage_service_SO_data === null){
                                    $storage_service_SO_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($SO_storage as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('storage_service_SO[]', $value, in_array($value, $storage_service_SO_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">60. Do you have any Professional Services ??</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">
                                        @php
                                            $SO_professional = array("Hosting of User Conference","Consultancy/Training", "User Portals","Dissemination of Information","Procurement Services","Software Licenses","Web Development","Software Development","Finance/Admin System");

                                $professional_service_SO_data = json_decode($service_platform->professional_service_SO);
                                 if($professional_service_SO_data === null){
                                    $professional_service_SO_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($SO_professional as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('professional_service_SO[]', $value, in_array($value, $professional_service_SO_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">61) Please name maximum 5 (Five) flagship services which
                                        gives you competitive advantage over commercial providers.</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Flagship Service-I:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="flagship_1_61" class="form-control input readonly-style"
                                               value="{{$service_platform->flagship_1_61}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Flagship Service-II:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="flagship_2_61" class="form-control input readonly-style"
                                               value="{{$service_platform->flagship_2_61}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Flagship Service-III:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="flagship_3_61" class="form-control input readonly-style"
                                               value="{{$service_platform->flagship_3_61}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Flagship Service-IV:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="flagship_4_61" class="form-control input readonly-style"
                                               value="{{$service_platform->flagship_4_61}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Flagship Service-V:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="flagship_5_61" class="form-control input readonly-style"
                                               value="{{$service_platform->flagship_5_61}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">62) Total Beneficiaries of these services (please
                                        maintain the serial number in line with previous question)</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Flagship Service-I:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="flagship_1_62" class="form-control input readonly-style"
                                               value="{{$service_platform->flagship_1_62}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Flagship Service-II:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="flagship_2_62" class="form-control input readonly-style"
                                               value="{{$service_platform->flagship_2_62}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Flagship Service-III:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="flagship_3_62" class="form-control input readonly-style"
                                               value="{{$service_platform->flagship_3_62}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Flagship Service-IV:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="flagship_4_62" class="form-control input readonly-style"
                                               value="{{$service_platform->flagship_4_62}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Flagship Service-V:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="flagship_5_62" class="form-control input readonly-style"
                                               value="{{$service_platform->flagship_5_62}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                        @endforeach

                        @foreach($dataCollect['advanced_services'] ??[] as $advanced_services)
                            <div class="cards__heading">
                                <h3>Advanced Services<span></span></h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">63) Services demanded by users? (Check all that
                                        apply)</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">

                                        @php
                                            $service_demand_63 = array("EduRoam","EduGain", "HPC","Computing/Storage","SDN","E-Library","Research Gateway Server", "High Performance VPS", "IPv6", "Virtual Labs/Smart Classroom", "IoT", "Access to Journal and Research Databases", "Connectivity to Public Cloud (Azure/AWS)","E-Learning","Big Data","Blockchain","Content Filtering","Telemedicine","Collocation");

                                $service_demand_data = json_decode($advanced_services->service_demand);
                                 if($service_demand_data === null){
                                    $service_demand_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($service_demand_63 as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('service_demand[]', $value, in_array($value, $service_demand_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">64) Services already implemented?</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">

                                        @php
                                            $service_implemented_64 = array("EduRoam","EduGain", "HPC","Computing/Storage","SDN","E-Library","Research Gateway Server", "High Performance VPS", "IPv6", "Virtual Labs/Smart Classroom", "IoT", "Access to Journal and Research Databases", "Connectivity to Public Cloud (Azure/AWS)","E-Learning","Big Data","Blockchain","Content Filtering","Telemedicine","Collocation");

                                $service_implemented_data = json_decode($advanced_services->service_implemented);
                                 if($service_implemented_data === null){
                                    $service_implemented_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($service_implemented_64 as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('service_implemented[]', $value, in_array($value, $service_implemented_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">65) Services under pipeline for implementation?</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">
                                        @php
                                            $service_pipeline_65 = array("EduRoam","EduGain", "HPC","Computing/Storage","SDN","E-Library","Research Gateway Server", "High Performance VPS", "IPv6", "Virtual Labs/Smart Classroom", "IoT", "Access to Journal and Research Databases", "Connectivity to Public Cloud (Azure/AWS)","E-Learning","Big Data","Blockchain","Content Filtering","Telemedicine","Collocation");

                                $service_pipeline_data = json_decode($advanced_services->service_pipeline);
                                 if($service_pipeline_data === null){
                                    $service_pipeline_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($service_pipeline_65 as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('service_pipeline[]', $value, in_array($value, $service_pipeline_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>


                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">66) Please state the implementation of IPv6 in:</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> DNS Server </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="DNS_Server"
                                                   value="Full"<?php if ($advanced_services->DNS_Server == 'Full') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Full
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="DNS_Server"
                                                   value="Partial"<?php if ($advanced_services->DNS_Server == 'Partial') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Partial
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="DNS_Server"
                                                   value="None"<?php if ($advanced_services->DNS_Server == 'None') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                None
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Web Server </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="web_Server"
                                                   value="Full"<?php if ($advanced_services->web_Server == 'Full') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Full
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="web_Server"
                                                   value="Partial"<?php if ($advanced_services->web_Server == 'Partial') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Partial
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="web_Server"
                                                   value="None"<?php if ($advanced_services->web_Server == 'None') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                None
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Mail Server </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="mail_Server"
                                                   value="Full"<?php if ($advanced_services->mail_Server == 'Full') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Full
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="mail_Server"
                                                   value="Partial"<?php if ($advanced_services->mail_Server == 'Partial') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Partial
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="mail_Server"
                                                   value="None"<?php if ($advanced_services->mail_Server == 'None') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                None
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">DHCP/SLAAC
                                        Server/Router </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="dhcp_Server"
                                                   value="Full"<?php if ($advanced_services->dhcp_Server == 'Full') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Full
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="dhcp_Server"
                                                   value="Partial"<?php if ($advanced_services->dhcp_Server == 'Partial') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Partial
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="dhcp_Server"
                                                   value="None"<?php if ($advanced_services->dhcp_Server == 'None') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                None
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Routing System </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="routing_system"
                                                   value="Full"<?php if ($advanced_services->routing_system == 'Full') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Full
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="routing_system"
                                                   value="Partial"<?php if ($advanced_services->routing_system == 'Partial') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Partial
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="routing_system"
                                                   value="None"<?php if ($advanced_services->routing_system == 'None') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                None
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"> Switching System </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="switching_system"
                                                   value="Full"<?php if ($advanced_services->switching_system == 'Full') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Full
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="switching_system"
                                                   value="Partial"<?php if ($advanced_services->switching_system == 'Partial') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Partial
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="switching_system"
                                                   value="None"<?php if ($advanced_services->switching_system == 'None') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                None
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">67) How many IPv6 BGP peers do you have?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Upstream (Gateways):</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="ipv6_bgp_upstream" class="form-control input readonly-style"
                                               value="{{ $advanced_services->ipv6_bgp_upstream}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Downstream (Clients):</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="ipv6_bgp_downstream" class="form-control input readonly-style"
                                               value="{{ $advanced_services->ipv6_bgp_downstream}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Domestic IXPs:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="ipv6_bgp_domestic" class="form-control input readonly-style"
                                               value="{{ $advanced_services->ipv6_bgp_domestic}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">CDNs and Others:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="ipv6_bgp_cdn" class="form-control input readonly-style"
                                               value="{{ $advanced_services->ipv6_bgp_cdn}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">68) How many prefixes received from Upstream Peers
                                        (Gateways)?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">IPv4:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="upstram_peer_ipv4" class="form-control input readonly-style"
                                               value="{{ $advanced_services->upstram_peer_ipv4}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">IPv6:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="upstram_peer_ipv6" class="form-control input readonly-style"
                                               value="{{ $advanced_services->upstram_peer_ipv6}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">69) How many prefixes received from Parallel Peers (CDNs
                                        and IXPs)?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">IPv4:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="parallei_peer_ipv4" class="form-control input readonly-style"
                                               value="{{ $advanced_services->parallei_peer_ipv4}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">IPv6:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="parallei_peer_ipv6" class="form-control input readonly-style"
                                               value="{{ $advanced_services->parallei_peer_ipv6}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">70) How many prefixes received from Downstream Peers
                                        (Clients)?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">IPv4:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="downstream_peer_ipv4" class="form-control input readonly-style"
                                               value="{{ $advanced_services->downstream_peer_ipv4}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">IPv6:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="downstream_peer_ipv6" class="form-control input readonly-style"
                                               value="{{ $advanced_services->downstream_peer_ipv6}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">71) Do you have IPv6 peering with TEIN upstream
                                                Gateway/s?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="ipv6_tein_upstream"
                                                   value="Yes"<?php if ($advanced_services->ipv6_tein_upstream == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="ipv6_tein_upstream"
                                                   value="No"<?php if ($advanced_services->ipv6_tein_upstream == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">72) How many Application Servers are setup in your Cloud
                                        System?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">IPv4 enabled</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="application_server_ipv4"
                                               class="form-control input readonly-style"
                                               value="{{ $advanced_services->application_server_ipv4}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">IPv6 enabled</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="application_server_ipv6"
                                               class="form-control input readonly-style"
                                               value="{{ $advanced_services->application_server_ipv6}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">73) Which type of configuration used in the
                                                network devices having IPv6?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="config_network_ipv6"
                                                   value="Dual Stack"<?php if ($advanced_services->config_network_ipv6 == 'Dual Stack') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Dual Stack
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="config_network_ipv6"
                                                   value="IPv6 Tunneling"<?php if ($advanced_services->config_network_ipv6 == 'IPv6 Tunneling') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                IPv6 Tunneling
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="config_network_ipv6"
                                                   value="Both Dual Stack and Tunneling"<?php if ($advanced_services->config_network_ipv6 == 'Both Dual Stack and Tunneling') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Both Dual Stack and Tunneling
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="config_network_ipv6"
                                                   value="IPv6 yet to be implemented"<?php if ($advanced_services->config_network_ipv6 == 'IPv6 yet to be implemented') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                IPv6 yet to be implemented
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">74) How many Public ASN do you [NREN] have?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="public_asn" class="form-control input readonly-style"
                                               value="{{ $advanced_services->public_asn}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">75) How many IPv4 (/24) Blocks do you have?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="ipv4_24_blocks" class="form-control input readonly-style"
                                               value="{{ $advanced_services->ipv4_24_blocks}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">76) How many Members under NREN having independent
                                        ASN?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="independent_ASN" class="form-control input readonly-style"
                                               value="{{ $advanced_services->independent_ASN}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">77) How many of the Members having IPv6 Routed
                                        ASN/Prefixes?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="IPv6_Routed_ASN" class="form-control input readonly-style"
                                               value="{{ $advanced_services->IPv6_Routed_ASN}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">78) Have you already joined Mutually Agreed
                                                Norms for Routing Security [MANRS]?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="MANRS"
                                                   value="Yes"<?php if ($advanced_services->MANRS == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="MANRS"
                                                   value="No"<?php if ($advanced_services->MANRS == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        @endforeach


                        @foreach($dataCollect['networkInfrastructureDarkFiber'] ??[] as $networkInfrastructureDarkFiber)

                            <div class="cards__heading">
                                <h3>Network Infrastructure: Dark Fibre<span></span></h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">79) Do you have your own/Long Term Leased Dark
                                                Fibre for Domestic Usage?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio"
                                                   name="dark_fiber_domestic_usage"
                                                   value="Yes"<?php if ($networkInfrastructureDarkFiber->dark_fiber_domestic_usage == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio"
                                                   name="dark_fiber_domestic_usage"
                                                   value="No"<?php if ($networkInfrastructureDarkFiber->dark_fiber_domestic_usage == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">80) Approximately how long is the length of
                                                owned/Long Term Leased Domestic Dark Fibre?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="dark_fiber_domestic_long"
                                                   value="Less 1000 km"<?php if ($networkInfrastructureDarkFiber->dark_fiber_domestic_long == 'Less 1000 km') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Less 1000 km
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="dark_fiber_domestic_long"
                                                   value="1000 to 10000 km"<?php if ($networkInfrastructureDarkFiber->dark_fiber_domestic_long == '1000 to 10000 km') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                1000 to 10000 km
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="dark_fiber_domestic_long"
                                                   value="Over 10000 km"<?php if ($networkInfrastructureDarkFiber->dark_fiber_domestic_long == 'Over 10000 km') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Over 10000 km
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="dark_fiber_domestic_long"
                                                   value="No Dark Fibre"<?php if ($networkInfrastructureDarkFiber->dark_fiber_domestic_long == 'No Dark Fibre') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                No Dark Fibre
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">81) Do You have your own/Long Term Leased Dark
                                                Fiber for Cross-border?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="dark_fiber_cross_border"
                                                   value="Yes"<?php if ($networkInfrastructureDarkFiber->dark_fiber_cross_border == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="dark_fiber_cross_border"
                                                   value="No"<?php if ($networkInfrastructureDarkFiber->dark_fiber_cross_border == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">82) Approximately how long is the length of
                                                owned/Long Term Leased Cross Border Dark Fibre?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio"
                                                   name="dark_fiber_cross_border_long"
                                                   value="Less 1000 km"<?php if ($networkInfrastructureDarkFiber->dark_fiber_cross_border_long == 'Less 1000 km') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Less 1000 km
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio"
                                                   name="dark_fiber_cross_border_long"
                                                   value="1000 to 10000 km"<?php if ($networkInfrastructureDarkFiber->dark_fiber_cross_border_long == '1000 to 10000 km') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                1000 to 10000 km
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio"
                                                   name="dark_fiber_cross_border_long"
                                                   value="Over 10000 km"<?php if ($networkInfrastructureDarkFiber->dark_fiber_cross_border_long == 'Over 10000 km') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Over 10000 km
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio"
                                                   name="dark_fiber_cross_border_long"
                                                   value="No Dark Fibre"<?php if ($networkInfrastructureDarkFiber->dark_fiber_cross_border_long == 'No Dark Fibre') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                No Dark Fibre
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">83) Have you deployed any Alien Wave technology
                                                in Transmission?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Alien_Wave_technology"
                                                   value="Yes"<?php if ($networkInfrastructureDarkFiber->Alien_Wave_technology == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Alien_Wave_technology"
                                                   value="No"<?php if ($networkInfrastructureDarkFiber->Alien_Wave_technology == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">84) Which services are being offered under this Alien
                                        waves technology?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="offer_services_Alien_Wave_technology"
                                               class="form-control input readonly-style"
                                               value="{{ $networkInfrastructureDarkFiber->offer_services_Alien_Wave_technology}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>


                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">85) Do you implement IP Trunk for the use of
                                                your network’s VoIP framework?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="IP_Trunk"
                                                   value="Yes"<?php if ($networkInfrastructureDarkFiber->IP_Trunk == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="IP_Trunk"
                                                   value="No"<?php if ($networkInfrastructureDarkFiber->IP_Trunk == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>


                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">86) What is the aggregate link capacity for IP
                                                Trunk?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="aggregate_link_capacity"
                                                   value="Fast Ethernet"<?php if ($networkInfrastructureDarkFiber->aggregate_link_capacity == 'Fast Ethernet') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Fast Ethernet
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="aggregate_link_capacity"
                                                   value="1G"<?php if ($networkInfrastructureDarkFiber->aggregate_link_capacity == '1G') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                1G
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="aggregate_link_capacity"
                                                   value="10G"<?php if ($networkInfrastructureDarkFiber->aggregate_link_capacity == '10G') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                10G
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="aggregate_link_capacity"
                                                   value="100G"<?php if ($networkInfrastructureDarkFiber->aggregate_link_capacity == '100G') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                100G
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">87) Status of SDN (Software Defined Network) services in
                                        your organization?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Pilot services</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="sdn_pilot" class="form-control input readonly-style"
                                               value="{{ $networkInfrastructureDarkFiber->sdn_pilot}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Production Services</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="sdn_production" class="form-control input readonly-style"
                                               value="{{ $networkInfrastructureDarkFiber->sdn_production}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Tested</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="sdn_tested" class="form-control input readonly-style"
                                               value="{{ $networkInfrastructureDarkFiber->sdn_tested}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Planned</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="sdn_planned" class="form-control input readonly-style"
                                               value="{{ $networkInfrastructureDarkFiber->sdn_planned}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Not Planned yet</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="sdn_not_planned" class="form-control input readonly-style"
                                               value="{{ $networkInfrastructureDarkFiber->sdn_not_planned}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">88) What is the state of NFV (Network Function
                                                Virtualization) in your Organization?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="NFV"
                                                   value="Implemented: Services Incorporated"<?php if ($networkInfrastructureDarkFiber->NFV == 'Implemented: Services Incorporated') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Implemented: Services Incorporated
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="NFV"
                                                   value="Planned: Services Planned"<?php if ($networkInfrastructureDarkFiber->NFV == 'Planned: Services Planned') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Planned: Services Planned
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="NFV"
                                                   value="Not Planned yet"<?php if ($networkInfrastructureDarkFiber->NFV == 'Not Planned yet') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Not Planned yet
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">89) What mode of Server Farm/Application services do you
                                        have? </label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">

                                        @php
                                            $days89 = array("Owned","Outsourced", "Not Available");
                                $Server_Farm_data = json_decode($networkInfrastructureDarkFiber->Server_Farm);

                                 if($Server_Farm_data === null){
                                    $Server_Farm_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($days89 as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('Server_Farm[]', $value, in_array($value, $Server_Farm_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">90) Please select the Protection availability of
                                        Application Services in your NREN.</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">TLS </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="TLS"
                                                   value="Available"<?php if ($networkInfrastructureDarkFiber->TLS == 'Available') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Available
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="TLS"
                                                   value="Not Available"<?php if ($networkInfrastructureDarkFiber->TLS == 'Not Available') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Not Available
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Email Security like Spam and
                                        Fishing protection </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="spam_andfishing"
                                                   value="Available"<?php if ($networkInfrastructureDarkFiber->spam_andfishing == 'Available') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Available
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="spam_andfishing"
                                                   value="Not Available"<?php if ($networkInfrastructureDarkFiber->spam_andfishing == 'Not Available') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Not Available
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">NGN Firewall </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="NGN_Firewall"
                                                   value="Available"<?php if ($networkInfrastructureDarkFiber->NGN_Firewall == 'Available') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Available
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="NGN_Firewall"
                                                   value="Not Available"<?php if ($networkInfrastructureDarkFiber->NGN_Firewall == 'Not Available') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Not Available
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Sand Boxing </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Sand_Boxing"
                                                   value="Available"<?php if ($networkInfrastructureDarkFiber->Sand_Boxing == 'Available') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Available
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Sand_Boxing"
                                                   value="Not Available"<?php if ($networkInfrastructureDarkFiber->Sand_Boxing == 'Not Available') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Not Available
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">DDOS Protection </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="DDOS_Protection"
                                                   value="Available"<?php if ($networkInfrastructureDarkFiber->DDOS_Protection == 'Available') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Available
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="DDOS_Protection"
                                                   value="Not Available"<?php if ($networkInfrastructureDarkFiber->DDOS_Protection == 'Not Available') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Not Available
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">DPI </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="DPI"
                                                   value="Available"<?php if ($networkInfrastructureDarkFiber->DPI == 'Available') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Available
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="DPI"
                                                   value="Not Available"<?php if ($networkInfrastructureDarkFiber->DPI == 'Not Available') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Not Available
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        @endforeach

                        @foreach($dataCollect['eduroamServices'] ??[] as $eduroamServices)

                            <div class="cards__heading">
                                <h3>eduRoam Services<span></span></h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">91) Do you have eduroam in your country?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="eduroam"
                                                   value="Yes"<?php if ($eduroamServices->eduroam == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="eduroam"
                                                   value="No"<?php if ($eduroamServices->eduroam == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">92) How many Campus do you have under eduRoam
                                                coverage?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Campus"
                                                   value="<10"<?php if ($eduroamServices->Campus == '<10') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                <10
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Campus"
                                                   value=">10 and <=50"<?php if ($eduroamServices->Campus == '>10 and <=50') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                >10 and <=50
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Campus"
                                                   value=">50 and <=100"<?php if ($eduroamServices->Campus == '>50 and <=100') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                >50 and <=100
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Campus"
                                                   value=">100 and <=500"<?php if ($eduroamServices->Campus == '>100 and <=500') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                >100 and <=500
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Campus"
                                                   value=">500"<?php if ($eduroamServices->Campus == '>500') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                >500
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">93) How many Access Points do you have with
                                                edurom configured?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Access_Points"
                                                   value="<100"<?php if ($eduroamServices->Access_Points == '<100') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                <100
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Access_Points"
                                                   value=">100 and <=500"<?php if ($eduroamServices->Access_Points == '>100 and <=500') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                >100 and <=500
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Access_Points"
                                                   value=">500 and <=1000"<?php if ($eduroamServices->Access_Points == '>500 and <=1000') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                >500 and <=1000
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Access_Points"
                                                   value=">1000 and <=5000"<?php if ($eduroamServices->Access_Points == '>1000 and <=5000') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                >1000 and <=5000
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Access_Points"
                                                   value=">5000"<?php if ($eduroamServices->Access_Points == '>5000') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                >5000
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">94) Does your NREN have presence in "Service
                                                Location Map" under https://monitor.eduroam.org?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Service_Location_Map"
                                                   value="Yes"<?php if ($eduroamServices->Service_Location_Map == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Service_Location_Map"
                                                   value="No"<?php if ($eduroamServices->Service_Location_Map == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">95) Have you configured Statistics (F-ticks)
                                                which is displayed in https://monitor.eduroam.org?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="F_ticks"
                                                   value="Yes"<?php if ($eduroamServices->F_ticks == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="F_ticks"
                                                   value="No"<?php if ($eduroamServices->F_ticks == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">96) Have you configured Configuration Automation
                                                Tool (CAT) for enhancing eduRoam security?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="CAT"
                                                   value="Yes"<?php if ($eduroamServices->CAT == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="CAT"
                                                   value="No"<?php if ($eduroamServices->CAT == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">97) Do you have govRoam Service?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="govRoam"
                                                   value="Yes"<?php if ($eduroamServices->govRoam == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="govRoam"
                                                   value="No"<?php if ($eduroamServices->govRoam == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">98) Do you have eduRoam Hotspot available beyond
                                                Educational/Research Institute?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="eduRoam_Hotspot"
                                                   value="Yes"<?php if ($eduroamServices->eduRoam_Hotspot == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="eduRoam_Hotspot"
                                                   value="No"<?php if ($eduroamServices->eduRoam_Hotspot == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">99) How many reported Authentications [from NRS] in
                                        2018?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">National:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Authentications_NRS_National_2018"
                                               class="form-control input readonly-style"
                                               value="{{$eduroamServices->Authentications_NRS_National_2018}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">International:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Authentications_NRS_International_2018"
                                               class="form-control input readonly-style"
                                               value="{{$eduroamServices->Authentications_NRS_International_2018}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">100) How many reported Authentications [from NRS] in
                                        2019?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">National:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Authentications_NRS_National_2019"
                                               class="form-control input readonly-style"
                                               value="{{$eduroamServices->Authentications_NRS_National_2019}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">International:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Authentications_NRS_International_2019"
                                               class="form-control input readonly-style"
                                               value="{{$eduroamServices->Authentications_NRS_International_2019}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                        @endforeach

                        @foreach($dataCollect['identityandTrustService'] ??[] as $identityandTrustService)

                            <div class="cards__heading">
                                <h3>Identity and Trust Service<span></span></h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">101) Status of Identity and Trust Services in your
                                        country:</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">

                                        @php
                                            $days101 = array("National Federation only","Federation of Federations (eduGAIN)", "Under Test","Under Plan","Not Planned yet");
                                $Identity_and_Trust_Services_data = json_decode($identityandTrustService->Identity_and_Trust_Services);

                                 if($Identity_and_Trust_Services_data === null){
                                    $Identity_and_Trust_Services_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($days101 as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('Identity_and_Trust_Services[]', $value, in_array($value, $Identity_and_Trust_Services_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">102) Which services are being provided/planned using
                                        Identity Federation? (Check all that apply)</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Video Collaboration </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Video_Collaboration"
                                                   value="Planned"<?php if ($identityandTrustService->Video_Collaboration == 'Planned') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Planned
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Video_Collaboration"
                                                   value="Provided"<?php if ($identityandTrustService->Video_Collaboration == 'Provided') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Provided
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Digital Library</label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Digital_Library"
                                                   value="Planned"<?php if ($identityandTrustService->Digital_Library == 'Planned') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Planned
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Digital_Library"
                                                   value="Provided"<?php if ($identityandTrustService->Digital_Library == 'Provided') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Provided
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Trusted Digital
                                        Certificate</label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio"
                                                   name="Trusted_Digital_Certificate"
                                                   value="Planned"<?php if ($identityandTrustService->Trusted_Digital_Certificate == 'Planned') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Planned
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio"
                                                   name="Trusted_Digital_Certificate"
                                                   value="Provided"<?php if ($identityandTrustService->Trusted_Digital_Certificate == 'Provided') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Provided
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Big File Sender</label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Big_File_Sender"
                                                   value="Planned"<?php if ($identityandTrustService->Big_File_Sender == 'Planned') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Planned
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Big_File_Sender"
                                                   value="Provided"<?php if ($identityandTrustService->Big_File_Sender == 'Provided') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Provided
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Cloud Storage</label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Cloud_Storage"
                                                   value="Planned"<?php if ($identityandTrustService->Cloud_Storage == 'Provided') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Planned
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Cloud_Storage"
                                                   value="Provided"<?php if ($identityandTrustService->Cloud_Storage == 'Provided') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Provided
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Desktop/Laptop Backup
                                        Services</label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Laptop_Backup_Services"
                                                   value="Planned"<?php if ($identityandTrustService->Laptop_Backup_Services == 'Planned') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Planned
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Laptop_Backup_Services"
                                                   value="Provided"<?php if ($identityandTrustService->Laptop_Backup_Services == 'Provided') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Provided
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Research Repository</label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Research_Repository"
                                                   value="Planned"<?php if ($identityandTrustService->Research_Repository == 'Planned') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Planned
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Research_Repository"
                                                   value="Provided"<?php if ($identityandTrustService->Research_Repository == 'Provided') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Provided
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">103) What is the architecture of Federation deployed by
                                        your organization? (Check all that apply)</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">

                                        @php
                                            $days103 = array("Mesh","Hub and Spoke", "Centralized","Hybrid");
                                $Federation_architecture_data = json_decode($identityandTrustService->Federation_architecture);

                                 if($Federation_architecture_data === null){
                                    $Federation_architecture_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($days103 as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('Federation_architecture[]', $value, in_array($value, $Federation_architecture_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">104) Please provide information regarding your
                                        Federation Services?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Number of participating Home
                                        Organisations (IdPs)</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="information_Federation_Services_IdPs"
                                               class="form-control input readonly-style"
                                               value="{{ $identityandTrustService->information_Federation_Services_IdPs}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Number of participating Services
                                        Providers (SPs)</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="information_Federation_Services_SPs"
                                               class="form-control input readonly-style"
                                               value="{{ $identityandTrustService->information_Federation_Services_SPs}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                        @endforeach

                        @foreach($dataCollect['researchAndEducationCollaboration'] ??[] as $researchAndEducationCollaboration)

                            <div class="cards__heading">
                                <h3>Major Research and Education Collaboration Activities<span></span></h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group ">
                                            <label class="input readonly__label">105) Do you have any Research and Education
                                                Collaboration Activities?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="ResEdu_activities"
                                                   value="Yes"<?php if ($researchAndEducationCollaboration->ResEdu_activities == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="ResEdu_activities"
                                                   value="No"<?php if ($researchAndEducationCollaboration->ResEdu_activities == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">106) Major Research and Education Collaboration
                                        Activities</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Title of Research
                                        Initiative</label>
                                    <div class="col-sm-6">

                                        <textarea readonly name="Title_of_Research"
                                                  class="form-control input readonly-style">{{ $researchAndEducationCollaboration->Title_of_Research}}</textarea readonly>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Associated URL [You may skip the
                                        following if it is covered in the URL] with credentials [if required]:</label>

                                    <div class="col-sm-6">

                                        <textarea readonly name="research_url"
                                                  class="form-control input readonly-style">{{ $researchAndEducationCollaboration->research_url}}</textarea readonly>

                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Abstract</label>

                                    <div class="col-sm-6">
                                        <textarea readonly name="Abstract"
                                                  class="form-control input readonly-style">{{ $researchAndEducationCollaboration->Abstract}}</textarea readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Name and Email Address of the
                                        1st Author</label>

                                    <div class="col-sm-6">
                                        <textarea readonly name="name_email_first_author"
                                                  class="form-control input readonly-style">{{ $researchAndEducationCollaboration->name_email_first_author}}</textarea readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Scope</label>
                                    <div class="col-sm-6">
                                        <textarea readonly name="scope"
                                                  class="form-control input readonly-style">{{ $researchAndEducationCollaboration->scope}}</textarea readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Beneficiaries</label>
                                    <div class="col-sm-6">
                                        <textarea readonly name="Beneficiaries"
                                                  class="form-control input readonly-style">{{ $researchAndEducationCollaboration->Beneficiaries}}</textarea readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Financing (Asi@Connect/ NREN/
                                        Others)</label>
                                    <div class="col-sm-6">
                                        <textarea readonly name="Financing"
                                                  class="form-control input readonly-style">{{ $researchAndEducationCollaboration->Financing}}</textarea readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">How NREN could contribute
                                        (Financing/ Providing Infrastructure/ Human Networking/consultancy/
                                        Others)</label>
                                    <div class="col-sm-6">
                                        <textarea readonly name="NREN_contribute"
                                                  class="form-control input readonly-style">{{ $researchAndEducationCollaboration->NREN_contribute}}</textarea readonly>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group ">
                                            <label class="input readonly__label">107) Do you have any other Research and
                                                Education Collaboration Activities?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="ResEdu_activities_other"
                                                   value="Yes"<?php if ($researchAndEducationCollaboration->ResEdu_activities_other == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="ResEdu_activities_other"
                                                   value="No"<?php if ($researchAndEducationCollaboration->ResEdu_activities_other == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">108) Major Research and Education Collaboration
                                        Activities</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Title of Research
                                        Initiative</label>
                                    <div class="col-sm-6">
                                        <textarea readonly name="Title_of_Research_other"
                                                  class="form-control input readonly-style">{{ $researchAndEducationCollaboration->Title_of_Research_other}}</textarea readonly>
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Associated URL [You may skip the
                                        following if it is covered in the URL] with credentials [if required]:</label>
                                    <div class="col-sm-6">
                                        <textarea readonly name="research_url_other"
                                                  class="form-control input readonly-style">{{ $researchAndEducationCollaboration->research_url_other}}</textarea readonly>
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Abstract</label>

                                    <div class="col-sm-6">
                                        <textarea readonly name="Abstract_other"
                                                  class="form-control input readonly-style">{{ $researchAndEducationCollaboration->Abstract_other}}</textarea readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Name and Email Address of the
                                        1st Author</label>

                                    <div class="col-sm-6">
                                        <textarea readonly name="name_email_first_author_other"
                                                  class="form-control input readonly-style">{{ $researchAndEducationCollaboration->name_email_first_author_other}}</textarea readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Scope</label>

                                    <div class="col-sm-6">
                                        <textarea readonly name="scope_other"
                                                  class="form-control input readonly-style">{{ $researchAndEducationCollaboration->scope_other}}</textarea readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Beneficiaries</label>
                                    <div class="col-sm-6">
                                        <textarea readonly name="Beneficiaries_other"
                                                  class="form-control input readonly-style">{{ $researchAndEducationCollaboration->Beneficiaries_other}}</textarea readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Financing (Asi@Connect/ NREN/
                                        Others)</label>

                                    <div class="col-sm-6">
                                        <textarea readonly name="Financing_other"
                                                  class="form-control input readonly-style">{{ $researchAndEducationCollaboration->Financing_other}}</textarea readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">How NREN could contribute
                                        (Financing/ Providing Infrastructure/ Human Networking/consultancy/
                                        Others)</label>

                                    <div class="col-sm-6">
                                        <textarea readonly name="NREN_contribute_other"
                                                  class="form-control input readonly-style">{{ $researchAndEducationCollaboration->NREN_contribute_other}}</textarea readonly>
                                    </div>
                                </div>
                            </fieldset>
                        @endforeach

                        @foreach($dataCollect['capacityDevelopment'] ??[] as $capacityDevelopment)

                            <div class="cards__heading">
                                <h3>Capacity Development<span></span></h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">116) Aproximate % level of expertise among NREN
                                        Engineers:</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Experts:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NREN_Engineers_expert" class="form-control input readonly-style"
                                               value="{{ $capacityDevelopment->NREN_Engineers_expert}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Intermediate:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NREN_Engineers_intermediate"
                                               class="form-control input readonly-style"
                                               value="{{ $capacityDevelopment->NREN_Engineers_intermediate}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Beginners:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NREN_Engineers_beginner"
                                               class="form-control input readonly-style"
                                               value="{{ $capacityDevelopment->NREN_Engineers_beginner}}">
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">117) Aproximate % level of Expertise of Engineers in
                                        Beneficiary Member Institutes?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Experts:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Member_Institutes_expert"
                                               class="form-control input readonly-style"
                                               value="{{ $capacityDevelopment->Member_Institutes_expert}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Intermediate:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Member_Institutes_intermediate"
                                               class="form-control input readonly-style"
                                               value="{{ $capacityDevelopment->Member_Institutes_intermediate}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Beginners:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Member_Institutes_beginner"
                                               class="form-control input readonly-style"
                                               value="{{ $capacityDevelopment->Member_Institutes_beginner}}">
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group ">
                                            <label class="input readonly__label">118) Is there any impact of WP2 and WP3 packages
                                                on the level of expertise of NREN Engineers?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="wp2andwp3_package"
                                                   value="Yes"<?php if ($capacityDevelopment->wp2andwp3_package == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="wp2andwp3_package"
                                                   value="No"<?php if ($capacityDevelopment->wp2andwp3_package == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">119) Comments of NRENs on enhancement of efficacy of WP2
                                        and WP3 packages</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="efficacy_WP2_WP3_packages"
                                               class="form-control input readonly-style"
                                               value="{{ $capacityDevelopment->efficacy_WP2_WP3_packages}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">120) Number of Asi@Connect Financed Training/Workshop in
                                        2019 organized by NREN?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">International:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="AsiaConnect_Financed_Training_International"
                                               class="form-control input readonly-style"
                                               value="{{ $capacityDevelopment->AsiaConnect_Financed_Training_International}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">National:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="AsiaConnect_Financed_Training_National"
                                               class="form-control input readonly-style"
                                               value="{{ $capacityDevelopment->AsiaConnect_Financed_Training_National}}">
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">121) Number of own Financed Training/Workshop in 2019
                                        organized by NREN?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">International:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="own_Financed_Training_International"
                                               class="form-control input readonly-style"
                                               value="{{ $capacityDevelopment->own_Financed_Training_International}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">National:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="own_Financed_Training_National"
                                               class="form-control input readonly-style"
                                               value="{{ $capacityDevelopment->own_Financed_Training_National}}">
                                    </div>
                                </div>
                            </fieldset>
                        @endforeach

                        @foreach($dataCollect['promotion_research'] ??[] as $promotion_research)

                            <div class="cards__heading">
                                <h3>Promotion of Research<span></span></h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">122) Initiative from NREN (Promotional Material) in the
                                        year 2019:</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Frequency of Printing
                                        Newsletter:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NREN_promotional_newsletter"
                                               class="form-control input readonly-style"
                                               value="{{ $promotion_research->NREN_promotional_newsletter}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Frequency of Printing
                                        Leaflets/Brochures:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NREN_promotional_leaflets"
                                               class="form-control input readonly-style"
                                               value="{{ $promotion_research->NREN_promotional_leaflets}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Tenders/Advertisements in Daily
                                        Newspapers/Web Portals:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NREN_promotional_tender"
                                               class="form-control input readonly-style"
                                               value="{{ $promotion_research->NREN_promotional_tender}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Frequency of Arranging Other
                                        Souvenirs Gift Materials:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NREN_promotional_gift" class="form-control input readonly-style"
                                               value="{{ $promotion_research->NREN_promotional_gift}}">
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">123) Initiative of NREN (Arranging Seminars/Meetings
                                        involving communities) in 2019:</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Number of Arranged Virtual
                                        Seminars/Meetings (TEIN Sponsored):</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NREN_virtual_seminarTEIN_Sponsored"
                                               class="form-control input readonly-style"
                                               value="{{ $promotion_research->NREN_virtual_seminarTEIN_Sponsored}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Number of Arranged Physical
                                        Seminars/Meetings (TEIN Sponsored):</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NREN_physical_seminarTEIN_Sponsored"
                                               class="form-control input readonly-style"
                                               value="{{ $promotion_research->NREN_physical_seminarTEIN_Sponsored}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Number of Arranged Virtual
                                        Seminars/Meetings (NREN Sponsored):</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NREN_virtual_seminar_NREN_Sponsored"
                                               class="form-control input readonly-style"
                                               value="{{ $promotion_research->NREN_virtual_seminar_NREN_Sponsored}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Number of Arranged Physical
                                        Seminars/Meetings (NREN Sponsored):</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NREN_Physical_seminar_NREN_Sponsored"
                                               class="form-control input readonly-style"
                                               value="{{ $promotion_research->NREN_Physical_seminar_NREN_Sponsored}}">
                                    </div>
                                </div>
                            </fieldset>


                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">124) Number of Registrants from NRENs' country:</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">In APAN47</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="number_registrants_apan47"
                                               class="form-control input readonly-style"
                                               value="{{ $promotion_research->number_registrants_apan47}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">In APAN48</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="number_registrants_apan48"
                                               class="form-control input readonly-style"
                                               value="{{ $promotion_research->number_registrants_apan48}}">
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">125) Mode of Communications with Member Institutions
                                        regarding any event:</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">

                                        @php
                                            $days125 = array("News Letter","Email", "Voice","Facebook","Youtube","Twitter","Web Portal");
                                $communicationWithInstitutions_data = json_decode($promotion_research->communicationWithInstitutions);

                                 if($communicationWithInstitutions_data === null){
                                    $communicationWithInstitutions_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($days125 as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('communicationWithInstitutions[]', $value, in_array($value, $communicationWithInstitutions_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        @endforeach

                        @foreach($dataCollect['initiativesofTEINCC'] ??[] as $initiativesofTEINCC)


                            <div class="cards__heading">
                                <h3>Initiatives of TEIN*CC<span></span></h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">126) Is there any way TEIN*CC can streamline its role in
                                        enhancing the efficacy of WP2 and WP3 packages?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">

                                        <textarea readonly name="efficacy_WP2_WP3_packages" class="form-control input readonly-style"
                                                  rows="10">{{ $initiativesofTEINCC->efficacy_WP2_WP3_packages}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">127) Is there any way TEIN*CC can streamline its role in
                                        enhancing the efficacy of WP4, WP5 and WP6 packages?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">

                                        <textarea readonly name="efficacy_WP4_WP5_packages" class="form-control input readonly-style"
                                                  rows="10">{{ $initiativesofTEINCC->efficacy_WP4_WP5_packages}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">128) Are you satisfied the way TEIN*CC reacted at the
                                        fallout of COVID-19? How do you think TEIN*CC
                                        could help more effectively the NRENs during the pandemic?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">


                                        <textarea readonly name="satisfied_on_TEINCC" class="form-control input readonly-style"
                                                  rows="10">{{ $initiativesofTEINCC->satisfied_on_TEINCC}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">129) What are the strengths of TEIN*CC?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">

                                        <textarea readonly name="strengths_of_TEINCC" class="form-control input readonly-style"
                                                  rows="10">{{ $initiativesofTEINCC->strengths_of_TEINCC}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">130) What are the weaknesses of TEIN*CC?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">


                                        <textarea readonly name="weaknesses_of_TEINCC" class="form-control input readonly-style"
                                                  rows="10">{{ $initiativesofTEINCC->weaknesses_of_TEINCC}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">131) Are there any opportunities TEIN*CC can look
                                        into?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">

                                        <textarea readonly name="opportunities_TEINCC" class="form-control input readonly-style"
                                                  rows="10">{{ $initiativesofTEINCC->opportunities_TEINCC}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">132) What are the looming Threats for TEIN*CC?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">

                                        <textarea readonly name="looming_Threats_TEINCC" class="form-control input readonly-style"
                                                  rows="10">{{ $initiativesofTEINCC->looming_Threats_TEINCC}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>

                        @endforeach

                        @foreach($dataCollect['challengesAndNrenAdaptibility'] ??[] as $challengesAndNrenAdaptibility)

                            <div class="cards__heading">
                                <h3>Impact of COVID-19: Challenges and NRENs' Adaptibility<span></span></h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">133) Month-wise Maximum Usage of Internet, Mbps</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Jan, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Internet_jan_2020" class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Internet_jan_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Feb, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Internet_feb_2020" class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Internet_feb_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">March, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Internet_march_2020" class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Internet_march_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">April, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Internet_april_2020" class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Internet_april_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">May, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Internet_may_2020" class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Internet_may_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">June, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Internet_june_2020" class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Internet_june_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">July, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Internet_july_2020" class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Internet_july_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">August, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Internet_august_2020" class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Internet_august_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">September, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Internet_september_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Internet_september_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">October, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Internet_october_2020" class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Internet_october_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">URL for the MRTG (if
                                        any):</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Internet_MRTG" class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Internet_MRTG}}">
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">134) Month-wise Maximum Usage of Research Traffic
                                        (Asi@Connect), Mbps</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Jan, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Research_Traffic_jan_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Research_Traffic_jan_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Feb, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Research_Traffic_feb_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Research_Traffic_feb_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">March, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Research_Traffic_march_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Research_Traffic_march_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">April, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Research_Traffic_april_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Research_Traffic_april_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">May, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Research_Traffic_may_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Research_Traffic_may_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">June, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Research_Traffic_june_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Research_Traffic_june_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">July, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Research_Traffic_july_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Research_Traffic_july_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">August, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Research_Traffic_august_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Research_Traffic_august_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">September, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Research_Traffic_september_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Research_Traffic_september_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">October, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Research_Traffic_october_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Research_Traffic_october_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">URL for the MRTG (if
                                        any):</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Research_Traffic_MRTG" class="form-control input readonly-style"
                                               value="{{ $challengesAndNrenAdaptibility->Research_Traffic_MRTG}}">
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">135) Impact on Revenue as a fallout of
                                                COVID:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Impact_Revenue"
                                                   value="Highly Increased"<?php if ($challengesAndNrenAdaptibility->Impact_Revenue == 'Highly Increased') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Highly Increased
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Impact_Revenue"
                                                   value="Increased"<?php if ($challengesAndNrenAdaptibility->Impact_Revenue == 'Increased') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Increased
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Campus"
                                                   value="Same as Before"<?php if ($challengesAndNrenAdaptibility->Impact_Revenue == 'Same as Before') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Same as Before
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Impact_Revenue"
                                                   value="Decreased"<?php if ($challengesAndNrenAdaptibility->Impact_Revenue == 'Decreased') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Decreased
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio" name="Impact_Revenue"
                                                   value="Highly Decreased"<?php if ($challengesAndNrenAdaptibility->Impact_Revenue == 'Highly Decreased') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Highly Decreased
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">136) Major Threat due to pandemic</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">

                                        @php
                                            $days136 = array("Reduction in Revenue","Unemployment", "Network Security","Hardware overrun","Shortage of Software Licences");
                                $Major_Threat_data = json_decode($challengesAndNrenAdaptibility->Major_Threat);

                                 if($Major_Threat_data === null){
                                    $Major_Threat_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($days136 as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('Major_Threat[]', $value, in_array($value, $Major_Threat_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">137) Big Opportunities</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">

                                        @php
                                            $days137 = array("Collaboration among NRENs","Supporting Tele-Medicine", "Supporting Tele-Medicine","Providing Computing/Virtualization Support");
                                $Big_Opportunities_data = json_decode($challengesAndNrenAdaptibility->Big_Opportunities);

                                 if($Big_Opportunities_data === null){
                                    $Big_Opportunities_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($days137 as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('Big_Opportunities[]', $value, in_array($value, $Big_Opportunities_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                        @endforeach

                        @foreach($dataCollect['NRENsupporting_online_education'] ??[] as $NRENsupporting_online_education)

                            <div class="cards__heading">
                                <h3>Impact of COVID-19: NRENs' Response through supporting Online Education<span></span>
                                </h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">138) Which Video Collaboration Software are being used
                                        by the Universities/Higher Education Institutes/Research Institutes</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">

                                        @php
                                            $days138 = array("Cisco Webex","Zoom", "Microsoft Team","Google Meet","eduMEET","Jitsi Meet","BigBlueButton");
                                $Video_Collaboration_Software_data = json_decode($NRENsupporting_online_education->Video_Collaboration_Software);

                                 if($Video_Collaboration_Software_data === null){
                                    $Video_Collaboration_Software_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($days138 as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('Video_Collaboration_Software[]', $value, in_array($value, $Video_Collaboration_Software_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">139) Month-wise Number of Virtual Classes/Meetings
                                        conducted by the Member Institutes under NREN
                                        (No need to answer if NREN didn't give any support to the Member
                                        Institutes)</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Jan, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NumberofVirtual_Classes_jan_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->NumberofVirtual_Classes_jan_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Feb, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NumberofVirtual_Classes_feb_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->NumberofVirtual_Classes_feb_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">March, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NumberofVirtual_Classes_march_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->NumberofVirtual_Classes_march_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">April, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NumberofVirtual_Classes_april_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->NumberofVirtual_Classes_april_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">May, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NumberofVirtual_Classes_may_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->NumberofVirtual_Classes_may_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">June, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NumberofVirtual_Classes_june_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->NumberofVirtual_Classes_june_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">July, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NumberofVirtual_Classes_july_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->NumberofVirtual_Classes_july_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">August, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NumberofVirtual_Classes_august_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->NumberofVirtual_Classes_august_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">September, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NumberofVirtual_Classes_september_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->NumberofVirtual_Classes_september_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">October, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NumberofVirtual_Classes_october_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->NumberofVirtual_Classes_october_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">URL for the Usage (if any) with
                                        Credentials (if required):</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="NumberofVirtual_Classes_url"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->NumberofVirtual_Classes_url}}">
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">140) Month-wise Total Duration (in Minutes) of Virtual
                                        Classes/Meetings conducted by the Member Institues under NREN (No need to answer
                                        if NREN didn't give any support to the Member Institutes)</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Jan, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Duration_Virtual_Classes_jan_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Duration_Virtual_Classes_jan_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Feb, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Duration_Virtual_Classes_feb_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Duration_Virtual_Classes_feb_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">March, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Duration_Virtual_Classes_march_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Duration_Virtual_Classes_march_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">April, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Duration_Virtual_Classes_april_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Duration_Virtual_Classes_april_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">May, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Duration_Virtual_Classes_may_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Duration_Virtual_Classes_may_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">June, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Duration_Virtual_Classes_june_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Duration_Virtual_Classes_june_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">July, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Duration_Virtual_Classes_july_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Duration_Virtual_Classes_july_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">August, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Duration_Virtual_Classes_august_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Duration_Virtual_Classes_august_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">September, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Duration_Virtual_Classes_september_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Duration_Virtual_Classes_september_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">October, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Duration_Virtual_Classes_october_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Duration_Virtual_Classes_october_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">URL for the Usage (if any) with
                                        Credentials (if required):</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Duration_Virtual_Classes_URL"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Duration_Virtual_Classes_URL}}">
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">141) Month-wise Total Participants in Virtual
                                        Classes/Meetings conducted by Member Institutes under NREN
                                        (No need to answer if NREN didn't give any support to the Member
                                        Institutes)</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Jan, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Total_Participants_VirtualClasses_jan_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Total_Participants_VirtualClasses_jan_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Feb, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Total_Participants_VirtualClasses_feb_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Total_Participants_VirtualClasses_feb_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">March, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Total_Participants_VirtualClasses_march_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Total_Participants_VirtualClasses_march_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">April, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Total_Participants_VirtualClasses_april_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Total_Participants_VirtualClasses_april_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">May, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Total_Participants_VirtualClasses_may_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Total_Participants_VirtualClasses_may_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">June, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Total_Participants_VirtualClasses_june_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Total_Participants_VirtualClasses_june_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">July, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Total_Participants_VirtualClasses_july_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Total_Participants_VirtualClasses_july_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">August, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Total_Participants_VirtualClasses_august_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Total_Participants_VirtualClasses_august_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">September, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Total_Participants_VirtualClasses_september_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Total_Participants_VirtualClasses_september_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">October, 2020</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Total_Participants_VirtualClasses_october_2020"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Total_Participants_VirtualClasses_october_2020}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">URL for the Usage (if any) with
                                        Credentials (if required):</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Total_Participants_VirtualClasses_URL"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Total_Participants_VirtualClasses_URL}}">
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input readonly__label">142) Video Collaboration Software run
                                                on:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio"
                                                   name="Video_Collaboration_Software_run"
                                                   value="Internet Cloud"<?php if ($NRENsupporting_online_education->Video_Collaboration_Software_run == 'Internet Cloud') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Internet Cloud
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio"
                                                   name="Video_Collaboration_Software_run"
                                                   value="Local Data Center"<?php if ($NRENsupporting_online_education->Video_Collaboration_Software_run == 'Local Data Center') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                Local Data Center
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group ">
                                            <label class="input readonly__label">143) Any increase in Domestic Internet Traffic
                                                (with IXPs)?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio"
                                                   name="Domestic_Internet_Traffic"
                                                   value="Yes"<?php if ($NRENsupporting_online_education->Domestic_Internet_Traffic == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio"
                                                   name="Domestic_Internet_Traffic"
                                                   value="No"<?php if ($NRENsupporting_online_education->Domestic_Internet_Traffic == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>


                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">144) Domestic Internet Traffic Status:</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Maximum IX (Internet eXchange)
                                        Traffic, January-2020, Mbps:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="DIT_maximum_IX_january"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->DIT_maximum_IX_january}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Maximum IX (Internet eXchange)
                                        Traffic, June-2020, Mbps:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="DIT_maximum_IX_june" class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->DIT_maximum_IX_june}}">
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">145) Number of Total Available Software [Video
                                        Collaboration] Licenses:</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Available_Software" class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Available_Software}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">146) Number of Maximum Software Licenses [Video
                                        Collaboration] used:</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Maximum_Software" class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Maximum_Software}}">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group ">
                                            <label class="input readonly__label">147) Is the quantity of number of available
                                                Licenses [Video Collaboration] enough for you?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio"
                                                   name="quantity_available_Licenses"
                                                   value="Yes"<?php if ($NRENsupporting_online_education->quantity_available_Licenses == 'Yes') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input readonly class="form-check-input readonly" type="radio"
                                                   name="quantity_available_Licenses"
                                                   value="No"<?php if ($NRENsupporting_online_education->quantity_available_Licenses == 'No') echo 'checked="checked"'; ?>>
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>


                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">148) Clients being Served through Video Collaboration
                                        Applications:</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Number of Institutes:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Clients_VCA_institutes"
                                               class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Clients_VCA_institutes}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Number of Faculty
                                        Members:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Clients_VCA_faculty" class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Clients_VCA_faculty}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Number of Students:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="Clients_VCA_student" class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->Clients_VCA_student}}">
                                    </div>
                                </div>
                            </fieldset>



                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">149) Challenges faced by students [Check all that
                                        Apply]</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">

                                        @php
                                            $days149 = array("Last Mile Network Quality","Bandwidth Cost", "Availability of End-user Devices","Nonchalance about the mode of delivery","Confidence in the Assessment Process","Quality of Delivery","Quality of Content");
                                $Challenges_faced_by_students_data = json_decode($NRENsupporting_online_education->Challenges_faced_by_students);

                                 if($Challenges_faced_by_students_data === null){
                                    $Challenges_faced_by_students_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($days149 as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('Challenges_faced_by_students[]', $value, in_array($value, $Challenges_faced_by_students_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">150) Challenges faced by faculty members/universities
                                        [Check all that Apply]</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">

                                        @php
                                            $days150 = array("Absence of Policy for Online Education","Lack of Training", "Last Mile Network Quality","Bandwidth Cost","Availability of End-user Devices","Nonchalance about the mode of delivery","Confidence in the Assessment Process","Lack of LMS Software","Performing Lab/Hands-on Activities");
                                $Challenges_faced_by_faculty_data = json_decode($NRENsupporting_online_education->Challenges_faced_by_faculty);

                                 if($Challenges_faced_by_faculty_data === null){
                                    $Challenges_faced_by_faculty_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($days150 as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('Challenges_faced_by_faculty[]', $value, in_array($value, $Challenges_faced_by_faculty_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">151) Challenges faced by NRENs [Check all that
                                        Apply]</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">

                                        @php
                                            $days151 = array("Limitations of Application Licenses","Lack of Hardware (Application running on own Data Center)", "High Bandwidth Cost","Confidence in Assessment Process","Lack of LMS Software","Providing 24x7 support");
                                $Challenges_faced_by_nren_data = json_decode($NRENsupporting_online_education->Challenges_faced_by_nren);

                                 if($Challenges_faced_by_nren_data === null){
                                    $Challenges_faced_by_nren_data = array('Torikul');
                                }
                                        @endphp
                                        <div class="form-check">
                                            @foreach($days151 as $value)
                                                <label class="form-check-label">
                                                    {!! Form::checkbox('Challenges_faced_by_nren[]', $value, in_array($value, $Challenges_faced_by_nren_data) ? true : false, array('class' => 'form-check-input readonly','disabled'=>'disabled')) !!}
                                                    {{ $value}}</label><br>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">152) Steps Taken for Mitigation of given
                                        Challenges</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Limitations of Video
                                        Collaboration Application Licenses:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="limitation_VCA" class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->limitation_VCA}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Lack of Hardware:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="lack_of_hardware" class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->lack_of_hardware}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">High Bandwidth Cost:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="high_bandwidth_cost" class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->high_bandwidth_cost}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Confidence in Assessment
                                        Process:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="confidence_assessment" class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->confidence_assessment}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Lack of LMS Software:</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="lack_lms_software" class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->lack_lms_software}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">24x7 Support</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="support_24" class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->support_24}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label">Any other step</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" name="other_step" class="form-control input readonly-style"
                                               value="{{ $NRENsupporting_online_education->other_step}}">
                                    </div>
                                </div>
                            </fieldset>

                        @endforeach
                        @foreach($dataCollect['nren_supporting_tele_medicine'] ??[] as $nren_supporting_tele_medicine)

                            <div class="cards__heading">
                                <h3>Impact of COVID-19: NRENs' Response through Supporting Tele-Medicine<span></span>
                                </h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">153) Steps taken for Supporting Tele-medicine</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">

                                        <textarea readonly name="Supporting_Tele_medicine" class="form-control input readonly-style"
                                                  rows="5">{{ $nren_supporting_tele_medicine->Supporting_Tele_medicine}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">154) Challenges Faced in Tele-medicine</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">

                                        <textarea readonly name="Challenges_Tele_medicine" class="form-control input readonly-style"
                                                  rows="5">{{ $nren_supporting_tele_medicine->Challenges_Tele_medicine}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">155) Mitigating Steps Taken in Tele-medicine</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">

                                        <textarea readonly name="Mitigating_Steps_Tele_medicine" class="form-control input readonly-style"
                                                  rows="5">{{ $nren_supporting_tele_medicine->Mitigating_Steps_Tele_medicine}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>

                            <div class="cards__heading">
                                <h3>Impact of COVID-19: NRENs' Response through Supporting Computing and
                                    Virtualization<span></span></h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">156) Steps Taken in supporting Computing and
                                        Virtualization</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">

                                        <textarea readonly name="step_supporting_CandV" class="form-control input readonly-style"
                                                  rows="5">{{ $nren_supporting_tele_medicine->step_supporting_CandV}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">157) Challenges Faced in Supporting Computing and
                                        Virtualization:</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">

                                        <textarea readonly name="challenges_supporting_CandV" class="form-control input readonly-style"
                                                  rows="5">{{ $nren_supporting_tele_medicine->challenges_supporting_CandV}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">158) Mitigating Steps Taken in Computing and
                                        Virtualization:</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">

                                        <textarea readonly name="Mitigating_Steps_CandV" class="form-control input readonly-style"
                                                  rows="5">{{ $nren_supporting_tele_medicine->Mitigating_Steps_CandV}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>
                            <div class="cards__heading">
                                <h3>Imapct of COVID-19: NRENs' response through offering Other Services<span></span>
                                </h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">159) Any other support services or Steps
                                        undertaken:</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">

                                        <textarea readonly name="anyOther_support_services" class="form-control input readonly-style"
                                                  rows="5">{{ $nren_supporting_tele_medicine->anyOther_support_services}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>
                            <div class="cards__heading">
                                <h3>Impact of COVID-19: Others<span></span></h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">160) What role TEIN*CC did play/could play to help
                                        NRENs?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">
                                        <textarea readonly name="role_of_teincc" class="form-control input readonly-style"
                                                  rows="5">{{ $nren_supporting_tele_medicine->role_of_teincc}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">161) Any Looming Threat?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">

                                        <textarea readonly name="Looming_Threat" class="form-control input readonly-style"
                                                  rows="5">{{ $nren_supporting_tele_medicine->Looming_Threat}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">162) Any Future Opportunity?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">

                                        <textarea readonly name="Future_Opportunity" class="form-control input readonly-style"
                                                  rows="5">{{ $nren_supporting_tele_medicine->Future_Opportunity}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">163) How TEIN*CC can help you in availing opportunity
                                        and dampening the threats?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">

                                        <textarea readonly name="availing_opportunity_dampening" class="form-control input readonly-style"
                                                  rows="5">{{ $nren_supporting_tele_medicine->availing_opportunity_dampening}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>

                            <div class="cards__heading">
                                <h3>Final Comments (You can make Comment/Comments with regard to survey or something you
                                    expected but
                                    didn't find while filling out the survey form)<span></span></h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input readonly__label">164) Comment/s on Conducted Survey</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input readonly__label"></label>
                                    <div class="col-sm-6">

                                        <textarea readonly name="CommentConductedSurvey" class="form-control input readonly-style"
                                                  rows="5">{{ $nren_supporting_tele_medicine->CommentConductedSurvey}}</textarea readonly>

                                    </div>
                                </div>
                            </fieldset>

                        @endforeach


                    </div>
                </div>
                @endif
            </section>
        </section>
        <!-- //content -->
    </div>
    <!-- main content end-->
@endsection
