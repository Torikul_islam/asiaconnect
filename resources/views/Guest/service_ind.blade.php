@extends('Guest.guestlayout.app')



@push('after-styles')
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/> -->

    <link rel="stylesheet" href="{{ asset('dist/css/bootstrap-multiselect.css') }}">

@endpush




@section('content')
    <!-- main content start -->
    <div class="main-content">

        <!-- content -->
        <section class="container-fluid content-top-gap">


            <!-- forms -->
            <section class="forms">
                <!-- forms 1 -->
                <div class="card card_border py-2 mb-4">

                    <div class="card-body">

                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://www.bdren.net.bd/" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.bdren.net.bd/" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.bdren.net.bd/" title="Asiaconnect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>
                        </div>

                        <form method="POST" action="{{route('service.individual',['slug'=>request('slug')])}}"
                              enctype="multipart/form-data">
                            @csrf

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group ">
                                            <label class="input readonly__label text-primary">Please Select the
                                                Organization: </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <!-- <label>Organization:</label> -->
                                            <select class="form-control form-control-sm required" name="ren_id[]"
                                                    multiple="multiple" id="example-selectAllValue" required>
                                                @foreach($users as $U_data)
                                                    <option value="{{ $U_data->id}}">{{ $U_data->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-12 d-flex justify-content-center">
                                        <button type="submit" class="btn btn-primary">Find Result</button>
                                    </div>

                                </div>
                            </fieldset>

                        </form>

                        @if(count($labelData??[]) >0)
                            <div style="width: 80%; height: 100%; margin: 0 auto;">
                                <canvas id="myChart"></canvas>
                            </div>
                        @endif

                        @if(count($key??[]) >0)
                            <div style="width: 80%; height: 100%; margin: 0 auto;">
                                <canvas id="toolschart"></canvas>
                            </div>
                        @endif

                    </div>
                </div>
            </section>
        </section>
        <!-- //content -->
    </div>
    <!-- main content end-->
@endsection


@push('script')
    <script type="text/javascript">
        /*$(document).ready(function () {
            $('.select2').select2({width: '100%'});
        });
*/
        $(document).ready(function () {
            $('#example-selectAllValue').multiselect({
                includeSelectAllOption: true,
                selectAllValue: 'select-all-value',
                maxHeight: 400
            });
        });

        var ctx = $("#myChart");

        var myChart = new Chart(ctx, {
            type: 'matrix',
            data: {
                datasets: [{
                    label: 'My Matrix',
                    data: @json($graph??[]),
                    backgroundColor(ctx) {
                        return ctx.chart.data.datasets[0].data.map(function (value, index) {
                            return value.v === 0 ? '#ecebeb' : '#00b000';
                        });
                    },
                    borderColor(ctx) {
                        return "rgb(20,20,20)";
                    },
                    borderWidth: 1,
                    width: 35,
                    height: 25,
                }]
            },
            options: {
                plugins: {
                    legend: false,
                    title: {
                        display: true,
                        text: @json($title??[])
                    },
                    tooltip: {
                        callbacks: {
                            label(context) {
                                const v = context.dataset.data[context.dataIndex];
                                return ['x: ' + v.x, 'y: ' + v.y, 'v: ' + v.v];
                            }
                        }
                    }
                },
                scales: {
                    x: {
                        type: 'category',
                        labels: @json($selectedUsers??[]),
                        ticks: {
                            display: true,
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        },
                        grid: {
                            display: false
                        }
                    },
                    y: {
                        type: 'category',
                        labels: @json($labelData??[]),
                        offset: true,
                        ticks: {
                            display: true
                        },
                        grid: {
                            display: false
                        }
                    }
                }
            }
        });

        //for popularity option of tools

        $(function () {

            //get the bar chart canvas
            var ctx = $("#toolschart");

            //bar chart data
            var data = {
                labels: @json($key??[]),
                datasets: [
                    {
                        label:@json($datasetlevel??[]),
                        data: @json($value??[]),

                        backgroundColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderWidth: 1
                    }
                ]
            };

            //options
            var options = {
                responsive: true,
                plugins: {
                    legend: {
                        display: false
                    },
                    title: {
                        display: true,
                        text: @json($title??[])
                    }
                },
                legend: {
                    display: true,
                    position: "bottom",
                    labels: {
                        fontColor: "#333",
                        fontSize: 16

                    }
                },
                scales: {
                    x: {
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });
    </script>
@endpush
