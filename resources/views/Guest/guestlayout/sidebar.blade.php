<!-- sidebar menu start -->
<div class="sidebar-menu sticky-sidebar-menu">

    <!-- logo start -->
    <!--     <div class="logo">
          <h1><a href="index.html">Collective</a></h1>
        </div> -->

    <!-- if logo is image enable this -->

    <div class="logo">
        <a href="{{ route('guestview') }}">
            <img src="{{ URL::asset('assets/images/a.gif') }}" alt="Your logo" title="Asi@Connect" class="img-fluid"
                 style="height:60px;"/>
        </a>
    </div>


    <div class="logo-icon text-center">
        <a href="{{ route('guestview') }}" title="Asi@Connect"><img src="{{ URL::asset('assets/images/a.gif') }}"
                                                              alt="logo-icon"> </a>
    </div>
    <!-- //logo end -->

    <div class="sidebar-menu-inner">

        <!-- sidebar nav start -->
        <ul class="nav nav-pills nav-stacked custom-nav">

            <li class="{{ Request::is('asiaconnect/guestview*') ? 'nav-active nav-hover' : '' }}"><a href="{{ route('guestview') }}"><i class="fa fa-file-text"></i> <span>Responses</span></a></li>

            <?php
            $policyArr = [
                'Policies' => 'Availability of Policies',
                'Employees' => 'Employees Profession and Status',
                'CapEx' => 'CapEx Financing Among NRENS',
                'OpEx' => 'OpEx Financing Among NRENS',
                'Expense' => 'Operating Expense Distribution',
                'Operations' => 'Network Operations and  Maintenance'
            ];
            ?>
            <li class="menu-list {{ Request::is('asiaconnect/policy*') ? 'nav-active nav-hover' : '' }}">
                <a href="#"><i class="fa fa-cogs"></i>
                    <span>Structure & Finance<i class="lnr lnr-chevron-right"></i></span></a>
                <ul class="sub-menu-list">
                    @foreach($policyArr as $key => $value)
                        <li class="{{request('slug')==$key ? 'active':''  }}"><a href="{{ route('policy',['slug'=>$key]) }}">{{ $value }}</a></li>
                    @endforeach
                </ul>
            </li>

            <?php
            $land = [
                'Institutions' => 'Connected Institutions',
                'Link' => 'Link Capacity',
                'Bandwidth' => 'Bandwidth for University and RI'
            ];
            ?>

            <li class="menu-list {{ Request::is('asiaconnect/landscape*') ? 'nav-active nav-hover' : '' }}">
                <a href="#"><i class="fa fa-cogs"></i>
                    <span>Landscape & Market Share<i class="lnr lnr-chevron-right"></i></span></a>
                <ul class="sub-menu-list">
                    @foreach($land as $key => $value)
                        <li class="{{request('slug')==$key ? 'active':''  }}"><a href="{{ route('landscape',['slug'=>$key]) }}">{{ $value }}</a></li>
                    @endforeach
                </ul>
            </li>

            <?php
            $topology = [
                'Traffic' => 'Comparison of Network Traffic',
                'BGP' => 'Total BGP Peers, 2018 vs 2019'
            ];
            ?>
            <li class="menu-list {{ Request::is('asiaconnect/topology*') ? 'nav-active nav-hover' : '' }}">
                <a href="#"><i class="fa fa-cogs"></i>
                    <span>NREN Topology<i class="lnr lnr-chevron-right"></i></span></a>
                <ul class="sub-menu-list">
                    @foreach($topology as $key => $value)
                        <li class="{{request('slug')==$key ? 'active':''  }}"><a href="{{ route('topology',['slug'=>$key]) }}">{{ $value }}</a></li>
                    @endforeach
                </ul>
            </li>


            <?php
            $network = [
                'type_of_traffic' => 'Type of Network Traffic',
                'tools_TMM' => 'Network Traffic Monitoring Tools',
                'statistical_data' => 'Sharing of Network Information by NRENS',
                'communication_way' => 'Mode of Communication with Stakeholders',
                'Volume' => 'Volume of Traffic',
                'Availballity' => 'Network Availballity',
                'NOC' => 'Availablity of NOC Service',
                'SLA' => 'Availablity of SLA'
            ];
            ?>
            <li class="menu-list {{ Request::is('asiaconnect/traffic*') ? 'nav-active nav-hover' : '' }}">
                <a href="#"><i class="fa fa-cogs"></i>
                    <span>NRENS Network Traffic and Monitoring<i class="lnr lnr-chevron-right"></i></span></a>
                <ul class="sub-menu-list">
                    @foreach($network as $key => $value)
                        <li class="{{request('slug')==$key ? 'active':''  }}"><a href="{{ route('traffic',['slug'=>$key]) }}">{{ $value }}</a></li>
                    @endforeach
                </ul>
            </li>


            <?php
            $serviceArr = [
                'network_service_SO' => 'Network Service',
                'security_service_SO' => 'Security Service',
                'identity_service_SO' => 'Identity Service',
                'collaboration_service_SO' => 'Collaboration Service',
                'multimedia_service_SO' => 'Multimedia Service',
                'storage_service_SO' => 'Storage and Hosting Service',
                'professional_service_SO' => 'Professional Service',
                'Service' => 'Most Popular Service',
                'Coverage' => 'Service Coverage'
            ];

            ?>
            <li class="menu-list {{ Request::is('asiaconnect/service*') ? 'nav-active nav-hover' : '' }}">
                <a href="#"><i class="fa fa-cogs"></i>
                    <span>Service Platform<i class="lnr lnr-chevron-right"></i></span></a>
                <ul class="sub-menu-list">
                    @foreach($serviceArr as $key => $value)
                        <li class="{{request('slug')==$key ? 'active':''  }}"><a href="{{ route('service.individual',['slug'=>$key]) }}">{{ $value }}</a></li>
                    @endforeach
                </ul>
            </li>




            <?php
            $ad_service = [
                'service_demand' => 'Demanded, Implemented & Pipeline Service by NRENS',
                'IPv6_Peers' => 'Number of IPv6 BGP Peers',
                'Alien_Wave_technology' => 'Availablity of ALIEN Waves',
                'IP_Trunk' => 'Status of IP Trunk',
                'NFV' => 'Implementation of NFV',
                'Protection' => 'Available Protection of Application Services',
                'Identity_and_Trust_Services' => 'Identity & Trust Services',
                'Federation_architecture' => 'Architecture of Federation Deployed',
                'IDPs' => 'IDPs and SPs'
            ];
            ?>
            <li class="menu-list {{ Request::is('asiaconnect/advance.service*') ? 'nav-active nav-hover' : '' }}">
                <a href="#"><i class="fa fa-cogs"></i>
                    <span>Advanced Services<i class="lnr lnr-chevron-right"></i></span></a>
                <ul class="sub-menu-list">
                    @foreach($ad_service as $key => $value)

                        <li class="{{request('slug')==$key ? 'active':''  }}"><a
                                href="{{route('advance.service',['slug'=>$key]) }}">{{ $value }}</a></li>
                    @endforeach
                </ul>
            </li>

            <?php
            $research = [
                'Promotion' => 'NRENS Promotion Initiative',
                'Participation' => 'APAN Participation',
                'communicationWithInstitutions' => 'Mode of Communication with Institution',
                'engineers' => 'Expertise Among NREN Engineers',
                'MemberInstitutions' => 'Expertise Among NREN Member Institutions',
                'Major_Threat' => 'Major Threats Due to COVID-19',
                'Big_Opportunities' => 'Big Opportunities Due to COVID-19',
                'Video_Collaboration_Software' => 'Video Collaboration Software Used By Institution',
                'Challenges' => 'Challenges'
            ];
            ?>
            <li class="menu-list {{ Request::is('asiaconnect/research*') ? 'nav-active nav-hover' : '' }}">
                <a href="#"><i class="fa fa-cogs"></i>
                    <span>Promoting Research & Capacity Building<i class="lnr lnr-chevron-right"></i></span></a>
                <ul class="sub-menu-list">
                    @foreach($research as $key => $value)
                            <li class="{{request('slug')==$key?'active':''}}">
                                <a href="{{route('research',['slug'=>$key]) }}">{{ $value }}</a>
                            </li>
                    @endforeach
                </ul>
            </li>

            <?php
            $link = [
                'AvailablityWith' => 'Link Availablity With Maintenance',
                'AvailablityWithout' => 'Link Availablity Without Maintenance',
                'Traffic' => 'Total Traffic',
                'Average' => 'Average Traffic',
                'Maximum' => 'Maximum Traffic',
            ];
            ?>
            <li class="menu-list {{ Request::is('asiaconnect/link*') ? 'nav-active nav-hover' : '' }}">
                <a href="#"><i class="fa fa-cogs"></i>
                    <span>Global Link<i class="lnr lnr-chevron-right"></i></span></a>
                <ul class="sub-menu-list">
                    @foreach($link as $key => $value)

                        <li class="{{request('slug')==$key?'active':''}}"><a
                                href="{{route('link',['slug'=>$key]) }}">{{ $value }}</a></li>
                    @endforeach
                </ul>
            </li>

        </ul>
        <!-- //sidebar nav end -->
        <!-- toggle button start -->
        <a class="toggle-btn" style="position: relative;">
            <i class="fa fa-angle-double-left menu-collapsed__left"><span>Collapse Sidebar</span></i>
            <i class="fa fa-angle-double-right menu-collapsed__right"></i>
        </a>
        <!-- //toggle button end -->

        </ul>

    </div>
</div>
<!-- //sidebar menu end -->
