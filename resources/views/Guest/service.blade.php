@extends('Guest.guestlayout.app')



@push('after-styles')
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/> -->

    <link rel="stylesheet" href="{{ asset('dist/css/bootstrap-multiselect.css') }}">

@endpush




@section('content')
    <!-- main content start -->
    <div class="main-content">

        <!-- content -->
        <section class="container-fluid content-top-gap">


            <!-- forms -->
            <section class="forms">
                <!-- forms 1 -->
                <div class="card card_border py-2 mb-4">

                    <div class="card-body">

                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://www.bdren.net.bd/" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.bdren.net.bd/" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.bdren.net.bd/" title="Asiaconnect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>
                        </div>

                        <form method="POST" action="{{route('serviceresult')}}" enctype="multipart/form-data">
                            @csrf

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group ">
                                            <label class="input readonly__label">Please Select the Organization: </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <!-- <label>Organization:</label> -->
                                            <select class="form-control form-control-sm required" name="ren_id[]" multiple="multiple" id="example-selectAllValue" required>
                                                @foreach($users as $U_data)
                                                    <option value="{{ $U_data->id}}" >{{ $U_data->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                     <div class="col-sm-2">
                                        <div class="form-group ">
                                            <label class="input readonly__label">Please Select: </label>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <!-- <label>Service Platform</label> -->
                                            <select class="form-control form-control-sm required" name="service_id" required="required">
                                                <option value="network_service_SO" @if($service_id  == 'network_service_SO') selected @endif >Network Service</option>
                                                <option value="security_service_SO" @if($service_id  == 'security_service_SO') selected @endif>Security Service</option>
                                                <option value="identity_service_SO"  @if($service_id  == 'identity_service_SO') selected @endif>Identity Service</option>
                                                <option value="collaboration_service_SO"  @if($service_id  == 'collaboration_service_SO') selected @endif >Collaboration Service</option>
                                                <option value="multimedia_service_SO"  @if($service_id  == 'multimedia_service_SO') selected @endif >Multimedia Service</option>
                                                <option value="storage_service_SO"  @if($service_id  == 'storage_service_SO') selected @endif >Storage and Hosting Service</option>
                                                <option value="professional_service_SO"  @if($service_id  == 'professional_service_SO') selected @endif >Professional Service</option>
                                                <option value="type_of_traffic"  @if($service_id  == 'type_of_traffic') selected @endif >Type of Network Traffic</option>
                                                <option value="tools_TMM"  @if($service_id  == 'tools_TMM') selected @endif >Network Traffic Monitoring Tools</option>
                                                <option value="tools_popularity"  @if($service_id  == 'Net_monitor_popularity') selected @endif >Popularity of Monitoring Tools by NRENS</option>

                                                <option value="statistical_data"  @if($service_id  == 'statistical_data') selected @endif >Sharing of Network Information by NRENS</option>

                                                <option value="netinfo_popularity"  @if($service_id  == 'netinfo_popularity') selected @endif >Popularity of Sharing Network Information</option>

                                                <option value="communication_way"  @if($service_id  == 'communication_way') selected @endif >Mode of Communication with Stakeholders</option>
                                                <option value="popularity_communication_way"  @if($service_id  == 'popularity_communication_way') selected @endif >Popularity of Communication Media</option>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-12 d-flex justify-content-center">
                                        <button type="submit" class="btn btn-primary">Find Result</button>
                                    </div>

                                </div>
                            </fieldset>

                        </form>

                        @if(count($labelData??[]) >0)
                        <div style="width: 80%; height: 480px; margin: 0 auto;">
                            <canvas id="myChart"></canvas>
                        </div>
                        @endif

                        @if(count($key??[]) >0)
                        <div style="width: 80%; height: 480px; margin: 0 auto;">
                            <canvas id="toolschart"></canvas>
                        </div>
                        @endif

                    </div>
                </div>
            </section>
        </section>
        <!-- //content -->
    </div>
    <!-- main content end-->
@endsection


@push('script')
    <script type="text/javascript">
        /*$(document).ready(function () {
            $('.select2').select2({width: '100%'});
        });
*/
                 $(document).ready(function() {
                    $('#example-selectAllValue').multiselect({
                        includeSelectAllOption: true,
                        selectAllValue: 'select-all-value',
                         maxHeight: 400
                    });
                });

        var ctx = $("#myChart");

        var myChart = new Chart(ctx, {
            type: 'matrix',
            data: {
                datasets: [{
                    label: 'My Matrix',
                    data: @json($graph??[]),
                    backgroundColor(ctx) {
                        return ctx.chart.data.datasets[0].data.map(function (value, index) {
                            return value.v === 0 ? '#ecebeb' : '#00b000';
                        });
                    },
                    borderColor(ctx) {
                        return "rgb(20,20,20)";
                    },
                    borderWidth: 1,
                    width: 35,
                    height: 25,
                }]
            },
            options: {
                plugins: {
                    legend: false,
                    tooltip: {
                        callbacks: {
                            title() {
                                return '';
                            },
                            label(context) {
                                const v = context.dataset.data[context.dataIndex];
                                return ['x: ' + v.x, 'y: ' + v.y, 'v: ' + v.v];
                            }
                        }
                    }
                },
                scales: {
                    x: {
                        type: 'category',
                        labels: @json($labelData??[]),
                        ticks: {
                            display: true,
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        },
                        grid: {
                            display: false
                        }
                    },
                    y: {
                        type: 'category',
                        labels: @json($selectedUsers??[]),
                        offset: true,
                        ticks: {
                            display: true
                        },
                        grid: {
                            display: false
                        }
                    }
                }
            }
        });

        //for popularity option of tools

        $(function () {

            //get the bar chart canvas
            var ctx = $("#toolschart");

            //bar chart data
            var data = {
                labels: @json($key??[]),
                datasets: [
                    {
                        label: "Data",
                        data: @json($value??[]),

                        backgroundColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderWidth: 1
                    }
                ]
            };

            //options
            var options = {
                responsive: true,
                plugins: {
                    title: {
                        display: true,
                        text: @json($title??[])
                    }
                },
                legend: {
                    display: true,
                    position: "bottom",
                    labels: {
                        fontColor: "#333",
                        fontSize: 16

                    }
                },
                scales: {
                    x: {
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options
            });
        });
    </script>
@endpush
