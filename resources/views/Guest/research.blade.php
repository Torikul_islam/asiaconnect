@extends('Guest.guestlayout.app')


@push('after-styles')
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/> -->

    <link rel="stylesheet" href="{{ asset('dist/css/bootstrap-multiselect.css') }}">

@endpush


@section('content')
    <!-- main content start -->
    <div class="main-content">

        <!-- content -->
        <section class="container-fluid content-top-gap">


            <!-- forms -->
            <section class="forms">
                <!-- forms 1 -->
                <div class="card card_border py-2 mb-4">

                    <div class="card-body">

                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://www.bdren.net.bd/" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.bdren.net.bd/" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.bdren.net.bd/" title="Asiaconnect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>
                        </div>

                        <form method="POST" action="{{route('research',['slug'=>request('slug')])}}" enctype="multipart/form-data">
                            @csrf

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group ">
                                            <label class="input readonly__label text-primary">Please Select the Organization: </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <!-- <label>Organization:</label> -->
                                            <select class="form-control form-control-sm required" name="ren_id[]" multiple="multiple" id="example-selectAllValue" required>
                                                @foreach($users as $U_data)
                                                    <option value="{{ $U_data->id}}" >{{ $U_data->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-12 d-flex justify-content-center">
                                        <button type="submit" class="btn btn-primary">Find Result</button>
                                    </div>

                                </div>
                            </fieldset>

                        </form>


                        

                         @if(count($apan47??[]) >0)
                        <div style="width: 80%; height: 100%; margin: 0 auto;">
                            <canvas id="ipv6"></canvas>
                        </div>
                        @endif

                        @if(count($alien_key??[]) >0)

                        <div class="row">
                             <div class="col-sm-8">
                               <canvas id="myChart"></canvas>
                            </div>
                            <div class="col-sm-4">
                                <canvas id="alien"></canvas>
                            </div>
                         </div>

                        @endif

                        @if(count($Newsletter??[]) >0)
                        <div style="width: 80%; height: 100%; margin: 0 auto;">
                            <canvas id="promotion"></canvas>
                        </div>
                        @endif

                        @if(count($NREN_Engineers_expert??[]) >0)
                        <div style="width: 80%; height: 100%; margin: 0 auto;">
                            <canvas id="expert"></canvas>
                        </div>
                        @endif

                        


                    </div>
                </div>
            </section>
        </section>
        <!-- //content -->
    </div>
    <!-- main content end-->
@endsection


@push('script')
    <script type="text/javascript">
        /*$(document).ready(function () {
            $('.select2').select2({width: '100%'});
        });
*/
                 $(document).ready(function() {
                    $('#example-selectAllValue').multiselect({
                        includeSelectAllOption: true,
                        selectAllValue: 'select-all-value',
                         maxHeight: 400
                    });
                });


        //for matrix representation 
        var ctx = $("#myChart");
        var myChart = new Chart(ctx, {
            type: 'matrix',
            data: {
                datasets: [{
                    label: 'My Matrix',
                    data: @json($graph??[]),
                    backgroundColor(ctx) {
                        return ctx.chart.data.datasets[0].data.map(function (value, index) {
                            return value.v === 0 ? '#ecebeb' : '#00b000';
                        });
                    },
                    borderColor(ctx) {
                        return "rgb(20,20,20)";
                    },
                    borderWidth: 1,
                    width: 35,
                    height: 25,
                }]
            },
            options: {
                plugins: {
                    legend: false,
                    title: {
                        display: true,
                        text: @json($titletext??[])
                    },
                    tooltip: {
                        callbacks: {
                            label(context) {
                                const v = context.dataset.data[context.dataIndex];
                                return ['x: ' + v.x, 'y: ' + v.y, 'v: ' + v.v];
                            }
                        }
                    }
                },
                scales: {
                    x: {
                        type: 'category',
                        labels: @json($selectedUsers??[]),
                        ticks: {
                            display: true,
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90,
                            align: 'start'
                        },
                        grid: {
                            display: false
                        }
                    },
                    y: {
                        type: 'category',
                        labels: @json($labelData??[]),
                        offset: true,
                        ticks: {
                            display: true
                        },
                        grid: {
                            display: false
                        }
                    }
                }
            }
        });

        //For advance service demand
        $(function () {

            //get the bar chart canvas
            var ctx = $("#promotion");

            //bar chart data
            var data = {
                labels: @json($selectedUsers??[]),
                datasets: [
                    {
                        label: 'Newsletters',
                        data: @json($Newsletter??[]).map(item=>item == 0 ? ' ' :item),

                        backgroundColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderWidth: 1
                    },
                    {
                        label: 'Leaflet/Brochures',
                        data: @json($leaflets??[]).map(item=>item == 0 ? ' ' :item),

                        backgroundColor: [
                            "rgba(211, 223, 84, 0.8)",
                        ],
                        borderColor: [
                            "rgba(211, 223, 84, 0.8)",
                        ],
                        borderWidth: 1
                    },
                    {
                        label: 'Tenders',
                        data: @json($Tenders??[]).map(item=>item == 0 ? ' ' :item),

                        backgroundColor: [
                            "rgba(178, 92, 136, 0.24)",
                        ],
                        borderColor: [
                            "rgba(178, 92, 136, 0.24)",
                        ],
                        borderWidth: 1
                    },
                    {
                        label: 'Souvenirs',
                        data: @json($souvenirs??[]).map(item=>item == 0 ? ' ' :item),

                        backgroundColor: [
                            "#e74c3c",
                        ],
                        borderColor: [
                            "#e74c3c",
                        ],
                        borderWidth: 1
                    }
                ]
            };

            //options
            var options = {
                
                responsive: true,
                plugins: {
                    legend: {
                        display: true,
                        position: "top",
                        labels: {
                            fontColor: "#333",
                            fontSize: 16

                        }
                    },
                    title: {
                        display: true,
                        text: @json($titletext??[])
                    }
                },

                scales: {
                    x: {
                        stacked: true,
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    },
                    y: {
                        stacked: true
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

        //For apan47 and apan48
        $(function () {

            //get the bar chart canvas
            var ctx = $("#ipv6");

            //bar chart data
            var data = {
                labels: @json($selectedUsers??[]),
                datasets: [
                    {
                        label: 'APAN47',
                        data: @json($apan47??[]).map(item=>item == 0 ? ' ' :item),

                        backgroundColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderWidth: 1
                    },
                    {
                        label: 'APAN48',
                        data: @json($apan48??[]).map(item=>item == 0 ? ' ' :item),

                        backgroundColor: [
                            "rgba(211, 223, 84, 0.8)",
                        ],
                        borderColor: [
                            "rgba(211, 223, 84, 0.8)",
                        ],
                        borderWidth: 1
                    }
                ]
            };

            //options
            var options = {
                responsive: true,
                plugins: {
                    legend: {
                        display: true,
                        position: "top",
                        labels: {
                            fontColor: "#333",
                            fontSize: 16

                        }
                    },
                    title: {
                        display: true,
                        text: @json($titletext??[])
                    }
                },

                scales: {
                    x: {
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    },
                     y: {
                        
                        type:'logarithmic'
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

        /* //for aline bar
        $(function () {

            //get the bar chart canvas
            var ctx = $("#alien");

            //bar chart data
            var data = {
                labels: @json($alien_key??[]),
                datasets: [
                    {
                        //label: @json($leveltitle??[]),
                        data: @json($alien_value??[]).map(item=>item == 0 ? ' ' :item),

                        backgroundColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderWidth: 1
                    }
                ]
            };

            //options
            var options = {
                responsive: true,
                plugins: {
                    title: {
                        display: true,
                        text: @json($titletext??[])
                    },
                    legend: {
                        display: false
                    }
                },

                scales: {
                    x: {
                        stacked: true,
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    },
                    y: {
                        stacked: true
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        }); */

        //Noc Services 24*7
        $(function () {
            var ctx = $("#alien");
            var data = {
                labels: @json($alien_key??[]),
                datasets: [
                    {
                        label: " ",
                        data: @json($alien_value??[]),
                        backgroundColor: ['#e74c3c', '#e67e22', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#34495e', '#95a5a6', '#f39c12', '#d35400', '#c0392b', '#bdc3c7', '#7f8c8d'],
                         hoverOffset: 12
                    }
                ]
            };

            //options
            var options = {
                plugins: {
                    title: {
                        display: true,
                        text: @json($title??[])
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });


        //expert enginners
        $(function () {

            //get the bar chart canvas
            var ctx = $("#expert");

            //bar chart data
            var data = {
                labels: @json($selectedUsers??[]),
                datasets: [
                    {
                        label: 'Experts',
                        data: @json($NREN_Engineers_expert??[]).map(item=>item == 0 ? ' ' :item),

                        backgroundColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderWidth: 1
                    },
                    {
                        label: 'Intermediate',
                        data: @json($NREN_Engineers_intermediate??[]).map(item=>item == 0 ? ' ' :item),

                        backgroundColor: [
                            "rgba(211, 223, 84, 0.8)",
                        ],
                        borderColor: [
                            "rgba(211, 223, 84, 0.8)",
                        ],
                        borderWidth: 1
                    },
                    {
                        label: 'Beginners',
                        data: @json($NREN_Engineers_beginner??[]).map(item=>item == 0 ? ' ' :item),

                        backgroundColor: [
                            "rgba(178, 92, 136, 0.24)",
                        ],
                        borderColor: [
                            "rgba(178, 92, 136, 0.24)",
                        ],
                        borderWidth: 1
                    }
                ]
            };

            //options
            var options = {
                responsive: true,
                plugins: {
                    legend: {
                        display: true,
                        position: "top",
                        labels: {
                            fontColor: "#333",
                            fontSize: 16

                        }
                    },
                    title: {
                        display: true,
                        text: @json($titletext??[])
                    }
                },

                scales: {
                    x: {
                        stacked: true,
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    },
                    y: {
                        stacked: true
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

    </script>
@endpush
