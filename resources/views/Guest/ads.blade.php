@extends('Guest.guestlayout.app')

@push('after-styles')
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/> -->

    <link rel="stylesheet" href="{{ asset('dist/css/bootstrap-multiselect.css') }}">

    <link rel="stylesheet" href="{{ asset('css/custom_table.css') }}">
@endpush


@section('content')
    <!-- main content start -->
    <div class="main-content">

        <!-- content -->
        <section class="container-fluid content-top-gap">


            <!-- forms -->
            <section class="forms">
                <!-- forms 1 -->
                <div class="card card_border py-2 mb-4">

                    <div class="card-body">

                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>
                        </div>

                        <form method="POST" action="{{route('advance.service',['slug'=>request('slug')])}}"
                              enctype="multipart/form-data">
                            @csrf

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group ">
                                            <label class="input readonly__label text-primary">Please Select the
                                                Organization: </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <!-- <label>Organization:</label> -->
                                            <select class="form-control form-control-sm required" name="ren_id[]"
                                                    multiple="multiple" id="example-selectAllValue" required>
                                                @foreach($users as $U_data)
                                                    <option value="{{ $U_data->id}}">{{ $U_data->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-12 d-flex justify-content-center">
                                        <button type="submit" class="btn btn-primary">Find Result</button>
                                    </div>

                                </div>
                            </fieldset>
                        </form>

                        @if(count($tableData??[]) >0)
                            <div class="graph-container">
                                <div class="graph-row justify-content-center align-items-center">
                                <div style="height: 1rem; width: 2.5rem" class="bg-warning mx-2">&nbsp;</div>
                                <span>Demand</span>
                                <div style="height: 1rem; width: 2.5rem" class="bg-success mx-2">&nbsp;</div>
                                <span>Implemented</span>
                                <div style="height: 1rem; width: 2.5rem" class="bg-info mx-2">&nbsp;</div>
                                <span>Planned</span>
                                </div>
                                @foreach($tableData as $rowKey => $rowValue)
                                    <div class="graph-row">
                                        <div class="graph-col" style="width:20%;">
                                            {{$rowKey}}
                                        </div>

                                        @foreach ($rowValue as $key => $value)
                                            <div style="width:{{80/count($rowValue)}}%;" title="{{$rowKey}}"
                                                 class="graph-col {{App\Http\Controllers\ads::class_generator($key, $value, $loop->index)}}">
                                                &nbsp;
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach


                                {{-- <div class="graph-row">
                                    <div class="graph-col" style="width:20%;"></div>

                                    <?php
                                    $ranName = "";
                                    foreach ($tableData[array_key_first($tableData)] as $key => $value) {
                                        $temp = explode('-', $key)[1];
                                        if ($temp != $ranName) {
                                            $ranName = $temp;
                                            if ($temp == 'service_demand') {
                                                echo '<div style="width:' . (80 / count($rowValue)) . '%;" class="graph-col">D</div>';
                                            } elseif ($temp == 'service_implemented') {
                                                echo '<div style="width:' . (80 / count($rowValue)) . '%;" class="graph-col">I</div>';
                                            } else {
                                                echo '<div style="width:' . (80 / count($rowValue)) . '%;" class="graph-col">P</div>';
                                            }

                                        }
                                    }
                                    ?>

                                </div> --}}


                                <div class="graph-row">
                                    <div class="graph-col" style="width:20%;"></div>

                                    <?php
                                    $ranName = "";
                                    $index =0;
                                    foreach ($tableData[array_key_first($tableData)] as $key => $value) {
                                        $temp = explode('-', $key)[0];
                                        if ($temp != $ranName) {
                                            $ranName = $temp;
                                            $index++;
                                            if ($index %2 == 1) {
                                                echo '<div class="graph-col color" style="width:' . ((80 / count($rowValue)) * 3) . '%;" class="graph-col">
                                                <div>' . $ranName . '</div>
                                                </div>';
                                            }else{
                                                echo '<div class="graph-col" style="width:' . ((80 / count($rowValue)) * 3) . '%;" class="graph-col">
                                                <div>' . $ranName . '</div>
                                                </div>';
                                            }
                                            
                                        }
                                    }
                                    ?>
                                </div>
                            </div>

                            <div style="width: 80%; height: 100%; margin: 0 auto;">
                                <canvas id="toolschart"></canvas>
                            </div>

                        @endif

                        {{-- @if(count($service_demand??[]) >0)
                            <div style="width: 80%; height: 480px; margin: 0 auto;">
                                <canvas id="toolschart"></canvas>
                            </div>
                        @endif --}}

                        @if(count($ipv6_bgp_upstream??[]) >0)
                            <div style="width: 80%; height: 100%; margin: 0 auto;">
                                <canvas id="ipv6"></canvas>
                            </div>
                        @endif

                        @if(count($alien_key??[]) >0)
                            <div class="row">
                                <div class="col-sm-6">
                                    <div style="overflow-x:auto;">
                                        <table id="myFileTable" class="table table-striped table-bordered"
                                               style="text-align: center;">
                                            <thead class="text-capitalize " style="text-align: center;">
                                            <tr>
                                                <th>RENS Name</th>
                                                <th>Availability</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($mainColl as $data)
                                                <tr>
                                                    <td>{{$data['User']}}</td>
                                                    <td>{{$data['data']}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <canvas id="alien"></canvas>
                                </div>
                            </div>
                        @endif

                        @if(count($pkey??[]) >0)
                            <div class="row">
                                <div class="col-sm-8">
                                    <canvas id="myChart"></canvas>
                                </div>
                                <div class="col-sm-4">
                                    <canvas id="protection"></canvas>
                                </div>
                            </div>
                        @endif

                        @if(count($ID_fed??[]) >0)
                            <div class="row">
                                <div class="col-sm-8">
                                    <canvas id="idfed"></canvas>
                                </div>
                                <div class="col-sm-4">
                                    <canvas id="idfedpie"></canvas>
                                </div>
                            </div>
                        @endif

                        @if(count($information_Federation_Services_IdPs??[]) >0)
                            <div style="width: 80%; height: 100%; margin: 0 auto;">
                                <canvas id="idp"></canvas>
                            </div>
                        @endif

                    </div>
                </div>
            </section>
        </section>
        <!-- //content -->
    </div>
    <!-- main content end-->
@endsection


@push('script')
    <script type="text/javascript">

        $(document).ready(function () {
            $('#example-selectAllValue').multiselect({
                includeSelectAllOption: true,
                selectAllValue: 'select-all-value',
                maxHeight: 400
            });
        });

        //for the previous one

        var ctx = $("#myChart");
        var myChart = new Chart(ctx, {
            type: 'matrix',
            data: {
                datasets: [{
                    label: 'My Matrix',
                    data: @json($mainColl??[]),
                    backgroundColor(ctx) {
                        return ctx.chart.data.datasets[0].data.map(function (value, index) {
                            return value.v === 0 ? '#ecebeb' : '#00b000';
                        });
                    },
                    borderColor(ctx) {
                        return "rgb(20,20,20)";
                    },
                    borderWidth: 1,
                    width: 35,
                    height: 25,
                }]
            },
            options: {
                plugins: {
                    legend: false,
                    title: {
                        display: true,
                        text: @json($titletext??[])
                    },
                    tooltip: {
                        callbacks: {
                            label(context) {
                                const v = context.dataset.data[context.dataIndex];
                                return ['x: ' + v.x, 'y: ' + v.y, 'v: ' + v.v];
                            }
                        }
                    }
                },
                scales: {
                    x: {
                        type: 'category',
                        //labels: @json($selectedUsers??[]),
                        ticks: {
                            display: true,
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90,
                            crossAlign: 'center'
                        },
                        offset: true
                    },
                    y: {
                        type: 'category',
                        labels: @json($labelData??[]),
                        offset: true,
                        ticks: {
                            display: true
                        },
                        grid: {
                            display: false
                        }
                    }
                }
            }
        });


        //For advance service demand
        $(function () {

            //get the bar chart canvas
            var ctx = $("#toolschart");

            //bar chart data
            var data = {
                labels: @json($selectedUsers??[]),
                datasets: [
                    {
                        label: @json($labelData[0]??[]),
                        data: @json($service_demand??[]).map(item => item == 0 ? ' ' : item),

                        backgroundColor: [
                            "#ffc107"
                        ],
                        borderColor: [
                             "#ffc107"
                        ],
                        borderWidth: 1,
                        datalabels: {
                            color: 'blue'
                        }
                    },
                    {
                        label: @json($labelData[1]??[]),
                        data: @json($service_implemented??[]).map(item => item == 0 ? ' ' : item),
                        backgroundColor: [
                            "#28a745"
                        ],
                        borderColor: [
                            "#28a745"
                        ],
                        borderWidth: 1,
                        datalabels: {
                            color: 'blue'
                        }
                    },
                    {
                        label: @json($labelData[2]??[]),
                        data: @json($service_pipeline??[]).map(item => item == 0 ? ' ' : item),

                        backgroundColor: [
                            "#17a2b8"
                        ],
                        borderColor: [
                            "#17a2b8"
                        ],
                        borderWidth: 1,
                        datalabels: {
                            color: 'blue'
                        }
                    }
                ]
            };

            //options
            var options = {
                responsive: true,
                plugins: {
                    legend: {
                        display: true,
                        position: "top",
                        labels: {
                            fontColor: "#333",
                            fontSize: 16

                        }
                    },
                    title: {
                        display: true,
                        text: @json($titletext??[])
                    }
                },

                scales: {
                    x: {
                        stacked: true,
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    },
                    y: {
                        stacked: true
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

        //For ipv6
        $(function () {

            //get the bar chart canvas
            var ctx = $("#ipv6");

            //bar chart data
            var data = {
                labels: @json($selectedUsers??[]),
                datasets: [
                    {
                        label: @json($labelData[0]??[]),
                        data: @json($total??[]).map(item => item == 0 ? ' ' : item),

                        backgroundColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderWidth: 1,
                        datalabels: {
                            color: 'blue',
                            anchor: 'end',
                            align: 'top',
                            rotation: '270'
                        }
                    },
                    {
                        label: @json($labelData[1]??[]),
                        data: @json($ipv6_bgp_upstream??[]).map(item => item == 0 ? ' ' : item),

                        backgroundColor: [
                            "rgba(211, 223, 84, 0.8)",
                        ],
                        borderColor: [
                            "rgba(211, 223, 84, 0.8)",
                        ],
                        borderWidth: 1,
                        datalabels: {
                            color: 'blue',
                            anchor: 'end',
                            align: 'top',
                            rotation: '270'
                        }
                    },
                    {
                        label: @json($labelData[2]??[]),
                        data: @json($ipv6_bgp_downstream??[]).map(item => item == 0 ? ' ' : item),

                        backgroundColor: [
                            "rgba(178, 92, 136, 0.24)",
                        ],
                        borderColor: [
                            "rgba(178, 92, 136, 0.24)",
                        ],
                        borderWidth: 1,
                        datalabels: {
                            color: 'blue',
                            anchor: 'end',
                            align: 'top',
                            rotation: '270'
                        }
                    },
                    {
                        label: @json($labelData[3]??[]),
                        data: @json($ipv6_bgp_domestic??[]).map(item => item == 0 ? ' ' : item),

                        backgroundColor: [
                            "rgba(53, 93, 25, 0.8)",
                        ],
                        borderColor: [
                            "rgba(53, 93, 25, 0.8)",
                        ],
                        borderWidth: 1,
                        datalabels: {
                            color: 'blue',
                            anchor: 'end',
                            align: 'top',
                            rotation: '270'
                        }
                    },
                    {
                        label: @json($labelData[4]??[]),
                        data: @json($ipv6_bgp_cdn??[]).map(item => item == 0 ? ' ' : item),

                        backgroundColor: [
                            "rgba(40, 191, 17, 0.8)",
                        ],
                        borderColor: [
                            "rgba(40, 191, 17, 0.8)",
                        ],
                        borderWidth: 1,
                        datalabels: {
                            color: 'blue',
                            anchor: 'end',
                            align: 'top',
                            rotation: '270'
                        }
                    }
                ]
            };

            //options
            var options = {

                responsive: true,
                plugins: {
                    legend: {
                        display: true,
                        position: "top",
                        labels: {
                            fontColor: "#333",
                            fontSize: 16

                        }
                    },
                    title: {
                        display: true,
                        text: @json($titletext??[])
                    }
                },

                scales: {
                    x: {

                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    },
                    y: {

                        type: 'logarithmic'

                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

        //for aline bar
        $(function () {

            //get the bar chart canvas
            var ctx = $("#alien");

            //bar chart data
            var data = {
                labels: @json($alien_key??[]),
                datasets: [
                    {
                        data: @json($alien_value??[]).map(item => item == 0 ? ' ' : item),

                        backgroundColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderWidth: 1,
                        datalabels: {
                            color: 'blue',
                            anchor: 'end',
                            align: 'top',
                            rotation: '270'
                        }
                    }
                ]
            };

            //options
            var options = {
                responsive: true,
                plugins: {
                    title: {
                        display: true,
                        text: @json($titletext??[])
                    },
                    legend: {
                        display: false
                    }
                },

                scales: {
                    x: {
                        stacked: true,
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    },
                    y: {
                        stacked: true
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

        //for idps
        $(function () {

            //get the bar chart canvas
            var ctx = $("#idp");

            //bar chart data
            var data = {
                labels: @json($user_name??[]),
                datasets: [
                    {
                        label: 'IdPs',
                        data: @json($information_Federation_Services_IdPs??[]).map(item => item == 0 ? ' ' : item),

                        backgroundColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderColor: [
                            "rgba(0,0,255,0.3)",
                        ],
                        borderWidth: 1,
                        datalabels: {
                            color: 'blue',
                            rotation: '270'
                        }
                    },
                    {
                        label: 'SPs',
                        data: @json($information_Federation_Services_SPs??[]).map(item => item == 0 ? ' ' : item),

                        backgroundColor: [
                            "rgba(211, 223, 84, 0.8)",
                        ],
                        borderColor: [
                            "rgba(211, 223, 84, 0.8)",
                        ],
                        borderWidth: 1,
                        datalabels: {
                            color: 'blue',
                            rotation: '270'
                        }
                    }
                ]
            };

            //options
            var options = {
                responsive: true,
                plugins: {
                    legend: {
                        display: true,
                        position: "top",
                        labels: {
                            fontColor: "#333",
                            fontSize: 16

                        }
                    },
                    title: {
                        display: true,
                        text: @json($titletext??[])
                    }
                },

                scales: {
                    x: {
                        stacked: true,
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    },
                    y: {
                        stacked: true,
                        type: 'logarithmic'
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });


        //Noc Services 24*7
        $(function () {
            var ctx = $("#protection");
            var data = {
                labels: @json($pkey??[]),
                datasets: [
                    {
                        label: " ",
                        data: @json($value??[]),

                        backgroundColor: [
                            'rgba(0,0,255,0.3)',
                            'rgba(211, 223, 84, 0.8)',
                            '#00a65a',
                            '#f39c12',
                            '#f56954',
                            '#00c0ef'
                        ],
                        hoverOffset: 12
                    }
                ]
            };

            //options
            var options = {
                plugins: {
                    title: {
                        display: true,
                        text: @json($title??[])
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });


        //for id and federation server
        var ctx = $("#idfed");
        var myChart = new Chart(ctx, {
            type: 'matrix',
            data: {
                datasets: [{
                    label: 'My Matrix',
                    data: @json($ID_fed??[]),
                    backgroundColor(ctx) {
                        return ctx.chart.data.datasets[0].data.map(function (value, index) {
                            return value.v === 0 ? '#ecebeb' : '#00b000';
                        });
                    },
                    borderColor(ctx) {
                        return "rgb(20,20,20)";
                    },
                    borderWidth: 1,
                    width: 35,
                    height: 25,
                }]
            },
            options: {
                plugins: {
                    legend: false,
                    title: {
                        display: true,
                        text: @json($titletext??[])
                    },
                    tooltip: {
                        callbacks: {
                            label(context) {
                                const v = context.dataset.data[context.dataIndex];
                                return ['x: ' + v.x, 'y: ' + v.y, 'v: ' + v.v];
                            }
                        }
                    }
                },
                scales: {
                    x: {
                        type: 'category',
                        offset: true,
                        labels: @json($selectedUsers??[]),
                        ticks: {
                            display: true,
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        },
                        grid: {
                            display: false
                        }
                    },
                    y: {
                        type: 'category',
                        labels: @json($labelData??[]),
                        offset: true,
                        ticks: {
                            display: true
                        },
                        grid: {
                            display: false
                        }
                    }
                }
            }
        });


        //for id fed pie chart
        //Noc Services 24*7
        $(function () {
            var ctx = $("#idfedpie");
            var data = {
                labels: @json($id_fed_key??[]),
                datasets: [
                    {
                        label: " ",
                        data: @json($id_fed_key_value??[]),

                        backgroundColor: ['#e74c3c', '#e67e22', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#34495e', '#95a5a6'],
                        hoverOffset: 12
                    }
                ]
            };

            //options
            var options = {
                plugins: {
                    title: {
                        display: true,
                        text: @json($titletext??[])
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });
    </script>
@endpush
