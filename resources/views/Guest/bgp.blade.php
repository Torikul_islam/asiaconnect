@extends('Guest.guestlayout.app')


@push('after-styles')
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/> -->

    <link rel="stylesheet" href="{{ asset('dist/css/bootstrap-multiselect.css') }}">

@endpush

@section('content')
    <!-- main content start -->
    <div class="main-content">

        <!-- content -->
        <section class="container-fluid content-top-gap">


            <!-- forms -->
            <section class="forms">
                <!-- forms 1 -->
                <div class="card card_border py-2 mb-4">

                    <div class="card-body">

                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://www.bdren.net.bd/" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.bdren.net.bd/" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.bdren.net.bd/" title="Asiaconnect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>
                        </div>

                        <form required method="POST"
                              action="{{route('topology',['slug'=>request('slug')])}}" enctype="multipart/form-data">
                            @csrf

                            <fieldset class="form-group">

                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group ">
                                            <label class="input readonly__label text-primary">Please Select the Organization</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <!-- <label>Organization:</label> -->
                                            <select class="form-control form-control-sm required" name="ren_id[]"
                                                    multiple="multiple" id="example-selectAllValue" required>

                                                @foreach($users as $U_data)
                                                    <option value="{{ $U_data->id}}"
                                                            @if($ren_id  == $U_data->id) selected @endif >{{ $U_data->name }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                    

                                    <div class="col-12 d-flex justify-content-center">
                                        <button type="submit" class="btn btn-primary">Find Result</button>
                                    </div>
                                </div>
                            </fieldset>

                        </form>

                        @if(count($data2019??[]) >0)
                            <div style="width: 80%;height: 680px;margin: 0 auto;">
                                <canvas id="traffic"></canvas>
                            </div>
                        @endif

                    </div>
                </div>
            </section>
        </section>
        <!-- //content -->
    </div>
    <!-- main content end-->
@endsection


@push('script')
    <script>

        $(document).ready(function () {
            $('#example-selectAllValue').multiselect({
                includeSelectAllOption: true,
                selectAllValue: 'select-all-value',
                maxHeight: 400
            });

        });

        $(function () {


            //get the bar chart canvas
            var ctx = $("#traffic");

            //bar chart data
            var data = {
                labels: @json($ren_name??[]),
                datasets: [
                    {
                        label: @json($level[0]??[]),
                        data: @json($data2019??[]).map(item=>item == 0 ? ' ' :item),

                        backgroundColor: [
                            "rgba(14, 231, 28, 0.8)", 
                        ],
                        borderColor: [
                            "rgba(14, 231, 28, 0.8)",
                        ],
                        borderWidth: 1,
                        datalabels:{
                            color: 'blue',
                            rotation: '270'

                        }
                    },
                    {
                        label:@json($level[1]??[]),
                        data: @json($data2018??[]).map(item=>item == 0 ? ' ' :item),

                        backgroundColor: [
                            "rgba(81, 37, 132, 0.62)",
                        ],
                        borderColor: [
                            "rgba(81, 37, 132, 0.62)",
                        ],
                        borderWidth: 1,
                        datalabels:{
                            color: 'blue',
                            rotation: '270'

                        }
                    }
                ]
            };

            //options
            var options = {
               
                responsive: true,
                elements: {
                  bar: {
                    borderWidth: 2,
                  }
                },
                plugins: {
                  legend: {
                    position: 'top',
                  },
                  title: {
                    display: true,
                    text: @json($title??[])
                  }
                },
                scales: {
                    x: {
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    },
                    y: {
                        type:'logarithmic'
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

    </script>


@endpush
