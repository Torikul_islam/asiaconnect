@extends('Guest.guestlayout.app')


@push('after-styles')
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/> -->

    <link rel="stylesheet" href="{{ asset('dist/css/bootstrap-multiselect.css') }}">

@endpush


@section('content')
    <!-- main content start -->
    <div class="main-content">

        <!-- content -->
        <section class="container-fluid content-top-gap">


            <!-- forms -->
            <section class="forms">

                <!-- forms 1 -->
                <div class="card card_border py-2 mb-4">

                    <div class="card-body">

                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://www.bdren.net.bd/" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.bdren.net.bd/" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.bdren.net.bd/" title="Asiaconnect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>
                        </div>

                        <form method="POST" action="{{route('landscape',['slug'=>request('slug')])}}"
                              enctype="multipart/form-data">
                            @csrf

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group ">
                                            <label class="input readonly__label text-primary">Select Organization: </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <!-- <label>Organization:</label> -->
                                            <select class="form-control form-control-sm required" name="ren_id[]" multiple="multiple" id="example-selectAllValue" required>
                                                @foreach($users as $U_data)
                                                    <option value="{{ $U_data->id}}" >{{ $U_data->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label class="input readonly__label text-primary">Institutions Type: </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">

                                            <select class="form-control form-control-sm required"
                                                    name="institution_type[]"
                                                    multiple="multiple" id="institution_type" required="required">
                                                    <option value="University">University</option>
                                                    <option value="RI">Research Institute</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- <div class="col-sm-2">
                                        <div class="form-group">

                                            <select class="form-control form-control-sm required"
                                                    name="band_type[]"
                                                    multiple="multiple" id="band_type" required="required">
                                                    <option value="Highest">Highest</option>
                                                    <option value="Average">Average</option>
                                                
                                            </select>
                                        </div>
                                    </div> -->

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label class="input readonly__label text-primary">Category: </label>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-check">
                                            <input class="form-check-input required" type="radio" name="band_type"
                                                value="Highest" @if($band_type  == 'Highest') checked @endif required>
                                            <label class="form-check-label">
                                                Highest
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input required" type="radio" name="band_type"
                                                value="Average" @if($band_type  == 'Average') checked @endif required>
                                            <label class="form-check-label">
                                                Average
                                            </label>
                                        </div>
                                    </div>


                                    <div class="col-12 d-flex justify-content-center">
                                        <button type="submit" class="btn btn-primary">Find Result</button>
                                    </div>

                                </div>
                            </fieldset>

                        </form>

                        @if(count($mainColl??[]) >0)
                            <div style="width: 80%; height: 100%; margin: 0 auto;">
                                <canvas id="myChart"></canvas>
                            </div>
                        @endif


                    </div>
                </div>
            </section>
        </section>
        <!-- //content -->
    </div>
    <!-- main content end-->
@endsection


@push('script')
    <script type="text/javascript">
        /*$(document).ready(function () {
            $('.select2').select2({width: '100%'});
        });
*/
        $(document).ready(function () {
            $('#example-selectAllValue').multiselect({
                includeSelectAllOption: true,
                selectAllValue: 'select-all-value',
                maxHeight: 400
            });

            $('#institution_type').multiselect({
                includeSelectAllOption: true,
                selectAllValue: 'select-all-value',
                maxHeight: 400
            });

            $('#band_type').multiselect({
                includeSelectAllOption: true,
                selectAllValue: 'select-all-value',
                maxHeight: 400
            });
        });


        //For advance service demand
        $(function () {

            //get the bar chart canvas
            var ctx = $("#myChart");

            var main_data = @json($mainColl??[]).
            reduce((group, product) => {
                const {User} = product;
                group[User] = group[User] || [];
                group[User].push(product);
                return group;
            }, {});

            // array row to column

            var reverseArr = {}
            var arrayData = @json($mainColl??[]).
            map(function (item) {
                for (var key in item) {
                    if (item.hasOwnProperty(key)) {
                        if (reverseArr[key] === undefined) {
                            reverseArr[key] = [];
                        }
                        reverseArr[key].push(item[key]);
                    }
                }
            });

            if (reverseArr.hasOwnProperty('User')) {
                delete reverseArr['User'];
            }

            // generate 12 colours
            var colorPattern = ['#e74c3c', '#e67e22', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#34495e', '#95a5a6', '#f39c12', '#d35400', '#c0392b', '#bdc3c7', '#7f8c8d'];

            // grnerare 24 unique colours

            var colorPattern2 = ['#e74c3c', '#e67e22', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#34495e', '#95a5a6', '#f39c12', '#d35400', '#c0392b', '#bdc3c7', '#7f8c8d', '#e74c3c', '#e67e22', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#34495e', '#95a5a6', '#f39c12', '#d35400', '#c0392b', '#bdc3c7', '#7f8c8d'];

            var data = {
                labels: Object.keys(main_data),
                datasets: [
                    ...Object.keys(reverseArr).map(function (key, i) {
                        console.log("Key", key)
                        return {
                            label: key,
                            data: reverseArr[key],
                            backgroundColor: colorPattern2[i] + '99',
                            borderColor: colorPattern2[i],
                            borderWidth: 1,
                            datalabels:{
                            color: 'blue',
                            anchor: 'end',
                            align: 'top',
                            rotation: '270'

                        }
                        }
                    })
                ]
            };

            //options
            var options = {
                responsive: true,
                plugins: {
                    legend: {
                        display: true,
                        position: "top",
                        labels: {
                            fontColor: "#333",
                            fontSize: 16

                        }
                    },
                    title: {
                        display: true,
                        text: @json($titletext??[])
                    }
                },

                scales: {
                    y: {
                        type:'logarithmic'
                    },
                    x: {
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    }
                }
            };

            //create Chart class object
            var chart = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options,
                plugins: [ChartDataLabels]
            });
        });

    </script>
@endpush
