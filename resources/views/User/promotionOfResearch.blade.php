
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page12.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif



                        <div class="cards__heading">
                            <h3>Promotion of Research<span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">122) Initiative from NREN (Promotional Material) in the year 2019:</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Frequency of Printing Newsletter:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NREN_promotional_newsletter" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Frequency of Printing Leaflets/Brochures:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NREN_promotional_leaflets" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Tenders/Advertisements in Daily Newspapers/Web Portals:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NREN_promotional_tender" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Frequency of Arranging Other Souvenirs Gift Materials:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NREN_promotional_gift" class="form-control input-style">
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">123) Initiative of NREN (Arranging Seminars/Meetings involving communities) in 2019:</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Number of Arranged Virtual Seminars/Meetings (TEIN Sponsored):</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NREN_virtual_seminarTEIN_Sponsored" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Number of Arranged Physical Seminars/Meetings (TEIN Sponsored):</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NREN_physical_seminarTEIN_Sponsored" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Number of Arranged Virtual Seminars/Meetings (NREN Sponsored):</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NREN_virtual_seminar_NREN_Sponsored" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Number of Arranged Physical Seminars/Meetings (NREN Sponsored):</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NREN_Physical_seminar_NREN_Sponsored" class="form-control input-style">
                                </div>
                            </div>
                        </fieldset>


                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">124) Number of Registrants from NRENs' country:</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">In APAN47</label>
                                <div class="col-sm-4">
                                    <input type="text" name="number_registrants_apan47" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">In APAN48</label>
                                <div class="col-sm-4">
                                    <input type="text" name="number_registrants_apan48" class="form-control input-style">
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">125) Mode of Communications with Member Institutions regarding any event:</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <div class="form-check">
                                    <input class="form-check-input policies" value="News Letter" name="communicationWithInstitutions[]" type="checkbox" >
                                    <label class="form-check-label">
                                      News Letter
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Email" name="communicationWithInstitutions[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                       Email
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Voice" name="communicationWithInstitutions[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                        Voice
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Facebook" name="communicationWithInstitutions[]"type="checkbox" >
                                    <label class="form-check-label" >
                                         Facebook
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Youtube" name="communicationWithInstitutions[]"type="checkbox" >
                                    <label class="form-check-label" >
                                        Youtube
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Twitter" name="communicationWithInstitutions[]"type="checkbox" >
                                    <label class="form-check-label" >
                                        Twitter
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Web Portal" name="communicationWithInstitutions[]"type="checkbox" >
                                    <label class="form-check-label" >
                                        Web Portal
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>



                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-11.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
