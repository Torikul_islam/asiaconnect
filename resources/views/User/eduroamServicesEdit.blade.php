
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page8.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif

                      @foreach($page8 as $page8_data)
                      
                        <div class="cards__heading">
                            <h3>eduRoam Services<span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">91) Do you have eduroam in your country?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="eduroam"
                                            value="YES"<?php if ($page8_data->eduroam == 'Yes') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="eduroam"
                                            value="NO"<?php if ($page8_data->eduroam == 'No') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">92) How many Campus do you have under eduRoam coverage?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Campus"
                                            value="<10"<?php if ($page8_data->Campus == '<10') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            <10
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Campus"
                                            value=">10 and <=50"<?php if ($page8_data->Campus == '>10 and <=50') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            >10 and <=50
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Campus"
                                            value=">50 and <=100"<?php if ($page8_data->Campus == '>50 and <=100') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            >50 and <=100
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Campus"
                                            value=">100 and <=500"<?php if ($page8_data->Campus == '>100 and <=500') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            >100 and <=500
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Campus"
                                            value=">500"<?php if ($page8_data->Campus == '>500') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                           >500
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">93) How many Access Points do you have with edurom configured?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Access_Points"
                                            value="<100"<?php if ($page8_data->Access_Points == '<100') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            <100
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Access_Points"
                                            value=">100 and <=500"<?php if ($page8_data->Access_Points == '>100 and <=500') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            >100 and <=500
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Access_Points"
                                            value=">500 and <=1000"<?php if ($page8_data->Access_Points == '>500 and <=1000') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            >500 and <=1000
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Access_Points"
                                            value=">1000 and <=5000"<?php if ($page8_data->Access_Points == '>1000 and <=5000') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            >1000 and <=5000
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Access_Points"
                                            value=">5000"<?php if ($page8_data->Access_Points == '>5000') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                           >5000
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">94) Does your NREN have presence in "Service Location Map" under https://monitor.eduroam.org?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Service_Location_Map"
                                            value="YES"<?php if ($page8_data->Service_Location_Map == 'Yes') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Service_Location_Map"
                                            value="NO"<?php if ($page8_data->Service_Location_Map == 'No') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">95) Have you configured Statistics (F-ticks) which is displayed in https://monitor.eduroam.org?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="F_ticks"
                                            value="YES"<?php if ($page8_data->F_ticks == 'Yes') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="F_ticks"
                                            value="NO"<?php if ($page8_data->F_ticks == 'No') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">96) Have you configured Configuration Automation Tool (CAT) for enhancing eduRoam security?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CAT"
                                            value="YES"<?php if ($page8_data->CAT == 'Yes') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="CAT"
                                            value="NO"<?php if ($page8_data->CAT == 'No') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">97) Do you have govRoam Service?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="govRoam"
                                            value="YES"<?php if ($page8_data->govRoam == 'Yes') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="govRoam"
                                            value="NO"<?php if ($page8_data->govRoam == 'No') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">98) Do you have eduRoam Hotspot available beyond Educational/Research Institute?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="eduRoam_Hotspot"
                                            value="YES"<?php if ($page8_data->eduRoam_Hotspot == 'Yes') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="eduRoam_Hotspot"
                                            value="NO"<?php if ($page8_data->eduRoam_Hotspot == 'No') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">99) How many reported Authentications [from NRS] in 2018?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">National:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Authentications_NRS_National_2018" class="form-control input-style" value="{{$page8_data->Authentications_NRS_National_2018}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">International:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Authentications_NRS_International_2018" class="form-control input-style"value="{{$page8_data->Authentications_NRS_International_2018}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">100) How many reported Authentications [from NRS] in 2019?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">National:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Authentications_NRS_National_2019" class="form-control input-style"value="{{$page8_data->Authentications_NRS_National_2019}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">International:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Authentications_NRS_International_2019" class="form-control input-style"value="{{$page8_data->Authentications_NRS_International_2019}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                   @endforeach


                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-7.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
