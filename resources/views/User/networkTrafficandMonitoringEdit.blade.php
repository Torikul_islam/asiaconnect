
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page4.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif

                        @foreach($page4 as $page4_data)

                        <div class="cards__heading">
                            <h3>NREN Network Traffic and Monitoring<span></span></h3>
                        </div>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">35) What type of traffic is/are carried through your NREN network/cloud? </label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">

                                @php
                                $days35 = array("Research Traffic","Commodity Traffic", "CDN Traffic","Domestic Internet Exchange Traffic");
                                $type_of_traffic_data = json_decode($page4_data->type_of_traffic);

                                 if($type_of_traffic_data === null){
                                    $type_of_traffic_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($days35 as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('type_of_traffic[]', $value, in_array($value, $type_of_traffic_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">36) Approximate what percentage of total traffic is used for Research and Education purpose?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Total Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="total_trafficfor_RE" class="form-control input-style"value="{{ $page4_data->total_trafficfor_RE}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">37) Your Total Upstream [Download] Traffic Capacity, Mbps [in 2018]:</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Commodity: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Upstream_commodity_2018" class="form-control input-style"value="{{ $page4_data->Upstream_commodity_2018}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Research and Education:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Upstream_RE_2018" class="form-control input-style"value="{{ $page4_data->Upstream_RE_2018}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Content Data Network [CDN]:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Upstream_CDN_2018" class="form-control input-style"value="{{ $page4_data->Upstream_CDN_2018}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Internet eXchange Points (IXPs):</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Upstream_IXP_2018" class="form-control input-style"value="{{ $page4_data->Upstream_IXP_2018}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">38) Your Total Upstream [Download] Traffic Capacity, Mbps [in 2019]:</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Commodity: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Upstream_commodity_2019" class="form-control input-style"value="{{ $page4_data->Upstream_commodity_2019}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Research and Education:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Upstream_RE_2019" class="form-control input-style"value="{{ $page4_data->Upstream_RE_2019}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Content Data Network [CDN]:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Upstream_CDN_2019" class="form-control input-style"value="{{ $page4_data->Upstream_CDN_2019}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Internet eXchange Points (IXPs):</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Upstream_IXP_2019" class="form-control input-style"value="{{ $page4_data->Upstream_IXP_2019}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">39) Approximate Ratio of Download vs Upload [Place in the format of ratio Download:Upload] Traffic:</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">For Commodity Traffic: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="ratio_DU_commodity_traffic" class="form-control input-style"value="{{ $page4_data->ratio_DU_commodity_traffic}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">For Research Traffic:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="ratio_DU_research_traffic" class="form-control input-style"value="{{ $page4_data->ratio_DU_research_traffic}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">40) Your Total Yearly Traffic Volume [Throughput] in 2019, TB:</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Commodity: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="yearly_traffic_commodity" class="form-control input-style"value="{{ $page4_data->yearly_traffic_commodity}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Research and Education:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="yearly_traffic_RE" class="form-control input-style"value="{{ $page4_data->yearly_traffic_RE}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Content Data Network [CDN]:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="yearly_traffic_CDN" class="form-control input-style"value="{{ $page4_data->yearly_traffic_CDN}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Internet eXchange Points (IXPs):</label>
                                <div class="col-sm-4">
                                    <input type="text" name="yearly_traffic_IXP" class="form-control input-style"value="{{ $page4_data->yearly_traffic_IXP}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>


                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">41) URL to your MRTG [2019 and 2018, if possible]?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Commodity Internet:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="MRTG_commodity" class="form-control input-style"value="{{ $page4_data->MRTG_commodity}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Credentials to Access, if required:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="MRTG_credentilas" class="form-control input-style"value="{{ $page4_data->MRTG_credentilas}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Research Traffic:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="MRTG_ResearchTraffic" class="form-control input-style"value="{{ $page4_data->MRTG_ResearchTraffic}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Credentials to Access, if required:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="MRTG_CreAccess" class="form-control input-style"value="{{ $page4_data->MRTG_CreAccess}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">CDN Traffic: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="MRTG_CDN" class="form-control input-style"value="{{ $page4_data->MRTG_CDN}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                              <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Credentials to Access, if required:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="MRTG_credentilas_2" class="form-control input-style"value="{{ $page4_data->MRTG_credentilas_2}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                              <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Domestic Internet Exchange Traffic:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="MRTG_domestic" class="form-control input-style"value="{{ $page4_data->MRTG_domestic}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                              <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Credentials to Access, if required:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="MRTG_credentilas_3" class="form-control input-style"value="{{ $page4_data->MRTG_credentilas_3}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">42) What is the percentage of your IPv6 Traffic in ratio with Total Traffic?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-4">
                                    <input type="text" name="ipv6_traffic" class="form-control input-style"value="{{ $page4_data->ipv6_traffic}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">43) How many IPv4 BGP Peers [in 2018]?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Upstream (Gateways): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="ipv4_bgp_upstream_2018" class="form-control input-style"value="{{ $page4_data->ipv4_bgp_upstream_2018}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Downstream (Clients):</label>
                                <div class="col-sm-4">
                                    <input type="text" name="ipv4_bgp_downstream_2018" class="form-control input-style"value="{{ $page4_data->ipv4_bgp_downstream_2018}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Domestic IXPs:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="ipv4_bgp_domestic_2018" class="form-control input-style"value="{{ $page4_data->ipv4_bgp_domestic_2018}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">CDNs & Others:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="ipv4_bgp_cdn_2018" class="form-control input-style"value="{{ $page4_data->ipv4_bgp_cdn_2018}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">44) How many IPv4 BGP Peers [in 2019]?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Upstream (Gateways): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="ipv4_bgp_upstream_2019" class="form-control input-style"value="{{ $page4_data->ipv4_bgp_upstream_2019}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Downstream (Clients):</label>
                                <div class="col-sm-4">
                                    <input type="text" name="ipv4_bgp_downstream_2019" class="form-control input-style" value="{{ $page4_data->ipv4_bgp_downstream_2019}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Domestic IXPs:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="ipv4_bgp_domestic_2019" class="form-control input-style" value="{{ $page4_data->ipv4_bgp_domestic_2019}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">CDNs & Others:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="ipv4_bgp_cdn_2019" class="form-control input-style"value="{{ $page4_data->ipv4_bgp_cdn_2019}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">45) Percentage of your upstream link availability => Commodity Traffic [in 2019]</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Availability considering planned maintenance as non-outage</label>
                                <div class="col-sm-4">
                                    <input type="text" name="upstream_non_outage" class="form-control input-style"value="{{ $page4_data->upstream_non_outage}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Availability considering planned maintenance as outage</label>
                                <div class="col-sm-4">
                                    <input type="text" name="upstream_outage" class="form-control input-style"value="{{ $page4_data->upstream_outage}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">46) Percentage of your downstream [Member Institutions'/Clients'] link availability (on Average) => [in2019]</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Availability considering planned maintenance as non-outage</label>
                                <div class="col-sm-4">
                                    <input type="text" name="downstream_non_outage" class="form-control input-style"value="{{ $page4_data->downstream_non_outage}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Availability considering planned maintenance as outage</label>
                                <div class="col-sm-4">
                                    <input type="text" name="downstream_outage" class="form-control input-style"value="{{ $page4_data->downstream_outage}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">47) URL to your Network Diagram:</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">URL:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="network_diagram_url" class="form-control input-style" value="{{ $page4_data->network_diagram_url}}">

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Credentials to Access, if required:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="network_diagram_credentilas" class="form-control input-style" value="{{ $page4_data->network_diagram_credentilas}}">
                                    
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">48) Do you maintain 7*24 NOC (Network Operation Centre) service?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                       
                                        <input class="form-check-input" type="radio" name="NOC"
                                            value="Yes" <?php if ($page4_data->NOC == 'Yes') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="NOC"
                                            value="No" <?php if ($page4_data->NOC == 'No') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">49) Which tools do you operate for traffic monitoring and measurement? (Check all that apply)</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">

                                @php
                                $days49 = array("Cacti","MRTG", "Nagios","Libre NMS","SolarWinds","PerfSONAR","SmokePing","Weathermap");
                                $tools_TMM_data = json_decode($page4_data->tools_TMM);

                                 if($tools_TMM_data === null){
                                    $tools_TMM_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($days49 as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('tools_TMM[]', $value, in_array($value, $tools_TMM_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">50) What kind of statistical data do you provide to your members? (Check all that apply)</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">

                                @php
                                $days50 = array("Real Time Traffic Data - MRTG","Email", "Real Time Fault Report","Monthly Usage Data","Fault/Outage Data (Fault Frequency, Response and Restoration Time)","Meetings/Seminars","Meetings/Seminars","Monthly Availability");
                                $statistical_data_data = json_decode($page4_data->statistical_data);

                                 if($statistical_data_data === null){
                                    $statistical_data_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($days50 as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('statistical_data[]', $value, in_array($value, $statistical_data_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">51) How do you communicate with your members/Institutions?</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">

                                @php
                                $days = array("Realtime through URL","Email", "Direct Phone Call","SMS","Social Media","Meetings/Seminars","Meetings/Seminars","Newsletter/Leaflets");
                                $communication_way_data = json_decode($page4_data->communication_way);

                                 if($communication_way_data === null){
                                    $communication_way_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($days as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('communication_way[]', $value, in_array($value, $communication_way_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">52) Do you have any SLA (Service Level Agreement)?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SLA"
                                            value="Yes" <?php if ($page4_data->SLA == 'Yes') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="SLA"value="No" <?php if ($page4_data->SLA == 'No') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">53) Does your SLA contain the option of compensating clients with Financial Penalty?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="option_of_compensation"
                                            value="Yes"<?php if ($page4_data->option_of_compensation == 'Yes') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="option_of_compensation"
                                            value="No"<?php if ($page4_data->option_of_compensation == 'No') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                         @endforeach



                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-3.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
