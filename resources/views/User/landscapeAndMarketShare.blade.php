
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page3.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif



                        <div class="cards__heading">
                            <h3>NREN Landscape and Market share<span></span></h3>
                        </div>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">22. Which category of Institutions/members are currently connected to your network? ?</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <div class="form-check">
                                    <input class="form-check-input policies" value=" Universities" name="Institutions_category[]" type="checkbox" >
                                    <label class="form-check-label">
                                        Universities
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Research Institutes" name="Institutions_category[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                       Research Institutes
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Government Organisations" name="Institutions_category[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                        Government Organisations
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Schools and Colleges" name="Institutions_category[]"type="checkbox" >
                                    <label class="form-check-label" >
                                         Schools and Colleges
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value=" Museums/Libraries" name="Institutions_category[]"type="checkbox" >
                                    <label class="form-check-label" >
                                          Museums/Libraries
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value=" Hospitals/Medical Colleges" name="Institutions_category[]"type="checkbox" >
                                    <label class="form-check-label" >
                                        Hospitals/Medical Colleges
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Others" name="Institutions_category[]"type="checkbox" >
                                    <label class="form-check-label" >
                                         Others
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">23. How many Institutions/members were connected to your network [end of 2019]?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_connected_Universities" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_connected_RI" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_connected_govt" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_connected_school" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_connected_other" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">24.  How many Institutions/members were in your Target to connect to your network [in 2019 only] ??</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_Universities" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_RI" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_govt" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_school" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_other" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">25.  How many Institutions are on your target to connect [in 2020 only]?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_Universities_now" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_RI_now" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_govt_now" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_school_now" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_other_now" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">26. Approximately how many users were using your network/services under the following institutes ??</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="user_Universities" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="user_RI" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="user_govt" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="user_school" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="user_other" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">27) How many universities are there in Total in your country?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Total Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="total_Universities" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">28. How many different types of Institutions are connected with less than Fast Ethernet(FE)
(100Mbps) physical link capacity?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="lessthan_FE_Universities" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="lessthan_FE_RI" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="lessthan_FE_govt" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="lessthan_FE_school" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="lessthan_FE_other" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">29) How many different types of Institutions are connected with Fast Ethernet(FE) or equivalent (STM1/T4)
physical link capacity?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="FE_Universities" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="FE_RI" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="FE_govt" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="FE_school" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="FE_other" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">30) How many different types of Institutions are connected with 1G or equivalent (STM4/STM16) physical
link capacity?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="1G_Universities" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="1G_RI" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="1G_govt" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="1G_school" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="1G_other" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">31) How many different types of Institutions are connected with 10G physical link capacity?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="10G_Universities" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="10G_RI" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="10G_govt" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="10G_school" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="10G_other" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">32) How many different types of Institutions are connected with 40G/100G physical link capacity?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="40G_Universities" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="40G_RI" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="40G_govt" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="40G_school" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="40G_other" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">33) What is the highest Throughput (in Mbps) institutions are subscribing?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="hightest_throughput_Universities" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="hightest_throughput_RI" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="hightest_throughput_govt" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="hightest_throughput_school" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="hightest_throughput_other" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">34) What is the Average Throughput (in Mbps) institutions are subscribing?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="average_throughput_Universities" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="average_throughput_RI" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="average_throughput_govt" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="average_throughput_school" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="average_throughput_other" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>





                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-2.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
