
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page6.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif

                       @foreach($page6 as $page6_data)
                        <div class="cards__heading">
                            <h3>Advanced Services<span></span></h3>
                        </div>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">63) Services demanded by users? (Check all that apply)</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">

                                @php
                                $service_demand_63 = array("EduRoam","EduGain", "HPC","Computing/Storage","SDN","E-Library","Research Gateway Server", "High Performance VPS", "IPv6", "Virtual Labs/Smart Classroom", "IoT", "Access to Journal and Research Databases", "Connectivity to Public Cloud (Azure/AWS)","E-Learning","Big Data","Blockchain","Content Filtering","Telemedicine","Collocation");

                                $service_demand_data = json_decode($page6_data->service_demand);
                                 if($service_demand_data === null){
                                    $service_demand_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($service_demand_63 as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('service_demand[]', $value, in_array($value, $service_demand_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>
                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">64) Services already implemented?</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">

                                @php
                                $service_implemented_64 = array("EduRoam","EduGain", "HPC","Computing/Storage","SDN","E-Library","Research Gateway Server", "High Performance VPS", "IPv6", "Virtual Labs/Smart Classroom", "IoT", "Access to Journal and Research Databases", "Connectivity to Public Cloud (Azure/AWS)","E-Learning","Big Data","Blockchain","Content Filtering","Telemedicine","Collocation");

                                $service_implemented_data = json_decode($page6_data->service_implemented);
                                 if($service_implemented_data === null){
                                    $service_implemented_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($service_implemented_64 as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('service_implemented[]', $value, in_array($value, $service_implemented_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>
                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">65) Services under pipeline for implementation?</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                @php
                                $service_pipeline_65 = array("EduRoam","EduGain", "HPC","Computing/Storage","SDN","E-Library","Research Gateway Server", "High Performance VPS", "IPv6", "Virtual Labs/Smart Classroom", "IoT", "Access to Journal and Research Databases", "Connectivity to Public Cloud (Azure/AWS)","E-Learning","Big Data","Blockchain","Content Filtering","Telemedicine","Collocation");

                                $service_pipeline_data = json_decode($page6_data->service_pipeline);
                                 if($service_pipeline_data === null){
                                    $service_pipeline_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($service_pipeline_65 as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('service_pipeline[]', $value, in_array($value, $service_pipeline_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>


                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">66) Please state the implementation of IPv6 in:</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> DNS Server </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="DNS_Server"
                                            value="Full"<?php if ($page6_data->DNS_Server == 'Full') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Full
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="DNS_Server"
                                            value="Partial"<?php if ($page6_data->DNS_Server == 'Partial') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Partial
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="DNS_Server"
                                            value="None"<?php if ($page6_data->DNS_Server == 'None') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           None
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-4 col-form-label input__label"> Web Server </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="web_Server"
                                            value="Full"<?php if ($page6_data->web_Server == 'Full') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Full
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="web_Server"
                                            value="Partial"<?php if ($page6_data->web_Server == 'Partial') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Partial
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="web_Server"
                                            value="None"<?php if ($page6_data->web_Server == 'None') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           None
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Mail Server </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="mail_Server"
                                            value="Full"<?php if ($page6_data->mail_Server == 'Full') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Full
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="mail_Server"
                                            value="Partial"<?php if ($page6_data->mail_Server == 'Partial') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Partial
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="mail_Server"
                                            value="None"<?php if ($page6_data->mail_Server == 'None') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           None
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">DHCP/SLAAC Server/Router </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dhcp_Server"
                                            value="Full"<?php if ($page6_data->dhcp_Server == 'Full') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Full
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dhcp_Server"
                                            value="Partial"<?php if ($page6_data->dhcp_Server == 'Partial') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Partial
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dhcp_Server"
                                            value="None"<?php if ($page6_data->dhcp_Server == 'None') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           None
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-4 col-form-label input__label"> Routing System </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="routing_system"
                                            value="Full"<?php if ($page6_data->routing_system == 'Full') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Full
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="routing_system"
                                            value="Partial"<?php if ($page6_data->routing_system == 'Partial') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Partial
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="routing_system"
                                            value="None"<?php if ($page6_data->routing_system == 'None') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           None
                                        </label>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group row">
                                <label  class="col-sm-4 col-form-label input__label"> Switching System </label>
                                 <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="switching_system"
                                            value="Full"<?php if ($page6_data->switching_system == 'Full') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Full
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="switching_system"
                                            value="Partial"<?php if ($page6_data->switching_system == 'Partial') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Partial
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="switching_system"
                                            value="None"<?php if ($page6_data->switching_system == 'None') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                           None
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">67) How many IPv6 BGP peers do you have?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Upstream (Gateways):</label>
                                <div class="col-sm-4">
                                    <input type="text" name="ipv6_bgp_upstream" class="form-control input-style"value="{{ $page6_data->ipv6_bgp_upstream}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Downstream (Clients):</label>
                                <div class="col-sm-4">
                                    <input type="text" name="ipv6_bgp_downstream" class="form-control input-style"value="{{ $page6_data->ipv6_bgp_downstream}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Domestic IXPs:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="ipv6_bgp_domestic" class="form-control input-style"value="{{ $page6_data->ipv6_bgp_domestic}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">CDNs and Others:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="ipv6_bgp_cdn" class="form-control input-style"value="{{ $page6_data->ipv6_bgp_cdn}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">68) How many prefixes received from Upstream Peers (Gateways)?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">IPv4:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="upstram_peer_ipv4" class="form-control input-style"value="{{ $page6_data->upstram_peer_ipv4}}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">IPv6:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="upstram_peer_ipv6" class="form-control input-style"value="{{ $page6_data->upstram_peer_ipv6}}">
                                    
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">69) How many prefixes received from Parallel Peers (CDNs and IXPs)?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">IPv4:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="parallei_peer_ipv4" class="form-control input-style"value="{{ $page6_data->parallei_peer_ipv4}}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">IPv6:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="parallei_peer_ipv6" class="form-control input-style"value="{{ $page6_data->parallei_peer_ipv6}}">
                                    
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">70) How many prefixes received from Downstream Peers (Clients)?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">IPv4:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="downstream_peer_ipv4" class="form-control input-style"value="{{ $page6_data->downstream_peer_ipv4}}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">IPv6:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="downstream_peer_ipv6" class="form-control input-style"value="{{ $page6_data->downstream_peer_ipv6}}">
                                    
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">71) Do you have IPv6 peering with TEIN upstream Gateway/s?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="ipv6_tein_upstream"
                                            value="Yes"<?php if ($page6_data->ipv6_tein_upstream == 'Yes') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="ipv6_tein_upstream"
                                            value="No"<?php if ($page6_data->ipv6_tein_upstream == 'NO') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">72) How many Application Servers are setup in your Cloud System?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">IPv4 enabled</label>
                                <div class="col-sm-4">
                                    <input type="text" name="application_server_ipv4" class="form-control input-style"value="{{ $page6_data->application_server_ipv4}}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">IPv6 enabled</label>
                                <div class="col-sm-4">
                                    <input type="text" name="application_server_ipv6" class="form-control input-style"value="{{ $page6_data->application_server_ipv6}}">
                                    
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">73) Which type of configuration used in the network devices having IPv6?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="config_network_ipv6"
                                            value="Dual Stack"<?php if ($page6_data->config_network_ipv6 == 'Dual Stack') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Dual Stack
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="config_network_ipv6"
                                            value="IPv6 Tunneling"<?php if ($page6_data->config_network_ipv6 == 'IPv6 Tunneling') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            IPv6 Tunneling
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="config_network_ipv6"
                                            value="Both Dual Stack and Tunneling"<?php if ($page6_data->config_network_ipv6 == 'Both Dual Stack and Tunneling') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Both Dual Stack and Tunneling
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="config_network_ipv6"
                                            value="IPv6 yet to be implemented"<?php if ($page6_data->config_network_ipv6 == 'IPv6 yet to be implemented') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            IPv6 yet to be implemented
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">74) How many Public ASN do you [NREN] have?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-4">
                                    <input type="text" name="public_asn" class="form-control input-style"value="{{ $page6_data->public_asn}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">75) How many IPv4 (/24) Blocks do you have?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-4">
                                    <input type="text" name="ipv4_24_blocks" class="form-control input-style"value="{{ $page6_data->ipv4_24_blocks}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">76) How many Members under NREN having independent ASN?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-4">
                                    <input type="text" name="independent_ASN" class="form-control input-style"value="{{ $page6_data->independent_ASN}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">77) How many of the Members having IPv6 Routed ASN/Prefixes?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-4">
                                    <input type="text" name="IPv6_Routed_ASN" class="form-control input-style"value="{{ $page6_data->IPv6_Routed_ASN}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">78) Have you already joined Mutually Agreed Norms for Routing Security [MANRS]?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="MANRS"
                                            value="Yes"<?php if ($page6_data->MANRS == 'Yes') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="MANRS"
                                            value="No"<?php if ($page6_data->MANRS == 'NO') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

     @endforeach


                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-5.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>


                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
