
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">

        <!-- breadcrumbs -->
        <nav aria-label="breadcrumb" class="mb-4">
            <ol class="breadcrumb my-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('page1') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Forms</li>
            </ol>
        </nav>
        <!-- //breadcrumbs -->
        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page1.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif

                            @foreach($page1 as $page1_data)

                            <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">1.Organization / NREN are you from</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Organization:</label>
                                        <select class="form-control form-control-sm" name="ren_type_id" id="RENOthers">

                                            <option value="{{ Auth::user()->id}}">{{ Auth::user()->name }}</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <div class="cards__heading">
                            <h3>NREN Contacts<span></span></h3>
                        </div>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">2.Administrative Contact [Governer in Asi@Connect]</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Name: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="ad_con_name" class="form-control input-style" value="{{ $page1_data->ad_con_name}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Designation: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="ad_con_designation" class="form-control input-style" value="{{ $page1_data->ad_con_designation}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Email Address: </label>
                                <div class="col-sm-4">
                                    <input type="email" name="ad_con_email" class="form-control input-style" value="{{ $page1_data->ad_con_email}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Phone Number: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="ad_con_phone" class="form-control input-style" value="{{ $page1_data->ad_con_phone}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">3.Technical Contact [Governer in Asi@Connect]</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Name: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="tec_con_name" class="form-control input-style" value="{{ $page1_data->tec_con_name}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Designation: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="tec_con_designation" class="form-control input-style" value="{{ $page1_data->tec_con_designation}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Email Address: </label>
                                <div class="col-sm-4">
                                    <input type="email" name="tec_con_email" class="form-control input-style" value="{{ $page1_data->tec_con_email}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Phone Number: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="tec_con_phone" class="form-control input-style" value="{{ $page1_data->tec_con_phone}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">4.Visiblity Contact [Governer in Asi@Connect]</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Name: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="visi_con_name" class="form-control input-style" value="{{ $page1_data->visi_con_name}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Designation: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="visi_con_designation" class="form-control input-style" value="{{ $page1_data->visi_con_designation}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Email Address: </label>
                                <div class="col-sm-4">
                                    <input type="email" name="visi_con_email" class="form-control input-style" value="{{ $page1_data->visi_con_email}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Phone Number: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="visi_con_phone" class="form-control input-style" value="{{ $page1_data->visi_con_phone}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        @endforeach

                        <div class="row">
                            <div class="col-sm-5"></div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
