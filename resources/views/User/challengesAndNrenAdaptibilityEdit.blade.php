
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page14.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif

                         @foreach($page14 as $page14_data)

                        <div class="cards__heading">
                            <h3>Impact of COVID-19: Challenges and NRENs' Adaptibility<span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">133) Month-wise Maximum Usage of Internet, Mbps</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Jan, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_jan_2020" class="form-control input-style"value="{{ $page14_data->Internet_jan_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Feb, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_feb_2020" class="form-control input-style"value="{{ $page14_data->Internet_feb_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">March, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_march_2020" class="form-control input-style"value="{{ $page14_data->Internet_march_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">April, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_april_2020" class="form-control input-style"value="{{ $page14_data->Internet_april_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">May, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_may_2020" class="form-control input-style"value="{{ $page14_data->Internet_may_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">June, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_june_2020" class="form-control input-style"value="{{ $page14_data->Internet_june_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">July, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_july_2020" class="form-control input-style"value="{{ $page14_data->Internet_july_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">August, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_august_2020" class="form-control input-style"value="{{ $page14_data->Internet_august_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">September, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_september_2020" class="form-control input-style"value="{{ $page14_data->Internet_september_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">October, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_october_2020" class="form-control input-style"value="{{ $page14_data->Internet_october_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">URL for the MRTG (if any):</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_MRTG" class="form-control input-style"value="{{ $page14_data->Internet_MRTG}}">
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">134) Month-wise Maximum Usage of Research Traffic (Asi@Connect), Mbps</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Jan, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_jan_2020" class="form-control input-style"value="{{ $page14_data->Research_Traffic_jan_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Feb, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_feb_2020" class="form-control input-style"value="{{ $page14_data->Research_Traffic_feb_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">March, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_march_2020" class="form-control input-style"value="{{ $page14_data->Research_Traffic_march_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">April, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_april_2020" class="form-control input-style"value="{{ $page14_data->Research_Traffic_april_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">May, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_may_2020" class="form-control input-style"value="{{ $page14_data->Research_Traffic_may_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">June, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_june_2020" class="form-control input-style"value="{{ $page14_data->Research_Traffic_june_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">July, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_july_2020" class="form-control input-style"value="{{ $page14_data->Research_Traffic_july_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">August, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_august_2020" class="form-control input-style"value="{{ $page14_data->Research_Traffic_august_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">September, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_september_2020" class="form-control input-style"value="{{ $page14_data->Research_Traffic_september_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">October, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_october_2020" class="form-control input-style"value="{{ $page14_data->Research_Traffic_october_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">URL for the MRTG (if any):</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_MRTG" class="form-control input-style"value="{{ $page14_data->Research_Traffic_MRTG}}">
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">135) Impact on Revenue as a fallout of COVID:</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Impact_Revenue"
                                            value="Highly Increased"<?php if ($page14_data->Impact_Revenue == 'Highly Increased') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Highly Increased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Impact_Revenue"
                                            value="Increased"<?php if ($page14_data->Impact_Revenue == 'Increased') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Increased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Campus"
                                            value="Same as Before"<?php if ($page14_data->Impact_Revenue == 'Same as Before') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Same as Before
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Impact_Revenue"
                                            value="Decreased"<?php if ($page14_data->Impact_Revenue == 'Decreased') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Decreased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Impact_Revenue"
                                            value="Highly Decreased"<?php if ($page14_data->Impact_Revenue == 'Highly Decreased') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                           Highly Decreased
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">136) Major Threat due to pandemic</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">

                                @php
                                $days136 = array("Reduction in Revenue","Unemployment", "Network Security","Hardware overrun","Shortage of Software Licences");
                                $Major_Threat_data = json_decode($page14_data->Major_Threat);

                                 if($Major_Threat_data === null){
                                    $Major_Threat_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($days136 as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('Major_Threat[]', $value, in_array($value, $Major_Threat_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">137) Big Opportunities</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">

                                @php
                                $days137 = array("Collaboration among NRENs","Supporting Tele-Medicine", "Supporting Tele-Medicine","Providing Computing/Virtualization Support");
                                $Big_Opportunities_data = json_decode($page14_data->Big_Opportunities);

                                 if($Big_Opportunities_data === null){
                                    $Big_Opportunities_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($days137 as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('Big_Opportunities[]', $value, in_array($value, $Big_Opportunities_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                      @endforeach


                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-13.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
