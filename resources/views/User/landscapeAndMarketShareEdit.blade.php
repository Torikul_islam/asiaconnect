
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page3.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif

                        @foreach($page3 as $page3_data)

                        <div class="cards__heading">
                            <h3>NREN Landscape and Market share<span></span></h3>
                        </div>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">22. Which category of Institutions/members are currently connected to your network? ?</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">

                                @php
                                $days = array("Universities","Research Institutes", "Government Organisations","Schools and Colleges","Museums/Libraries","Hospitals/Medical Colleges","Others");
                                $Institutions_category_data = json_decode($page3_data->Institutions_category);

                                 if($Institutions_category_data === null){
                                    $Institutions_category_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($days as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('Institutions_category[]', $value, in_array($value, $Institutions_category_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">23. How many Institutions/members were connected to your network [end of 2019]?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_connected_Universities" class="form-control input-style" value="{{ $page3_data->Institutions_connected_Universities}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_connected_RI" class="form-control input-style" value="{{ $page3_data->Institutions_connected_RI}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_connected_govt" class="form-control input-style" value="{{ $page3_data->Institutions_connected_govt}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_connected_school" class="form-control input-style"value="{{ $page3_data->Institutions_connected_school}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_connected_other" class="form-control input-style"value="{{ $page3_data->Institutions_connected_other}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">24.  How many Institutions/members were in your Target to connect to your network [in 2019 only] ??</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_Universities" class="form-control input-style"value="{{ $page3_data->Institutions_Target_Universities}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_RI" class="form-control input-style"value="{{ $page3_data->Institutions_Target_RI}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_govt" class="form-control input-style"value="{{ $page3_data->Institutions_Target_govt}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_school" class="form-control input-style"value="{{ $page3_data->Institutions_Target_school}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_other" class="form-control input-style"value="{{ $page3_data->Institutions_Target_other}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">25.  How many Institutions are on your target to connect [in 2020 only]?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_Universities_now" class="form-control input-style"value="{{ $page3_data->Institutions_Target_Universities_now}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_RI_now" class="form-control input-style"value="{{ $page3_data->Institutions_Target_RI_now}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_govt_now" class="form-control input-style"value="{{ $page3_data->Institutions_Target_govt_now}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_school_now" class="form-control input-style"value="{{ $page3_data->Institutions_Target_school_now}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="Institutions_Target_other_now" class="form-control input-style"value="{{ $page3_data->Institutions_Target_other_now}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">26. Approximately how many users were using your network/services under the following institutes ??</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="user_Universities" class="form-control input-style"value="{{ $page3_data->user_Universities}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="user_RI" class="form-control input-style"value="{{ $page3_data->user_RI}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="user_govt" class="form-control input-style"value="{{ $page3_data->user_govt}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="user_school" class="form-control input-style"value="{{ $page3_data->user_school}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="user_other" class="form-control input-style"value="{{ $page3_data->user_other}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">27) How many universities are there in Total in your country?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Total Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="total_Universities" class="form-control input-style"value="{{ $page3_data->total_Universities}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">28. How many different types of Institutions are connected with less than Fast Ethernet(FE)
(100Mbps) physical link capacity?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="lessthan_FE_Universities" class="form-control input-style"value="{{ $page3_data->lessthan_FE_Universities}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="lessthan_FE_RI" class="form-control input-style"value="{{ $page3_data->lessthan_FE_RI}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="lessthan_FE_govt" class="form-control input-style"value="{{ $page3_data->lessthan_FE_govt}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="lessthan_FE_school" class="form-control input-style"value="{{ $page3_data->lessthan_FE_school}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="lessthan_FE_other" class="form-control input-style"value="{{ $page3_data->lessthan_FE_other}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">29) How many different types of Institutions are connected with Fast Ethernet(FE) or equivalent (STM1/T4)
physical link capacity?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="FE_Universities" class="form-control input-style"value="{{ $page3_data->FE_Universities}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="FE_RI" class="form-control input-style"value="{{ $page3_data->FE_RI}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="FE_govt" class="form-control input-style"value="{{ $page3_data->FE_govt}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="FE_school" class="form-control input-style"value="{{ $page3_data->FE_school}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="FE_other" class="form-control input-style"value="{{ $page3_data->FE_other}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">30) How many different types of Institutions are connected with 1G or equivalent (STM4/STM16) physical
link capacity?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="1G_Universities" class="form-control input-style"value="{{ $page3_data->t1G_Universities}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="1G_RI" class="form-control input-style"value="{{ $page3_data->t1G_RI}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="1G_govt" class="form-control input-style"value="{{ $page3_data->t1G_govt}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="1G_school" class="form-control input-style"value="{{ $page3_data->t1G_school}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="1G_other" class="form-control input-style"value="{{ $page3_data->t1G_other}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">31) How many different types of Institutions are connected with 10G physical link capacity?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="10G_Universities" class="form-control input-style"value="{{ $page3_data->t10G_Universities}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="10G_RI" class="form-control input-style"value="{{ $page3_data->t10G_RI}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="10G_govt" class="form-control input-style"value="{{ $page3_data->t10G_govt}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="10G_school" class="form-control input-style"value="{{ $page3_data->t10G_school}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="10G_other" class="form-control input-style"value="{{ $page3_data->t10G_other}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">32) How many different types of Institutions are connected with 40G/100G physical link capacity?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="40G_Universities" class="form-control input-style"value="{{ $page3_data->t40G_Universities}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="40G_RI" class="form-control input-style"value="{{ $page3_data->t40G_RI}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="40G_govt" class="form-control input-style"value="{{ $page3_data->t40G_govt}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="40G_school" class="form-control input-style"value="{{ $page3_data->t40G_school}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="40G_other" class="form-control input-style"value="{{ $page3_data->t40G_other}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">33) What is the highest Throughput (in Mbps) institutions are subscribing?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="hightest_throughput_Universities" class="form-control input-style"value="{{ $page3_data->hightest_throughput_Universities}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="hightest_throughput_RI" class="form-control input-style"value="{{ $page3_data->hightest_throughput_RI}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="hightest_throughput_govt" class="form-control input-style"value="{{ $page3_data->hightest_throughput_govt}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="hightest_throughput_school" class="form-control input-style"value="{{ $page3_data->hightest_throughput_school}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="hightest_throughput_other" class="form-control input-style"value="{{ $page3_data->hightest_throughput_other}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">34) What is the Average Throughput (in Mbps) institutions are subscribing?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Universities: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="average_throughput_Universities" class="form-control input-style"value="{{ $page3_data->average_throughput_Universities}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Research Institutes: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="average_throughput_RI" class="form-control input-style"value="{{ $page3_data->average_throughput_RI}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Government Organizations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="average_throughput_govt" class="form-control input-style"value="{{ $page3_data->average_throughput_govt}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Schools and Colleges: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="average_throughput_school" class="form-control input-style"value="{{ $page3_data->average_throughput_school}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others(Museums, Libraries, Hospitals/Medical Colleges, Industry, etc): </label>
                                <div class="col-sm-4">
                                    <input type="text" name="average_throughput_other" class="form-control input-style"value="{{ $page3_data->average_throughput_other}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                         @endforeach


                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-2.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
