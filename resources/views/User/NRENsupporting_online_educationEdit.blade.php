
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page15.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif

                         @foreach($page15 as $page15_data)

                        <div class="cards__heading">
                            <h3>Impact of COVID-19: NRENs' Response through supporting Online Education<span></span></h3>
                        </div>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">138) Which Video Collaboration Software are being used by the Universities/Higher Education Institutes/Research Institutes</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">

                                @php
                                $days138 = array("Cisco Webex","Zoom", "Microsoft Team","Google Meet","eduMEET","Jitsi Meet","BigBlueButton");
                                $Video_Collaboration_Software_data = json_decode($page15_data->Video_Collaboration_Software);

                                 if($Video_Collaboration_Software_data === null){
                                    $Video_Collaboration_Software_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($days138 as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('Video_Collaboration_Software[]', $value, in_array($value, $Video_Collaboration_Software_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">139) Month-wise Number of Virtual Classes/Meetings conducted by the Member Institutes under NREN
(No need to answer if NREN didn't give any support to the Member Institutes)</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Jan, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NumberofVirtual_Classes_jan_2020" class="form-control input-style" value="{{ $page15_data->NumberofVirtual_Classes_jan_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Feb, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NumberofVirtual_Classes_feb_2020" class="form-control input-style"value="{{ $page15_data->NumberofVirtual_Classes_feb_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">March, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NumberofVirtual_Classes_march_2020" class="form-control input-style"value="{{ $page15_data->NumberofVirtual_Classes_march_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">April, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NumberofVirtual_Classes_april_2020" class="form-control input-style"value="{{ $page15_data->NumberofVirtual_Classes_april_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">May, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NumberofVirtual_Classes_may_2020" class="form-control input-style"value="{{ $page15_data->NumberofVirtual_Classes_may_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">June, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NumberofVirtual_Classes_june_2020" class="form-control input-style"value="{{ $page15_data->NumberofVirtual_Classes_june_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">July, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NumberofVirtual_Classes_july_2020" class="form-control input-style"value="{{ $page15_data->NumberofVirtual_Classes_july_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">August, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NumberofVirtual_Classes_august_2020" class="form-control input-style"value="{{ $page15_data->NumberofVirtual_Classes_august_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">September, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NumberofVirtual_Classes_september_2020" class="form-control input-style"value="{{ $page15_data->NumberofVirtual_Classes_september_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">October, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NumberofVirtual_Classes_october_2020" class="form-control input-style"value="{{ $page15_data->NumberofVirtual_Classes_october_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">URL for the Usage (if any) with Credentials (if required):</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NumberofVirtual_Classes_url" class="form-control input-style"value="{{ $page15_data->NumberofVirtual_Classes_url}}">
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">140) Month-wise Total Duration (in Minutes) of Virtual Classes/Meetings conducted by the Member Institues under NREN (No need to answer if NREN didn't give any support to the Member Institutes)</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Jan, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Duration_Virtual_Classes_jan_2020" class="form-control input-style"value="{{ $page15_data->Duration_Virtual_Classes_jan_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Feb, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Duration_Virtual_Classes_feb_2020" class="form-control input-style"value="{{ $page15_data->Duration_Virtual_Classes_feb_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">March, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Duration_Virtual_Classes_march_2020" class="form-control input-style"value="{{ $page15_data->Duration_Virtual_Classes_march_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">April, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Duration_Virtual_Classes_april_2020" class="form-control input-style"value="{{ $page15_data->Duration_Virtual_Classes_april_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">May, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Duration_Virtual_Classes_may_2020" class="form-control input-style"value="{{ $page15_data->Duration_Virtual_Classes_may_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">June, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Duration_Virtual_Classes_june_2020" class="form-control input-style"value="{{ $page15_data->Duration_Virtual_Classes_june_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">July, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Duration_Virtual_Classes_july_2020" class="form-control input-style"value="{{ $page15_data->Duration_Virtual_Classes_july_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">August, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Duration_Virtual_Classes_august_2020" class="form-control input-style"value="{{ $page15_data->Duration_Virtual_Classes_august_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">September, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Duration_Virtual_Classes_september_2020" class="form-control input-style"value="{{ $page15_data->Duration_Virtual_Classes_september_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">October, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Duration_Virtual_Classes_october_2020" class="form-control input-style"value="{{ $page15_data->Duration_Virtual_Classes_october_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">URL for the Usage (if any) with Credentials (if required):</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Duration_Virtual_Classes_URL" class="form-control input-style"value="{{ $page15_data->Duration_Virtual_Classes_URL}}">
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">141) Month-wise Total Participants in Virtual Classes/Meetings conducted by Member Institutes under NREN
(No need to answer if NREN didn't give any support to the Member Institutes)</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Jan, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Total_Participants_VirtualClasses_jan_2020" class="form-control input-style"value="{{ $page15_data->Total_Participants_VirtualClasses_jan_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Feb, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Total_Participants_VirtualClasses_feb_2020" class="form-control input-style"value="{{ $page15_data->Total_Participants_VirtualClasses_feb_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">March, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Total_Participants_VirtualClasses_march_2020" class="form-control input-style"value="{{ $page15_data->Total_Participants_VirtualClasses_march_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">April, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Total_Participants_VirtualClasses_april_2020" class="form-control input-style"value="{{ $page15_data->Total_Participants_VirtualClasses_april_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">May, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Total_Participants_VirtualClasses_may_2020" class="form-control input-style"value="{{ $page15_data->Total_Participants_VirtualClasses_may_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">June, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Total_Participants_VirtualClasses_june_2020" class="form-control input-style"value="{{ $page15_data->Total_Participants_VirtualClasses_june_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">July, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Total_Participants_VirtualClasses_july_2020" class="form-control input-style"value="{{ $page15_data->Total_Participants_VirtualClasses_july_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">August, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Total_Participants_VirtualClasses_august_2020" class="form-control input-style"value="{{ $page15_data->Total_Participants_VirtualClasses_august_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">September, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Total_Participants_VirtualClasses_september_2020" class="form-control input-style"value="{{ $page15_data->Total_Participants_VirtualClasses_september_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">October, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Total_Participants_VirtualClasses_october_2020" class="form-control input-style"value="{{ $page15_data->Total_Participants_VirtualClasses_october_2020}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">URL for the Usage (if any) with Credentials (if required):</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Total_Participants_VirtualClasses_URL" class="form-control input-style"value="{{ $page15_data->Total_Participants_VirtualClasses_URL}}">
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">142) Video Collaboration Software run on:</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Video_Collaboration_Software_run"
                                            value="Internet Cloud"<?php if ($page15_data->Video_Collaboration_Software_run == 'Internet Cloud') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Internet Cloud
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Video_Collaboration_Software_run"
                                            value="Local Data Center"<?php if ($page15_data->Video_Collaboration_Software_run == 'Local Data Center') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                           Local Data Center
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-8">
                                    <div class="form-group ">
                                    <label class="input__label">143) Any increase in Domestic Internet Traffic (with IXPs)?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Domestic_Internet_Traffic"
                                            value="Yes"<?php if ($page15_data->Domestic_Internet_Traffic == 'Yes') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Domestic_Internet_Traffic"
                                            value="No"<?php if ($page15_data->Domestic_Internet_Traffic == 'No') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>


                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">144) Domestic Internet Traffic Status:</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Maximum IX (Internet eXchange) Traffic, January-2020, Mbps:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="DIT_maximum_IX_january" class="form-control input-style"value="{{ $page15_data->DIT_maximum_IX_january}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Maximum IX (Internet eXchange) Traffic, June-2020, Mbps:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="DIT_maximum_IX_june" class="form-control input-style"value="{{ $page15_data->DIT_maximum_IX_june}}">
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">145) Number of Total Available Software [Video Collaboration] Licenses:</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-4">
                                    <input type="text" name="Available_Software" class="form-control input-style"value="{{ $page15_data->Available_Software}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">146) Number of Maximum Software Licenses [Video Collaboration] used:</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-4">
                                    <input type="text" name="Maximum_Software" class="form-control input-style"value="{{ $page15_data->Maximum_Software}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-8">
                                    <div class="form-group ">
                                    <label class="input__label">147) Is the quantity of number of available Licenses [Video Collaboration] enough for you?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="quantity_available_Licenses"
                                            value="Yes"<?php if ($page15_data->quantity_available_Licenses == 'Yes') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="quantity_available_Licenses"
                                            value="No"<?php if ($page15_data->quantity_available_Licenses == 'No') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>


                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">148) Clients being Served through Video Collaboration Applications:</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Number of Institutes:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Clients_VCA_institutes" class="form-control input-style"value="{{ $page15_data->Clients_VCA_institutes}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Number of Faculty Members:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Clients_VCA_faculty" class="form-control input-style"value="{{ $page15_data->Clients_VCA_faculty}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Number of Students:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Clients_VCA_student" class="form-control input-style"value="{{ $page15_data->Clients_VCA_student}}">
                                </div>
                            </div>
                        </fieldset>



                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">149) Challenges faced by students [Check all that Apply]</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">

                                @php
                                $days149 = array("Last Mile Network Quality","Bandwidth Cost", "Availability of End-user Devices","Nonchalance about the mode of delivery","Confidence in the Assessment Process","Quality of Delivery","Quality of Content");
                                $Challenges_faced_by_students_data = json_decode($page15_data->Challenges_faced_by_students);

                                 if($Challenges_faced_by_students_data === null){
                                    $Challenges_faced_by_students_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($days149 as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('Challenges_faced_by_students[]', $value, in_array($value, $Challenges_faced_by_students_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">150) Challenges faced by faculty members/universities [Check all that Apply]</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">

                                @php
                                $days150 = array("Absence of Policy for Online Education","Lack of Training", "Last Mile Network Quality","Bandwidth Cost","Availability of End-user Devices","Nonchalance about the mode of delivery","Confidence in the Assessment Process","Lack of LMS Software","Performing Lab/Hands-on Activities");
                                $Challenges_faced_by_faculty_data = json_decode($page15_data->Challenges_faced_by_faculty);

                                 if($Challenges_faced_by_faculty_data === null){
                                    $Challenges_faced_by_faculty_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($days150 as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('Challenges_faced_by_faculty[]', $value, in_array($value, $Challenges_faced_by_faculty_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">151) Challenges faced by NRENs [Check all that Apply]</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">

                                @php
                                $days151 = array("Limitations of Application Licenses","Lack of Hardware (Application running on own Data Center)", "High Bandwidth Cost","Confidence in Assessment Process","Lack of LMS Software","Providing 24x7 support");
                                $Challenges_faced_by_nren_data = json_decode($page15_data->Challenges_faced_by_nren);

                                 if($Challenges_faced_by_nren_data === null){
                                    $Challenges_faced_by_nren_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($days151 as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('Challenges_faced_by_nren[]', $value, in_array($value, $Challenges_faced_by_nren_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">152) Steps Taken for Mitigation of given Challenges</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Limitations of Video Collaboration Application Licenses:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="limitation_VCA" class="form-control input-style"value="{{ $page15_data->limitation_VCA}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Lack of Hardware:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="lack_of_hardware" class="form-control input-style"value="{{ $page15_data->lack_of_hardware}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">High Bandwidth Cost:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="high_bandwidth_cost" class="form-control input-style"value="{{ $page15_data->high_bandwidth_cost}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Confidence in Assessment Process:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="confidence_assessment" class="form-control input-style"value="{{ $page15_data->confidence_assessment}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Lack of LMS Software:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="lack_lms_software" class="form-control input-style"value="{{ $page15_data->lack_lms_software}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">24x7 Support</label>
                                <div class="col-sm-4">
                                    <input type="text" name="support_24" class="form-control input-style"value="{{ $page15_data->support_24}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Any other step</label>
                                <div class="col-sm-4">
                                    <input type="text" name="other_step" class="form-control input-style"value="{{ $page15_data->other_step}}">
                                </div>
                            </div>
                        </fieldset>

                        @endforeach



                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary align-self-center mr-4" href="{{route('page-14.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
