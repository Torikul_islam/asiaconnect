
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page9.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif

                    @foreach($page9 as $page9_data)

                        <div class="cards__heading">
                            <h3>Identity and Trust Service<span></span></h3>
                        </div>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">101) Status of Identity and Trust Services in your country:</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">

                                @php
                                $days101 = array("National Federation only","Federation of Federations (eduGAIN)", "Under Test","Under Plan","Not Planned yet");
                                $Identity_and_Trust_Services_data = json_decode($page9_data->Identity_and_Trust_Services);

                                 if($Identity_and_Trust_Services_data === null){
                                    $Identity_and_Trust_Services_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($days101 as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('Identity_and_Trust_Services[]', $value, in_array($value, $Identity_and_Trust_Services_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">102) Which services are being provided/planned using Identity Federation? (Check all that apply)</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Video Collaboration </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Video_Collaboration"
                                            value="Planned"<?php if ($page9_data->Video_Collaboration == 'Planned') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Planned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Video_Collaboration"
                                            value="Provided"<?php if ($page9_data->Video_Collaboration == 'Provided') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Provided
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Digital Library</label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Digital_Library"
                                            value="Planned"<?php if ($page9_data->Digital_Library == 'Planned') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Planned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Digital_Library"
                                            value="Provided"<?php if ($page9_data->Digital_Library == 'Provided') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Provided
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Trusted Digital Certificate</label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Trusted_Digital_Certificate"
                                            value="Planned"<?php if ($page9_data->Trusted_Digital_Certificate == 'Planned') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Planned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Trusted_Digital_Certificate"
                                            value="Provided"<?php if ($page9_data->Trusted_Digital_Certificate == 'Provided') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Provided
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Big File Sender</label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Big_File_Sender"
                                            value="Planned"<?php if ($page9_data->Big_File_Sender == 'Planned') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Planned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Big_File_Sender"
                                            value="Provided"<?php if ($page9_data->Big_File_Sender == 'Provided') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Provided
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Cloud Storage</label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Cloud_Storage"
                                            value="Planned"<?php if ($page9_data->Cloud_Storage == 'Provided') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Planned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Cloud_Storage"
                                            value="Provided"<?php if ($page9_data->Cloud_Storage == 'Provided') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Provided
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Desktop/Laptop Backup Services</label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Laptop_Backup_Services"
                                            value="Planned"<?php if ($page9_data->Laptop_Backup_Services == 'Planned') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Planned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Laptop_Backup_Services"
                                            value="Provided"<?php if ($page9_data->Laptop_Backup_Services == 'Provided') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Provided
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Research Repository</label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Research_Repository"
                                            value="Planned"<?php if ($page9_data->Research_Repository == 'Planned') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Planned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Research_Repository"
                                            value="Provided"<?php if ($page9_data->Research_Repository == 'Provided') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Provided
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">103) What is the architecture of Federation deployed by your organization? (Check all that apply)</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">

                                @php
                                $days103 = array("Mesh","Hub and Spoke", "Centralized","Hybrid");
                                $Federation_architecture_data = json_decode($page9_data->Federation_architecture);

                                 if($Federation_architecture_data === null){
                                    $Federation_architecture_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($days103 as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('Federation_architecture[]', $value, in_array($value, $Federation_architecture_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">104) Please provide information regarding your Federation Services?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Number of participating Home Organisations (IdPs)</label>
                                <div class="col-sm-4">
                                    <input type="text" name="information_Federation_Services_IdPs" class="form-control input-style"value="{{ $page9_data->information_Federation_Services_IdPs}}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Number of participating Services Providers (SPs)</label>
                                <div class="col-sm-4">
                                    <input type="text" name="information_Federation_Services_SPs" class="form-control input-style"value="{{ $page9_data->information_Federation_Services_SPs}}">
                                    
                                </div>
                            </div>
                        </fieldset>

                        @endforeach


                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-8.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
