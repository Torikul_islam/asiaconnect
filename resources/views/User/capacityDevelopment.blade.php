
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page11.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif



                        <div class="cards__heading">
                            <h3>Capacity Development<span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">116) Aproximate % level of expertise among NREN Engineers:</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Experts:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NREN_Engineers_expert" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Intermediate:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NREN_Engineers_intermediate" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Beginners:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="NREN_Engineers_beginner" class="form-control input-style">
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">117) Aproximate % level of Expertise of Engineers in Beneficiary Member Institutes?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Experts:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Member_Institutes_expert" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Intermediate:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Member_Institutes_intermediate" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Beginners:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Member_Institutes_beginner" class="form-control input-style">
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-8">
                                    <div class="form-group ">
                                    <label class="input__label">118) Is there any impact of WP2 and WP3 packages on the level of expertise of NREN Engineers?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="wp2andwp3_package"
                                            value="Yes">
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="wp2andwp3_package"
                                            value="No">
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">119) Comments of NRENs on enhancement of efficacy of WP2 and WP3 packages</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-4">
                                    <input type="text" name="efficacy_WP2_WP3_packages" class="form-control input-style">
                                    
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">120) Number of Asi@Connect Financed Training/Workshop in 2019 organized by NREN?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">International:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="AsiaConnect_Financed_Training_International" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">National:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="AsiaConnect_Financed_Training_National" class="form-control input-style">
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">121) Number of own Financed Training/Workshop in 2019 organized by NREN?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">International:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="own_Financed_Training_International" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">National:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="own_Financed_Training_National" class="form-control input-style">
                                </div>
                            </div>
                        </fieldset>



                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-10.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
