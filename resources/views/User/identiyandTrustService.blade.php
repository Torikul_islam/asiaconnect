
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page9.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif



                        <div class="cards__heading">
                            <h3>Identity and Trust Service<span></span></h3>
                        </div>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">101) Status of Identity and Trust Services in your country:</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <div class="form-check">
                                    <input class="form-check-input policies" value="National Federation only" name="Identity_and_Trust_Services[]" type="checkbox" >
                                    <label class="form-check-label">
                                       National Federation only
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Federation of Federations (eduGAIN)" name="Identity_and_Trust_Services[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                       Federation of Federations (eduGAIN)
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Under Test" name="Identity_and_Trust_Services[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                        Under Test
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Under Plan" name="Identity_and_Trust_Services[]"type="checkbox" >
                                    <label class="form-check-label" >
                                         Under Plan
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Not Planned yet" name="Identity_and_Trust_Services[]"type="checkbox" >
                                    <label class="form-check-label" >
                                        Not Planned yet
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">102) Which services are being provided/planned using Identity Federation? (Check all that apply)</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Video Collaboration </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Video_Collaboration"
                                            value="Planned">
                                        <label class="form-check-label" >
                                            Planned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Video_Collaboration"
                                            value="Provided">
                                        <label class="form-check-label" >
                                            Provided
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Digital Library</label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Digital_Library"
                                            value="Planned">
                                        <label class="form-check-label" >
                                            Planned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Digital_Library"
                                            value="Provided">
                                        <label class="form-check-label" >
                                            Provided
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Trusted Digital Certificate</label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Trusted_Digital_Certificate"
                                            value="Planned">
                                        <label class="form-check-label" >
                                            Planned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Trusted_Digital_Certificate"
                                            value="Provided">
                                        <label class="form-check-label" >
                                            Provided
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Big File Sender</label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Big_File_Sender"
                                            value="Planned">
                                        <label class="form-check-label" >
                                            Planned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Big_File_Sender"
                                            value="Provided">
                                        <label class="form-check-label" >
                                            Provided
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Cloud Storage</label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Cloud_Storage"
                                            value="Planned">
                                        <label class="form-check-label" >
                                            Planned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Cloud_Storage"
                                            value="Provided">
                                        <label class="form-check-label" >
                                            Provided
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Desktop/Laptop Backup Services</label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Laptop_Backup_Services"
                                            value="Planned">
                                        <label class="form-check-label" >
                                            Planned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Laptop_Backup_Services"
                                            value="Provided">
                                        <label class="form-check-label" >
                                            Provided
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Research Repository</label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Research_Repository"
                                            value="Planned">
                                        <label class="form-check-label" >
                                            Planned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Research_Repository"
                                            value="Provided">
                                        <label class="form-check-label" >
                                            Provided
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">103) What is the architecture of Federation deployed by your organization? (Check all that apply)</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Mesh" name="Federation_architecture[]" type="checkbox" >
                                    <label class="form-check-label">
                                       Mesh
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Hub and Spoke" name="Federation_architecture[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                       Hub and Spoke
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Centralized" name="Federation_architecture[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                        Centralized
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Hybrid" name="Federation_architecture[]"type="checkbox" >
                                    <label class="form-check-label" >
                                         Hybrid
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">104) Please provide information regarding your Federation Services?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Number of participating Home Organisations (IdPs)</label>
                                <div class="col-sm-4">
                                    <input type="text" name="information_Federation_Services_IdPs" class="form-control input-style">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Number of participating Services Providers (SPs)</label>
                                <div class="col-sm-4">
                                    <input type="text" name="information_Federation_Services_SPs" class="form-control input-style">
                                    
                                </div>
                            </div>
                        </fieldset>

                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-8.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
