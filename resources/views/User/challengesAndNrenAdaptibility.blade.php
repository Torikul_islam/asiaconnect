
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page14.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif



                        <div class="cards__heading">
                            <h3>Impact of COVID-19: Challenges and NRENs' Adaptibility<span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">133) Month-wise Maximum Usage of Internet, Mbps</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Jan, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_jan_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Feb, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_feb_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">March, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_march_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">April, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_april_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">May, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_may_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">June, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_june_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">July, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_july_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">August, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_august_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">September, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_september_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">October, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_october_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">URL for the MRTG (if any):</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Internet_MRTG" class="form-control input-style">
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">134) Month-wise Maximum Usage of Research Traffic (Asi@Connect), Mbps</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Jan, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_jan_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Feb, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_feb_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">March, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_march_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">April, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_april_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">May, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_may_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">June, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_june_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">July, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_july_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">August, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_august_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">September, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_september_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">October, 2020</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_october_2020" class="form-control input-style">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">URL for the MRTG (if any):</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Research_Traffic_MRTG" class="form-control input-style">
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">135) Impact on Revenue as a fallout of COVID:</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Impact_Revenue"
                                            value="Highly Increased">
                                        <label class="form-check-label" >
                                            Highly Increased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Impact_Revenue"
                                            value="Increased">
                                        <label class="form-check-label">
                                            Increased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Campus"
                                            value="Same as Before">
                                        <label class="form-check-label">
                                            Same as Before
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Impact_Revenue"
                                            value="Decreased">
                                        <label class="form-check-label">
                                            Decreased
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Impact_Revenue"
                                            value="Highly Decreased">
                                        <label class="form-check-label">
                                           Highly Decreased
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">136) Major Threat due to pandemic</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Reduction in Revenue" name="Major_Threat[]" type="checkbox" >
                                    <label class="form-check-label">
                                      Reduction in Revenue
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Unemployment" name="Major_Threat[]"type="checkbox">
                                    <label class="form-check-label" >
                                       Unemployment
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Network Security" name="Major_Threat[]"type="checkbox">
                                    <label class="form-check-label">
                                        Network Security
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Hardware overrun" name="Major_Threat[]"type="checkbox" >
                                    <label class="form-check-label" >
                                         Hardware overrun
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Shortage of Software Licences" name="Major_Threat[]"type="checkbox" >
                                    <label class="form-check-label" >
                                        Shortage of Software Licences
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">137) Big Opportunities</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Collaboration among NRENs" name="Big_Opportunities[]" type="checkbox" >
                                    <label class="form-check-label">
                                      Collaboration among NRENs
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Supporting Online Education" name="Big_Opportunities[]"type="checkbox"  >
                                    <label class="form-check-label">
                                       Supporting Online Education
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Supporting Tele-Medicine" name="Big_Opportunities[]"type="checkbox"  >
                                    <label class="form-check-label">
                                        Supporting Tele-Medicine
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Providing Computing/Virtualization Support" name="Big_Opportunities[]"type="checkbox" >
                                    <label class="form-check-label" >
                                         Providing Computing/Virtualization Support
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>




                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-13.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
