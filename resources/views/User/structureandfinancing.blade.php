
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page2.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif



                        <div class="cards__heading">
                            <h3>NREN Organization Structure and Financing<span></span></h3>
                        </div>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">7. Which Strategical Policies are available in your organization ?</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Usage Policy" name="Strapolicies[]" type="checkbox" >
                                    <label class="form-check-label">
                                       Usage Policy
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Connectivity Policy" name="Strapolicies[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                       Connectivity Policy
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Work from Home Policy" name="Strapolicies[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                        Work from Home Policy
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Environmental Policy" name="Strapolicies[]"type="checkbox" >
                                    <label class="form-check-label" >
                                         Environmental Policy
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Procurement Policy" name="Strapolicies[]"type="checkbox" >
                                    <label class="form-check-label" >
                                         Procurement Policy
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Vehicle Usage Policy" name="Strapolicies[]"type="checkbox" >
                                    <label class="form-check-label" >
                                         Vehicle Usage Policy
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">8. How many employees were working in your organizaton under the following distict categories ??</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Technical: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_technical" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Technical: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_non_technical" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Permanent/Full Time: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_permanent" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Contractual/Part-time: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_contract" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Outsourced/Honorary: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_outsourced" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">9. How many Female employees were working in your organizaton under the following distict categories ??</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Technical: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_female_technical" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Technical: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_fem_non_technical" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Permanent/Full Time: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_fem_permanent" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Contractual/Part-time: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_fem_contract" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Outsourced/Honorary: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_fem_outsourced" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">10. How is your Capital Expenditure Financed (in %) ??</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Own Fund: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="capital_ownfund" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Government Fund: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="capital_government" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Donation: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="capital_donation" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="capital_others" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">11. How is your Operating Expenditure Financed (in %) ??</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Own Fund: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="operating_ownfund" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Government Fund: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="operating_government" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Donation: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="operating_donation" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="operating_others" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">12. How do you distribute the operating Expenditure among the following categories (% of OPex) ??</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Network Operations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="distribute_netoperation" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Network Maintenance: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="distribute_netmaintenance" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Administration: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="distribute_administration" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="distribute_other" class="form-control input-style">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">13. How do you perform your "Network Operations" ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="network_operation"
                                            value="Fully Self" >
                                        <label class="form-check-label" >
                                            Fully Self
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="network_operation"
                                            value="Hybrid">
                                        <label class="form-check-label">
                                            Hybrid
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="network_operation"
                                            value="Fully Outsourced">
                                        <label class="form-check-label">
                                            Fully Outsourced
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">14. How do you perform your "Network Maintenance" ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="network_maintenance"
                                            value="Fully Self" >
                                        <label class="form-check-label" >
                                            Fully Self
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="network_maintenance"
                                            value="Hybrid">
                                        <label class="form-check-label">
                                            Hybrid
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="network_maintenance"
                                            value="Fully Outsourced">
                                        <label class="form-check-label">
                                            Fully Outsourced
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">15. How do you arrange your connectivity [Backbone] "Node-to-Node" [Fiber Optic Infrastructure] ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="backbone_connectivity"
                                            value="Short Term Lease" >
                                        <label class="form-check-label" >
                                            Short Term Lease
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="backbone_connectivity"
                                            value="Long Term Lease">
                                        <label class="form-check-label">
                                            Long Term Lease
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="backbone_connectivity"
                                            value="Self Owned">
                                        <label class="form-check-label">
                                            Self Owned
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="backbone_connectivity"
                                            value="Blended">
                                        <label class="form-check-label">
                                            Blended
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">16. How do you arrange your connectivity [Access] "Node-to-Node" [Fiber Optic Infrastructure] ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="access_connectivity"
                                            value="Short Term Lease" >
                                        <label class="form-check-label" >
                                            Short Term Lease
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="access_connectivity"
                                            value="Long Term Lease">
                                        <label class="form-check-label">
                                            Long Term Lease
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="access_connectivity"
                                            value="Self Owned">
                                        <label class="form-check-label">
                                            Self Owned
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="access_connectivity"
                                            value="Blended">
                                        <label class="form-check-label">
                                            Blended
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">17. How do you maintain your Backbone Fiber ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="backbone_fiber"
                                            value="Short Term Lease" >
                                        <label class="form-check-label" >
                                            Short Term Lease
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="backbone_fiber"
                                            value="Long Term lease">
                                        <label class="form-check-label">
                                            Long Term lease
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="backbone_fiber"
                                            value="Self">
                                        <label class="form-check-label">
                                            Self
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="backbone_fiber"
                                            value="Blended">
                                        <label class="form-check-label">
                                            Blended
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">18. How do you maintain your Access Fiber ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="access_fiber"
                                            value="Short Term Lease" >
                                        <label class="form-check-label" >
                                            Short Term Lease
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="access_fiber"
                                            value="Long Term lease">
                                        <label class="form-check-label">
                                            Long Term lease
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="access_fiber"
                                            value="Self">
                                        <label class="form-check-label">
                                            Self
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="access_fiber"
                                            value="Blended">
                                        <label class="form-check-label">
                                            Blended
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">19. How do you charge the Membership fees?</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Free of Charge" name="membership_fees[]" type="checkbox" >
                                    <label class="form-check-label">
                                       Free of Charge
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Fixed/Flat Charge" name="membership_fees[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                       Fixed/Flat Charge
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="pay per user" name="membership_fees[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                        Charged on "pay per user"
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>
                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">20. How do you charge the "Bandwidth" Service ?</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Free of Charge" name="bandwidth_service[]" type="checkbox" >
                                    <label class="form-check-label">
                                       Free of Charge or Paid by the Government
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Fixed/Flat Charge" name="bandwidth_service[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                       Fixed/Flat Charge
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Included in the Membership Fees" name="bandwidth_service[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                        Included in the Membership Fees
                                    </label>
                                </div>
                                 <div class="form-check">
                                    <input class="form-check-input policies" value="Charged on Usage" name="bandwidth_service[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                        Charged on Usage (per Mbps)
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">21. How do you charge the "Other" Services ?</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-18">
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Free of Charge or Paid by the Government" name="other_service[]" type="checkbox" >
                                    <label class="form-check-label">
                                       Free of Charge or Paid by the Government
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Fixed/Flat Charge" name="other_service[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                       Fixed/Flat Charge
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Included in the Membership Fees" name="other_service[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                        Included in the Membership Fees
                                    </label>
                                </div>
                                 <div class="form-check">
                                    <input class="form-check-input policies" value="Charged on Usage" name="other_service[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                        Charged on Usage (per Mbps)
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>



                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-1.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
