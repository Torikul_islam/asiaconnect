
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page16.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                           <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif



                        <div class="cards__heading">
                            <h3>Impact of COVID-19: NRENs' Response through Supporting Tele-Medicine<span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">153) Steps taken for Supporting Tele-medicine</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-6">
                                    
                                    <textarea name="Supporting_Tele_medicine" class="form-control input-style" rows="5"></textarea>

                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">154) Challenges Faced in Tele-medicine</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-6">

                                    <textarea name="Challenges_Tele_medicine" class="form-control input-style" rows="5"></textarea>

                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">155) Mitigating Steps Taken in Tele-medicine</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-6">

                                    <textarea name="Mitigating_Steps_Tele_medicine" class="form-control input-style" rows="5"></textarea>

                                </div>
                            </div>
                        </fieldset>

                        <div class="cards__heading">
                            <h3>Impact of COVID-19: NRENs' Response through Supporting Computing and Virtualization<span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">156) Steps Taken in supporting Computing and Virtualization</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-6">

                                    <textarea name="step_supporting_CandV" class="form-control input-style" rows="5"></textarea>

                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">157) Challenges Faced in Supporting Computing and Virtualization:</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-6">

                                    <textarea name="challenges_supporting_CandV" class="form-control input-style" rows="5"></textarea>

                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">158) Mitigating Steps Taken in Computing and Virtualization:</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-6">

                                    <textarea name="Mitigating_Steps_CandV" class="form-control input-style" rows="5"></textarea>

                                </div>
                            </div>
                        </fieldset>
                        <div class="cards__heading">
                            <h3>Imapct of COVID-19: NRENs' response through offering Other Services<span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">159) Any other support services or Steps undertaken:</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-6">

                                    <textarea name="anyOther_support_services" class="form-control input-style" rows="5"></textarea>

                                </div>
                            </div>
                        </fieldset>
                        <div class="cards__heading">
                            <h3>Impact of COVID-19: Others<span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">160) What role TEIN*CC did play/could play to help NRENs?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-6">
                                    <textarea name="role_of_teincc" class="form-control input-style" rows="5"></textarea>

                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">161) Any Looming Threat?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-6">
                                    
                                    <textarea name="Looming_Threat" class="form-control input-style" rows="5"></textarea>

                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">162) Any Future Opportunity?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-6">

                                    <textarea name="Future_Opportunity" class="form-control input-style" rows="5"></textarea>

                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">163) How TEIN*CC can help you in availing opportunity and dampening the threats?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-6">

                                    <textarea name="availing_opportunity_dampening" class="form-control input-style" rows="5"></textarea>

                                </div>
                            </div>
                        </fieldset>

                        <div class="cards__heading">
                            <h3>Final Comments (You can make Comment/Comments with regard to survey or something you expected but
didn't find while filling out the survey form)<span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">164) Comment/s on Conducted Survey</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-6">

                                    <textarea name="CommentConductedSurvey" class="form-control input-style" rows="5"></textarea>

                                </div>
                            </div>
                        </fieldset>





                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-15.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
