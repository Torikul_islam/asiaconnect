
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page5.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif

                         @foreach($page5 as $page5_data)

                        <div class="cards__heading">
                            <h3>NRENS' Service Platform<span></span></h3>
                        </div>

                        <fieldset class ="form-group">

                            <div class="form-group">
                                <label class="input__label">54. Which network services are you currently facilitating to REN Community ??</label>
                            </div>
                            <div class="form-group row">

                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">

                                @php
                                $SO_Network = array("IPv4 Connectivity","IPv6 Connectivity", "Network Monitoring","Virtual Circuit/VPN","Multicast","Network Troubleshooting","NetFlow Tool", "VehiclOptical Wavelength", "Quality of Service", "Managed Router Services", "Remote Access VPN Services", "PERT", "SDN","Open Lightpath Exchange","Disaster Recovery Services");

                                $network_service_SO_data = json_decode($page5_data->network_service_SO);
                                 if($network_service_SO_data === null){
                                    $network_service_SO_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($SO_Network as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('network_service_SO[]', $value, in_array($value, $network_service_SO_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>

                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">55. Which security services are you offering ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                @php
                                $SO_SS = array("CERT/CSIRT","DDoS Mitigation", "Network Troubleshooting","Anti-spam Solution","Vulnerability Scanning","Firewall-on-demand", "Intrusion Detection", "Identifier Registry", "Security Auditing", "Web Filtering", "PGP Key Server", "Online Payment");

                                $security_service_SO_data = json_decode($page5_data->security_service_SO);
                                 if($security_service_SO_data === null){
                                    $security_service_SO_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($SO_SS as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('security_service_SO[]', $value, in_array($value, $security_service_SO_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>

                            </div>
                        </div>
                        </fieldset>
                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">56. Which Identity services have you already deployed ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                @php
                                $SO_identity = array("Eduroam","Federated Services", "EduGAIN","GovRoam Services","Hosted Campus AAI");

                                $identity_service_SO_data = json_decode($page5_data->identity_service_SO);
                                if($identity_service_SO_data === null){
                                    $identity_service_SO_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($SO_identity as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('identity_service_SO[]', $value, in_array($value, $identity_service_SO_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">57. Which Collaboration services are you currently promoting ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                @php
                                $SO_colla = array("Mailing Lists","Email Server Hosting", "Journal Access","Research Gateway"," VoIP","Project Collaboration Tools","CMS Services","Database Services","Survey-Polling Tool","Instant Messages","Scheduling Tools","SMS Messaging","Plagiarism","Learning Management Services","ePortfolios","Class Registration Services","Video Conferencing Services","SIS( Students Information Service)");

                                $collaboration_service_SO_data = json_decode($page5_data->collaboration_service_SO);
                                 if($collaboration_service_SO_data === null){
                                    $collaboration_service_SO_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($SO_colla as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('collaboration_service_SO[]', $value, in_array($value, $collaboration_service_SO_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">58. Do you cover any of the following Multimedia Services to your REN Community ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                @php
                                $SO_multimedia = array("Web/Desktop Conferencing","Video Streaming (E-Lesson)", "Event Recording / Streaming","TV / Radio Streaming","Media Post Production","Multicast");

                                $multimedia_service_SO_data = json_decode($page5_data->multimedia_service_SO);
                                 if($multimedia_service_SO_data === null){
                                    $multimedia_service_SO_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($SO_multimedia as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('multimedia_service_SO[]', $value, in_array($value, $multimedia_service_SO_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">59. Do you offer the following Storage and Hosting Services ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                @php
                                $SO_storage = array("DNS Hosting","File Sender", "Virtual Machines","Cloud Storage","Housing/Colocation","Web Hosting","SaaS","Content Delivery","Disaster Recovery","Hot-Standby","Netnews");

                                $storage_service_SO_data = json_decode($page5_data->storage_service_SO);
                                 if($storage_service_SO_data === null){
                                    $storage_service_SO_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($SO_storage as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('storage_service_SO[]', $value, in_array($value, $storage_service_SO_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>
                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">60. Do you have any Professional Services ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                @php
                                $SO_professional = array("Hosting of User Conference","Consultancy/Training", "User Portals","Dissemination of Information","Procurement Services","Software Licenses","Web Development","Software Development","Finance/Admin System");

                                $professional_service_SO_data = json_decode($page5_data->professional_service_SO);
                                 if($professional_service_SO_data === null){
                                    $professional_service_SO_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($SO_professional as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('professional_service_SO[]', $value, in_array($value, $professional_service_SO_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">61) Please name maximum 5 (Five) flagship services which gives you competitive advantage over commercial providers.</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-I:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_1_61" class="form-control input-style" value="{{$page5_data->flagship_1_61}}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-II:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_2_61" class="form-control input-style"value="{{$page5_data->flagship_2_61}}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-III:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_3_61" class="form-control input-style"value="{{$page5_data->flagship_3_61}}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-IV:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_4_61" class="form-control input-style"value="{{$page5_data->flagship_4_61}}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-V:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_5_61" class="form-control input-style"value="{{$page5_data->flagship_5_61}}">
                                    
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">62) Total Beneficiaries of these services (please maintain the serial number in line with previous question)</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-I:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_1_62" class="form-control input-style"value="{{$page5_data->flagship_1_62}}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-II:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_2_62" class="form-control input-style"value="{{$page5_data->flagship_2_62}}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-III:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_3_62" class="form-control input-style"value="{{$page5_data->flagship_3_62}}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-IV:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_4_62" class="form-control input-style"value="{{$page5_data->flagship_4_62}}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-V:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_5_62" class="form-control input-style"value="{{$page5_data->flagship_5_62}}">
                                    
                                </div>
                            </div>
                        </fieldset>
                       @endforeach


                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-4.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
