
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page2.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif

                           @foreach($page2 as $page2_data)

                        <div class="cards__heading">
                            <h3>NREN Organization Structure and Financing<span></span></h3>
                        </div>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">7. Which Strategical Policies are available in your organization ?</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">

                                @php
                                $days = array("Usage Policy","Connectivity Policy", "Work from Home Policy","Environmental Policy","Procurement Policy","Vehicle Usage Policy");
                                $Strapolicies_data = json_decode($page2_data->Strapolicies);

                                 if($Strapolicies_data === null){
                                    $Strapolicies_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($days as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('Strapolicies[]', $value, in_array($value, $Strapolicies_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">8. How many employees were working in your organizaton under the following distict categories ??</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Technical: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_technical" class="form-control input-style" value="{{ $page2_data->emp_technical}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Technical: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_non_technical" class="form-control input-style" value="{{ $page2_data->emp_non_technical}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Permanent/Full Time: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_permanent" class="form-control input-style" value="{{ $page2_data->emp_permanent}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Contractual/Part-time: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_contract" class="form-control input-style" value="{{ $page2_data->emp_contract}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Outsourced/Honorary: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_outsourced" class="form-control input-style" value="{{ $page2_data->emp_outsourced}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">9. How many Female employees were working in your organizaton under the following distict categories ??</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Technical: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_female_technical" class="form-control input-style" value="{{ $page2_data->emp_female_technical}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Non- Technical: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_fem_non_technical" class="form-control input-style" value="{{ $page2_data->emp_fem_non_technical}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Permanent/Full Time: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_fem_permanent" class="form-control input-style" value="{{ $page2_data->emp_fem_permanent}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Contractual/Part-time: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_fem_contract" class="form-control input-style" value="{{ $page2_data->emp_fem_contract}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Outsourced/Honorary: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="emp_fem_outsourced" class="form-control input-style" value="{{ $page2_data->emp_fem_outsourced}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">10. How is your Capital Expenditure Financed (in %) ??</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Own Fund: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="capital_ownfund" class="form-control input-style" value="{{ $page2_data->capital_ownfund}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Government Fund: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="capital_government" class="form-control input-style" value="{{ $page2_data->capital_government}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Donation: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="capital_donation" class="form-control input-style" value="{{ $page2_data->capital_donation}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="capital_others" class="form-control input-style" value="{{ $page2_data->capital_others}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">11. How is your Operating Expenditure Financed (in %) ??</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Own Fund: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="operating_ownfund" class="form-control input-style" value="{{ $page2_data->operating_ownfund}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Government Fund: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="operating_government" class="form-control input-style" value="{{ $page2_data->operating_government}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Donation: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="operating_donation" class="form-control input-style" value="{{ $page2_data->operating_donation}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="operating_others" class="form-control input-style" value="{{ $page2_data->operating_others}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">12. How do you distribute the operating Expenditure among the following categories (% of OPex) ??</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Network Operations: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="distribute_netoperation" class="form-control input-style" value="{{ $page2_data->distribute_netoperation}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Network Maintenance: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="distribute_netmaintenance" class="form-control input-style" value="{{ $page2_data->distribute_netmaintenance}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"> Administration: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="distribute_administration" class="form-control input-style" value="{{ $page2_data->distribute_administration}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Others: </label>
                                <div class="col-sm-4">
                                    <input type="text" name="distribute_other" class="form-control input-style" value="{{ $page2_data->distribute_other}}">
                                    <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">13. How do you perform your "Network Operations" ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="network_operation"
                                            value="Fully Self" <?php if ($page2_data->network_operation == 'Fully Self') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Self
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="network_operation"
                                            value="Hybrid" <?php if ($page2_data->network_operation == 'Hybrid') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Hybrid
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="network_operation"
                                            value="Fully Outsourced" <?php if ($page2_data->network_operation == 'Fully Outsourced') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Fully Outsourced
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">14. How do you perform your "Network Maintenance" ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="network_maintenance"
                                            value="Fully Self" <?php if ($page2_data->network_maintenance == 'Fully Self') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fully Self
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="network_maintenance"
                                            value="Hybrid" <?php if ($page2_data->network_maintenance == 'Hybrid') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Hybrid
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="network_maintenance"
                                            value="Fully Outsourced" <?php if ($page2_data->network_maintenance == 'Fully Outsourced') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Fully Outsourced
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">15. How do you arrange your connectivity [Backbone] "Node-to-Node" [Fiber Optic Infrastructure] ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="backbone_connectivity"
                                            value="Short Term Lease" <?php if ($page2_data->backbone_connectivity == 'Short Term Lease') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Short Term Lease
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="backbone_connectivity"
                                            value="Long Term lease" <?php if ($page2_data->backbone_connectivity == 'Long Term lease') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Long Term Lease
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="backbone_connectivity"
                                            value="Self Owned" <?php if ($page2_data->backbone_connectivity == 'Self Owned') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Self Owned
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="backbone_connectivity"
                                            value="Blended" <?php if ($page2_data->backbone_connectivity == 'Blended') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Blended
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">16. How do you arrange your connectivity [Access] "Node-to-Node" [Fiber Optic Infrastructure] ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="access_connectivity"
                                            value="Short Term Lease" <?php if ($page2_data->access_connectivity == 'Short Term Lease') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Short Term Lease
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="access_connectivity"
                                            value="Long Term Lease"<?php if ($page2_data->access_connectivity == 'Long Term Lease') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Long Term Lease
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="access_connectivity"
                                            value="Self Owned"<?php if ($page2_data->access_connectivity == 'Self Owned') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Self Owned
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="access_connectivity"
                                            value="Blended"<?php if ($page2_data->access_connectivity == 'Blended') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Blended
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">17. How do you maintain your Backbone Fiber ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="backbone_fiber"
                                            value="Short Term Lease" <?php if ($page2_data->backbone_fiber == 'Short Term Lease') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Short Term Lease
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="backbone_fiber"
                                            value="Long Term lease" <?php if ($page2_data->backbone_fiber == 'Long Term lease') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Long Term lease
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="backbone_fiber"
                                            value="Self" <?php if ($page2_data->backbone_fiber == 'Self') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Self
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="backbone_fiber"
                                            value="Blended" <?php if ($page2_data->backbone_fiber == 'Blended') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Blended
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">18. How do you maintain your Access Fiber ?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="access_fiber"
                                            value="Short Term Lease" <?php if ($page2_data->access_fiber == 'Short Term Lease') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Short Term Lease
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="access_fiber"
                                            value="Long Term lease" <?php if ($page2_data->access_fiber == 'Long Term lease') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Long Term Lease
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="access_fiber"
                                            value="Self" <?php if ($page2_data->access_fiber == 'Self') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Self
                                        </label>
                                    </div>
                                     <div class="form-check">
                                        <input class="form-check-input" type="radio" name="access_fiber"
                                            value="Blended" <?php if ($page2_data->access_fiber == 'Blended') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Blended
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">19. How do you charge the Membership fees?</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                @php
                                $days = array("Free of Charge","Fixed/Flat Charge", "pay per user");
                                $membership_fees_data = json_decode($page2_data->membership_fees);

                                 if($membership_fees_data === null){
                                    $membership_fees_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($days as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('membership_fees[]', $value, in_array($value, $membership_fees_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>
                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">20. How do you charge the "Bandwidth" Service ?</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                @php
                                $bds = array("Free of Charge or Paid by the Government","Fixed/Flat Charge", "Included in the Membership Fees","Charged on Usage");
                                $bandwidth_service_data = json_decode($page2_data->bandwidth_service);

                                 if($bandwidth_service_data === null){
                                    $bandwidth_service_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($bds as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('bandwidth_service[]', $value, in_array($value, $bandwidth_service_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">21. How do you charge the "Other" Services ?</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-18">
                                @php
                                $ods = array("Free of Charge or Paid by the Government","Fixed/Flat Charge", "Included in the Membership Fees","Charged on Usage");
                                $other_service_data = json_decode($page2_data->other_service);

                                 if($other_service_data === null){
                                    $other_service_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($ods as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('other_service[]', $value, in_array($value, $other_service_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                          @endforeach


                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-1.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
