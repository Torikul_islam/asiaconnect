@extends('User.layouts.app')

@section('content')
    <!-- main content start -->
    <div class="main-content">

        <!-- content -->
        <div class="container-fluid content-top-gap">


            <!-- forms -->
            <section class="forms">
                <!-- forms 1 -->
                <div class="card card_border py-2 mb-4">
                    <div class="card-body">
                        <form method="POST" action="{{route('page6.store')}}" enctype="multipart/form-data">
                            @csrf


                            <div class="cards__heading">
                                <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                                <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                            </div>


                            @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                            @endif

                            @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif


                            <div class="cards__heading">
                                <h3>Advanced Services<span></span></h3>
                            </div>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input__label">63) Services demanded by users? (Check all that
                                        apply)</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="EduRoam"
                                                   name="service_demand[]" type="checkbox">
                                            <label class="form-check-label">
                                                EduRoam
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="EduGain"
                                                   name="service_demand[]" type="checkbox">
                                            <label class="form-check-label">
                                                EduGain
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="HPC"
                                                   name="service_demand[]" type="checkbox">
                                            <label class="form-check-label">
                                                HPC
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Computing/Storage"
                                                   name="service_demand[]" type="checkbox">
                                            <label class="form-check-label">
                                                Computing/Storage
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="SDN"
                                                   name="service_demand[]" type="checkbox">
                                            <label class="form-check-label">
                                                SDN
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="E-Library"
                                                   name="service_demand[]" type="checkbox">
                                            <label class="form-check-label">
                                                E-Library
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO"
                                                   value="Research Gateway Server" name="service_demand[]"
                                                   type="checkbox">
                                            <label class="form-check-label">
                                                Research Gateway Server
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO"
                                                   value="High Performance VPS" name="service_demand[]" type="checkbox">
                                            <label class="form-check-label">
                                                High Performance VPS
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="IPv6"
                                                   name="service_demand[]" type="checkbox">
                                            <label class="form-check-label">
                                                IPv6
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Security/NFV/SOC"
                                                   name="service_demand[]" type="checkbox">
                                            <label class="form-check-label">
                                                Security/NFV/SOC
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO"
                                                   value="Virtual Labs/Smart Classroom" name="service_demand[]"
                                                   type="checkbox">
                                            <label class="form-check-label">
                                                Virtual Labs/Smart Classroom
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="IoT"
                                                   name="service_demand[]" type="checkbox">
                                            <label class="form-check-label">IoT
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO"
                                                   value="Access to Journal and Research Databases"
                                                   name="service_demand[]" type="checkbox">
                                            <label class="form-check-label">
                                                Access to Journal and Research Databases
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO"
                                                   value="Connectivity to Public Cloud (Azure/AWS)"
                                                   name="service_demand[]" type="checkbox">
                                            <label class="form-check-label">
                                                Connectivity to Public Cloud (Azure/AWS)
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="E-Learning"
                                                   name="service_demand[]" type="checkbox">
                                            <label class="form-check-label">
                                                E-Learning
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Big Data"
                                                   name="service_demand[]" type="checkbox">
                                            <label class="form-check-label">
                                                Big Data
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Blockchain"
                                                   name="service_demand[]" type="checkbox">
                                            <label class="form-check-label">
                                                Blockchain
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Content Filtering"
                                                   name="service_demand[]" type="checkbox">
                                            <label class="form-check-label">
                                                Content Filtering
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Telemedicine"
                                                   name="service_demand[]" type="checkbox">
                                            <label class="form-check-label">
                                                Telemedicine
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Collocation"
                                                   name="service_demand[]" type="checkbox">
                                            <label class="form-check-label">
                                                Collocation
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input__label">64) Services already implemented?</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="EduRoam"
                                                   name="service_implemented[]" type="checkbox">
                                            <label class="form-check-label">
                                                EduRoam
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="EduGain"
                                                   name="service_implemented[]" type="checkbox">
                                            <label class="form-check-label">
                                                EduGain
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="HPC"
                                                   name="service_implemented[]" type="checkbox">
                                            <label class="form-check-label">
                                                HPC
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Computing/Storage"
                                                   name="service_implemented[]" type="checkbox">
                                            <label class="form-check-label">
                                                Computing/Storage
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="SDN"
                                                   name="service_implemented[]" type="checkbox">
                                            <label class="form-check-label">
                                                SDN
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="E-Library"
                                                   name="service_implemented[]" type="checkbox">
                                            <label class="form-check-label">
                                                E-Library
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO"
                                                   value="Research Gateway Server" name="service_implemented[]"
                                                   type="checkbox">
                                            <label class="form-check-label">
                                                Research Gateway Server
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO"
                                                   value="High Performance VPS" name="service_implemented[]"
                                                   type="checkbox">
                                            <label class="form-check-label">
                                                High Performance VPS
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="IPv6"
                                                   name="service_implemented[]" type="checkbox">
                                            <label class="form-check-label">
                                                IPv6
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Security/NFV/SOC"
                                                   name="service_implemented[]" type="checkbox">
                                            <label class="form-check-label">
                                                Security/NFV/SOC
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO"
                                                   value="Virtual Labs/Smart Classroom" name="service_implemented[]"
                                                   type="checkbox">
                                            <label class="form-check-label">
                                                Virtual Labs/Smart Classroom
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="IoT"
                                                   name="service_implemented[]" type="checkbox">
                                            <label class="form-check-label">IoT
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO"
                                                   value="Access to Journal and Research Databases"
                                                   name="service_implemented[]" type="checkbox">
                                            <label class="form-check-label">
                                                Access to Journal and Research Databases
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO"
                                                   value="Connectivity to Public Cloud (Azure/AWS)"
                                                   name="service_implemented[]" type="checkbox">
                                            <label class="form-check-label">
                                                Connectivity to Public Cloud (Azure/AWS)
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="E-Learning"
                                                   name="service_implemented[]" type="checkbox">
                                            <label class="form-check-label">
                                                E-Learning
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Big Data"
                                                   name="service_implemented[]" type="checkbox">
                                            <label class="form-check-label">
                                                Big Data
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Blockchain"
                                                   name="service_implemented[]" type="checkbox">
                                            <label class="form-check-label">
                                                Blockchain
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Content Filtering"
                                                   name="service_implemented[]" type="checkbox">
                                            <label class="form-check-label">
                                                Content Filtering
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Telemedicine"
                                                   name="service_implemented[]" type="checkbox">
                                            <label class="form-check-label">
                                                Telemedicine
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Collocation"
                                                   name="service_implemented[]" type="checkbox">
                                            <label class="form-check-label">
                                                Collocation
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input__label">65) Services under pipeline for implementation?</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="EduRoam"
                                                   name="service_pipeline[]" type="checkbox">
                                            <label class="form-check-label">
                                                EduRoam
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="EduGain"
                                                   name="service_pipeline[]" type="checkbox">
                                            <label class="form-check-label">
                                                EduGain
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="HPC"
                                                   name="service_pipeline[]" type="checkbox">
                                            <label class="form-check-label">
                                                HPC
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Computing/Storage"
                                                   name="service_pipeline[]" type="checkbox">
                                            <label class="form-check-label">
                                                Computing/Storage
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="SDN"
                                                   name="service_pipeline[]" type="checkbox">
                                            <label class="form-check-label">
                                                SDN
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="E-Library"
                                                   name="service_pipeline[]" type="checkbox">
                                            <label class="form-check-label">
                                                E-Library
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO"
                                                   value="Research Gateway Server" name="service_pipeline[]"
                                                   type="checkbox">
                                            <label class="form-check-label">
                                                Research Gateway Server
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO"
                                                   value="High Performance VPS" name="service_pipeline[]"
                                                   type="checkbox">
                                            <label class="form-check-label">
                                                High Performance VPS
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="IPv6"
                                                   name="service_pipeline[]" type="checkbox">
                                            <label class="form-check-label">
                                                IPv6
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Security/NFV/SOC"
                                                   name="service_pipeline[]" type="checkbox">
                                            <label class="form-check-label">
                                                Security/NFV/SOC
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO"
                                                   value="Virtual Labs/Smart Classroom" name="service_pipeline[]"
                                                   type="checkbox">
                                            <label class="form-check-label">
                                                Virtual Labs/Smart Classroom
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="IoT"
                                                   name="service_pipeline[]" type="checkbox">
                                            <label class="form-check-label">IoT
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO"
                                                   value="Access to Journal and Research Databases"
                                                   name="service_pipeline[]" type="checkbox">
                                            <label class="form-check-label">
                                                Access to Journal and Research Databases
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO"
                                                   value="Connectivity to Public Cloud (Azure/AWS)"
                                                   name="service_pipeline[]" type="checkbox">
                                            <label class="form-check-label">
                                                Connectivity to Public Cloud (Azure/AWS)
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="E-Learning"
                                                   name="service_pipeline[]" type="checkbox">
                                            <label class="form-check-label">
                                                E-Learning
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Big Data"
                                                   name="service_pipeline[]" type="checkbox">
                                            <label class="form-check-label">
                                                Big Data
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Blockchain"
                                                   name="service_pipeline[]" type="checkbox">
                                            <label class="form-check-label">
                                                Blockchain
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Content Filtering"
                                                   name="service_pipeline[]" type="checkbox">
                                            <label class="form-check-label">
                                                Content Filtering
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Telemedicine"
                                                   name="service_pipeline[]" type="checkbox">
                                            <label class="form-check-label">
                                                Telemedicine
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input network_service_SO" value="Collocation"
                                                   name="service_pipeline[]" type="checkbox">
                                            <label class="form-check-label">
                                                Collocation
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>


                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input__label">66) Please state the implementation of IPv6 in:</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label"> DNS Server </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="DNS_Server"
                                                   value="Full">
                                            <label class="form-check-label">
                                                Full
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="DNS_Server"
                                                   value="Partial">
                                            <label class="form-check-label">
                                                Partial
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="DNS_Server"
                                                   value="None">
                                            <label class="form-check-label">
                                                None
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label"> Web Server </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="web_Server"
                                                   value="Full">
                                            <label class="form-check-label">
                                                Full
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="web_Server"
                                                   value="Partial">
                                            <label class="form-check-label">
                                                Partial
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="web_Server"
                                                   value="None">
                                            <label class="form-check-label">
                                                None
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label"> Mail Server </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="mail_Server"
                                                   value="Full">
                                            <label class="form-check-label">
                                                Full
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="mail_Server"
                                                   value="Partial">
                                            <label class="form-check-label">
                                                Partial
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="mail_Server"
                                                   value="None">
                                            <label class="form-check-label">
                                                None
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label">DHCP/SLAAC
                                        Server/Router </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="dhcp_Server"
                                                   value="Full">
                                            <label class="form-check-label">
                                                Full
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="dhcp_Server"
                                                   value="Partial">
                                            <label class="form-check-label">
                                                Partial
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="dhcp_Server"
                                                   value="None">
                                            <label class="form-check-label">
                                                None
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label"> Routing System </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="routing_system"
                                                   value="Full">
                                            <label class="form-check-label">
                                                Full
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="routing_system"
                                                   value="Partial">
                                            <label class="form-check-label">
                                                Partial
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="routing_system"
                                                   value="None">
                                            <label class="form-check-label">
                                                None
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label"> Switching System </label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="switching_system"
                                                   value="Full">
                                            <label class="form-check-label">
                                                Full
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="switching_system"
                                                   value="Partial">
                                            <label class="form-check-label">
                                                Partial
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="switching_system"
                                                   value="None">
                                            <label class="form-check-label">
                                                None
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input__label">67) How many IPv6 BGP peers do you have?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label">Upstream (Gateways):</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="ipv6_bgp_upstream" class="form-control input-style">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label">Downstream (Clients):</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="ipv6_bgp_downstream" class="form-control input-style">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label">Domestic IXPs:</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="ipv6_bgp_domestic" class="form-control input-style">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label">CDNs and Others:</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="ipv6_bgp_cdn" class="form-control input-style">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input__label">68) How many prefixes received from Upstream Peers
                                        (Gateways)?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label">IPv4:</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="upstram_peer_ipv4" class="form-control input-style">
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label">IPv6:</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="upstram_peer_ipv6" class="form-control input-style">
                                        
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input__label">69) How many prefixes received from Parallel Peers (CDNs
                                        and IXPs)?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label">IPv4:</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="parallei_peer_ipv4" class="form-control input-style">
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label">IPv6:</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="parallei_peer_ipv6" class="form-control input-style">
                                       
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input__label">70) How many prefixes received from Downstream Peers
                                        (Clients)?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label">IPv4:</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="downstream_peer_ipv4" class="form-control input-style">
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label">IPv6:</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="downstream_peer_ipv6" class="form-control input-style">
                                        
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input__label">71) Do you have IPv6 peering with TEIN upstream
                                                Gateway/s?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="ipv6_tein_upstream"
                                                   value="Yes">
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="ipv6_tein_upstream"
                                                   value="No">
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input__label">72) How many Application Servers are setup in your Cloud
                                        System?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label">IPv4 enabled</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="application_server_ipv4"
                                               class="form-control input-style">
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label">IPv6 enabled</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="application_server_ipv6"
                                               class="form-control input-style">
                                        
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input__label">73) Which type of configuration used in the
                                                network devices having IPv6?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="config_network_ipv6"
                                                   value="Dual Stack">
                                            <label class="form-check-label">
                                                Dual Stack
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="config_network_ipv6"
                                                   value="IPv6 Tunneling">
                                            <label class="form-check-label">
                                                IPv6 Tunneling
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="config_network_ipv6"
                                                   value="Both Dual Stack and Tunneling">
                                            <label class="form-check-label">
                                                Both Dual Stack and Tunneling
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="config_network_ipv6"
                                                   value="IPv6 yet to be implemented">
                                            <label class="form-check-label">
                                                IPv6 yet to be implemented
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input__label">74) How many Public ASN do you [NREN] have?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label"></label>
                                    <div class="col-sm-4">
                                        <input type="text" name="public_asn" class="form-control input-style">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input__label">75) How many IPv4 (/24) Blocks do you have?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label"></label>
                                    <div class="col-sm-4">
                                        <input type="text" name="ipv4_24_blocks" class="form-control input-style">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input__label">76) How many Members under NREN having independent
                                        ASN?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label"></label>
                                    <div class="col-sm-4">
                                        <input type="text" name="independent_ASN" class="form-control input-style">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="form-group">
                                    <label class="input__label">77) How many of the Members having IPv6 Routed
                                        ASN/Prefixes?</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label input__label"></label>
                                    <div class="col-sm-4">
                                        <input type="text" name="IPv6_Routed_ASN" class="form-control input-style">
                                        <small class="form-text text-muted">* Please Provide Numeric Value</small>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="input__label">78) Have you already joined Mutually Agreed
                                                Norms for Routing Security [MANRS]?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="MANRS"
                                                   value="Yes">
                                            <label class="form-check-label">
                                                YES
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="MANRS"
                                                   value="No">
                                            <label class="form-check-label">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>


                            <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-5.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- //forms 1 -->


        </div>
        <!-- //content -->

    </div>
    <!-- main content end-->
@endsection
