
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page7.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif



                        <div class="cards__heading">
                            <h3>Network Infrastructure: Dark Fibre<span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">79) Do you have your own/Long Term Leased Dark Fibre for Domestic Usage?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_domestic_usage"
                                            value="Yes">
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_domestic_usage"
                                            value="No">
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">80) Approximately how long is the length of owned/Long Term Leased Domestic Dark Fibre?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_domestic_long"
                                            value="Less 1000 km">
                                        <label class="form-check-label" >
                                            Less 1000 km
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_domestic_long"
                                            value="1000 to 10000 km">
                                        <label class="form-check-label">
                                            1000 to 10000 km
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_domestic_long"
                                            value="Over 10000 km">
                                        <label class="form-check-label">
                                            Over 10000 km
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_domestic_long"
                                            value="No Dark Fibre">
                                        <label class="form-check-label">
                                            No Dark Fibre
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">81) Do You have your own/Long Term Leased Dark Fiber for Cross-border?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_cross_border"
                                            value="Yes">
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_cross_border"
                                            value="No">
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">82) Approximately how long is the length of owned/Long Term Leased Cross Border Dark Fibre?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_cross_border_long"
                                            value="Less 1000 km">
                                        <label class="form-check-label" >
                                            Less 1000 km
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_cross_border_long"
                                            value="1000 to 10000 km">
                                        <label class="form-check-label">
                                            1000 to 10000 km
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_cross_border_long"
                                            value="Over 10000 km">
                                        <label class="form-check-label">
                                            Over 10000 km
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_cross_border_long"
                                            value="No Dark Fibre">
                                        <label class="form-check-label">
                                            No Dark Fibre
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">83) Have you deployed any Alien Wave technology in Transmission?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Alien_Wave_technology"
                                            value="Yes">
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Alien_Wave_technology"
                                            value="No">
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">84) Which services are being offered under this Alien waves technology?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-4">
                                    <input type="text" name="offer_services_Alien_Wave_technology" class="form-control input-style">
                                    
                                </div>
                            </div>
                        </fieldset>


                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">85) Do you implement IP Trunk for the use of your network’s VoIP framework?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="IP_Trunk"
                                            value="Yes">
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="IP_Trunk"
                                            value="No">
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>


                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">86) What is the aggregate link capacity for IP Trunk?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="aggregate_link_capacity"
                                            value="Fast Ethernet">
                                        <label class="form-check-label" >
                                            Fast Ethernet
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="aggregate_link_capacity"
                                            value="1G">
                                        <label class="form-check-label">
                                            1G
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="aggregate_link_capacity"
                                            value="10G">
                                        <label class="form-check-label">
                                            10G
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="aggregate_link_capacity"
                                            value="100G">
                                        <label class="form-check-label">
                                            100G
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">87) Status of SDN (Software Defined Network) services in your organization?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Pilot services</label>
                                <div class="col-sm-4">
                                    <input type="text" name="sdn_pilot" class="form-control input-style">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Production Services</label>
                                <div class="col-sm-4">
                                    <input type="text" name="sdn_production" class="form-control input-style">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Tested</label>
                                <div class="col-sm-4">
                                    <input type="text" name="sdn_tested" class="form-control input-style">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Planned</label>
                                <div class="col-sm-4">
                                    <input type="text" name="sdn_planned" class="form-control input-style">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Not Planned yet</label>
                                <div class="col-sm-4">
                                    <input type="text" name="sdn_not_planned" class="form-control input-style">
                                    
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">88) What is the state of NFV (Network Function Virtualization) in your Organization?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="NFV"
                                            value="Implemented: Services Incorporated">
                                        <label class="form-check-label" >
                                            Implemented: Services Incorporated
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="NFV"
                                            value="Planned: Services Planned">
                                        <label class="form-check-label">
                                            Planned: Services Planned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="NFV"
                                            value="Not Planned yet">
                                        <label class="form-check-label">
                                            Not Planned yet
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">89) What mode of Server Farm/Application services do you have? </label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Owned" name="Server_Farm[]" type="checkbox" >
                                    <label class="form-check-label">
                                        Owned
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Outsourced" name="Server_Farm[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                       Outsourced
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input policies" value="Not Available" name="Server_Farm[]"type="checkbox"  >
                                    <label class="form-check-label" >
                                        Not Available
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">90) Please select the Protection availability of Application Services in your NREN.</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">TLS </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="TLS"
                                            value="Available">
                                        <label class="form-check-label" >
                                            Available
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="TLS"
                                            value="Not Available">
                                        <label class="form-check-label" >
                                            Not Available
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Email Security like Spam and Fishing protection </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="spam_andfishing"
                                            value="Available">
                                        <label class="form-check-label" >
                                            Available
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="spam_andfishing"
                                            value="Not Available">
                                        <label class="form-check-label" >
                                            Not Available
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">NGN Firewall </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="NGN_Firewall"
                                            value="Available">
                                        <label class="form-check-label" >
                                            Available
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="NGN_Firewall"
                                            value="Not Available">
                                        <label class="form-check-label" >
                                            Not Available
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Sand Boxing </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Sand_Boxing"
                                            value="Available">
                                        <label class="form-check-label" >
                                            Available
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Sand_Boxing"
                                            value="Not Available">
                                        <label class="form-check-label" >
                                            Not Available
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">DDOS Protection </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="DDOS_Protection"
                                            value="Available">
                                        <label class="form-check-label" >
                                            Available
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="DDOS_Protection"
                                            value="Not Available">
                                        <label class="form-check-label" >
                                            Not Available
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">DPI </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="DPI"
                                            value="Available">
                                        <label class="form-check-label" >
                                            Available
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="DPI"
                                            value="Not Available">
                                        <label class="form-check-label">
                                            Not Available
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>


                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-6.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
