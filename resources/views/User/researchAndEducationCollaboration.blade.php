
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page10.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif



                        <div class="cards__heading">
                            <h3>Major Research and Education Collaboration Activities<span></span></h3>
                        </div>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-8">
                                    <div class="form-group ">
                                    <label class="input__label">105) Do you have any Research and Education Collaboration Activities?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="ResEdu_activities"
                                            value="Yes">
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="ResEdu_activities"
                                            value="No">
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">106) Major Research and Education Collaboration Activities</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Title of Research Initiative</label>
                                <div class="col-sm-6">

                                    <textarea name="Title_of_Research" class="form-control input-style"></textarea>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Associated URL [You may skip the following if it is covered in the URL] with credentials [if required]:</label>
                                <div class="col-sm-6">

                                    <textarea name="research_url" class="form-control input-style"></textarea>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Abstract</label>
                                <div class="col-sm-6">
                                    <textarea name="Abstract" class="form-control input-style"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Name and Email Address of the 1st Author</label>
                                <div class="col-sm-6">
                                    <textarea name="name_email_first_author" class="form-control input-style"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Scope</label>
                                <div class="col-sm-6">
                                    <textarea name="scope" class="form-control input-style"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Beneficiaries</label>
                                <div class="col-sm-6">
                                    <textarea name="Beneficiaries" class="form-control input-style"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Financing (Asi@Connect/ NREN/ Others)</label>
                                <div class="col-sm-6">
                                    <textarea name="Financing" class="form-control input-style"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">How NREN could contribute (Financing/ Providing Infrastructure/ Human Networking/consultancy/ Others)</label>
                                <div class="col-sm-6">
                                    <textarea name="NREN_contribute" class="form-control input-style"></textarea>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-8">
                                    <div class="form-group ">
                                    <label class="input__label">107) Do you have any other Research and Education Collaboration Activities?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="ResEdu_activities_other"
                                            value="Yes">
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="ResEdu_activities_other"
                                            value="No">
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">108) Major Research and Education Collaboration Activities</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Title of Research Initiative</label>
                                <div class="col-sm-6">
                                    <textarea name="Title_of_Research_other" class="form-control input-style"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Associated URL [You may skip the following if it is covered in the URL] with credentials [if required]:</label>
                                <div class="col-sm-6">
                                    <textarea name="research_url_other" class="form-control input-style"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Abstract</label>
                                <div class="col-sm-6">
                                    <textarea name="Abstract_other" class="form-control input-style"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Name and Email Address of the 1st Author</label>
                                <div class="col-sm-6">
                                    <textarea name="name_email_first_author_other" class="form-control input-style"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Scope</label>
                                <div class="col-sm-6">
                                    <textarea name="scope_other" class="form-control input-style"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Beneficiaries</label>
                                <div class="col-sm-6">
                                    <textarea name="Beneficiaries_other" class="form-control input-style"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Financing (Asi@Connect/ NREN/ Others)</label>
                                <div class="col-sm-6">
                                    <textarea name="Financing_other" class="form-control input-style"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">How NREN could contribute (Financing/ Providing Infrastructure/ Human Networking/consultancy/ Others)</label>
                                <div class="col-sm-6">
                                    <textarea name="NREN_contribute_other" class="form-control input-style"></textarea>
                                </div>
                            </div>
                        </fieldset>


                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-9.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
