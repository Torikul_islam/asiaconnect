
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page7.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif



                        <div class="cards__heading">
                            <h3>Network Infrastructure: Dark Fibre<span></span></h3>
                        </div>

                        @foreach($page7 as $page7_data)

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">79) Do you have your own/Long Term Leased Dark Fibre for Domestic Usage?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_domestic_usage"
                                            value="Yes"<?php if ($page7_data->dark_fiber_domestic_usage == 'Yes') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_domestic_usage"
                                            value="No"<?php if ($page7_data->dark_fiber_domestic_usage == 'No') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">80) Approximately how long is the length of owned/Long Term Leased Domestic Dark Fibre?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_domestic_long"
                                            value="Less 1000 km"<?php if ($page7_data->dark_fiber_domestic_long == 'Less 1000 km') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Less 1000 km
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_domestic_long"
                                            value="1000 to 10000 km"<?php if ($page7_data->dark_fiber_domestic_long == '1000 to 10000 km') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            1000 to 10000 km
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_domestic_long"
                                            value="Over 10000 km"<?php if ($page7_data->dark_fiber_domestic_long == 'Over 10000 km') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Over 10000 km
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_domestic_long"
                                            value="No Dark Fibre"<?php if ($page7_data->dark_fiber_domestic_long == 'No Dark Fibre') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            No Dark Fibre
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">81) Do You have your own/Long Term Leased Dark Fiber for Cross-border?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_cross_border"
                                            value="Yes"<?php if ($page7_data->dark_fiber_cross_border == 'Yes') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_cross_border"
                                            value="No"<?php if ($page7_data->dark_fiber_cross_border == 'No') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">82) Approximately how long is the length of owned/Long Term Leased Cross Border Dark Fibre?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_cross_border_long"
                                            value="Less 1000 km"<?php if ($page7_data->dark_fiber_cross_border_long == 'Less 1000 km') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Less 1000 km
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_cross_border_long"
                                            value="1000 to 10000 km"<?php if ($page7_data->dark_fiber_cross_border_long == '1000 to 10000 km') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            1000 to 10000 km
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_cross_border_long"
                                            value="Over 10000 km"<?php if ($page7_data->dark_fiber_cross_border_long == 'Over 10000 km') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Over 10000 km
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dark_fiber_cross_border_long"
                                            value="No Dark Fibre"<?php if ($page7_data->dark_fiber_cross_border_long == 'No Dark Fibre') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            No Dark Fibre
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">83) Have you deployed any Alien Wave technology in Transmission?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Alien_Wave_technology"
                                            value="Yes"<?php if ($page7_data->Alien_Wave_technology == 'Yes') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Alien_Wave_technology"
                                            value="No"<?php if ($page7_data->Alien_Wave_technology == 'No') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">84) Which services are being offered under this Alien waves technology?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label"></label>
                                <div class="col-sm-4">
                                    <input type="text" name="offer_services_Alien_Wave_technology" class="form-control input-style" value="{{ $page7_data->offer_services_Alien_Wave_technology}}">
                                    
                                </div>
                            </div>
                        </fieldset>


                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">85) Do you implement IP Trunk for the use of your network’s VoIP framework?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="IP_Trunk"
                                            value="Yes"<?php if ($page7_data->IP_Trunk == 'Yes') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            YES
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="IP_Trunk"
                                            value="No"<?php if ($page7_data->IP_Trunk == 'No') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            NO
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>


                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">86) What is the aggregate link capacity for IP Trunk?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="aggregate_link_capacity"
                                            value="Fast Ethernet"<?php if ($page7_data->aggregate_link_capacity == 'Fast Ethernet') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Fast Ethernet
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="aggregate_link_capacity"
                                            value="1G"<?php if ($page7_data->aggregate_link_capacity == '1G') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            1G
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="aggregate_link_capacity"
                                            value="10G"<?php if ($page7_data->aggregate_link_capacity == '10G') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            10G
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="aggregate_link_capacity"
                                            value="100G"<?php if ($page7_data->aggregate_link_capacity == '100G') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            100G
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">87) Status of SDN (Software Defined Network) services in your organization?</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Pilot services</label>
                                <div class="col-sm-4">
                                    <input type="text" name="sdn_pilot" class="form-control input-style"value="{{ $page7_data->sdn_pilot}}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Production Services</label>
                                <div class="col-sm-4">
                                    <input type="text" name="sdn_production" class="form-control input-style"value="{{ $page7_data->sdn_production}}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Tested</label>
                                <div class="col-sm-4">
                                    <input type="text" name="sdn_tested" class="form-control input-style"value="{{ $page7_data->sdn_tested}}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Planned</label>
                                <div class="col-sm-4">
                                    <input type="text" name="sdn_planned" class="form-control input-style"value="{{ $page7_data->sdn_planned}}">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Not Planned yet</label>
                                <div class="col-sm-4">
                                    <input type="text" name="sdn_not_planned" class="form-control input-style"value="{{ $page7_data->sdn_not_planned}}">
                                    
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="row">
                                <div class = "col-sm-4">
                                    <div class="form-group ">
                                    <label class="input__label">88) What is the state of NFV (Network Function Virtualization) in your Organization?</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="NFV"
                                            value="Implemented: Services Incorporated"<?php if ($page7_data->NFV == 'Implemented: Services Incorporated') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Implemented: Services Incorporated
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="NFV"
                                            value="Planned: Services Planned"<?php if ($page7_data->NFV == 'Planned: Services Planned') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Planned: Services Planned
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="NFV"
                                            value="Not Planned yet"<?php if ($page7_data->NFV == 'Not Planned yet') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Not Planned yet
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">89) What mode of Server Farm/Application services do you have? </label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">

                                @php
                                $days89 = array("Owned","Outsourced", "Not Available");
                                $Server_Farm_data = json_decode($page7_data->Server_Farm);

                                 if($Server_Farm_data === null){
                                    $Server_Farm_data = array('Torikul');
                                }
                                @endphp
                                <div class="form-check">
                                       @foreach($days89 as $value)
                                       <label class="form-check-label">
                                        {!! Form::checkbox('Server_Farm[]', $value, in_array($value, $Server_Farm_data) ? true : false, array('class' => 'form-check-input')) !!}
                                     {{ $value}}</label><br>

                                       @endforeach
                                 </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">90) Please select the Protection availability of Application Services in your NREN.</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">TLS </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="TLS"
                                            value="Available"<?php if ($page7_data->TLS == 'Available') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Available
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="TLS"
                                            value="Not Available"<?php if ($page7_data->TLS == 'Not Available') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Not Available
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Email Security like Spam and Fishing protection </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="spam_andfishing"
                                            value="Available"<?php if ($page7_data->spam_andfishing == 'Available') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Available
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="spam_andfishing"
                                            value="Not Available"<?php if ($page7_data->spam_andfishing == 'Not Available') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Not Available
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">NGN Firewall </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="NGN_Firewall"
                                            value="Available"<?php if ($page7_data->NGN_Firewall == 'Available') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Available
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="NGN_Firewall"
                                            value="Not Available"<?php if ($page7_data->NGN_Firewall == 'Not Available') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Not Available
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Sand Boxing </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Sand_Boxing"
                                            value="Available"<?php if ($page7_data->Sand_Boxing == 'Available') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Available
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="Sand_Boxing"
                                            value="Not Available"<?php if ($page7_data->Sand_Boxing == 'Not Available') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Not Available
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">DDOS Protection </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="DDOS_Protection"
                                            value="Available"<?php if ($page7_data->DDOS_Protection == 'Available') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Available
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="DDOS_Protection"
                                            value="Not Available"<?php if ($page7_data->DDOS_Protection == 'Not Available') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Not Available
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">DPI </label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="DPI"
                                            value="Available"<?php if ($page7_data->DPI == 'Available') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label" >
                                            Available
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="DPI"
                                            value="Not Available"<?php if ($page7_data->DPI == 'Not Available') echo 'checked="checked"'; ?>>
                                        <label class="form-check-label">
                                            Not Available
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        @endforeach


                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-6.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
