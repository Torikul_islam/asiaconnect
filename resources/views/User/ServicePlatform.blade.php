
@extends('User.layouts.app')

@section('content')
<!-- main content start -->
<div class="main-content">

    <!-- content -->
    <div class="container-fluid content-top-gap">


        <!-- forms -->
        <section class="forms">
            <!-- forms 1 -->
            <div class="card card_border py-2 mb-4">
                <div class="card-body">
                    <form method="POST" action="{{route('page5.store')}}" enctype="multipart/form-data">
                         @csrf


                        <div class="cards__heading">
                            <h4 style="text-align:center; font-size: 40px;">e-Compendium for Asi@Connect</h4>

                            <div class="text-center">
                                <a href="https://european-union.europa.eu/index_en" title="EU"><img
                                        src="{{ URL::asset('assets/images/e.png') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="TEIN"><img
                                        src="{{ URL::asset('assets/images/t.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                                <a href="https://www.tein.asia/main/?mc=0" title="Asi@connect"><img
                                        src="{{ URL::asset('assets/images/a.gif') }}" alt="logo-icon"
                                        style="height:80px;"> </a>
                            </div>

                        </div>


                             @if (session('success'))
                                <div class="alert alert-success"><strong>{{ session('success') }}</strong></div>
                             @endif

                             @if (session('error'))
                                <div class="alert alert-danger"><strong>{{ session('error') }}</strong></div>
                            @endif



                        <div class="cards__heading">
                            <h3>NRENS' Service Platform<span></span></h3>
                        </div>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">54. Which network services are you currently facilitating to REN Community ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="IPv4 Connectivity" name="network_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        IPv4 Connectivity
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="IPv6 Connectivity" name="network_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        IPv6 Connectivity
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Network Monitoring" name="network_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Network Monitoring
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Virtual Circuit/VPN" name="network_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Virtual Circuit/VPN
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Multicast" name="network_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Multicast
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Network Troubleshooting" name="network_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Network Troubleshooting
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="NetFlow Tool" name="network_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        NetFlow Tool
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Optical Wavelength" name="network_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Optical Wavelength
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Quality of Service" name="network_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Quality of Service
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Managed Router Services" name="network_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Managed Router Services
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Remote Access VPN Services" name="network_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Remote Access VPN Services
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="PERT" name="network_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Performance Enhancement Response Team (PERT)
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="SDN" name="network_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        SDN
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Open Lightpath Exchange" name="network_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Open Lightpath Exchange
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input network_service_SO" value="Disaster Recovery Services" name="network_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Disaster Recovery Services
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">55. Which security services are you offering ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="CERT/CSIRT" name="security_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        CERT/CSIRT
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="DDoS Mitigation" name="security_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        DDoS Mitigation
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="Network Troubleshooting" name="security_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Network Troubleshooting
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="Anti-spam Solution" name="security_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Anti-spam Solution
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="Vulnerability Scanning" name="security_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Vulnerability Scanning
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="Firewall-on-demand" name="security_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                       Firewall-on-demand
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="Intrusion Detection" name="security_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Intrusion Detection
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="Identifier Registry" name="security_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                       Identifier Registry
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="Security Auditing" name="security_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Security Auditing
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="Web Filtering" name="security_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Web Filtering
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="PGP Key Server" name="security_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        PGP Key Server
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input security_service_SO" value="Online Payment" name="security_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Online Payment
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">56. Which Identity services have you already deployed ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <div class="form-check ">
                                    <input class="form-check-input identity_service_SO" value="Eduroam" name="identity_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Eduroam
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input identity_service_SO" value="Federated Services" name="identity_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Federated Services
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input identity_service_SO" value="EduGAIN" name="identity_service_SO[]" type="checkbox" >
                                    <label class="form-check-label">
                                        EduGAIN
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input identity_service_SO" value="GovRoam Services" name="identity_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        GovRoam Services
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input identity_service_SO" value="Hosted Campus AAI" name="identity_service_SO[]"  type="checkbox" >
                                    <label class="form-check-label" >
                                        Hosted Campus AAI
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">57. Which Collaboration services are you currently promoting ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Mailing Lists" name="collaboration_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                       Mailing Lists
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Email Server Hosting" name="collaboration_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Email Server Hosting
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Journal Access" name="collaboration_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Journal Access
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Video Conferencing Services" name="collaboration_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Video Conferencing Services
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="SIS( Students Information Service)" name="collaboration_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        SIS( Students Information Service)
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Research Gateway" name="collaboration_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                       Research Gateway
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value=" VoIP" name="collaboration_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        VoIP
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Project Collaboration Tools" name="collaboration_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Project Collaboration Tools
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="CMS Services" name="collaboration_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                       CMS Services
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Database Services" name="collaboration_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                       Database Services
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Survey-Polling Tool" name="collaboration_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                       Survey-Polling Tool
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Instant Messages" name="collaboration_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Instant Messages
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Scheduling Tools" name="collaboration_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Scheduling Tools
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="SMS Messaging" name="collaboration_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        SMS Messaging
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Plagiarism" name="collaboration_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Plagiarism
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Learning Management Services" name="collaboration_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Learning Management Services
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="ePortfolios" name="collaboration_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        ePortfolios
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input collaboration_service_SO" value="Class Registration Services" name="collaboration_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                       Class Registration Services
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">58. Do you cover any of the following Multimedia Services to your REN Community ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <div class="form-check">
                                    <input class="form-check-input multimedia_service_SO" value="Web/Desktop Conferencing" name="multimedia_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                       Web/Desktop Conferencing
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input multimedia_service_SO" value="Video Streaming (E-Lesson)" name="multimedia_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                       Video Streaming (E-Lesson)
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input multimedia_service_SO" value="Event Recording / Streaming" name="multimedia_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Event Recording / Streaming
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input multimedia_service_SO" value="TV / Radio Streaming" name="multimedia_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        TV / Radio Streaming
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input multimedia_service_SO" value="Media Post Production" name="multimedia_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Media Post Production
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input multimedia_service_SO" value="Multicast" name="multimedia_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Multicast
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">59. Do you offer the following Storage and Hosting Services ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="DNS Hosting" name="storage_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                      DNS Hosting
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="File Sender" name="storage_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        File Sender
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="Virtual Machines" name="storage_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                       Virtual Machines/IaaS
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="Cloud Storage" name="storage_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                       Cloud Storage
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="Housing/Colocation" name="storage_service_SO[]" type="checkbox" >
                                    <label class="form-check-label">
                                      Housing/Colocation
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="Web Hosting" name="storage_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                       Web Hosting
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="SaaS" name="storage_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                       SaaS
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="Content Delivery" name="storage_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                      Content Delivery
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="Disaster Recovery" name="storage_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                      Disaster Recovery
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="Hot-Standby" name="storage_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                       Hot-Standby
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input storage_service_SO" value="Netnews" name="storage_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Netnews
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>
                        <fieldset class ="form-group">
                            <div class="form-group">
                                <label class="input__label">60. Do you have any Professional Services ??</label>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8">
                                <div class="form-check">
                                    <input class="form-check-input professional_service_SO" value="Hosting of User Conference" name="professional_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                     Hosting of User Conference
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input professional_service_SO" value="Consultancy/Training" name="professional_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                        Consultancy/Training
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input professional_service_SO" value="User Portals" name="professional_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                       User Portals
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input professional_service_SO" value="Dissemination of Information" name="professional_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                       Dissemination of Information (Messages/News Letter)
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input professional_service_SO" value="Procurement Services" name="professional_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                     Procurement Services
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input professional_service_SO" value="Software Licenses" name="professional_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                       Software Licenses
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input professional_service_SO" value="Web Development" name="professional_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                     Web Development
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input professional_service_SO" value="Software Development" name="professional_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                      Software Development
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input professional_service_SO" value="Finance/Admin System" name="professional_service_SO[]" type="checkbox" >
                                    <label class="form-check-label" >
                                     Finance/Admin System
                                    </label>
                                </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">61) Please name maximum 5 (Five) flagship services which gives you competitive advantage over commercial providers.</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-I:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_1_61" class="form-control input-style">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-II:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_2_61" class="form-control input-style">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-III:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_3_61" class="form-control input-style">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-IV:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_4_61" class="form-control input-style">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-V:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_5_61" class="form-control input-style">
                                    
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                            <div class="form-group">
                                <label class="input__label">62) Total Beneficiaries of these services (please maintain the serial number in line with previous question)</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-I:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_1_62" class="form-control input-style">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-II:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_2_62" class="form-control input-style">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-III:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_3_62" class="form-control input-style">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-IV:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_4_62" class="form-control input-style">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label input__label">Flagship Service-V:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="flagship_5_62" class="form-control input-style">
                                    
                                </div>
                            </div>
                        </fieldset>


                        <div class="d-flex justify-content-center">
                                <a class="btn btn-secondary mr-4 align-self-center" href="{{route('page-4.index')}}">Previous Page</a>
                                <button type="submit" class="btn btn-primary">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //forms 1 -->


    </div>
    <!-- //content -->

</div>
<!-- main content end-->
@endsection
