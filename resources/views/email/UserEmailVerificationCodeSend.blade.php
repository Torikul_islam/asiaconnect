<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<p>Dear Sir,</p>
	<p>To verify your registration please submit your verification code on <?php echo $app_name ?> or <a href="{{url('/verifyEmail')}}?email=<?php echo $email_address ?>" target="_blank">Submit Here</a> .</p>
	<p>Your verification code : <b><?php echo $email_verification_code ?></b> </p>

	<p>After your email verification , BdREN will also verify your eligibility to use the system.</p>
	<p>Once you receive final verification email from <?php echo $app_name ?>, You will be able to login on the <a href="<?php echo $app_url; ?>" target="_blank"><?php echo $app_url; ?></a> with your provided email and password. And then you will be able to Access the Maturity Calculator.</p>
	<hr>
	<i>This is an auto generated email from the system. If you face any problem please inform BdREN Programmer</i>
</body>
</html>