<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
 <h2 style="text-align:center;color:blue;">NREN Maturity Calculator Outcome</h2>
 <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;top: 22%;left: 50%;border-top-color: coral;border-style: solid;height:150px;margin-left: 30%;margin-right:30%;">

      <p style="font-size: 20px;text-align:center;">Maturity Score of {{$Organization}}</p>
      <h3 style="text-align:center;">@php echo(round($maturity_level,2)) @endphp /100</h3>
      <p style="font-size: 17px;text-align:center;">Placement in {{$placement_tier}}</p>
 </div>
 <div style="margin-left: 10%;">
      <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;border-top-color: coral;border-style: solid;height:90px;width:150px; Display: Inline-block;margin-top: 30px;">
          <h3 >@php echo(round($score,2)) @endphp /10</h3>
          <p style="font-size: 10px;">Coverage</p>
     </div>
     <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;border-top-color: coral;border-style: solid;height:90px;width:150px;Display: Inline-block;margin-top: 30px;">
          <h3 >{{$lifetimeValue}}/10</h3>
          <p style="font-size: 10px;">Lifetime</p>
     </div>
      <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;border-top-color: coral;border-style: solid;height:90px;width:150px;Display: Inline-block;margin-top: 30px;">
          <h3 >{{$connected_institute_typescore}}/10</h3>
          <p style="font-size: 10px;">Connected Institution Type</p>
     </div>
 </div>
 <br>
 <div style="margin-left: 10%;">
      <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;border-top-color: coral;border-style: solid;height:90px;width:150px;margin-top: 5px;Display: Inline-block;">
          <h3 >{{$connected_institute_coverage_score}}/10</h3>
          <p style="font-size: 10px;">Connected Institution Coverage</p>
     </div>
      <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;border-top-color: coral;border-style: solid;height:90px;width:150px;margin-top: 5px;Display: Inline-block;">
          <h3 >{{$Maximum_BW_U_score}}/10</h3>
          <p style="font-size: 10px;">Maximum BW University</p>
     </div>
      <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;border-top-color: coral;border-style: solid;height:90px;width:150px;margin-top: 5px;Display: Inline-block;">
          <h3 >{{$Maximum_BW_RI_score}}/10</h3>
          <p style="font-size: 10px;">Maximum BW Research Institute</p>
     </div>
 </div>  
 <br>
 <div style="margin-left: 10%;">
      <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;border-top-color: coral;border-style: solid;height:90px;width:150px;margin-top: 5px;Display: Inline-block;">
          <h3 >{{$Commodity_score}}/10</h3>
          <p style="font-size: 10px;">Aggregate Up BW Commodity</p>
     </div>
      <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;border-top-color: coral;border-style: solid;height:90px;width:150px;margin-top: 5px;Display: Inline-block;">
          <h3 >{{$Res_Edu_score}}/10</h3>
          <p style="font-size: 10px;">Aggregate Up BW R&E</p>
     </div>
      <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;border-top-color: coral;border-style: solid;height:90px;width:150px;margin-top: 5px;Display: Inline-block;">
          <h3 >@php echo(round($ownership_score,2)) @endphp/10</h3>
          <p style="font-size: 10px;">Device of Ownership</p>
     </div>
 </div>  
 <br>
 <div style="margin-left: 10%;">
      <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;border-top-color: coral;border-style: solid;height:90px;width:150px;margin-top: 5px;Display: Inline-block;">
          <h3 >@php echo(round($offered_service_final_score,2)) @endphp/10</h3>
          <p style="font-size: 10px;">Service Offer</p>
     </div>
      <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;border-top-color: coral;border-style: solid;height:90px;width:150px;margin-top: 5px;Display: Inline-block;">
          <h3 >{{$financitial_stablity_grading}}/10</h3>
          <p style="font-size: 10px;">Financial Stablity</p>
     </div>
      <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;border-top-color: coral;border-style: solid;height:90px;width:150px;margin-top: 5px;Display: Inline-block;">
          <h3 >{{$human_resouce_technical_final_score}}/10</h3>
          <p style="font-size: 10px;">Human Resouces Tech</p>
     </div>
 </div>  
 <br>
 <div style="margin-left: 10%;">
      <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;border-top-color: coral;border-style: solid;height:90px;width:150px;margin-top: 5px;Display: Inline-block;">
          <h3 >{{$human_resouce_nontechnical_final_score}}/10</h3>
          <p style="font-size: 10px;">Human Resouces Admin</p>
     </div>
      <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;border-top-color: coral;border-style: solid;height:90px;width:150px;margin-top: 5px;Display: Inline-block;">
          <h3 >@php echo(round($omg_aggregate_score,2)) @endphp/10</h3>
          <p style="font-size: 10px;">Org, Management & Governance</p>
     </div>
      <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;border-top-color: coral;border-style: solid;height:90px;width:150px;margin-top: 5px;Display: Inline-block;">
          <h3 >{{$PV_aggregate_score}}/10</h3>
          <p style="font-size: 10px;">Promotion & Visibility</p>
     </div>
 </div>  
 <br>
 <div style="margin-left: 10%;">
      <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;border-top-color: coral;border-style: solid;height:90px;width:150px;Display: Inline-block;">
          <h3 >{{$aggregate_collaborate_score}}/10</h3>
          <p style="font-size: 10px;">Collaboration</p>
     </div>
      <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;border-top-color: coral;border-style: solid;height:90px;width:150px;Display: Inline-block;">
          <h3 >@php echo(round($aggreagte_tech_score,2)) @endphp/10</h3>
          <p style="font-size: 10px;">Human Expert Tech</p>
     </div>
      <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s; padding: 2px 16px;border-top-color: coral;border-style: solid;height:90px;width:150px;Display: Inline-block;">
          <h3 >@php echo(round($aggreagte_admin_score,2)) @endphp/10</h3>
          <p style="font-size: 10px;">Human Expert Admin</p>
     </div>
 </div>  

</body>
</html>