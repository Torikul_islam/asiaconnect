<?php

use App\Http\Controllers\AccessDeniedController;
use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\GuestController;
use App\Http\Controllers\serviceGraph;
use App\Http\Controllers\traffic;
use App\Http\Controllers\ads;
use App\Http\Controllers\research;
use App\Http\Controllers\link;

use App\Http\Controllers\landscape;

use App\Http\Controllers\User\AdvancedServicesController;
use App\Http\Controllers\User\CapacityDevelopmentController;
use App\Http\Controllers\User\ChallengesAndNrenAdaptibilityController;
use App\Http\Controllers\User\CommonController;
use App\Http\Controllers\User\EduroamServicesController;
use App\Http\Controllers\User\HomeController;
use App\Http\Controllers\User\IdentityandTrustServiceController;
use App\Http\Controllers\User\InitiativesofTEINCCController;
use App\Http\Controllers\User\LandscapeAndMarketShareController;
use App\Http\Controllers\User\NetworkInfrastructureDarkFiberController;
use App\Http\Controllers\User\NetworkTrafficandMonitoringController;
use App\Http\Controllers\User\NRENsupportingOnlineEducationController;
use App\Http\Controllers\User\NrenSupportingTeleMedicineController;
use App\Http\Controllers\User\PromotionResearchController;
use App\Http\Controllers\User\ResearchAndEducationCollaborationController;
use App\Http\Controllers\User\ServicePlatformController;
use App\Http\Controllers\User\StructureAndFinancingController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




/*Route::get('/import', function () {
    try{
       
       $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("C:/xampp/htdocs/asiaconnect/data/availablity.csv");

    dd( $spreadsheet);

    }catch (Exception $e){
        dd($e);
    }

    
});*/


Route::get('/', function () {
    return redirect()->route('login');
});


Auth::routes();

Route::group([], __DIR__ . '/web/registration.php');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/access-denied', [AccessDeniedController::class, 'index']);
    Route::get('/change-password', [ChangePasswordController::class, 'index'])->name('change.password');
    Route::post('/change-password', [ChangePasswordController::class, 'store'])->name('change.password.execute');
});

Route::prefix('admin')->middleware('AdminAuth')->group(__DIR__ . '/web/admin.php');

// Route Group for users
Route::group(['prefix' => 'user', 'middleware' => 'UserAuth'], function () {

    Route::get('/page1', [HomeController::class, 'create'])->name('page1');
    Route::post('page1/store', [CommonController::class, 'CoverageStore'])->name('page1.store');
    Route::resource('page-1', HomeController::class);
    Route::get('/page2', [StructureAndFinancingController::class, 'page2'])->name('page2');
    Route::post('/page2/store', [StructureAndFinancingController::class, 'store'])->name('page2.store');
    Route::resource('page-2', StructureAndFinancingController::class);

    Route::get('/page3', [LandscapeAndMarketShareController::class, 'page3'])->name('page3');
    Route::post('/page3/store', [LandscapeAndMarketShareController::class, 'store'])->name('page3.store');
    Route::resource('page-3', LandscapeAndMarketShareController::class);

    Route::get('/page4', [NetworkTrafficandMonitoringController::class, 'page4'])->name('page4');
    Route::post('/page4/store', [NetworkTrafficandMonitoringController::class, 'store'])->name('page4.store');
    Route::resource('page-4', NetworkTrafficandMonitoringController::class);

    Route::get('/page5', [ServicePlatformController::class, 'page5'])->name('page5');
    Route::post('/page5/store', [ServicePlatformController::class, 'store'])->name('page5.store');
    Route::resource('page-5', ServicePlatformController::class);

    Route::get('/page6', [AdvancedServicesController::class, 'page6'])->name('page6');
    Route::post('/page6/store', [AdvancedServicesController::class, 'store'])->name('page6.store');
    Route::resource('page-6', AdvancedServicesController::class);

    Route::get('/page7', [NetworkInfrastructureDarkFiberController::class, 'page7'])->name('page7');
    Route::post('/page7/store', [NetworkInfrastructureDarkFiberController::class, 'store'])->name('page7.store');
    Route::resource('page-7', NetworkInfrastructureDarkFiberController::class);

    Route::get('/page8', [EduroamServicesController::class, 'page8'])->name('page8');
    Route::post('/page8/store', [EduroamServicesController::class, 'store'])->name('page8.store');
    Route::resource('page-8', EduroamServicesController::class);

    Route::get('/page9', [IdentityandTrustServiceController::class, 'page9'])->name('page9');
    Route::post('/page9/store', [IdentityandTrustServiceController::class, 'store'])->name('page9.store');
    Route::resource('page-9', IdentityandTrustServiceController::class);

    Route::get('/page10', [ResearchAndEducationCollaborationController::class, 'page10'])->name('page10');
    Route::post('/page10/store', [ResearchAndEducationCollaborationController::class, 'store'])->name('page10.store');
    Route::resource('page-10', ResearchAndEducationCollaborationController::class);

    Route::get('/page11', [CapacityDevelopmentController::class, 'page11'])->name('page11');
    Route::post('/page11/store', [CapacityDevelopmentController::class, 'store'])->name('page11.store');
    Route::resource('page-11', CapacityDevelopmentController::class);

    Route::get('/page12', [PromotionResearchController::class, 'page12'])->name('page12');
    Route::post('/page12/store', [PromotionResearchController::class, 'store'])->name('page12.store');
    Route::resource('page-12', PromotionResearchController::class);

    Route::get('/page13', [InitiativesofTEINCCController::class, 'page13'])->name('page13');
    Route::post('/page13/store', [InitiativesofTEINCCController::class, 'store'])->name('page13.store');
    Route::resource('page-13', InitiativesofTEINCCController::class);


    Route::get('/page14', [ChallengesAndNrenAdaptibilityController::class, 'page14'])->name('page14');
    Route::post('/page14/store', [ChallengesAndNrenAdaptibilityController::class, 'store'])->name('page14.store');
    Route::resource('page-14', ChallengesAndNrenAdaptibilityController::class);

    Route::get('/page15', [NRENsupportingOnlineEducationController::class, 'page15'])->name('page15');
    Route::post('/page15/store', [NRENsupportingOnlineEducationController::class, 'store'])->name('page15.store');
    Route::resource('page-15', NRENsupportingOnlineEducationController::class);

    Route::get('/page16', [NrenSupportingTeleMedicineController::class, 'page16'])->name('page16');
    Route::post('/page16/store', [NrenSupportingTeleMedicineController::class, 'store'])->name('page16.store');
    Route::resource('page-16', NrenSupportingTeleMedicineController::class);

    Route::get('/page17', [NrenSupportingTeleMedicineController::class, 'page17'])->name('page17');
});

Route::get('guestview', [GuestController::class, 'create'])->name('guestview');
Route::resource('guest', GuestController::class);
Route::match(['get', 'post'], 'landscape/{slug}', [landscape::class, 'index'])->name('landscape');
Route::match(['get', 'post'], 'service/{slug?}', [serviceGraph::class, 'ServiceView'])->name('service.individual');
Route::match(['get', 'post'], 'policy/{slug?}', [serviceGraph::class, 'policyView'])->name('policy');
Route::match(['get', 'post'], 'traffic/{slug}', [traffic::class, 'trafficView'])->name('traffic');
Route::match(['get', 'post'], 'topology/{slug}', [traffic::class, 'trafficView'])->name('topology');
Route::match(['get', 'post'], 'advance.service/{slug}', [ads::class, 'index'])->name('advance.service');
Route::match(['get', 'post'], 'research/{slug}', [research::class, 'index'])->name('research');
Route::match(['get', 'post'], 'link/{slug}', [link::class, 'index'])->name('link');
