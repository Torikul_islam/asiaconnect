<?php


use Illuminate\Support\Facades\Route;

Route::get('/registration_message', [App\Http\Controllers\Auth\EmailVerificationController::class, 'success_register'])->name('registration_message');

Route::get('/verifyEmail', [App\Http\Controllers\Auth\EmailVerificationController::class, 'index'])->name('verifyEmail');

Route::get('/resendCode', [App\Http\Controllers\Auth\EmailVerificationController::class, 'ResendCode'])->name('resendCode');

Route::post('/verifyNow', [App\Http\Controllers\Auth\EmailVerificationController::class, 'verifyNow'])->name('verifyNow');
