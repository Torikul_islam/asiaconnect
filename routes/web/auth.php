<?php 

Route::get('/change-password', 'ChangePasswordController@index');
Route::get('/profile', 'ProfileController@index');
Route::get('/access-denied', 'AccessDeniedController@index');
Route::post('/change-password', 'ChangePasswordController@store')->name('change.password');

