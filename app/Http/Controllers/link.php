<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\linkdata;
use App\Models\allmonth;
use App\Models\link as linkName;
use App\Models\network_trafficand_monitoring;
use App\Models\landscapeAndMarketShare;
use App\Models\User;
use PDF;
use Response;
use Exception;

class link extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug, Request $request)
    {
        $link_id = $request->get('link_id');
        $month_type = $request->get('month_type');
        $traffic_type = $request->get('traffic_type');
        $service = $slug;

        if ($request->isMethod('post')) {

            if ($service == 'AvailablityWith') {

                $link = linkName::get();
                $month = allmonth::get();

                $Data = linkdata::whereIn('link_id', $link_id)
                    ->whereIn('month_id', $month_type)
                    ->get();

                $mainColl = collect();
                foreach ($Data as $key => $value) {

                    $linkName = linkName::find($value->link_id)->LinkName;
                    $monthName = allmonth::find($value->month_id)->MonthName;

                    $mainColl->push([
                        $linkName => $value->{'AvailabilitywithMaintenance'} == "" ? 0 : $value->{'AvailabilitywithMaintenance'},
                        'monthName' => $monthName
                    ]);
                }

                $titletext = 'Comparison of Link Availablity With Maintenance, 2019';

                return view('Guest.utilization', compact('mainColl', 'link', 'month', 'titletext'));
            } elseif ($service == 'AvailablityWithout') {

                $link = linkName::get();
                $month = allmonth::get();

                $Data = linkdata::whereIn('link_id', $link_id)
                    ->whereIn('month_id', $month_type)
                    ->get();

                $mainColl = collect();
                foreach ($Data as $key => $value) {

                    $linkName = linkName::find($value->link_id)->LinkName;
                    $monthName = allmonth::find($value->month_id)->MonthName;

                    $mainColl->push([
                        $linkName => $value->{'AvailabilitywithoutMaintenance'} == "" ? 0 : $value->{'AvailabilitywithoutMaintenance'},
                        'monthName' => $monthName
                    ]);
                }

                $titletext = 'Comparison of Link Availablity Without Maintenance, 2019';

                return view('Guest.utilization', compact('mainColl', 'link', 'month', 'titletext'));
            } elseif ($service == 'Traffic') {

                $link = linkName::get();
                $month = allmonth::get();

                $Data = linkdata::whereIn('link_id', $link_id)
                    ->whereIn('month_id', $month_type)
                    ->get();

                $typeList = [
                    'In' => 'Traffic_in',
                    'Out' => 'Traffic_out'
                ];

                $mainColl = collect();
                foreach ($Data as $key => $value) {

                    $linkName = linkName::find($value->link_id)->LinkName;
                    $monthName = allmonth::find($value->month_id)->MonthName;

                    $mainColl->push([
                        $linkName => $value->{$typeList[$traffic_type]} == "" ? 0 : $value->{$typeList[$traffic_type]},
                        'monthName' => $monthName
                    ]);
                }

                $titletext = 'Comparison of Link Total Traffic (' . $traffic_type . ') in TB, 2019';

                return view('Guest.test', compact('mainColl', 'link', 'month', 'titletext'));
            } elseif ($service == 'Average') {

                $link = linkName::get();
                $month = allmonth::get();

                $Data = linkdata::whereIn('link_id', $link_id)
                    ->whereIn('month_id', $month_type)
                    ->get();

                $typeList = [
                    'In' => 'Average_in',
                    'Out' => 'Average_out'
                ];

                $mainColl = collect();
                foreach ($Data as $key => $value) {

                    $linkName = linkName::find($value->link_id)->LinkName;
                    $monthName = allmonth::find($value->month_id)->MonthName;

                    $mainColl->push([
                        $linkName => $value->{$typeList[$traffic_type]} == "" ? 0 : $value->{$typeList[$traffic_type]},
                        'monthName' => $monthName
                    ]);
                }

                $titletext = 'Comparison of Link Average Traffic (' . $traffic_type . ') in Mbps, 2019';

                return view('Guest.test', compact('mainColl', 'link', 'month', 'titletext'));
            } elseif ($service == 'Maximum') {

                $link = linkName::get();
                $month = allmonth::get();

                $Data = linkdata::whereIn('link_id', $link_id)
                    ->whereIn('month_id', $month_type)
                    ->get();

                $typeList = [
                    'In' => 'max_in',
                    'Out' => 'max_out'
                ];

                $mainColl = collect();
                foreach ($Data as $key => $value) {

                    $linkName = linkName::find($value->link_id)->LinkName;
                    $monthName = allmonth::find($value->month_id)->MonthName;

                    $mainColl->push([
                        $linkName => $value->{$typeList[$traffic_type]} == "" ? 0 : $value->{$typeList[$traffic_type]},
                        'monthName' => $monthName
                    ]);
                }


                $titletext = 'Comparison of Link Maximum Traffic (' . $traffic_type . ') in Mbps, 2019';

                return view('Guest.test', compact('mainColl', 'link', 'month', 'titletext'));
            }
        } else {

            if ($service == 'Traffic' || $service == 'Average' || $service == 'Maximum') {
                $link = linkName::get();
                $month = allmonth::get();
                $month_type = null;
                $link_id = null;
                return view('Guest.test', compact('link', 'link_id', 'month', 'month_type'));
            } else {

                $link = linkName::get();
                $month = allmonth::get();
                $month_type = null;
                $link_id = null;
                return view('Guest.utilization', compact('link', 'link_id', 'month', 'month_type'));
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
