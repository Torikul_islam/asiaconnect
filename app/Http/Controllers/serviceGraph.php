<?php

namespace App\Http\Controllers;

use App\Models\landscapeAndMarketShare;
use App\Models\network_trafficand_monitoring;
use App\Models\service_platform;
use App\Models\structure_and_financing;
use App\Models\User;
use Illuminate\Http\Request;
use PDF;
use Response;

class serviceGraph extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */

    public function policyView($slug, Request $request)
    {

        $users = User::where('role', '!=', 'admin')->get();
        $ren_id = $request->get('ren_id');
        $policies_id = $slug;


        if ($request->isMethod('post')) {

            $userData = structure_and_financing::whereIn('user_id', $ren_id)->get();

            if ($policies_id == 'Policies') {

                $listedData = [
                    'Strapolicies' => array("Usage Policy", "Connectivity Policy", "Work from Home Policy", "Environmental Policy", "Procurement Policy", "Vehicle Usage Policy", "ISO 27000", "IP Policy", "Network Security Policy", "Video Conference Service Policy", "HEMIS Deployment Policy", "Use of Genuine Software Policy")
                ];
                $graph = collect();
                $selectedUsers = collect();
                $barvalue = collect();
                $mainColl = collect();

                foreach ($userData as $key => $value) {

                    $collectedData = json_decode($value->{'Strapolicies'}) ?? [];

                    //$barvalue->push(count($collectedData));
                    $userName = User::find($value->user_id)->name;
                    //$selectedUsers->push($userName);

                    $mainColl->push([
                        'Value' => count($collectedData),
                        'user_name' => $userName
                    ]);

                    for ($i = 0; $i < count($listedData['Strapolicies']); $i++) {
                        $graph->push([
                            'x' => $userName,
                            'y' => $listedData['Strapolicies'][$i],
                            'v' => in_array($listedData['Strapolicies'][$i], $collectedData) ? 1 : 0,
                        ]);
                    }
                }

                $mainColl = $mainColl->sortByDesc('Value');

                $barvalue = $mainColl->map(function ($item) {
                    return $item['Value'];
                })->values();
                $selectedUsers = $mainColl->map(function ($item) {
                    return $item['user_name'];
                })->values();

                $labelData = $listedData['Strapolicies'];

                $title = 'NREN-Wise Availability of Policies';

                return view('Guest.policies', compact('graph', 'users', 'selectedUsers', 'ren_id', 'labelData', 'title', 'barvalue'));
            } elseif ($policies_id == 'Employees') {

                $Employees_type = $request->get('Employees_type');
                $job_type = $request->get('job_type');


                if ($job_type == 'Profession') {
                    $emp_type = [
                        'male' => array("emp_technical", "emp_non_technical"),
                        'female' => array("emp_female_technical", "emp_fem_non_technical")
                    ];
                } else {

                    $emp_type = [
                        'male' => array("emp_permanent", "emp_contract", "emp_outsourced"),
                        'female' => array("emp_fem_permanent", "emp_fem_contract", "emp_fem_outsourced")
                    ];
                }

                $List = [
                    'emp_technical' => 'Technical',
                    'emp_non_technical' => 'Non Technical',
                    'emp_female_technical' => 'Technical',
                    'emp_fem_non_technical' => 'Non Technical',
                    'emp_permanent' => 'Permanent',
                    'emp_contract' => 'Contract',
                    'emp_outsourced' => 'Outsourced',
                    'emp_fem_permanent' => 'Permanent',
                    'emp_fem_contract' => 'Contract',
                    'emp_fem_outsourced' => 'Outsourced'
                ];


                $mainColl = collect();
                foreach ($userData as $key => $value) {

                    $userName = User::find($value->user_id)->name;

                    if (!$mainColl->has(['UserName' => $userName])) {
                        $mainColl->push(['UserName' => $userName]);
                    }

                    for ($i = 0; $i < count($emp_type[$Employees_type]); $i++) {

                        $mainColl = $mainColl->map(function ($item) use ($userName, $emp_type, $Employees_type, $value, $i, $List) {
                            if ($item['UserName'] == $userName) {

                                $item[$List[$emp_type[$Employees_type][$i]]] =  $value->{$emp_type[$Employees_type][$i]} == "" ? 0 : $value->{$emp_type[$Employees_type][$i]};
                            }
                            return $item;
                        });
                    }
                }


                $sortBy  = [];
                for ($i = 0; $i < count($emp_type[$Employees_type]); $i++)
                    $sortBy[] =  [$List[$emp_type[$Employees_type][$i]], 'desc'];

                $mainColl = $mainColl->sortBy($sortBy);


                $titlelist = [
                    'Profession' => 'Distribution of ' . $Employees_type . ' Employees Based On Professions',
                    'Status' => 'Distribution of ' . $Employees_type . ' Employees Based On Job Status'
                ];

                $titletext = $titlelist[$job_type];

                return view('Guest.employee', compact('users', 'mainColl', 'titletext', 'Employees_type', 'job_type'));
            } elseif ($policies_id == 'CapEx') {

                $mainColl = collect();
                foreach ($userData as $key => $value) {
                    $userName = User::find($value->user_id)->name;

                    $mainColl->push([
                        'capital_ownfund' => $value->{'capital_ownfund'} == "" ? 0 : $value->{'capital_ownfund'},
                        'capital_government' => $value->{'capital_government'} == "" ? 0 : $value->{'capital_government'},
                        'capital_donation' => $value->{'capital_donation'} == "" ? 0 : $value->{'capital_donation'},
                        'capital_others' => $value->{'capital_others'} == "" ? 0 : $value->{'capital_others'},
                        'user_name' => $userName
                    ]);
                }


                $mainColl = $mainColl->sortBy([
                    ['capital_government', 'desc'],
                    ['capital_ownfund', 'desc'],
                    ['capital_donation', 'desc'],
                    ['capital_others', 'desc'],
                    ['user_name', 'asc']
                ]);

                $govt_fund = $mainColl->map(function ($item) {
                    return $item['capital_government'];
                })->values();
                $own_fund = $mainColl->map(function ($item) {
                    return $item['capital_ownfund'];
                })->values();
                $donation = $mainColl->map(function ($item) {
                    return $item['capital_donation'];
                })->values();
                $others = $mainColl->map(function ($item) {
                    return $item['capital_others'];
                })->values();
                $selectedUsers = $mainColl->map(function ($item) {
                    return $item['user_name'];
                })->values();


                $titletext = 'CapEX FINANCING AMONG NRENS';
                $labeltitle = ['Government Fund', 'Own Fund', 'Donation', 'Others'];

                return view('Guest.policies', compact('users', 'selectedUsers', 'own_fund', 'govt_fund', 'donation', 'others', 'titletext', 'labeltitle'));
            } elseif ($policies_id == 'OpEx') {

                $mainColl = collect();
                foreach ($userData as $key => $value) {

                    $userName = User::find($value->user_id)->name;

                    $mainColl->push([
                        'operating_ownfund' => $value->{'operating_ownfund'},
                        'operating_government' => $value->{'operating_government'} == "" ? 0 : $value->{'operating_government'},
                        'operating_donation' => $value->{'operating_donation'},
                        'operating_others' => $value->{'operating_others'},
                        'user_name' => $userName
                    ]);
                }

                $mainColl = $mainColl->sortBy([
                    ['operating_government', 'desc'],
                    ['operating_ownfund', 'desc'],
                    ['operating_donation', 'desc'],
                    ['operating_others', 'desc'],
                    ['user_name', 'asc']
                ]);

                $govt_fund = $mainColl->map(function ($item) {
                    return $item['operating_government'];
                })->values();

                $own_fund = $mainColl->map(function ($item) {
                    return $item['operating_ownfund'];
                })->values();
                $donation = $mainColl->map(function ($item) {
                    return $item['operating_donation'];
                })->values();
                $others = $mainColl->map(function ($item) {
                    return $item['operating_others'];
                })->values();
                $selectedUsers = $mainColl->map(function ($item) {
                    return $item['user_name'];
                })->values();


                $titletext = 'OpEX FINANCING AMONG NRENS';
                $labeltitle = ['Government Fund', 'Own Fund', 'Donation', 'Others'];

                return view('Guest.policies', compact('users', 'selectedUsers', 'own_fund', 'govt_fund', 'donation', 'others', 'titletext', 'labeltitle'));
            } elseif ($policies_id == 'Expense') {

                $mainColl = collect();
                foreach ($userData as $key => $value) {

                    $userName = User::find($value->user_id)->name;
                    $mainColl->push([
                        'distribute_netoperation' => $value->{'distribute_netoperation'},
                        'distribute_netmaintenance' => $value->{'distribute_netmaintenance'} == "" ? 0 : $value->{'distribute_netmaintenance'},
                        'distribute_administration' => $value->{'distribute_administration'},
                        'distribute_other' => $value->{'distribute_other'},
                        'user_name' => $userName
                    ]);
                }

                $mainColl = $mainColl->sortBy([
                    ['distribute_netoperation', 'desc'],
                    ['distribute_netmaintenance', 'desc'],
                    ['distribute_administration', 'desc'],
                    ['distribute_other', 'desc'],
                    ['user_name', 'asc']
                ]);

                $govt_fund = $mainColl->map(function ($item) {
                    return $item['distribute_netoperation'];
                })->values();
                $own_fund = $mainColl->map(function ($item) {
                    return $item['distribute_netmaintenance'];
                })->values();
                $donation = $mainColl->map(function ($item) {
                    return $item['distribute_administration'];
                })->values();
                $others = $mainColl->map(function ($item) {
                    return $item['distribute_other'];
                })->values();
                $selectedUsers = $mainColl->map(function ($item) {
                    return $item['user_name'];
                })->values();

                $titletext = 'OPERATING EXPENSE DISTRIBUTION';
                $labeltitle = ['Network Operations', 'Network Maintenance', 'Administration', 'Others'];

                return view('Guest.policies', compact('users', 'selectedUsers', 'own_fund', 'govt_fund', 'donation', 'others', 'titletext', 'labeltitle'));
            } elseif ($policies_id == 'Operations') {
                $selectedUsers = collect();
                $network_operation = collect();
                $network_maintenance = collect();

                $test = collect();
                foreach ($userData as $key => $value) {
                    $userName = User::find($value->user_id)->name;
                    $test->push([
                        'network_operation' => $value->{'network_operation'},
                        'network_maintenance' => $value->{'network_maintenance'},
                        'user_name' => $userName
                    ]);
                }

                foreach ($userData as $key => $value) {

                    $network_operation->push($value->{'network_operation'});
                    $network_maintenance->push($value->{'network_maintenance'});

                    $userName = User::find($value->user_id)->name;
                    $selectedUsers->push($userName);
                }

                $count1 = collect();
                foreach ($network_operation as $item) {
                    if ($count1->has($item)) {
                        $count1->put($item, $count1->get($item) + 1);
                    } else {
                        $count1->put($item, 1);
                    }
                }
                $count1 = $count1->sortKeys();

                $operation_key = $count1->keys();
                $operation_value = $count1->values();


                $count2 = collect();
                foreach ($network_maintenance as $item) {
                    if ($count2->has($item)) {
                        $count2->put($item, $count2->get($item) + 1);
                    } else {
                        $count2->put($item, 1);
                    }
                }
                $count2 = $count2->sortKeys();
                $maintenance_key = $count2->keys();
                $maintenance_value = $count2->values();

                return view('Guest.policies', compact('test', 'users', 'selectedUsers', 'operation_key', 'operation_value', 'maintenance_key', 'maintenance_value'));
            }
        } else {

            if ($policies_id == 'Employees') {

                $users = User::where('role', '!=', 'admin')->get();

                $Employees_type = null;
                $job_type = null;

                return view('Guest.employee', compact('users', 'Employees_type', 'job_type'));
            } else {

                $users = User::where('role', '!=', 'admin')->get();
                $ren_id = null;
                $service_id = null;
                return view('Guest.policies', compact('users', 'ren_id', 'service_id'));
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function ServiceView($slug, Request $request)
    {
        $users = User::where('role', '!=', 'admin')->get();
        $ren_id = $request->get('ren_id');
        $service_id = $slug;

        if ($request->isMethod('post')) {
            if ($service_id == 'Service') {

                $userData = service_platform::whereIn('user_id', $ren_id)->get();
                $collectedData = collect();

                $serviceList = ['network_service_SO', 'security_service_SO', 'identity_service_SO', 'collaboration_service_SO', 'multimedia_service_SO', 'storage_service_SO', 'professional_service_SO'];

                foreach ($userData as $k => $v) {
                    for ($i = 0; $i < count($serviceList); $i++) {

                        $collectedData->push($v->{$serviceList[$i]});
                    }
                }

                $count1 = collect();
                foreach ($collectedData as $mainItem)
                    foreach (json_decode($mainItem) ?? [] as $item) {
                        if ($count1->has($item)) {
                            $count1->put($item, $count1->get($item) + 1);
                        } else {
                            $count1->put($item, 1);
                        }
                    }

                $count1 = $count1->sortDesc();

                $key = $count1->keys();
                $value = $count1->values();
                $datasetlevel = 'Popular Services';


                $title = 'The Most Popular services Offered By NRENS Under Asi@Connect';

                return view('Guest.service_ind', compact('key', 'users', 'value', 'ren_id', 'title', 'datasetlevel'));
            } elseif ($service_id == 'Coverage') {
                $userData = service_platform::whereIn('user_id', $ren_id)->get();
                $mainColl = collect();

                $serviceList = ['network_service_SO', 'security_service_SO', 'identity_service_SO', 'collaboration_service_SO', 'multimedia_service_SO', 'storage_service_SO', 'professional_service_SO'];

                foreach ($userData as $k => $v) {

                    $collectedData = collect();
                    for ($i = 0; $i < count($serviceList); $i++) {

                        $collectedData->push(count(json_decode($v->{$serviceList[$i]}) ?? []));
                    }

                    $userName = User::find($v->user_id)->name;

                    $mainColl->push([
                        'Service Value' => $collectedData->sum() == "" ? 0 : $collectedData->sum(),
                        'user_name' => $userName
                    ]);
                }

                $mainColl = $mainColl->sortBy([
                    ['Service Value', 'desc'],
                    ['user_name', 'asc']
                ]);

                $value = $mainColl->map(function ($item) {
                    return $item['Service Value'];
                })->values();

                $key = $mainColl->map(function ($item) {
                    return $item['user_name'];
                })->values();


                $title = 'Service Coverage of NRENS';
                $datasetlevel = 'Total Services';

                return view('Guest.service_ind', compact('key', 'users', 'value', 'ren_id', 'title', 'datasetlevel'));
            } else {

                $userData = service_platform::whereIn('user_id', $ren_id)->get();


                $listedData = [
                    'network_service_SO' => array("IPv4 Connectivity", "IPv6 Connectivity", "Network Monitoring", "Virtual Circuit/VPN", "Multicast", "Network Troubleshooting", "NetFlow Tool", "VehiclOptical Wavelength", "Quality of Service", "Managed Router Services", "Remote Access VPN Services", "PERT", "SDN", "Open Lightpath Exchange", "Disaster Recovery Services"),
                    'security_service_SO' => array("CERT/CSIRT", "DDoS Mitigation", "Network Troubleshooting", "Anti-spam Solution", "Vulnerability Scanning", "Firewall-on-demand", "Intrusion Detection", "Identifier Registry", "Security Auditing", "Web Filtering", "PGP Key Server", "Online Payment"),
                    'identity_service_SO' => array("Eduroam", "Federated Services", "EduGAIN", "GovRoam Services", "Hosted Campus AAI"),
                    'collaboration_service_SO' => array("Mailing Lists", "Email Server Hosting", "Journal Access", "Research Gateway", " VoIP", "Project Collaboration Tools", "CMS Services", "Database Services", "Survey-Polling Tool", "Instant Messages", "Scheduling Tools", "SMS Messaging", "Plagiarism", "Learning Management Services", "ePortfolios", "Class Registration Services", "Video Conferencing Services", "SIS( Students Information Service)"),
                    'multimedia_service_SO' => array("Web/Desktop Conferencing", "Video Streaming (E-Lesson)", "Event Recording / Streaming", "TV / Radio Streaming", "Media Post Production", "Multicast"),
                    'storage_service_SO' => array("DNS Hosting", "File Sender", "Virtual Machines", "Cloud Storage", "Housing/Colocation", "Web Hosting", "SaaS", "Content Delivery", "Disaster Recovery", "Hot-Standby", "Netnews"),
                    'professional_service_SO' => array("Hosting of User Conference", "Consultancy/Training", "User Portals", "Dissemination of Information", "Procurement Services", "Software Licenses", "Web Development", "Software Development", "Finance/Admin System"),

                ];

                $graph = collect();
                $selectedUsers = collect();

                foreach ($userData as $key => $value) {

                    $collectedData = json_decode($value->{$service_id}) ?? [];

                    $userName = User::find($value->user_id)->name;
                    $selectedUsers->push($userName);

                    for ($i = 0; $i < count($listedData[$service_id]); $i++) {
                        $graph->push([
                            'x' => $userName,
                            'y' => $listedData[$service_id][$i],
                            'v' => in_array($listedData[$service_id][$i], $collectedData) ? 1 : 0,
                        ]);
                    }
                }

                $labelData = $listedData[$service_id];

                $titleList = [
                    'network_service_SO' => 'Network Services Offered by NRENS',
                    'security_service_SO' => 'Security Service Offered by NRENS',
                    'identity_service_SO' => 'Identity Service Offered by NRENS',
                    'collaboration_service_SO' => 'Collaboration Service Offered by NRENS',
                    'multimedia_service_SO' => 'Multimedia Service Offered by NRENS',
                    'storage_service_SO' => 'Storage and Hosting Service Offered by NRENS',
                    'professional_service_SO' => 'Professional Service Offered by NRENS'
                ];

                $title = $titleList[$service_id];

                return view('Guest.service_ind', compact('graph', 'users', 'selectedUsers', 'ren_id', 'labelData', 'service_id', 'title'));
            }
        } else {
            $users = User::where('role', '!=', 'admin')
                ->get();
            $ren_id = null;
            $service_id = null;

            return view('Guest.service_ind', compact('users', 'ren_id', 'service_id'));
        }
    }
}
