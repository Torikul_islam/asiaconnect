<?php

namespace App\Http\Controllers;

use App\Models\advanced_services;
use App\Models\capacityDevelopment;
use App\Models\challengesAndNrenAdaptibility;
use App\Models\contact;
use App\Models\eduroamServices;
use App\Models\identityandTrustService;
use App\Models\initiativesofTEINCC;
use App\Models\landscapeAndMarketShare;
use App\Models\network_trafficand_monitoring;
use App\Models\networkInfrastructureDarkFiber;
use App\Models\nren_supporting_tele_medicine;
use App\Models\NRENsupporting_online_education;
use App\Models\promotion_research;
use App\Models\researchAndEducationCollaboration;
use App\Models\service_platform;
use App\Models\structure_and_financing;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use PDF;
use Response;


class GuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /* $ren_id = $request->get('ren_id');

         return $ren_id;*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $users = User::where('role', '!=', 'admin')
            ->get();
        $ren_id = null;

        return view('Guest.form', compact('users', 'ren_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ren_id = $request->get('ren_id');

        $dataCollect = collect();

        $dataCollect['advanced_services'] = advanced_services::where('user_id', $ren_id)->get();
        $dataCollect['capacityDevelopment'] = capacityDevelopment::where('user_id', $ren_id)->get();
        $dataCollect['challengesAndNrenAdaptibility'] = challengesAndNrenAdaptibility::where('user_id', $ren_id)->get();
        $dataCollect['contact'] = contact::where('user_id', $ren_id)->get();
        $dataCollect['eduroamServices'] = eduroamServices::where('user_id', $ren_id)->get();
        $dataCollect['identityandTrustService'] = identityandTrustService::where('user_id', $ren_id)->get();
        $dataCollect['initiativesofTEINCC'] = initiativesofTEINCC::where('user_id', $ren_id)->get();
        $dataCollect['landscapeAndMarketShare'] = landscapeAndMarketShare::where('user_id', $ren_id)->get();
        $dataCollect['network_trafficand_monitoring'] = network_trafficand_monitoring::where('user_id', $ren_id)->get();
        $dataCollect['networkInfrastructureDarkFiber'] = networkInfrastructureDarkFiber::where('user_id', $ren_id)->get();
        $dataCollect['nren_supporting_tele_medicine'] = nren_supporting_tele_medicine::where('user_id', $ren_id)->get();
        $dataCollect['NRENsupporting_online_education'] = NRENsupporting_online_education::where('user_id', $ren_id)->get();
        $dataCollect['promotion_research'] = promotion_research::where('user_id', $ren_id)->get();
        $dataCollect['researchAndEducationCollaboration'] = researchAndEducationCollaboration::where('user_id', $ren_id)->get();
        $dataCollect['service_platform'] = service_platform::where('user_id', $ren_id)->get();
        $dataCollect['structure_and_financing'] = structure_and_financing::where('user_id', $ren_id)->get();

        $dataCollect['user_name'] = User::where('id', $ren_id)->get();

        $users = User::where('role', '!=', 'admin')
            ->get();


        return view('Guest.form', compact('users', 'dataCollect', 'ren_id'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
