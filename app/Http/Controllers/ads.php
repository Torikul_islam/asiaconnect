<?php

namespace App\Http\Controllers;

use App\Models\advanced_services;
use App\Models\identityandTrustService;
use App\Models\networkInfrastructureDarkFiber;
use App\Models\User;
use Illuminate\Http\Request;
use PDF;
use Response;

class ads extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug, Request $request)
    {
        $users = User::where('role', '!=', 'admin')->get();
        $ren_id = $request->get('ren_id');
        $ads_id = $slug;

        if ($request->isMethod('post')) {

            $userData = advanced_services::whereIn('user_id', $ren_id)->get();


            if ($ads_id == 'service_demand') {

                $tableData = [];
                $rows = array("EduRoam", "EduGain", "HPC", "Computing/Storage", "SDN", "E-Library", "Research Gateway Server", "High Performance VPS", "IPv6", "Virtual Labs/Smart Classroom", "IoT", "Access to Journal and Research Databases", "Connectivity to Public Cloud (Azure/AWS)", "E-Learning", "Big Data", "Blockchain", "Content Filtering", "Telemedicine", "Collocation");
                $columns = ['service_demand', 'service_implemented', 'service_pipeline'];

                foreach ($rows as $row) {
                    foreach ($userData as $key => $value) {
                        $userName = User::find($value->user_id)->name;
                        foreach ($columns as $column) {
                            $collectedData = json_decode($value->{$column}) ?? [];
                            if (in_array($row, $collectedData)) {
                                $tableData[$row][$userName . '-' . $column] = 1;
                            } else {
                                $tableData[$row][$userName . '-' . $column] = 0;
                            }
                        }
                    }
                }

                //for bar graph 
                $mainColl = collect();
                foreach ($userData as $key => $value) {

                    $userName = User::find($value->user_id)->name;
                    $mainColl->push([
                        'service_demand' => count(json_decode($value->{'service_demand'}) ?? []) == "" ? 0 : count(json_decode($value->{'service_demand'}) ?? []),
                        'service_implemented' => count(json_decode($value->{'service_implemented'}) ?? []) == "" ? 0 : count(json_decode($value->{'service_implemented'}) ?? []),
                        'service_pipeline' => count(json_decode($value->{'service_pipeline'}) ?? []) == "" ? 0 : count(json_decode($value->{'service_pipeline'}) ?? []),
                        'user_name' => $userName
                    ]);
                }

                $mainColl = $mainColl->sortBy([
                    ['service_demand', 'desc'],
                    ['service_implemented', 'desc'],
                    ['service_pipeline', 'desc'],
                    ['user_name', 'asc']
                ]);

                $service_demand = $mainColl->map(function ($item) {
                    return $item['service_demand'];
                })->values();

                $service_implemented = $mainColl->map(function ($item) {
                    return $item['service_implemented'];
                })->values();

                $service_pipeline = $mainColl->map(function ($item) {
                    return $item['service_pipeline'];
                })->values();

                $selectedUsers = $mainColl->map(function ($item) {
                    return $item['user_name'];
                })->values();

                $labelData = ['Demand', 'Implemented', 'Plannned'];
                $titletext = "NRENS Effort in Meeting the Demand of the Community";

                return view('Guest.ads', compact('users', 'ren_id', 'tableData', 'rows', 'columns', 'labelData', 'selectedUsers', 'service_demand', 'service_implemented', 'service_pipeline', 'titletext'));
            } elseif ($ads_id == 'IPv6_Peers') {

                $mainColl = collect();

                foreach ($userData as $key => $value) {

                    $userName = User::find($value->user_id)->name;
                    $mainColl->push([
                        'ipv6_bgp_upstream' => $value->{'ipv6_bgp_upstream'} == "" ? 0 : $value->{'ipv6_bgp_upstream'},
                        'ipv6_bgp_downstream' => $value->{'ipv6_bgp_downstream'} == "" ? 0 : $value->{'ipv6_bgp_downstream'},
                        'ipv6_bgp_domestic' => $value->{'ipv6_bgp_domestic'} == "" ? 0 : $value->{'ipv6_bgp_domestic'},
                        'ipv6_bgp_cdn' => $value->{'ipv6_bgp_cdn'} == "" ? 0 : $value->{'ipv6_bgp_cdn'},
                        'total' => intval($value->{'ipv6_bgp_upstream'}) + intval($value->{'ipv6_bgp_downstream'}) + intval($value->{'ipv6_bgp_domestic'}) + intval($value->{'ipv6_bgp_cdn'}),
                        'user_name' => $userName
                    ]);
                }


                $mainColl = $mainColl->sortBy([
                    ['total', 'desc'],
                    ['ipv6_bgp_upstream', 'desc'],
                    ['ipv6_bgp_downstream', 'desc'],
                    ['ipv6_bgp_domestic', 'desc'],
                    ['ipv6_bgp_cdn', 'desc'],
                    ['user_name', 'asc']
                ]);

                $ipv6_bgp_upstream = $mainColl->map(function ($item) {
                    return $item['ipv6_bgp_upstream'];
                })->values();

                $ipv6_bgp_downstream = $mainColl->map(function ($item) {
                    return $item['ipv6_bgp_downstream'];
                })->values();

                $ipv6_bgp_domestic = $mainColl->map(function ($item) {
                    return $item['ipv6_bgp_domestic'];
                })->values();
                $ipv6_bgp_cdn = $mainColl->map(function ($item) {
                    return $item['ipv6_bgp_cdn'];
                })->values();
                $total = $mainColl->map(function ($item) {
                    return $item['total'];
                })->values();

                $selectedUsers = $mainColl->map(function ($item) {
                    return $item['user_name'];
                })->values();

                $labelData = ['Total', 'Upstream', 'Downstream', 'Domestic', 'CDN'];
                $titletext = "NREN-Wise Number of IPV6 Peers";

                return view('Guest.ads', compact('users', 'ren_id', 'labelData', 'selectedUsers', 'total', 'ipv6_bgp_upstream', 'ipv6_bgp_downstream', 'ipv6_bgp_domestic', 'ipv6_bgp_cdn', 'titletext'));
            } elseif ($ads_id == 'Alien_Wave_technology' || $ads_id == 'IP_Trunk' || $ads_id == 'NFV') {


                $userData = networkInfrastructureDarkFiber::whereIn('user_id', $ren_id)->get();
                $collectedData = collect();
                $mainColl = collect();
                foreach ($userData as $key => $value) {
                    $collectedData->push($value->{$ads_id});
                    $userName = User::find($value->user_id)->name;

                    $mainColl->push([
                        'User' => $userName,
                        'data' => $value->{$ads_id}
                    ]);
                }

                $count1 = collect();
                foreach ($collectedData as $item) {
                    if ($count1->has($item)) {
                        $count1->put($item, $count1->get($item) + 1);
                    } else {
                        $count1->put($item, 1);
                    }
                }

                $alien_key = $count1->keys();
                $alien_value = $count1->values();
                if ($ads_id == 'Alien_Wave_technology') {
                    $titletext = "Availbility of Alien Waves";
                    $levelname = 'Alien Waves';
                } elseif ($ads_id == 'IP_Trunk') {
                    $titletext = "Status of IP Trunk";
                    $levelname = 'IP Trunk';
                } else {
                    $titletext = "Status of Implementation of NFV Under Asi@Connect";
                    $levelname = 'NFV';
                }


                return view('Guest.ads', compact('users', 'ren_id', 'alien_key', 'alien_value', 'titletext', 'levelname', 'mainColl'));
            } elseif ($ads_id == 'Protection') {
                $userData = networkInfrastructureDarkFiber::whereIn('user_id', $ren_id)->get();
                $mainColl = collect();
                $pretection = collect();

                $t = 0;
                $e = 0;
                $n = 0;
                $s = 0;
                $DD = 0;
                $DP = 0;

                $protectionlist = array('TLS', 'spam_andfishing', 'NGN_Firewall', 'Sand_Boxing', 'DDOS_Protection', 'DPI');

                foreach ($userData as $key => $value) {

                    $userName = User::find($value->user_id)->name;

                    $t = $value->{'TLS'} == "Available" ? ++$t : $t;
                    $e = $value->{'spam_andfishing'} == "Available" ? ++$e : $e;
                    $n = $value->{'NGN_Firewall'} == "Available" ? ++$n : $n;
                    $s = $value->{'Sand_Boxing'} == "Available" ? ++$s : $s;
                    $DD = $value->{'DDOS_Protection'} == "Available" ? ++$DD : $DD;
                    $DP = $value->{'DPI'} == "Available" ? ++$DP : $DP;

                    for ($i = 0; $i < count($protectionlist); $i++) {
                        $mainColl->push([
                            'x' => $userName,
                            'y' => $protectionlist[$i],
                            'v' => $value->{$protectionlist[$i]} == 'Available' ? 1 : 0,
                        ]);
                    }
                }

                $pretection->push([
                    'Transport Layer Security' => $t,
                    'Email Security' => $e,
                    'NGN Firewall' => $n,
                    'Sand Boxing' => $s,
                    'DDOS Protection' => $DD,
                    'DPI' => $DP,
                ]);
                $pkey = collect($pretection[0])->keys();
                $value = collect($pretection[0])->values();

                $titletext = "Available Protection in Different NRENS";
                $levelname = 'Available Protection';

                return view('Guest.ads', compact('users', 'ren_id', 'pkey', 'value', 'titletext', 'levelname', 'mainColl'));
            } elseif ($ads_id == 'Identity_and_Trust_Services' || $ads_id == 'Federation_architecture') {

                $userData = identityandTrustService::whereIn('user_id', $ren_id)->get();

                $listedData = [
                    'Identity_and_Trust_Services' => array("Under Plan", "Not Planned yet", "National Federation only", "Federation of Federations (eduGAIN)", "Under Test"),
                    'Federation_architecture' => array("Hub and Spoke", "Centralized", "Hybrid", "Shibboleth", "radius", "Under plan", "Mesh")
                ];
                $ID_fed = collect();
                $selectedUsers = collect();
                foreach ($userData as $key => $value) {

                    $collectedData = json_decode($value->{$ads_id}) ?? [];

                    $userName = User::find($value->user_id)->name;
                    $selectedUsers->push($userName);

                    for ($i = 0; $i < count($listedData[$ads_id]); $i++) {
                        $ID_fed->push([
                            'x' => $userName,
                            'y' => $listedData[$ads_id][$i],
                            'v' => in_array($listedData[$ads_id][$i], $collectedData) ? 1 : 0,
                        ]);
                    }
                }
                $labelData = $listedData[$ads_id];
                $collectedData = collect();

                foreach ($userData as $key => $value) {
                    $collectedData->push($value->{$ads_id});
                }

                $count1 = collect();
                foreach ($collectedData as $mainItem)
                    foreach (json_decode($mainItem) ?? [] as $item) {
                        if ($count1->has($item)) {
                            $count1->put($item, $count1->get($item) + 1);
                        } else {
                            $count1->put($item, 1);
                        }
                    }

                $id_fed_key = $count1->keys();
                $id_fed_key_value = $count1->values();

                if ($ads_id == 'Identity_and_Trust_Services') {
                    $titletext = "Status of Identity Federation and Trust Services Under Asi@Connect";
                    $levelname = 'Federation and Trust';
                } elseif ($ads_id == 'Federation_architecture') {
                    $titletext = "Distribution of Federation Architecture";
                    $levelname = 'Architecture';
                }

                return view('Guest.ads', compact('users', 'ren_id', 'id_fed_key', 'id_fed_key_value', 'titletext', 'levelname', 'ID_fed', 'selectedUsers', 'labelData'));
            } elseif ($ads_id == 'IDPs') {
                $userData = identityandTrustService::whereIn('user_id', $ren_id)->get();
                $mainColl = collect();
                foreach ($userData as $key => $value) {
                    $userName = User::find($value->user_id)->name;
                    $mainColl->push([
                        'information_Federation_Services_IdPs' => $value->{'information_Federation_Services_IdPs'} == "" ? 0 : $value->{'information_Federation_Services_IdPs'},
                        'information_Federation_Services_SPs' => $value->{'information_Federation_Services_SPs'} == "" ? 0 : $value->{'information_Federation_Services_SPs'},
                        'user_name' => $userName
                    ]);
                }

                $mainColl = $mainColl->sortBy([
                    ['information_Federation_Services_IdPs', 'desc'],
                    ['information_Federation_Services_SPs', 'desc'],
                    ['user_name', 'asc']
                ]);

                $information_Federation_Services_IdPs = $mainColl->map(function ($item) {
                    return $item['information_Federation_Services_IdPs'];
                })->values();

                $information_Federation_Services_SPs = $mainColl->map(function ($item) {
                    return $item['information_Federation_Services_SPs'];
                })->values();

                $user_name = $mainColl->map(function ($item) {
                    return $item['user_name'];
                })->values();

                $titletext = "Number of participating IDPs and SPs";

                return view('Guest.ads', compact('users', 'ren_id', 'information_Federation_Services_IdPs', 'information_Federation_Services_SPs', 'titletext', 'user_name'));
            }
        } else {
            $users = User::where('role', '!=', 'admin')->get();
            $ren_id = null;
            return view('Guest.ads', compact('users', 'ren_id'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public static function class_generator($key, $value, $index)
    {
        $k = explode('-', $key)[1];

        if ($k == 'service_demand' && $value == 1) {
            return 'bg-warning';
        } elseif ($k == 'service_implemented' && $value == 1) {
            //return 'bg-danger';
            return 'bg-success';
        } elseif ($k == 'service_pipeline' && $value == 1) {
            return 'bg-info';
        } else {
            $i = $index % 6;
            if ($i < 3)
                return 'color';
            else
                return 'bg-white';
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
