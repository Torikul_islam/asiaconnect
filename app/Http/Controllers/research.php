<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\promotion_research;
use App\Models\capacityDevelopment;
use App\Models\challengesAndNrenAdaptibility;
use App\Models\NRENsupporting_online_education;
use App\Models\User;
use Response;

class research extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug, Request $request)
    {
        $users = User::where('role', '!=', 'admin')->get();
        $ren_id = $request->get('ren_id');
        $option_id = $slug;

        if ($request->isMethod('post')) {

            $userData = promotion_research::whereIn('user_id', $ren_id)->get();
            if ($option_id == 'Promotion') {

                $mainColl = collect();
                foreach ($userData as $key => $value) {
                    $userName = User::find($value->user_id)->name;
                    $mainColl->push([
                        'Newsletter' => $value->{'NREN_promotional_newsletter'} == "" ? 0 : $value->{'NREN_promotional_newsletter'},
                        'leaflets' => $value->{'NREN_promotional_leaflets'} == "" ? 0 : $value->{'NREN_promotional_leaflets'},
                        'Tenders' => $value->{'NREN_promotional_tender'} == "" ? 0 : $value->{'NREN_promotional_tender'},
                        'souvenirs' => $value->{'NREN_promotional_gift'} == "" ? 0 : $value->{'NREN_promotional_gift'},
                        'user_name' => $userName
                    ]);
                }

                $mainColl = $mainColl->sortBy([
                    ['Newsletter', 'desc'],
                    ['leaflets', 'desc'],
                    ['Tenders', 'desc'],
                    ['souvenirs', 'desc']
                ]);

                $Newsletter = $mainColl->map(function ($item) {
                    return $item['Newsletter'];
                })->values();

                $leaflets = $mainColl->map(function ($item) {
                    return $item['leaflets'];
                })->values();

                $Tenders = $mainColl->map(function ($item) {
                    return $item['Tenders'];
                })->values();

                $souvenirs = $mainColl->map(function ($item) {
                    return $item['souvenirs'];
                })->values();

                $selectedUsers = $mainColl->map(function ($item) {
                    return $item['user_name'];
                })->values();

                $titletext = "Pictorial Representation of NRENS Promotion Initiative";

                return view('Guest.research', compact('users', 'ren_id', 'Newsletter', 'leaflets', 'Tenders', 'souvenirs', 'selectedUsers', 'titletext'));
            } elseif ($option_id == 'Participation') {
                $mainColl = collect();
                foreach ($userData as $key => $value) {
                    $userName = User::find($value->user_id)->name;
                    $mainColl->push([
                        'apan47' => $value->{'number_registrants_apan47'} == "" ? 0 : $value->{'number_registrants_apan47'},
                        'apan48' => $value->{'number_registrants_apan48'} == "" ? 0 : $value->{'number_registrants_apan48'},
                        'user_name' => $userName
                    ]);
                }

                //return $mainColl;

                $mainColl = $mainColl->sortBy([
                    ['apan47', 'desc'],
                    ['apan48', 'desc']
                ]);

                $apan47 = $mainColl->map(function ($item) {
                    return $item['apan47'];
                })->values();

                $apan48 = $mainColl->map(function ($item) {
                    return $item['apan48'];
                })->values();

                $selectedUsers = $mainColl->map(function ($item) {
                    return $item['user_name'];
                })->values();
                $titletext = "Particpants from Different Countries in APAN47 and APAN48";

                return view('Guest.research', compact('users', 'ren_id', 'apan47', 'apan48', 'selectedUsers', 'titletext'));
            } elseif ($option_id == 'communicationWithInstitutions') {

                $userData = promotion_research::whereIn('user_id', $ren_id)->get();

                $listedData = [
                    'communicationWithInstitutions' => array("Voice", "Email", "Facebook", "News Letter", "Youtube", "Web Portal", "Twitter", "Viber Group", "Letters", "SMS", "WhatsApp", "EduTV")
                ];
                $graph = collect();
                $selectedUsers = collect();
                foreach ($userData as $key => $value) {

                    $collectedData = json_decode($value->{$option_id}) ?? [];

                    $userName = User::find($value->user_id)->name;
                    $selectedUsers->push($userName);

                    for ($i = 0; $i < count($listedData[$option_id]); $i++) {
                        $graph->push([
                            'x' => $userName,
                            'y' => $listedData[$option_id][$i],
                            'v' => in_array($listedData[$option_id][$i], $collectedData) ? 1 : 0,
                        ]);
                    }
                }
                $labelData = $listedData[$option_id];

                $collectedData = collect();
                foreach ($userData as $key => $value) {
                    $collectedData->push($value->{$option_id});
                }

                $count1 = collect();
                foreach ($collectedData as $mainItem)
                    foreach (json_decode($mainItem) ?? [] as $item) {
                        if ($count1->has($item)) {
                            $count1->put($item, $count1->get($item) + 1);
                        } else {
                            $count1->put($item, 1);
                        }
                    }

                $alien_key = $count1->keys();

                $alien_value = $count1->values();

                $titletext = "Pictorial Representation of Mode of Communications to Promote an Event";
                $leveltitle = "Communication Media";

                return view('Guest.research', compact('users', 'ren_id', 'alien_key', 'alien_value', 'titletext', 'leveltitle', 'selectedUsers', 'labelData', 'graph'));
            } elseif ($option_id == 'engineers') {

                $userData = capacityDevelopment::whereIn('user_id', $ren_id)->get();

                $mainColl = collect();
                foreach ($userData as $key => $value) {
                    $userName = User::find($value->user_id)->name;
                    $mainColl->push([
                        'NREN_Engineers_expert' => $value->{'NREN_Engineers_expert'} == "" ? 0 : $value->{'NREN_Engineers_expert'},
                        'NREN_Engineers_intermediate' => $value->{'NREN_Engineers_intermediate'} == "" ? 0 : $value->{'NREN_Engineers_intermediate'},
                        'NREN_Engineers_beginner' => $value->{'NREN_Engineers_beginner'} == "" ? 0 : $value->{'NREN_Engineers_beginner'},
                        'user_name' => $userName
                    ]);
                }

                $mainColl = $mainColl->sortBy([
                    ['NREN_Engineers_expert', 'desc'],
                    ['NREN_Engineers_intermediate', 'desc'],
                    ['NREN_Engineers_beginner', 'desc'],
                ]);

                $NREN_Engineers_expert = $mainColl->map(function ($item) {
                    return $item['NREN_Engineers_expert'];
                })->values();

                $NREN_Engineers_intermediate = $mainColl->map(function ($item) {
                    return $item['NREN_Engineers_intermediate'];
                })->values();

                $NREN_Engineers_beginner = $mainColl->map(function ($item) {
                    return $item['NREN_Engineers_beginner'];
                })->values();

                $selectedUsers = $mainColl->map(function ($item) {
                    return $item['user_name'];
                })->values();

                $titletext = "Distribution of Expertise Level of NREN Engineers";

                return view('Guest.research', compact('users', 'ren_id', 'NREN_Engineers_expert', 'NREN_Engineers_intermediate', 'titletext', 'NREN_Engineers_beginner', 'selectedUsers'));
            } elseif ($option_id == 'MemberInstitutions') {

                $userData = capacityDevelopment::whereIn('user_id', $ren_id)->get();

                $mainColl = collect();
                foreach ($userData as $key => $value) {
                    $userName = User::find($value->user_id)->name;
                    $mainColl->push([
                        'Member_Institutes_expert' => $value->{'Member_Institutes_expert'} == "" ? 0 : $value->{'Member_Institutes_expert'},
                        'Member_Institutes_intermediate' => $value->{'Member_Institutes_intermediate'} == "" ? 0 : $value->{'Member_Institutes_intermediate'},
                        'Member_Institutes_beginner' => $value->{'Member_Institutes_beginner'} == "" ? 0 : $value->{'Member_Institutes_beginner'},
                        'user_name' => $userName
                    ]);
                }

                $mainColl = $mainColl->sortBy([
                    ['Member_Institutes_expert', 'desc'],
                    ['Member_Institutes_intermediate', 'desc'],
                    ['Member_Institutes_beginner', 'desc'],
                ]);


                $NREN_Engineers_expert = $mainColl->map(function ($item) {
                    return $item['Member_Institutes_expert'];
                })->values();

                $NREN_Engineers_intermediate = $mainColl->map(function ($item) {
                    return $item['Member_Institutes_intermediate'];
                })->values();

                $NREN_Engineers_beginner = $mainColl->map(function ($item) {
                    return $item['Member_Institutes_beginner'];
                })->values();

                $selectedUsers = $mainColl->map(function ($item) {
                    return $item['user_name'];
                })->values();

                $titletext = "Expertise Level of Engineers of NREN Member Institutions";

                return view('Guest.research', compact('users', 'ren_id', 'NREN_Engineers_expert', 'NREN_Engineers_intermediate', 'titletext', 'NREN_Engineers_beginner', 'selectedUsers'));
            } elseif ($option_id == 'Major_Threat' || $option_id == 'Big_Opportunities' || $option_id == 'Video_Collaboration_Software') {

                $userData = challengesAndNrenAdaptibility::whereIn('user_id', $ren_id)->get();

                if ($option_id == 'Video_Collaboration_Software') {
                    $userData = NRENsupporting_online_education::whereIn('user_id', $ren_id)->get();
                }

                $listedData = [
                    'Major_Threat' => array("Reduction in Revenue", "Unemployment", "Hardware overrun", "Reduction of research expenses and research activities", "Network Security", "Shortage of Software Licences", "planned REN activities are delayed or cancelled altogether", "Online working & learning via Virutal means", "Efficiency"),
                    'Big_Opportunities' => array("Collaboration among NRENs", "Supporting Tele-Medicine", "Providing Computing\/Virtualization Support", "Supporting Online Education", "Interest of the higher education community on LEARN has increased"),
                    'Video_Collaboration_Software' => array("Zoom", "Microsoft Team", "Google Meet", "Jitsi Meet", "BigBlueButton", "Cisco Webex", "TOMMS", "BreakOut", "Classin,Tencent Meeting", "etc.", "Huawei TE Client")
                ];

                $graph = collect();
                $selectedUsers = collect();
                foreach ($userData as $key => $value) {

                    $collectedData = json_decode($value->{$option_id}) ?? [];

                    $userName = User::find($value->user_id)->name;
                    $selectedUsers->push($userName);

                    for ($i = 0; $i < count($listedData[$option_id]); $i++) {
                        $graph->push([
                            'x' => $userName,
                            'y' => $listedData[$option_id][$i],
                            'v' => in_array($listedData[$option_id][$i], $collectedData) ? 1 : 0,
                        ]);
                    }
                }
                $labelData = $listedData[$option_id];


                $collectedData = collect();
                foreach ($userData as $key => $value) {
                    $collectedData->push($value->{$option_id});
                }


                $count1 = collect();
                foreach ($collectedData as $mainItem)
                    foreach (json_decode($mainItem) ?? [] as $item) {
                        if ($count1->has($item)) {
                            $count1->put($item, $count1->get($item) + 1);
                        } else {
                            $count1->put($item, 1);
                        }
                    }

                $alien_key = $count1->keys();

                $alien_value = $count1->values();
                if ($option_id == 'Major_Threat') {
                    $titletext = "Major Threats Due to Covid-19";
                    $leveltitle = "Threat";
                } elseif ($option_id == 'Video_Collaboration_Software') {
                    $titletext = "Preferred Video Collaboration Applications";
                    $leveltitle = "Applications";
                } else {
                    $titletext = "Opportunities Identified By NRENS at The Fallout of the Pandemic";
                    $leveltitle = "Opportunities";
                }

                return view('Guest.research', compact('labelData', 'selectedUsers', 'graph', 'users', 'ren_id', 'alien_key', 'alien_value', 'titletext', 'leveltitle'));
            } elseif ($option_id == 'Challenges') {
                $userData = NRENsupporting_online_education::whereIn('user_id', $ren_id)->get();
                $Challenges_type = $request->get('Challenges_type');

                $listedData = [
                    'Challenges_faced_by_students' => array("Bandwidth Cost", "Confidence in the Assessment Process", "Quality of Delivery", "Quality of Content", "Availability of End-user Devices", "Last Mile Network Quality", "Nonchalance about the mode of delivery"),
                    'Challenges_faced_by_faculty' => array("Lack of Training", "Last Mile Network Quality", "Bandwidth Cost", "Nonchalance about the mode of delivery", "Lack of LMS Software", "Performing Lab\/Hands-on Activities", "Absence of Policy for \"Online Education\"", "Availability of End-user Devices", "Confidence in the Assessment Process"),
                    'Challenges_faced_by_nren' => array("Limitations of Application Licenses", "High Bandwidth Cost", "Lack of LMS Software", "Providing 24x7 support", "Engineer work from home", "Lack of Hardware (Application running on own Data Center)", "Confidence in Assessment Process")
                ];

                $graph = collect();
                $selectedUsers = collect();
                foreach ($userData as $key => $value) {

                    $collectedData = json_decode($value->{$Challenges_type}) ?? [];

                    $userName = User::find($value->user_id)->name;
                    $selectedUsers->push($userName);

                    for ($i = 0; $i < count($listedData[$Challenges_type]); $i++) {
                        $graph->push([
                            'x' => $userName,
                            'y' => $listedData[$Challenges_type][$i],
                            'v' => in_array($listedData[$Challenges_type][$i], $collectedData) ? 1 : 0,
                        ]);
                    }
                }
                $labelData = $listedData[$Challenges_type];


                $collectedData = collect();
                foreach ($userData as $key => $value) {
                    $collectedData->push($value->{$Challenges_type});
                }


                $count1 = collect();
                foreach ($collectedData as $mainItem)
                    foreach (json_decode($mainItem) ?? [] as $item) {
                        if ($count1->has($item)) {
                            $count1->put($item, $count1->get($item) + 1);
                        } else {
                            $count1->put($item, 1);
                        }
                    }

                $alien_key = $count1->keys();
                $alien_value = $count1->values();
                if ($Challenges_type == 'Challenges_faced_by_students') {
                    $titletext = "Grouping Challenges By Students";
                    $leveltitle = "Challenges";
                } elseif ($Challenges_type == 'Challenges_faced_by_faculty') {
                    $titletext = "Grouping Challenges By Faculty Members";
                    $leveltitle = "Challenges";
                } elseif ($Challenges_type == 'Challenges_faced_by_nren') {
                    $titletext = "Grouping Challenges By NRENS";
                    $leveltitle = "Challenges";
                }

                return view('Guest.challenges', compact('labelData', 'selectedUsers', 'graph', 'users', 'ren_id', 'alien_key', 'alien_value', 'titletext', 'Challenges_type'));
            }
        } else {
            if ($option_id == 'Challenges') {
                $users = User::where('role', '!=', 'admin')->get();
                $Challenges_type = null;
                return view('Guest.challenges', compact('users', 'Challenges_type'));
            } else {
                $users = User::where('role', '!=', 'admin')->get();
                $ren_id = null;
                return view('Guest.research', compact('users', 'ren_id'));
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
