<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\service_platform;
use App\Models\structure_and_financing;
use App\Models\network_trafficand_monitoring;
use App\Models\landscapeAndMarketShare;
use App\Models\User;
use PDF;
use Response;
use Exception;

class traffic extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function trafficView($slug, Request $request)
    {
        /* try { */

        $users = User::where('role', '!=', 'admin')->get();
        $ren_id = $request->get('ren_id');
        $traffic_name = $slug;

        if ($request->isMethod('post')) {

            $userData = network_trafficand_monitoring::whereIn('user_id', $ren_id)->get();

            if ($traffic_name == 'Traffic') {

                $mainColl = collect();
                $year = $request->get('year');
                $traffic_type = $request->get('traffic_type');

                foreach ($userData as $key => $value) {
                    $userName = User::find($value->user_id)->name;

                    if (!$mainColl->has(['user_name' => $userName])) {
                        $mainColl->push(['user_name' => $userName]);
                    }

                    foreach ($year as $k => $y) {

                        $mainColl = $mainColl->map(function ($item) use ($userName, $traffic_type, $value, $y) {
                            if ($item['user_name'] == $userName) {
                                $item[$y] =  $value->{'Upstream' . '_' . $traffic_type . '_' . $y} == "" ? 0 : $value->{'Upstream' . '_' . $traffic_type . '_' . $y};
                            }
                            return $item;
                        });
                    }
                }

                $sortBy  = [];
                foreach ($year as $k => $y)
                    $sortBy[] =  [$y, 'desc'];

                $mainColl = $mainColl->sortBy($sortBy);

                $title = 'Comparison of ' . $traffic_type . ' Traffic';

                return view('Guest.comparisontraffic', compact('users', 'ren_id', 'mainColl', 'title', 'traffic_type'));
            } elseif ($traffic_name == 'type_of_traffic' || $traffic_name == 'tools_TMM' || $traffic_name == 'statistical_data' || $traffic_name == 'communication_way') {

                $userData = network_trafficand_monitoring::whereIn('user_id', $ren_id)->get();
                $listedData = [
                    'type_of_traffic' => array("Research Traffic", "Commodity Traffic", "CDN Traffic", "Domestic Internet Exchange Traffic"),
                    'tools_TMM' => array("Cacti", "MRTG", "Nagios", "Libre NMS", "SolarWinds", "PerfSONAR", "SmokePing", "Weathermap"),
                    'statistical_data' => array("Real Time Traffic Data - MRTG", "Email", "Real Time Fault Report", "Monthly Usage Data", "Meetings/Seminars", "Monthly Availability"),
                    'communication_way' => array("Realtime through URL", "Email", "Direct Phone Call", "SMS", "Social Media", "Meetings/Seminars", "Newsletter/Leaflets")
                ];

                $graph = collect();
                $selectedUsers = collect();

                foreach ($userData as $key => $value) {

                    $collectedData = json_decode($value->{$traffic_name}) ?? [];

                    $userName = User::find($value->user_id)->name;
                    $selectedUsers->push($userName);

                    for ($i = 0; $i < count($listedData[$traffic_name]); $i++) {
                        $graph->push([
                            'x' => $userName,
                            'y' => $listedData[$traffic_name][$i],
                            'v' => in_array($listedData[$traffic_name][$i], $collectedData) ? 1 : 0,
                        ]);
                    }
                }

                $labelData = $listedData[$traffic_name];

                //For popularity segment
                $popularity = collect();
                $titleList = [
                    'type_of_traffic' => 'Accessing to Different Types of Traffic NRENS',
                    'statistical_data' => 'Popularity of Sharing Network Information',
                    'tools_TMM' => 'Popularity of Monitoring Tools by NRENS',
                    'communication_way' => 'Media Used By NRENS For Communicating With Their Members'
                ];

                foreach ($userData as $key => $value) {
                    $popularity->push($value->{$traffic_name});
                }

                $count1 = collect();
                foreach ($popularity as $mainItem)
                    foreach (json_decode($mainItem) ?? [] as $item) {
                        if ($count1->has($item)) {
                            $count1->put($item, $count1->get($item) + 1);
                        } else {
                            $count1->put($item, 1);
                        }
                    }

                $count1 = $count1->sortDesc();

                $doublegraph = $count1->keys();
                $doublegraphvalue = $count1->values();

                $title = $titleList[$traffic_name];

                $dataset = [
                    'type_of_traffic' => 'Traffic',
                    'tools_TMM' => 'Monitoring Tools',
                    'statistical_data' => 'Network Information',
                    'communication_way' => 'Communicating Media'
                ];
                $datasetlevel = $dataset[$traffic_name];

                return view('Guest.traffic', compact('graph', 'users', 'selectedUsers', 'ren_id', 'labelData', 'traffic_name', 'doublegraph', 'doublegraphvalue', 'title', 'datasetlevel'));
            } elseif ($traffic_name == 'Volume') {
                $mainColl = collect();
                $traffic_type = $request->get('traffic_type');

                $typeList = [
                    'yearly_traffic_commodity'   => 'Commodity',
                    'yearly_traffic_RE'          => 'Research Institutes',
                    'yearly_traffic_CDN'         => 'CDN Traffic',
                    'yearly_traffic_IXP'         => 'Internet Exchnage'
                ];

                foreach ($userData as $key => $value) {

                    $userName = User::find($value->user_id)->name;
                    if (!$mainColl->has(['User' => $userName])) {
                        $mainColl->push(['User' => $userName]);
                    }

                    foreach ($traffic_type as $traffic)

                        $mainColl = $mainColl->map(function ($item) use ($userName, $typeList, $value, $traffic) {
                            if ($item['User'] == $userName) {
                                $item[$typeList[$traffic]] =  $value->{$traffic} == "" ? 0 : $value->{$traffic};
                            }
                            return $item;
                        });
                }

                $sortBy  = [];
                foreach ($traffic_type as $traffic)
                    $sortBy[] =  [$typeList[$traffic], 'desc'];

                $mainColl = $mainColl->sortBy($sortBy);

                $titletext = 'Volume of Traffic';

                return view('Guest.VofT', compact('users', 'ren_id', 'titletext', 'mainColl'));
            } elseif ($traffic_name == 'Availballity') {

                $mainColl = collect();
                $availability_type = $request->get('availability_type');

                foreach ($userData as $key => $value) {
                    $userName = User::find($value->user_id)->name;

                    $mainColl->push([
                        'Planned Maintenence as Outage' => $value->{$availability_type . '_' . 'outage'} == "" ? 0 : $value->{$availability_type . '_' . 'outage'},
                        'Planned Maintenence as non-Outage' => $value->{$availability_type . '_' . 'non_outage'} == "" ? 0 : $value->{$availability_type . '_' . 'non_outage'},
                        'user_name' => $userName
                    ]);
                }

                $mainColl = $mainColl->sortBy([
                    ['Planned Maintenence as Outage', 'desc'],
                    ['Planned Maintenence as non-Outage', 'desc'],
                    ['user_name', 'asc']
                ]);

                $title = 'NREN Wise Network Availballity ' . $availability_type .  ' Connectivity';

                return view('Guest.na_updown', compact('users', 'ren_id', 'title', 'mainColl', 'availability_type'));
            } elseif ($traffic_name == 'NOC') {
                $data2019 = collect();

                $noccollect = collect();

                foreach ($userData as $key => $value) {

                    $userName = User::find($value->user_id)->name;
                    $data2019->push($value->{'NOC'});
                    $noccollect->push([
                        'NOC' => $value->{'NOC'},
                        'user_name' => $userName
                    ]);
                }

                $count1  = collect();
                foreach ($data2019 as $item) {
                    if ($count1->has($item)) {
                        $count1->put($item, $count1->get($item) + 1);
                    } else {
                        $count1->put($item, 1);
                    }
                }

                $key = $count1->keys();
                $value = $count1->values();

                $title = 'Availballity Of 7*24 NOC Services Among NRENS';

                return view('Guest.traffic', compact('key', 'users', 'ren_id', 'value', 'title', 'noccollect'));
            } elseif ($traffic_name == 'SLA') {

                $sla = collect();
                $option_of_compensation = collect();
                $sla_compensation = collect();

                foreach ($userData as $key => $value) {
                    $userName = User::find($value->user_id)->name;
                    $sla->push($value->{'SLA'});
                    $option_of_compensation->push($value->{'option_of_compensation'});

                    $sla_compensation->push([
                        'SLA' => $value->{'SLA'},
                        'option_of_compensation' => $value->{'option_of_compensation'},
                        'user_name' => $userName
                    ]);
                }

                $count1  = collect();
                foreach ($sla as $item) {
                    if ($count1->has($item)) {
                        $count1->put($item, $count1->get($item) + 1);
                    } else {
                        $count1->put($item, 1);
                    }
                }

                $count2  = collect();
                foreach ($option_of_compensation as $item) {
                    if ($count2->has($item)) {
                        $count2->put($item, $count2->get($item) + 1);
                    } else {
                        $count2->put($item, 1);
                    }
                }

                $key1 = $count1->keys();
                $value1 = $count1->values();

                $key2 = $count2->keys();
                $value2 = $count2->values();

                $title1 = 'Service Level Agreement';
                $title2 = 'Presence of Financial Demurrage';

                return view('Guest.traffic', compact('key1', 'users', 'ren_id', 'value1', 'title1', 'key2', 'value2', 'title2', 'sla_compensation'));
            } else {

                $alldata = collect();


                foreach ($userData as $key => $value) {

                    $bgpall2019 = collect();
                    $bgpall2018 = collect();


                    $bgpall2019->push($value->{'ipv4_bgp_upstream_2019'});
                    $bgpall2019->push($value->{'ipv4_bgp_downstream_2019'});
                    $bgpall2019->push($value->{'ipv4_bgp_domestic_2019'});
                    $bgpall2019->push($value->{'ipv4_bgp_cdn_2019'});

                    $data2019 = $bgpall2019->filter(function ($item) {
                        return $item != null  && $item != 'none' && $item != '-';
                    })->sum();

                    $bgpall2018->push($value->{'ipv4_bgp_upstream_2018'});
                    $bgpall2018->push($value->{'ipv4_bgp_downstream_2018'});
                    $bgpall2018->push($value->{'ipv4_bgp_domestic_2018'});
                    $bgpall2018->push($value->{'ipv4_bgp_cdn_2018'});

                    $data2018 = $bgpall2018->filter(function ($item) {
                        return $item != null && $item != 'none' && $item != '-';
                    })->sum();

                    $userName = User::find($value->user_id)->name;

                    $alldata->push([
                        '2019' => $data2019,
                        '2018' => $data2018,
                        'user_name' => $userName
                    ]);
                }

                $alldata = $alldata->sortBy([
                    ['2019', 'desc'],
                    ['2018', 'desc'],
                    ['user_name', 'asc']
                ]);


                $data2019 = $alldata->map(function ($item) {
                    return $item['2019'];
                })->values();

                $data2018 = $alldata->map(function ($item) {
                    return $item['2018'];
                })->values();

                $ren_name = $alldata->map(function ($item) {
                    return $item['user_name'];
                })->values();

                $title = 'Total BGP Peers, 2018 vs 2019';
                $level = ['2019', '2018'];

                return view('Guest.bgp', compact('data2019', 'users', 'ren_id', 'data2018', 'ren_name', 'title', 'level'));
            }
        } else {
            if ($traffic_name == 'Traffic') {

                $users = User::where('role', '!=', 'admin')->get();
                $band_type = null;
                $traffic_type = null;
                return view('Guest.comparisontraffic', compact('users', 'band_type', 'traffic_type'));
            } elseif ($traffic_name == 'BGP') {
                $users = User::where('role', '!=', 'admin')->get();
                $ren_id = null;
                $service_id = null;
                return view('Guest.bgp', compact('users', 'ren_id', 'service_id'));
            } elseif ($traffic_name == 'Volume') {
                $users = User::where('role', '!=', 'admin')->get();
                $ren_id = null;
                return view('Guest.VofT', compact('users', 'ren_id'));
            } elseif ($traffic_name == 'Availballity') {
                $users = User::where('role', '!=', 'admin')->get();
                $ren_id = null;
                $availability_type = null;
                return view('Guest.na_updown', compact('users', 'ren_id', 'availability_type'));
            } else {

                $users = User::where('role', '!=', 'admin')->get();
                $ren_id = null;
                $service_id = null;
                return view('Guest.traffic', compact('users', 'ren_id', 'service_id'));
            }
        }
        /* } catch (Exception $exception) {
            return back()->withError('Some Error Occurred!  ' . $exception->getMessage());
        } */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
