<?php

namespace App\Http\Controllers;

use App\Models\advanced_services;
use App\Models\capacityDevelopment;
use App\Models\challengesAndNrenAdaptibility;
use App\Models\contact;
use App\Models\eduroamServices;
use App\Models\identityandTrustService;
use App\Models\initiativesofTEINCC;
use App\Models\landscapeAndMarketShare;
use App\Models\network_trafficand_monitoring;
use App\Models\networkInfrastructureDarkFiber;
use App\Models\nren_supporting_tele_medicine;
use App\Models\NRENsupporting_online_education;
use App\Models\promotion_research;
use App\Models\researchAndEducationCollaboration;
use App\Models\service_platform;
use App\Models\structure_and_financing;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use PDF;
use Response;

class landscape extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug, Request $request)
    {
        $users = User::where('role', '!=', 'admin')->get();
        $ren_id = $request->get('ren_id');
        $land_id = $slug;

        if ($request->isMethod('post')) {

            if ($land_id == 'Institutions') {

                $users = User::where('role', '!=', 'admin')->get();
                $ren_id = $request->get('ren_id');
                $institution_type = $request->get('institution_type');

                $userData = landscapeAndMarketShare::whereIn('user_id', $ren_id)->get();

                $data = collect();
                $level_data = collect();
                //$ren_name = collect();

                $mainColl = collect();

                $typeList = [
                    'Institutions_connected_Universities' => 'Universities',
                    'Institutions_connected_RI' => 'Research Institutes',
                    'Institutions_connected_govt' => 'Government Organizations',
                    'Institutions_connected_school' => 'Schools and Colleges',
                    'Institutions_connected_other' => 'Others'
                ];

                foreach ($userData as $key => $value) {
                    $userName = User::find($value->user_id)->name;

                    if (!$mainColl->has(['ren_name' => $userName])) {
                        $mainColl->push(['ren_name' => $userName]);
                    }

                    foreach ($institution_type as $key => $ins_type) {

                        $mainColl = $mainColl->map(function ($item) use ($userName, $ins_type, $typeList, $value) {
                            if ($item['ren_name'] == $userName) {
                                $item[$typeList[$ins_type]] =  $value->{$ins_type} == "" ? 0 : $value->{$ins_type};
                            }
                            return $item;
                        });
                    }
                }

                $sortBy  = [];

                foreach ($institution_type as $key => $ins_type)
                    $sortBy[] =  [$typeList[$ins_type], 'desc'];

                $mainColl = $mainColl->sortBy($sortBy);

                return view('Guest.summary', compact('mainColl', 'users', 'ren_id'));
            } elseif ($land_id == 'Link') {

                $mainColl = collect();
                $userData = landscapeAndMarketShare::whereIn('user_id', $ren_id)->get();
                $institution_type = $request->get('institution_type');

                $typelist = [
                    'University' => array("lessthan_FE_Universities", "FE_Universities", "t1G_Universities", "t10G_Universities", "t40G_Universities"),
                    'RI' => array("lessthan_FE_RI", "FE_RI", "t1G_RI", "t10G_RI", "t40G_RI"),
                    'GovtOrg' => array("lessthan_FE_govt", "FE_govt", "t1G_govt", "t10G_govt", "t40G_govt"),
                    'School' => array("lessthan_FE_school", "FE_school", "t1G_school", "t10G_school", "t40G_school")
                ];

                $List = [
                    'lessthan_FE_Universities' => '<100M',
                    'FE_Universities' => '100M',
                    't1G_Universities' => '1G',
                    't10G_Universities' => '10G',
                    't40G_Universities' => '100G',
                    'lessthan_FE_RI' => '<100M',
                    'FE_RI' => '100M',
                    't1G_RI' => '1G',
                    't10G_RI' => '10G',
                    't40G_RI' => '100G',
                    'lessthan_FE_govt' => '<100M',
                    'FE_govt' => '100M',
                    't1G_govt' => '1G',
                    't10G_govt' => '10G',
                    't40G_govt' => '100G',
                    'lessthan_FE_school' => '<100M',
                    'FE_school' => '100M',
                    't1G_school' => '1G',
                    't10G_school' => '10G',
                    't40G_school' => '100G'
                ];

                foreach ($userData as $key => $value) {

                    $userName = User::find($value->user_id)->name;

                    if (!$mainColl->has(['User' => $userName])) {
                        $mainColl->push(['User' => $userName]);
                    }

                    for ($i = 0; $i < count($typelist[$institution_type]); $i++) {

                        $mainColl = $mainColl->map(function ($item) use ($userName, $typelist, $institution_type, $value, $i, $List) {
                            if ($item['User'] == $userName) {

                                $item[$List[$typelist[$institution_type][$i]]] =  $value->{$typelist[$institution_type][$i]} == "" ? 0 : $value->{$typelist[$institution_type][$i]};
                            }
                            return $item;
                        });
                    }
                }

                $sortBy  = [];
                for ($i = 0; $i < count($typelist[$institution_type]); $i++)
                    $sortBy[] =  [$List[$typelist[$institution_type][$i]], 'desc'];

                $mainColl = $mainColl->sortBy($sortBy);


                $titletext = 'Distribution of Link Capacity to ' . $institution_type;
                return view('Guest.linkcapacity', compact('users', 'titletext', 'mainColl', 'institution_type'));
            } else {
                $mainColl = collect();
                $userData = landscapeAndMarketShare::whereIn('user_id', $ren_id)->get();

                $institution_type = $request->get('institution_type');
                $band_type = $request->get('band_type');


                $typeList = [
                    'University_Highest' => 'hightest_throughput_Universities',
                    'RI_Highest'         => 'hightest_throughput_RI',
                    'University_Average' => 'average_throughput_Universities',
                    'RI_Average'         => 'average_throughput_RI'
                ];

                foreach ($userData as $key => $value) {

                    $userName = User::find($value->user_id)->name;

                    if (!$mainColl->has(['User' => $userName])) {
                        $mainColl->push(['User' => $userName]);
                    }

                    foreach ($institution_type as $ins)

                        $mainColl = $mainColl->map(function ($item) use ($userName, $band_type, $typeList, $value, $ins) {
                            if ($item['User'] == $userName) {
                                $item[$ins] =  $value->{$typeList[$ins . '_' . $band_type]};
                            }
                            return $item;
                        });
                }

                $sortBy  = [];
                foreach ($institution_type as $ins)
                    $sortBy[] =  [$ins, 'desc'];

                $mainColl = $mainColl->sortBy($sortBy);


                $titletext = $band_type . ' Bandwidth for Institutes';

                return view('Guest.bandwidth', compact('users', 'mainColl', 'titletext', 'band_type'));
            }
        } else {
            if ($land_id == 'Institutions') {
                $users = User::where('role', '!=', 'admin')->get();
                $ren_id = null;

                return view('Guest.summary', compact('users', 'ren_id'));
            } elseif ($land_id == 'Link') {
                $users = User::where('role', '!=', 'admin')->get();

                $ren_id = null;
                $institution_type = null;
                $job_type = null;

                return view('Guest.linkcapacity', compact('users', 'institution_type', 'job_type', 'ren_id'));
            } else {
                $users = User::where('role', '!=', 'admin')->get();

                $ren_id = null;
                $institution_type = null;
                $band_type = null;

                return view('Guest.bandwidth', compact('users', 'institution_type', 'band_type', 'ren_id'));
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
