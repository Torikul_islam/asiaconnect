<?php

namespace App\Http\Controllers\User;

use App\Models\network_trafficand_monitoring;
use App\Models\service_platform;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PDF;
use Response;

class NetworkTrafficandMonitoringController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page4 = network_trafficand_monitoring::where('user_id',Auth::user()->id)
                   ->get();
                  
        return view('User.networkTrafficandMonitoringEdit',compact('page4'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

      public function page4()
    {
        return view('User.networkTrafficandMonitoring');
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

          $type_of_traffic = $request->get('type_of_traffic');
          $total_trafficfor_RE = $request->get('total_trafficfor_RE');
          $Upstream_commodity_2018 = $request->get('Upstream_commodity_2018');
          $Upstream_RE_2018 = $request->get('Upstream_RE_2018');
          $Upstream_CDN_2018 = $request->get('Upstream_CDN_2018');
          $Upstream_IXP_2018 = $request->get('Upstream_IXP_2018');
          $Upstream_commodity_2019 = $request->get('Upstream_commodity_2019');
          $Upstream_RE_2019 = $request->get('Upstream_RE_2019');
          $Upstream_CDN_2019 = $request->get('Upstream_CDN_2019');
          $Upstream_IXP_2019 = $request->get('Upstream_IXP_2019');
          $ratio_DU_commodity_traffic = $request->get('ratio_DU_commodity_traffic');
          $ratio_DU_research_traffic = $request->get('ratio_DU_research_traffic');
          $yearly_traffic_commodity = $request->get('yearly_traffic_commodity');
          $yearly_traffic_RE = $request->get('yearly_traffic_RE');
          $yearly_traffic_CDN = $request->get('yearly_traffic_CDN');
          $yearly_traffic_IXP = $request->get('yearly_traffic_IXP');
          $MRTG_commodity = $request->get('MRTG_commodity');
          $MRTG_credentilas = $request->get('MRTG_credentilas');
          $MRTG_ResearchTraffic = $request->get('MRTG_ResearchTraffic');
          $MRTG_CreAccess = $request->get('MRTG_CreAccess');
          $MRTG_CDN = $request->get('MRTG_CDN');
          $MRTG_credentilas_2 = $request->get('MRTG_credentilas_2');
          $MRTG_domestic = $request->get('MRTG_domestic');
          $MRTG_credentilas_3 = $request->get('MRTG_credentilas_3');
          $ipv6_traffic = $request->get('ipv6_traffic');
          $ipv4_bgp_upstream_2018 = $request->get('ipv4_bgp_upstream_2018');
          $ipv4_bgp_downstream_2018 = $request->get('ipv4_bgp_downstream_2018');
          $ipv4_bgp_domestic_2018 = $request->get('ipv4_bgp_domestic_2018');
          $ipv4_bgp_cdn_2018 = $request->get('ipv4_bgp_cdn_2018');
          $ipv4_bgp_upstream_2019 = $request->get('FE_govt');
          $ipv4_bgp_downstream_2019 = $request->get('ipv4_bgp_downstream_2019');
          $ipv4_bgp_domestic_2019 = $request->get('ipv4_bgp_domestic_2019');
          $ipv4_bgp_cdn_2019 = $request->get('ipv4_bgp_cdn_2019');
          $upstream_non_outage = $request->get('upstream_non_outage');
          $upstream_outage = $request->get('upstream_outage');
          $downstream_non_outage = $request->get('downstream_non_outage');
          $downstream_outage = $request->get('downstream_outage');
          $network_diagram_url = $request->get('network_diagram_url');
          $network_diagram_credentilas = $request->get('network_diagram_credentilas');
          $NOC = $request->get('NOC');
          $tools_TMM = $request->get('tools_TMM');
          $statistical_data = $request->get('statistical_data');
          $communication_way = $request->get('communication_way');
          $SLA = $request->get('SLA');
          $option_of_compensation = $request->get('option_of_compensation');


          $contact_table = network_trafficand_monitoring::updateOrCreate(
                ['user_id'                         =>  Auth::user()->id],
                ['type_of_traffic'                    =>  json_encode($type_of_traffic),
                'total_trafficfor_RE'                 =>  $total_trafficfor_RE,
                'Upstream_commodity_2018'                =>  $Upstream_commodity_2018,
                'Upstream_RE_2018'                =>  $Upstream_RE_2018,
                'Upstream_CDN_2018'                    =>  $Upstream_CDN_2018,
                'Upstream_IXP_2018'                     =>  $Upstream_IXP_2018,
                'Upstream_commodity_2019'                   =>  $Upstream_commodity_2019,
                'Upstream_RE_2019'             =>  $Upstream_RE_2019,
                'Upstream_CDN_2019'            =>  $Upstream_CDN_2019,
                'Upstream_IXP_2019'                =>  $Upstream_IXP_2019,
                'ratio_DU_commodity_traffic'                 =>  $ratio_DU_commodity_traffic,
                'ratio_DU_research_traffic'               =>  $ratio_DU_research_traffic,
                'yearly_traffic_commodity'                  =>  $yearly_traffic_commodity,
                'yearly_traffic_RE'               =>  $yearly_traffic_RE,
                'yearly_traffic_CDN'                 =>  $yearly_traffic_CDN,
                'yearly_traffic_IXP'                   =>  $yearly_traffic_IXP,
                'MRTG_commodity'                =>  $MRTG_commodity,
                'MRTG_credentilas'             =>  $MRTG_credentilas,
                'MRTG_ResearchTraffic'               =>  $MRTG_ResearchTraffic,
                'MRTG_CreAccess'                 =>  $MRTG_CreAccess,
                'MRTG_CDN'          =>  $MRTG_CDN,
                'MRTG_credentilas_2'        =>  $MRTG_credentilas_2,
                'MRTG_domestic'        =>  $MRTG_domestic,
                'MRTG_credentilas_3'                 =>  $MRTG_credentilas_3,
                'ipv6_traffic'                =>  $ipv6_traffic,
                'ipv4_bgp_upstream_2018'              =>  $ipv4_bgp_upstream_2018,
                'ipv4_bgp_downstream_2018'            =>  $ipv4_bgp_downstream_2018,
                'ipv4_bgp_domestic_2018'              =>  $ipv4_bgp_domestic_2018,
                'ipv4_bgp_cdn_2018'                   =>  $ipv4_bgp_cdn_2018,
                'ipv4_bgp_upstream_2019'              =>  $ipv4_bgp_upstream_2019,
                'ipv4_bgp_downstream_2019'                  => $ipv4_bgp_downstream_2019,
                'ipv4_bgp_domestic_2019'                =>  $ipv4_bgp_domestic_2019,
                'upstream_non_outage'                    =>  $upstream_non_outage,
                'upstream_outage'                   =>  $upstream_outage,
                'downstream_non_outage'                     =>  $downstream_non_outage,
                'downstream_outage'                  => $downstream_outage,
                'network_diagram_url'                =>  $network_diagram_url,
                'network_diagram_credentilas'     =>  $network_diagram_credentilas,
                'NOC'                             =>  $NOC,
                'tools_TMM'                       =>  json_encode($tools_TMM),
                'statistical_data'                => json_encode($statistical_data),
                'communication_way'               =>  json_encode($communication_way),
                'SLA'                             =>  $SLA,
                'option_of_compensation'          =>  $option_of_compensation],
            );

                User::where('id','=', Auth::user()->id)
                    ->update([
                    'page_status' => 'page4'
                ]);

         $check = service_platform::where('user_id',Auth::id())->get();
         if ($check->isEmpty()) {
              return redirect()->route('page5');
            }else{
               return redirect()->route('page-5.index');
            }           

    }catch (\Exception $exception) {
            //dd($exception->getMessage());
            return back()->withError('Some Error Occured!  '.$exception->getMessage());
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\network_trafficand_monitoring  $network_trafficand_monitoring
     * @return \Illuminate\Http\Response
     */
    public function show(network_trafficand_monitoring $network_trafficand_monitoring)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\network_trafficand_monitoring  $network_trafficand_monitoring
     * @return \Illuminate\Http\Response
     */
    public function edit(network_trafficand_monitoring $network_trafficand_monitoring)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\network_trafficand_monitoring  $network_trafficand_monitoring
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, network_trafficand_monitoring $network_trafficand_monitoring)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\network_trafficand_monitoring  $network_trafficand_monitoring
     * @return \Illuminate\Http\Response
     */
    public function destroy(network_trafficand_monitoring $network_trafficand_monitoring)
    {
        //
    }
}
