<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\contact;
use App\Models\User;
use App\Models\structure_and_financing;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PDF;
use Response;
class CommonController extends Controller
{
    public function CoverageStore(Request $request)
    {

         try{

          $ad_con_name = $request->get('ad_con_name');
          $ad_con_designation = $request->get('ad_con_designation');
          $ad_con_email = $request->get('ad_con_email');
          $ad_con_phone = $request->get('ad_con_phone');

          $tec_con_name = $request->get('tec_con_name');
          $tec_con_designation = $request->get('tec_con_designation');
          $tec_con_email = $request->get('tec_con_email');
          $tec_con_phone = $request->get('tec_con_phone');

          $visi_con_name = $request->get('visi_con_name');
          $visi_con_designation = $request->get('visi_con_designation');
          $visi_con_email = $request->get('visi_con_email');
          $visi_con_phone = $request->get('visi_con_phone');
       

          $contact_table = contact::updateOrCreate(
                ['user_id'                        =>  Auth::user()->id],
                ['ad_con_name'                    =>  $ad_con_name,
                'ad_con_designation'              =>  $ad_con_designation,
                'ad_con_email'                    =>  $ad_con_email,
                'ad_con_phone'                    =>  $ad_con_phone,
                'tec_con_name'                    =>  $tec_con_name,
                'tec_con_designation'             =>  $tec_con_designation,
                'tec_con_email'                   =>  $tec_con_email,
                'tec_con_phone'                   =>  $tec_con_phone,
                'visi_con_name'                   =>  $visi_con_name,
                'visi_con_designation'            =>  $visi_con_designation,
                'visi_con_email'                  =>  $visi_con_email,
                'visi_con_phone'                  =>  $visi_con_phone],
            );

          User::where('id','=', Auth::user()->id)
                    ->update([
                    'page_status' => 'page1'
                ]);         
      

            $check = structure_and_financing::where('user_id',Auth::id())->get();

         if ($check->isEmpty()) {
              return redirect()->route('page2');
            }else{
               return redirect()->route('page-2.index');
            }                

    }catch (\Exception $exception) {
            //dd($exception->getMessage());
            return back()->withError('Some Error Occured!  '.$exception->getMessage());
            
        }
    }
}
