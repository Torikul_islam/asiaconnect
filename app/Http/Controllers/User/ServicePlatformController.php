<?php

namespace App\Http\Controllers\User;

use App\Models\service_platform;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\advanced_services;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PDF;
use Response;

class ServicePlatformController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page5 = service_platform::where('user_id',Auth::user()->id)
                   ->get();
                  
        return view('User.ServicePlatformEdit',compact('page5'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

       public function page5()
    {
        return view('User.ServicePlatform');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

          $network_service_SO = $request->get('network_service_SO');
          $security_service_SO = $request->get('security_service_SO');
          $identity_service_SO = $request->get('identity_service_SO');
          $collaboration_service_SO = $request->get('collaboration_service_SO');
          $multimedia_service_SO = $request->get('multimedia_service_SO');
          $storage_service_SO = $request->get('storage_service_SO');
          $professional_service_SO = $request->get('professional_service_SO');
          $Upstream_RE_2019 = $request->get('Upstream_RE_2019');
          $Upstream_CDN_2019 = $request->get('Upstream_CDN_2019');
          $Upstream_IXP_2019 = $request->get('Upstream_IXP_2019');
          $flagship_1_61 = $request->get('flagship_1_61');
          $flagship_2_61 = $request->get('flagship_2_61');
          $flagship_3_61 = $request->get('flagship_3_61');
          $flagship_4_61 = $request->get('flagship_4_61');
          $flagship_5_61 = $request->get('flagship_5_61');
          $flagship_1_62 = $request->get('flagship_1_62');
          $flagship_2_62 = $request->get('flagship_2_62');
          $flagship_3_62 = $request->get('flagship_3_62');
          $flagship_4_62 = $request->get('flagship_4_62');
          $flagship_5_62 = $request->get('flagship_5_62');
          
          $contact_table = service_platform::updateOrCreate(
                ['user_id'                             =>  Auth::user()->id],
                ['network_service_SO'                  =>  json_encode($network_service_SO),
                'security_service_SO'                  =>  json_encode($security_service_SO),
                'identity_service_SO'                  =>  json_encode($identity_service_SO),
                'collaboration_service_SO'             =>  json_encode($collaboration_service_SO),
                'multimedia_service_SO'                =>  json_encode($multimedia_service_SO),
                'storage_service_SO'                   =>  json_encode($storage_service_SO),
                'professional_service_SO'              =>  json_encode($professional_service_SO),
                'flagship_1_61'            =>  $flagship_1_61,
                'flagship_2_61'                =>  $flagship_2_61,
                'flagship_3_61'                 =>  $flagship_3_61,
                'flagship_4_61'               =>  $flagship_4_61,
                'flagship_1_62'                  =>  $flagship_1_62,
                'flagship_2_62'               =>  $flagship_2_62,
                'flagship_3_62'                 =>  $flagship_3_62,
                'flagship_4_62'                   =>  $flagship_4_62,
                'flagship_5_62'                =>  $flagship_5_62],
            );

                User::where('id','=', Auth::user()->id)
                    ->update([
                    'page_status' => 'page5'
                ]);

        $check = advanced_services::where('user_id',Auth::id())->get();
         if ($check->isEmpty()) {
              return redirect()->route('page6');
            }else{
               return redirect()->route('page-6.index');
            }           

    }catch (\Exception $exception) {
            //dd($exception->getMessage());
            return back()->withError('Some Error Occured!  '.$exception->getMessage());
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\service_platform  $service_platform
     * @return \Illuminate\Http\Response
     */
    public function show(service_platform $service_platform)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\service_platform  $service_platform
     * @return \Illuminate\Http\Response
     */
    public function edit(service_platform $service_platform)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\service_platform  $service_platform
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, service_platform $service_platform)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\service_platform  $service_platform
     * @return \Illuminate\Http\Response
     */
    public function destroy(service_platform $service_platform)
    {
        //
    }
}
