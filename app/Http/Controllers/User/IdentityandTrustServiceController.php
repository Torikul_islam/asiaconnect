<?php

namespace App\Http\Controllers\User;

use App\Models\identityandTrustService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\researchAndEducationCollaboration;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PDF;
use Response;

class IdentityandTrustServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page9 = identityandTrustService::where('user_id',Auth::user()->id)
                   ->get();
                  
        return view('User.identiyandTrustServiceEdit',compact('page9'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

      public function page9()
    {
        return view('User.identiyandTrustService');
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

          $Identity_and_Trust_Services = $request->get('Identity_and_Trust_Services');
          $Video_Collaboration = $request->get('Video_Collaboration');
          $Digital_Library = $request->get('Digital_Library');
          $Trusted_Digital_Certificate = $request->get('Trusted_Digital_Certificate');
          $Big_File_Sender = $request->get('Big_File_Sender');
          $Cloud_Storage = $request->get('Cloud_Storage');
          $Laptop_Backup_Services = $request->get('Laptop_Backup_Services');
          $Research_Repository = $request->get('Research_Repository');
          $Federation_architecture = $request->get('Federation_architecture');
          $information_Federation_Services_IdPs = $request->get('information_Federation_Services_IdPs');
          $information_Federation_Services_SPs = $request->get('information_Federation_Services_SPs');
         


          
          $contact_table = identityandTrustService::updateOrCreate(
                ['user_id'                             =>  Auth::user()->id],
                ['Identity_and_Trust_Services'                  => json_encode($Identity_and_Trust_Services),
                'Video_Collaboration'                  =>  $Video_Collaboration,
                'Digital_Library'                  =>  $Digital_Library,
                'Trusted_Digital_Certificate'            =>  $Trusted_Digital_Certificate,
                'Big_File_Sender'                =>  $Big_File_Sender,
                'Cloud_Storage'                        =>  $Cloud_Storage,
                'Laptop_Backup_Services'               =>  $Laptop_Backup_Services,
                'Research_Repository'                  =>  $Research_Repository,
                'Federation_architecture'              =>  json_encode($Federation_architecture),
                'information_Federation_Services_IdPs' =>  $information_Federation_Services_IdPs,
                'information_Federation_Services_SPs'  =>  $information_Federation_Services_SPs],
            );

                User::where('id','=', Auth::user()->id)
                    ->update([
                    'page_status' => 'page9'
                ]);

         $check = researchAndEducationCollaboration::where('user_id',Auth::id())->get();
         if ($check->isEmpty()) {
              return redirect()->route('page10');
            }else{
               return redirect()->route('page-10.index');
            }  
 

    }catch (\Exception $exception) {
            //dd($exception->getMessage());
            return back()->withError('Some Error Occured!  '.$exception->getMessage());
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\identityandTrustService  $identityandTrustService
     * @return \Illuminate\Http\Response
     */
    public function show(identityandTrustService $identityandTrustService)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\identityandTrustService  $identityandTrustService
     * @return \Illuminate\Http\Response
     */
    public function edit(identityandTrustService $identityandTrustService)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\identityandTrustService  $identityandTrustService
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, identityandTrustService $identityandTrustService)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\identityandTrustService  $identityandTrustService
     * @return \Illuminate\Http\Response
     */
    public function destroy(identityandTrustService $identityandTrustService)
    {
        //
    }
}
