<?php

namespace App\Http\Controllers\User;

use App\Models\structure_and_financing;
use App\Models\landscapeAndMarketShare;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\contact;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PDF;
use Response;

class StructureAndFinancingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function page2()
    {
        return view('User.structureandfinancing');
    }


    public function index()
    {
        $page2 = structure_and_financing::where('user_id',Auth::user()->id)
                   ->get();
        return view('User.structureandfinancingEdit',compact('page2'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

          $Strapolicies = $request->get('Strapolicies');
          $emp_technical = $request->get('emp_technical');
          $emp_non_technical = $request->get('emp_non_technical');
          $emp_permanent = $request->get('emp_permanent');
          $emp_contract = $request->get('emp_contract');
          $emp_outsourced = $request->get('emp_outsourced');
          $emp_female_technical = $request->get('emp_female_technical');
          $emp_fem_non_technical = $request->get('emp_fem_non_technical');
          $emp_fem_permanent = $request->get('emp_fem_permanent');
          $emp_fem_contract = $request->get('emp_fem_contract');
          $emp_fem_outsourced = $request->get('emp_fem_outsourced');
          $capital_ownfund = $request->get('capital_ownfund');
          $capital_government = $request->get('capital_government');
          $capital_donation = $request->get('capital_donation');
          $capital_others = $request->get('capital_others');
          $operating_ownfund = $request->get('operating_ownfund');
          $operating_government = $request->get('operating_government');
          $operating_donation = $request->get('operating_donation');
          $operating_others = $request->get('operating_others');
          $distribute_netoperation = $request->get('distribute_netoperation');
          $distribute_netmaintenance = $request->get('distribute_netmaintenance');
          $distribute_administration = $request->get('distribute_administration');
          $distribute_other = $request->get('distribute_other');
          $network_operation = $request->get('network_operation');
          $network_maintenance = $request->get('network_maintenance');
          $backbone_connectivity = $request->get('backbone_connectivity');
          $access_connectivity = $request->get('access_connectivity');
          $backbone_fiber = $request->get('backbone_fiber');
          $access_fiber = $request->get('access_fiber');
          $membership_fees = $request->get('membership_fees');
          $bandwidth_service = $request->get('bandwidth_service');
          $other_service = $request->get('other_service');
       

          $contact_table = structure_and_financing::updateOrCreate(
                ['user_id'                         =>  Auth::user()->id],
                ['Strapolicies'                    =>  json_encode($Strapolicies),
                'emp_technical'                    =>  $emp_technical,
                'emp_non_technical'                =>  $emp_non_technical,
                'emp_permanent'                    =>  $emp_permanent,
                'emp_contract'                     =>  $emp_contract,
                'emp_outsourced'                   =>  $emp_outsourced,
                'emp_female_technical'             =>  $emp_female_technical,
                'emp_fem_non_technical'            =>  $emp_fem_non_technical,
                'emp_fem_permanent'                =>  $emp_fem_permanent,
                'emp_fem_contract'                 =>  $emp_fem_contract,
                'emp_fem_outsourced'               =>  $emp_fem_outsourced,
                'capital_ownfund'                  =>  $capital_ownfund,
                'capital_government'               =>  $capital_government,
                'capital_donation'                 =>  $capital_donation,
                'capital_others'                   =>  $capital_others,
                'operating_ownfund'                =>  $operating_ownfund,
                'operating_government'             =>  $operating_government,
                'operating_donation'               =>  $operating_donation,
                'operating_others'                 =>  $operating_others,
                'distribute_netoperation'          =>  $distribute_netoperation,
                'distribute_netmaintenance'        =>  $distribute_netmaintenance,
                'distribute_administration'        =>  $distribute_administration,
                'distribute_other'                 =>  $distribute_other,
                'network_operation'                =>  $network_operation,
                'network_maintenance'              =>  $network_maintenance,
                'backbone_connectivity'            =>  $backbone_connectivity,
                'access_connectivity'              =>  $access_connectivity,
                'backbone_fiber'                   =>  $backbone_fiber,
                'access_fiber'                     =>  $access_fiber,
                'membership_fees'                  =>  json_encode($membership_fees),
                'bandwidth_service'                =>  json_encode($bandwidth_service),
                'other_service'                    =>  json_encode($other_service)],
            );

                User::where('id','=', Auth::user()->id)
                    ->update([
                    'page_status' => 'page2'
                ]);         
      

         $check = landscapeAndMarketShare::where('user_id',Auth::id())->get();
         if ($check->isEmpty()) {
              return redirect()->route('page3');
            }else{
               return redirect()->route('page-3.index');
            }              

    }catch (\Exception $exception) {
            //dd($exception->getMessage());
            return back()->withError('Some Error Occured!  '.$exception->getMessage());
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\structure_and_financing  $structure_and_financing
     * @return \Illuminate\Http\Response
     */
    public function show(structure_and_financing $structure_and_financing)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\structure_and_financing  $structure_and_financing
     * @return \Illuminate\Http\Response
     */
    public function edit(structure_and_financing $structure_and_financing)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\structure_and_financing  $structure_and_financing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, structure_and_financing $structure_and_financing)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\structure_and_financing  $structure_and_financing
     * @return \Illuminate\Http\Response
     */
    public function destroy(structure_and_financing $structure_and_financing)
    {
        //
    }
}
