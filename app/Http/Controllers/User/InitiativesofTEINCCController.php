<?php

namespace App\Http\Controllers\User;

use App\Models\initiativesofTEINCC;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\challengesAndNrenAdaptibility;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PDF;
use Response;

class InitiativesofTEINCCController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page13 = initiativesofTEINCC::where('user_id',Auth::user()->id)
                   ->get();
                  
        return view('User.initiativesOfTEINCCEdit',compact('page13'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function page13()
    {
        return view('User.initiativesOfTEINCC');
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

          $efficacy_WP2_WP3_packages = $request->get('efficacy_WP2_WP3_packages');
          $efficacy_WP4_WP5_packages = $request->get('efficacy_WP4_WP5_packages');
          $satisfied_on_TEINCC = $request->get('satisfied_on_TEINCC');
          $strengths_of_TEINCC = $request->get('strengths_of_TEINCC');
          $weaknesses_of_TEINCC = $request->get('weaknesses_of_TEINCC');
          $opportunities_TEINCC = $request->get('opportunities_TEINCC');
          $looming_Threats_TEINCC = $request->get('looming_Threats_TEINCC');
          

               $contact_table = initiativesofTEINCC::updateOrCreate(
                ['user_id'                             =>  Auth::user()->id],
                ['efficacy_WP2_WP3_packages'                  => $efficacy_WP2_WP3_packages,
                'efficacy_WP4_WP5_packages'                  => $efficacy_WP4_WP5_packages,
                'satisfied_on_TEINCC'                  =>  $satisfied_on_TEINCC,
                'strengths_of_TEINCC'            =>  $strengths_of_TEINCC,
                'weaknesses_of_TEINCC'                => $weaknesses_of_TEINCC,
                'opportunities_TEINCC'                        =>  $opportunities_TEINCC,
                'looming_Threats_TEINCC'               =>  $looming_Threats_TEINCC],
            );

                User::where('id','=', Auth::user()->id)
                    ->update([
                    'page_status' => 'page13'
                ]);

         $check = challengesAndNrenAdaptibility::where('user_id',Auth::id())->get();
         if ($check->isEmpty()) {
              return redirect()->route('page14');
            }else{
               return redirect()->route('page-14.index');
            }  


    }catch (\Exception $exception) {
            //dd($exception->getMessage());
            return back()->withError('Some Error Occured!  '.$exception->getMessage());
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\initiativesofTEINCC  $initiativesofTEINCC
     * @return \Illuminate\Http\Response
     */
    public function show(initiativesofTEINCC $initiativesofTEINCC)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\initiativesofTEINCC  $initiativesofTEINCC
     * @return \Illuminate\Http\Response
     */
    public function edit(initiativesofTEINCC $initiativesofTEINCC)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\initiativesofTEINCC  $initiativesofTEINCC
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, initiativesofTEINCC $initiativesofTEINCC)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\initiativesofTEINCC  $initiativesofTEINCC
     * @return \Illuminate\Http\Response
     */
    public function destroy(initiativesofTEINCC $initiativesofTEINCC)
    {
        //
    }
}
