<?php

namespace App\Http\Controllers\User;

use App\Models\challengesAndNrenAdaptibility;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\NRENsupporting_online_education;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PDF;
use Response;

class ChallengesAndNrenAdaptibilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page14 = challengesAndNrenAdaptibility::where('user_id',Auth::user()->id)
                   ->get();
                  
        return view('User.challengesAndNrenAdaptibilityEdit',compact('page14'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function page14()
    {
        return view('User.challengesAndNrenAdaptibility');
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

          $Internet_jan_2020 = $request->get('Internet_jan_2020');
          $Internet_feb_2020 = $request->get('Internet_feb_2020');
          $Internet_march_2020 = $request->get('Internet_march_2020');
          $Internet_april_2020 = $request->get('Internet_april_2020');
          $Internet_may_2020 = $request->get('Internet_may_2020');
          $Internet_june_2020 = $request->get('Internet_june_2020');
          $Internet_july_2020 = $request->get('Internet_july_2020');
          $Internet_august_2020 = $request->get('Internet_august_2020');
          $Internet_september_2020 = $request->get('Internet_september_2020');
          $Internet_october_2020 = $request->get('Internet_october_2020');
          $Internet_MRTG = $request->get('Internet_MRTG');
          $Research_Traffic_jan_2020 = $request->get('Research_Traffic_jan_2020');
          $Research_Traffic_feb_2020 = $request->get('Research_Traffic_feb_2020');
          $Research_Traffic_march_2020 = $request->get('Research_Traffic_march_2020');
          $Research_Traffic_april_2020 = $request->get('Research_Traffic_april_2020');
          $Research_Traffic_may_2020 = $request->get('Research_Traffic_may_2020');
          $Research_Traffic_june_2020 = $request->get('Research_Traffic_june_2020');
          $Research_Traffic_july_2020 = $request->get('Research_Traffic_july_2020');
          $Research_Traffic_august_2020 = $request->get('Research_Traffic_august_2020');
          $Research_Traffic_september_2020 = $request->get('Research_Traffic_september_2020');
          $Research_Traffic_october_2020 = $request->get('Research_Traffic_october_2020');
          $Research_Traffic_MRTG = $request->get('Research_Traffic_MRTG');
          $Impact_Revenue = $request->get('Impact_Revenue');
          $Major_Threat = $request->get('Major_Threat');
          $Big_Opportunities = $request->get('Big_Opportunities');
          
          

               $contact_table = challengesAndNrenAdaptibility::updateOrCreate(
                ['user_id'                             =>  Auth::user()->id],
                ['Internet_jan_2020'                  => $Internet_jan_2020,
                'Internet_feb_2020'                  => $Internet_feb_2020,
                'Internet_march_2020'                  =>  $Internet_march_2020,
                'Internet_april_2020'            =>  $Internet_april_2020,
                'Internet_may_2020'                => $Internet_may_2020,
                'Internet_june_2020'                        =>  $Internet_june_2020,
                'Internet_july_2020'               =>  $Internet_july_2020,
            'Internet_august_2020'                  => $Internet_august_2020,
                'Internet_september_2020'                  => $Internet_september_2020,
                'Internet_october_2020'                  =>  $Internet_october_2020,
                'Internet_MRTG'            =>  $Internet_MRTG,
                'Research_Traffic_jan_2020'                => $Research_Traffic_jan_2020,
                'Research_Traffic_feb_2020'                        =>  $Research_Traffic_feb_2020,
                'Research_Traffic_march_2020'               =>  $Research_Traffic_march_2020,
            'Research_Traffic_april_2020'                  => $Research_Traffic_april_2020,
                'Research_Traffic_may_2020'                  => $Research_Traffic_may_2020,
                'Research_Traffic_june_2020'                  =>  $Research_Traffic_june_2020,
                'Research_Traffic_july_2020'            =>  $Research_Traffic_july_2020,
                'Research_Traffic_august_2020'                => $Research_Traffic_august_2020,
                'Research_Traffic_september_2020'                        =>  $Research_Traffic_september_2020,
                'Research_Traffic_october_2020'               =>  $Research_Traffic_october_2020,
            'Research_Traffic_MRTG'                  => $Research_Traffic_MRTG,
                'Impact_Revenue'                  => $Impact_Revenue,
                'Major_Threat'                  => json_encode($Major_Threat),
                'Big_Opportunities'            => json_encode($Big_Opportunities)],
            );

                User::where('id','=', Auth::user()->id)
                    ->update([
                    'page_status' => 'page14'
                ]);

         $check = NRENsupporting_online_education::where('user_id',Auth::id())->get();
         if ($check->isEmpty()) {
              return redirect()->route('page15');
            }else{
               return redirect()->route('page-15.index');
            }   

    }catch (\Exception $exception) {
            //dd($exception->getMessage());
            return back()->withError('Some Error Occured!  '.$exception->getMessage());
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\challengesAndNrenAdaptibility  $challengesAndNrenAdaptibility
     * @return \Illuminate\Http\Response
     */
    public function show(challengesAndNrenAdaptibility $challengesAndNrenAdaptibility)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\challengesAndNrenAdaptibility  $challengesAndNrenAdaptibility
     * @return \Illuminate\Http\Response
     */
    public function edit(challengesAndNrenAdaptibility $challengesAndNrenAdaptibility)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\challengesAndNrenAdaptibility  $challengesAndNrenAdaptibility
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, challengesAndNrenAdaptibility $challengesAndNrenAdaptibility)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\challengesAndNrenAdaptibility  $challengesAndNrenAdaptibility
     * @return \Illuminate\Http\Response
     */
    public function destroy(challengesAndNrenAdaptibility $challengesAndNrenAdaptibility)
    {
        //
    }
}
