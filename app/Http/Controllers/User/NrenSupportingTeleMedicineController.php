<?php

namespace App\Http\Controllers\User;

use App\Models\nren_supporting_tele_medicine;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PDF;
use Response;

class NrenSupportingTeleMedicineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page16 = nren_supporting_tele_medicine::where('user_id',Auth::user()->id)
                   ->get();
                  
        return view('User.NRENsupporting_tele_medicineEdit',compact('page16'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

     public function page16()
    {
        return view('User.NRENsupporting_tele_medicine');
    }
    public function page17()
    {
        return view('User.receiveResponce');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

          $Supporting_Tele_medicine = $request->get('Supporting_Tele_medicine');
          $Challenges_Tele_medicine = $request->get('Challenges_Tele_medicine');
          $Mitigating_Steps_Tele_medicine = $request->get('Mitigating_Steps_Tele_medicine');
          $step_supporting_CandV = $request->get('step_supporting_CandV');
          $challenges_supporting_CandV = $request->get('challenges_supporting_CandV');
          $Mitigating_Steps_CandV = $request->get('Mitigating_Steps_CandV');
          $anyOther_support_services = $request->get('anyOther_support_services');
          $role_of_teincc = $request->get('role_of_teincc');
          $Looming_Threat = $request->get('Looming_Threat');
          $Future_Opportunity = $request->get('Future_Opportunity');
          $availing_opportunity_dampening = $request->get('availing_opportunity_dampening');
          $CommentConductedSurvey = $request->get('CommentConductedSurvey');
          
          
         
          

               $contact_table = nren_supporting_tele_medicine::updateOrCreate(
                ['user_id'                             =>  Auth::user()->id],
                ['Supporting_Tele_medicine'                  => $Supporting_Tele_medicine,
                'Challenges_Tele_medicine'                  => $Challenges_Tele_medicine,
                'Mitigating_Steps_Tele_medicine'                  =>  $Mitigating_Steps_Tele_medicine,
                'step_supporting_CandV'            =>  $step_supporting_CandV,
                'challenges_supporting_CandV'                => $challenges_supporting_CandV,
                'Mitigating_Steps_CandV'                        =>  $Mitigating_Steps_CandV,
                'anyOther_support_services'               =>  $anyOther_support_services,
                'role_of_teincc'            =>  $role_of_teincc,
                'Looming_Threat'                => $Looming_Threat,
                'Future_Opportunity'                        =>  $Future_Opportunity,
                'availing_opportunity_dampening'               =>  $availing_opportunity_dampening,
                'CommentConductedSurvey'               =>  $CommentConductedSurvey],
            );

                User::where('id','=', Auth::user()->id)
                    ->update([
                    'page_status' => 'page16'
                ]);


            return redirect()->route('page17');    

    }catch (\Exception $exception) {
            //dd($exception->getMessage());
            return back()->withError('Some Error Occured!  '.$exception->getMessage());
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\nren_supporting_tele_medicine  $nren_supporting_tele_medicine
     * @return \Illuminate\Http\Response
     */
    public function show(nren_supporting_tele_medicine $nren_supporting_tele_medicine)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\nren_supporting_tele_medicine  $nren_supporting_tele_medicine
     * @return \Illuminate\Http\Response
     */
    public function edit(nren_supporting_tele_medicine $nren_supporting_tele_medicine)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\nren_supporting_tele_medicine  $nren_supporting_tele_medicine
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, nren_supporting_tele_medicine $nren_supporting_tele_medicine)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\nren_supporting_tele_medicine  $nren_supporting_tele_medicine
     * @return \Illuminate\Http\Response
     */
    public function destroy(nren_supporting_tele_medicine $nren_supporting_tele_medicine)
    {
        //
    }
}
