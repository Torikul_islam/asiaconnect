<?php

namespace App\Http\Controllers\User;

use App\Models\researchAndEducationCollaboration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\capacityDevelopment;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PDF;
use Response;

class ResearchAndEducationCollaborationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page10 = researchAndEducationCollaboration::where('user_id',Auth::user()->id)
                   ->get();
                  
        return view('User.researchAndEducationCollaborationEdit',compact('page10'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

     public function page10()
    {
        return view('User.researchAndEducationCollaboration');
    }

     
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

          $ResEdu_activities = $request->get('ResEdu_activities');
          $Title_of_Research = $request->get('Title_of_Research');
          $research_url = $request->get('research_url');
          $Abstract = $request->get('Abstract');
          $name_email_first_author = $request->get('name_email_first_author');
          $scope = $request->get('scope');
          $Beneficiaries = $request->get('Beneficiaries');
          $Financing = $request->get('Financing');
          $NREN_contribute = $request->get('NREN_contribute');
          $ResEdu_activities_other = $request->get('ResEdu_activities_other');
          $Title_of_Research_other = $request->get('Title_of_Research_other');
          $research_url_other = $request->get('research_url_other');
          $Abstract_other = $request->get('Abstract_other');
          $name_email_first_author_other = $request->get('name_email_first_author_other');
          $scope_other = $request->get('scope_other');
          $Beneficiaries_other = $request->get('Beneficiaries_other');
          $Financing_other = $request->get('Financing_other');
          $NREN_contribute_other = $request->get('NREN_contribute_other');
          
         


          
          $contact_table = researchAndEducationCollaboration::updateOrCreate(
                ['user_id'                             =>  Auth::user()->id],
                ['ResEdu_activities'                  => $ResEdu_activities,
                'Title_of_Research'                  =>  $Title_of_Research,
                'research_url'                  =>  $research_url,
                'Abstract'            =>  $Abstract,
                'name_email_first_author'                =>  $name_email_first_author,
                'scope'                        =>  $scope,
                'Beneficiaries'               =>  $Beneficiaries,
                'Financing'                  =>  $Financing,
                'NREN_contribute'              =>  $NREN_contribute,
                'ResEdu_activities_other' =>  $ResEdu_activities_other,
                'Title_of_Research_other'  =>  $Title_of_Research_other,
            'research_url_other' =>  $research_url_other,
                'Abstract_other'  =>  $Abstract_other,
            'name_email_first_author_other' =>  $name_email_first_author_other,
                'scope_other'  =>  $scope_other,
            'Beneficiaries_other' =>  $Beneficiaries_other,
                'Financing_other'  =>  $Financing_other,
            'NREN_contribute_other' =>  $NREN_contribute_other],
            );

                User::where('id','=', Auth::user()->id)
                    ->update([
                    'page_status' => 'page10'
                ]);

         $check = capacityDevelopment::where('user_id',Auth::id())->get();
         if ($check->isEmpty()) {
              return redirect()->route('page11');
            }else{
               return redirect()->route('page-11.index');
            }  
  

    }catch (\Exception $exception) {
            //dd($exception->getMessage());
            return back()->withError('Some Error Occured!  '.$exception->getMessage());
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\researchAndEducationCollaboration  $researchAndEducationCollaboration
     * @return \Illuminate\Http\Response
     */
    public function show(researchAndEducationCollaboration $researchAndEducationCollaboration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\researchAndEducationCollaboration  $researchAndEducationCollaboration
     * @return \Illuminate\Http\Response
     */
    public function edit(researchAndEducationCollaboration $researchAndEducationCollaboration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\researchAndEducationCollaboration  $researchAndEducationCollaboration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, researchAndEducationCollaboration $researchAndEducationCollaboration)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\researchAndEducationCollaboration  $researchAndEducationCollaboration
     * @return \Illuminate\Http\Response
     */
    public function destroy(researchAndEducationCollaboration $researchAndEducationCollaboration)
    {
        //
    }
}
