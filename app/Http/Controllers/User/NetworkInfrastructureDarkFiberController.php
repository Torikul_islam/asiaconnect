<?php

namespace App\Http\Controllers\User;

use App\Models\networkInfrastructureDarkFiber;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\eduroamServices;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PDF;
use Response;

class NetworkInfrastructureDarkFiberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page7 = networkInfrastructureDarkFiber::where('user_id',Auth::user()->id)
                   ->get();
                  
        return view('User.networkInfrastructureDarkFiberEdit',compact('page7'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function page7()
    {
        return view('User.networkInfrastructureDarkFiber');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

          $dark_fiber_domestic_usage = $request->get('dark_fiber_domestic_usage');
          $dark_fiber_domestic_long = $request->get('dark_fiber_domestic_long');
          $dark_fiber_cross_border = $request->get('dark_fiber_cross_border');
          $dark_fiber_cross_border_long = $request->get('dark_fiber_cross_border_long');
          $Alien_Wave_technology = $request->get('Alien_Wave_technology');
          $offer_services_Alien_Wave_technology = $request->get('offer_services_Alien_Wave_technology');
          $IP_Trunk = $request->get('IP_Trunk');
          $aggregate_link_capacity = $request->get('aggregate_link_capacity');
          $sdn_pilot = $request->get('sdn_pilot');
          $sdn_production = $request->get('sdn_production');
          $sdn_tested = $request->get('sdn_tested');
          $sdn_planned = $request->get('sdn_planned');
          $sdn_not_planned = $request->get('sdn_not_planned');
          $NFV = $request->get('NFV');
          $Server_Farm = $request->get('Server_Farm');
          $TLS = $request->get('TLS');
          $spam_andfishing = $request->get('spam_andfishing');
          $NGN_Firewall = $request->get('NGN_Firewall');
          $Sand_Boxing = $request->get('Sand_Boxing');
           $DDOS_Protection = $request->get('DDOS_Protection');
          $DPI = $request->get('DPI');


          
          $contact_table = networkInfrastructureDarkFiber::updateOrCreate(
                ['user_id'                             =>  Auth::user()->id],
                ['dark_fiber_domestic_usage'                  => $dark_fiber_domestic_usage,
                'dark_fiber_domestic_long'                  =>  $dark_fiber_domestic_long,
                'dark_fiber_cross_border'                  =>  $dark_fiber_cross_border,
                'dark_fiber_cross_border_long'            =>  $dark_fiber_cross_border_long,
                'Alien_Wave_technology'                =>  $Alien_Wave_technology,
                'offer_services_Alien_Wave_technology'                 =>  $offer_services_Alien_Wave_technology,
                'IP_Trunk'               =>  $IP_Trunk,
                'aggregate_link_capacity'                  =>  $aggregate_link_capacity,
                'sdn_pilot'               =>  $sdn_pilot,
                'sdn_production'                 =>  $sdn_production,
                'sdn_tested'                   =>  $sdn_tested,
                'sdn_planned'               =>  $sdn_planned,
                'sdn_not_planned'                  =>  $sdn_not_planned,
                'NFV'               =>  $NFV,
                'Server_Farm'                 =>  json_encode($Server_Farm),
                'TLS'                   =>  $TLS,
                'spam_andfishing'                =>  $spam_andfishing,
                'NGN_Firewall'               =>  $NGN_Firewall,
                'Sand_Boxing'                 =>  $Sand_Boxing,
                'DDOS_Protection'                   =>  $DDOS_Protection,
                'DPI'                =>  $DPI],
            );

                User::where('id','=', Auth::user()->id)
                    ->update([
                    'page_status' => 'page7'
                ]);

         $check = eduroamServices::where('user_id',Auth::id())->get();
         if ($check->isEmpty()) {
              return redirect()->route('page8');
            }else{
               return redirect()->route('page-8.index');
            }          

    }catch (\Exception $exception) {
            //dd($exception->getMessage());
            return back()->withError('Some Error Occured!  '.$exception->getMessage());
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\networkInfrastructureDarkFiber  $networkInfrastructureDarkFiber
     * @return \Illuminate\Http\Response
     */
    public function show(networkInfrastructureDarkFiber $networkInfrastructureDarkFiber)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\networkInfrastructureDarkFiber  $networkInfrastructureDarkFiber
     * @return \Illuminate\Http\Response
     */
    public function edit(networkInfrastructureDarkFiber $networkInfrastructureDarkFiber)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\networkInfrastructureDarkFiber  $networkInfrastructureDarkFiber
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, networkInfrastructureDarkFiber $networkInfrastructureDarkFiber)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\networkInfrastructureDarkFiber  $networkInfrastructureDarkFiber
     * @return \Illuminate\Http\Response
     */
    public function destroy(networkInfrastructureDarkFiber $networkInfrastructureDarkFiber)
    {
        //
    }
}
