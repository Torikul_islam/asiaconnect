<?php

namespace App\Http\Controllers\User;

use App\Models\NRENsupporting_online_education;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\nren_supporting_tele_medicine;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PDF;
use Response;
class NRENsupportingOnlineEducationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page15 = NRENsupporting_online_education::where('user_id',Auth::user()->id)
                   ->get();
                  
        return view('User.NRENsupporting_online_educationEdit',compact('page15'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


     public function page15()
    {
        return view('User.NRENsupporting_online_education');
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

          $Video_Collaboration_Software = $request->get('Video_Collaboration_Software');
          $NumberofVirtual_Classes_jan_2020 = $request->get('NumberofVirtual_Classes_jan_2020');
          $NumberofVirtual_Classes_feb_2020 = $request->get('NumberofVirtual_Classes_feb_2020');
          $NumberofVirtual_Classes_march_2020 = $request->get('NumberofVirtual_Classes_march_2020');
          $NumberofVirtual_Classes_april_2020 = $request->get('NumberofVirtual_Classes_april_2020');
          $NumberofVirtual_Classes_may_2020 = $request->get('NumberofVirtual_Classes_may_2020');
          $NumberofVirtual_Classes_june_2020 = $request->get('NumberofVirtual_Classes_june_2020');
          $NumberofVirtual_Classes_july_2020 = $request->get('NumberofVirtual_Classes_july_2020');
          $NumberofVirtual_Classes_august_2020 = $request->get('NumberofVirtual_Classes_august_2020');
          $NumberofVirtual_Classes_september_2020 = $request->get('NumberofVirtual_Classes_september_2020');
          $NumberofVirtual_Classes_october_2020 = $request->get('NumberofVirtual_Classes_october_2020');
          $NumberofVirtual_Classes_url = $request->get('NumberofVirtual_Classes_url');

          $Duration_Virtual_Classes_jan_2020 = $request->get('Duration_Virtual_Classes_jan_2020');
          $Duration_Virtual_Classes_feb_2020 = $request->get('Duration_Virtual_Classes_feb_2020');
          $Duration_Virtual_Classes_march_2020 = $request->get('Duration_Virtual_Classes_march_2020');
          $Duration_Virtual_Classes_april_2020 = $request->get('Duration_Virtual_Classes_april_2020');
          $Duration_Virtual_Classes_may_2020 = $request->get('Duration_Virtual_Classes_may_2020');
          $Duration_Virtual_Classes_june_2020 = $request->get('Duration_Virtual_Classes_june_2020');
          $Duration_Virtual_Classes_july_2020 = $request->get('Duration_Virtual_Classes_july_2020');
          $Duration_Virtual_Classes_august_2020 = $request->get('Duration_Virtual_Classes_august_2020');
          $Duration_Virtual_Classes_september_2020 = $request->get('Duration_Virtual_Classes_september_2020');
          $Duration_Virtual_Classes_october_2020 = $request->get('Duration_Virtual_Classes_october_2020');
          $Duration_Virtual_Classes_URL = $request->get('Duration_Virtual_Classes_URL');

          $Total_Participants_VirtualClasses_jan_2020 = $request->get('Total_Participants_VirtualClasses_jan_2020');
          $Total_Participants_VirtualClasses_feb_2020 = $request->get('Total_Participants_VirtualClasses_feb_2020');
          $Total_Participants_VirtualClasses_march_2020 = $request->get('Total_Participants_VirtualClasses_march_2020');
          $Total_Participants_VirtualClasses_april_2020 = $request->get('Total_Participants_VirtualClasses_april_2020');
          $Total_Participants_VirtualClasses_may_2020 = $request->get('Total_Participants_VirtualClasses_may_2020');
          $Total_Participants_VirtualClasses_june_2020 = $request->get('Total_Participants_VirtualClasses_june_2020');
          $Total_Participants_VirtualClasses_july_2020 = $request->get('Total_Participants_VirtualClasses_july_2020');
          $Total_Participants_VirtualClasses_august_2020 = $request->get('Total_Participants_VirtualClasses_august_2020');
          $Total_Participants_VirtualClasses_september_2020 = $request->get('Total_Participants_VirtualClasses_september_2020');
          $Total_Participants_VirtualClasses_october_2020 = $request->get('Total_Participants_VirtualClasses_october_2020');
          $Total_Participants_VirtualClasses_URL = $request->get('Total_Participants_VirtualClasses_URL');
          $Video_Collaboration_Software_run = $request->get('Video_Collaboration_Software_run');
          $Domestic_Internet_Traffic = $request->get('Domestic_Internet_Traffic');
          $DIT_maximum_IX_january = $request->get('DIT_maximum_IX_january');
          $DIT_maximum_IX_june = $request->get('DIT_maximum_IX_june');
          $Available_Software = $request->get('Available_Software');
          $Maximum_Software = $request->get('Maximum_Software');
          $quantity_available_Licenses = $request->get('quantity_available_Licenses');
          $Clients_VCA_institutes = $request->get('Clients_VCA_institutes');
          $Clients_VCA_faculty = $request->get('Clients_VCA_faculty');
          $Clients_VCA_student = $request->get('Clients_VCA_student');
          $Challenges_faced_by_students = $request->get('Challenges_faced_by_students');
          $Challenges_faced_by_faculty = $request->get('Challenges_faced_by_faculty');
          $Challenges_faced_by_nren = $request->get('Challenges_faced_by_nren');
          $limitation_VCA = $request->get('limitation_VCA');
          $lack_of_hardware = $request->get('lack_of_hardware');
          $high_bandwidth_cost = $request->get('high_bandwidth_cost');
          $confidence_assessment = $request->get('confidence_assessment');
          $lack_lms_software = $request->get('lack_lms_software');
          $support_24 = $request->get('support_24');
          $other_step = $request->get('other_step');
          

          
          

               $contact_table = NRENsupporting_online_education::updateOrCreate(
                ['user_id'                             =>  Auth::user()->id],
                ['Video_Collaboration_Software'                  => $Video_Collaboration_Software,
                'NumberofVirtual_Classes_jan_2020'                  => $NumberofVirtual_Classes_jan_2020,
                'NumberofVirtual_Classes_feb_2020'                  =>  $NumberofVirtual_Classes_feb_2020,
                'NumberofVirtual_Classes_march_2020'            =>  $NumberofVirtual_Classes_march_2020,
                'NumberofVirtual_Classes_april_2020'                => $NumberofVirtual_Classes_april_2020,
                'NumberofVirtual_Classes_may_2020'                        =>  $NumberofVirtual_Classes_may_2020,
                'NumberofVirtual_Classes_june_2020'               =>  $NumberofVirtual_Classes_june_2020,
            'NumberofVirtual_Classes_july_2020'                  => $NumberofVirtual_Classes_july_2020,
                'NumberofVirtual_Classes_august_2020'                  => $NumberofVirtual_Classes_august_2020,
                'NumberofVirtual_Classes_september_2020'                  =>  $NumberofVirtual_Classes_september_2020,
                'NumberofVirtual_Classes_october_2020'            =>  $NumberofVirtual_Classes_october_2020,
                'NumberofVirtual_Classes_url'                => $NumberofVirtual_Classes_url,
                'Duration_Virtual_Classes_jan_2020'                        =>  $Duration_Virtual_Classes_jan_2020,
                'Duration_Virtual_Classes_feb_2020'               =>  $Duration_Virtual_Classes_feb_2020,
            'Duration_Virtual_Classes_march_2020'                  => $Duration_Virtual_Classes_march_2020,
                'Duration_Virtual_Classes_april_2020'                  => $Duration_Virtual_Classes_april_2020,
                'Duration_Virtual_Classes_may_2020'                  =>  $Duration_Virtual_Classes_may_2020,
                'Duration_Virtual_Classes_june_2020'            =>  $Duration_Virtual_Classes_june_2020,
                'Duration_Virtual_Classes_july_2020'                => $Duration_Virtual_Classes_july_2020,
                'Duration_Virtual_Classes_august_2020'                        =>  $Duration_Virtual_Classes_august_2020,
                'Duration_Virtual_Classes_october_2020'               =>  $Duration_Virtual_Classes_october_2020,
            'Duration_Virtual_Classes_URL'                  => $Duration_Virtual_Classes_URL,
                'Total_Participants_VirtualClasses_jan_2020'                  => $Total_Participants_VirtualClasses_jan_2020,
                'Total_Participants_VirtualClasses_feb_2020'                  => $Total_Participants_VirtualClasses_feb_2020,
                'Total_Participants_VirtualClasses_march_2020'            => $Total_Participants_VirtualClasses_march_2020,
            'Total_Participants_VirtualClasses_april_2020'                  => $Total_Participants_VirtualClasses_april_2020,
                'Total_Participants_VirtualClasses_may_2020'                  => $Total_Participants_VirtualClasses_may_2020,
                'Total_Participants_VirtualClasses_june_2020'                  => $Total_Participants_VirtualClasses_june_2020,
                'Total_Participants_VirtualClasses_july_2020'            => $Total_Participants_VirtualClasses_july_2020,
            'Total_Participants_VirtualClasses_august_2020'                  => $Total_Participants_VirtualClasses_august_2020,
                'Total_Participants_VirtualClasses_september_2020'                  => $Total_Participants_VirtualClasses_september_2020,
                'Total_Participants_VirtualClasses_october_2020'                  => $Total_Participants_VirtualClasses_october_2020,
                'Total_Participants_VirtualClasses_URL'            => $Total_Participants_VirtualClasses_URL,
            'Video_Collaboration_Software_run'                  => $Video_Collaboration_Software_run,
                'Domestic_Internet_Traffic'                  => $Domestic_Internet_Traffic,
                'DIT_maximum_IX_january'                  => $DIT_maximum_IX_january,
                'DIT_maximum_IX_june'            => $DIT_maximum_IX_june,
            'Duration_Virtual_Classes_URL'                  => $Duration_Virtual_Classes_URL,
                'Available_Software'                  => $Available_Software,
                'Maximum_Software'                  => $Maximum_Software,
                'quantity_available_Licenses'            => $quantity_available_Licenses,
            'Clients_VCA_institutes'                  => $Clients_VCA_institutes,
                'Clients_VCA_faculty'                  => $Clients_VCA_faculty,
                'Clients_VCA_student'                  => $Clients_VCA_student,
                'Challenges_faced_by_students'            => json_encode( $Challenges_faced_by_students),
            'Challenges_faced_by_faculty'                  =>json_encode( $Challenges_faced_by_faculty),
                'Challenges_faced_by_nren'                  =>json_encode($Challenges_faced_by_nren),
                'limitation_VCA'                  => $limitation_VCA,
                'lack_of_hardware'            => $lack_of_hardware,
                'high_bandwidth_cost'            => $high_bandwidth_cost,
                'confidence_assessment'            => $confidence_assessment,
                'lack_lms_software'            => $lack_lms_software,
                'support_24'            => $support_24,
                'other_step'            => $other_step],
            );

                User::where('id','=', Auth::user()->id)
                    ->update([
                    'page_status' => 'page15'
                ]);

         $check = nren_supporting_tele_medicine::where('user_id',Auth::id())->get();
         if ($check->isEmpty()) {
              return redirect()->route('page16');
            }else{
               return redirect()->route('page-16.index');
            }  
            
    }catch (\Exception $exception) {
            //dd($exception->getMessage());
            return back()->withError('Some Error Occured!  '.$exception->getMessage());
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NRENsupporting_online_education  $nRENsupporting_online_education
     * @return \Illuminate\Http\Response
     */
    public function show(NRENsupporting_online_education $nRENsupporting_online_education)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NRENsupporting_online_education  $nRENsupporting_online_education
     * @return \Illuminate\Http\Response
     */
    public function edit(NRENsupporting_online_education $nRENsupporting_online_education)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NRENsupporting_online_education  $nRENsupporting_online_education
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NRENsupporting_online_education $nRENsupporting_online_education)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NRENsupporting_online_education  $nRENsupporting_online_education
     * @return \Illuminate\Http\Response
     */
    public function destroy(NRENsupporting_online_education $nRENsupporting_online_education)
    {
        //
    }
}
