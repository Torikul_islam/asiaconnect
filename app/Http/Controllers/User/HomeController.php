<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\coverage;
use App\Models\contact;
use App\Models\User;
use App\Models\all_ren;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page1 = contact::where('user_id',Auth::user()->id)
                   ->get();
        return view('User.contactedit',compact('page1'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_status = Auth::user()->page_status;

        if ($page_status ==null) {
           return view('User.form');
        }elseif ($page_status == 'page1') {
           return redirect()->route('page-1.index');
        }elseif ($page_status == 'page2') {
           return redirect()->route('page-2.index');
        }elseif ($page_status == 'page3') {
           return redirect()->route('page-3.index');
        }elseif ($page_status == 'page4') {
           return redirect()->route('page-4.index');
        }elseif ($page_status == 'page5') {
           return redirect()->route('page-5.index');
        }elseif ($page_status == 'page6') {
           return redirect()->route('page-6.index');
        }elseif ($page_status == 'page7') {
           return redirect()->route('page-7.index');
        }elseif ($page_status == 'page8') {
           return redirect()->route('page-8.index');
        }elseif ($page_status == 'page9') {
           return redirect()->route('page-9.index');
        }elseif ($page_status == 'page10') {
           return redirect()->route('page-10.index');
        }elseif ($page_status == 'page11') {
           return redirect()->route('page-11.index');
        }elseif ($page_status == 'page12') {
           return redirect()->route('page-12.index');
        }elseif ($page_status == 'page13') {
           return redirect()->route('page-13.index');
        }elseif ($page_status == 'page14') {
           return redirect()->route('page-14.index');
        }elseif ($page_status == 'page15') {
           return redirect()->route('page-15.index');
        }else{
            return redirect()->route('page-16.index');
        }
        
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }

    public function maturity()
    {
        $matu_data = common::where('ren_type_id', Auth::user()->id)
                    ->get();

        return view('User.final_maturity',compact('matu_data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
