<?php

namespace App\Http\Controllers\User;

use App\Models\landscapeAndMarketShare;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\contact;
use App\Models\network_trafficand_monitoring;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PDF;
use Response;

class LandscapeAndMarketShareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page3 = landscapeAndMarketShare::where('user_id',Auth::user()->id)
                   ->get();
                  
        return view('User.landscapeAndMarketShareEdit',compact('page3'));
    }

    public function page3()
    {
        return view('User.landscapeAndMarketShare');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

          $Institutions_category = $request->get('Institutions_category');
          $Institutions_connected_Universities = $request->get('Institutions_connected_Universities');
          $Institutions_connected_RI = $request->get('Institutions_connected_RI');
          $Institutions_connected_govt = $request->get('Institutions_connected_govt');
          $Institutions_connected_school = $request->get('Institutions_connected_school');
          $Institutions_connected_other = $request->get('Institutions_connected_other');
          $Institutions_Target_Universities = $request->get('Institutions_Target_Universities');
          $Institutions_Target_RI = $request->get('Institutions_Target_RI');
          $Institutions_Target_govt = $request->get('Institutions_Target_govt');
          $Institutions_Target_school = $request->get('Institutions_Target_school');
          $Institutions_Target_other = $request->get('Institutions_Target_other');
          $Institutions_Target_Universities_now = $request->get('Institutions_Target_Universities_now');
          $Institutions_Target_RI_now = $request->get('Institutions_Target_RI_now');
          $Institutions_Target_govt_now = $request->get('Institutions_Target_govt_now');
          $Institutions_Target_school_now = $request->get('Institutions_Target_school_now');
          $Institutions_Target_other_now = $request->get('Institutions_Target_other_now');
          $user_Universities = $request->get('user_Universities');
          $user_RI = $request->get('user_RI');
          $user_govt = $request->get('user_govt');
          $user_school = $request->get('user_school');
          $user_other = $request->get('user_other');
          $total_Universities = $request->get('total_Universities');
          $lessthan_FE_Universities = $request->get('lessthan_FE_Universities');
          $lessthan_FE_RI = $request->get('lessthan_FE_RI');
          $lessthan_FE_govt = $request->get('lessthan_FE_govt');
          $lessthan_FE_school = $request->get('lessthan_FE_school');
          $lessthan_FE_other = $request->get('lessthan_FE_other');
          $FE_Universities = $request->get('FE_Universities');
          $FE_RI = $request->get('FE_RI');
          $FE_govt = $request->get('FE_govt');
          $FE_school = $request->get('FE_school');
          $FE_other = $request->get('FE_other');
          $t1G_Universities = $request->get('1G_Universities');
          $t1G_RI = $request->get('1G_RI');
          $t1G_govt = $request->get('1G_govt');
          $t1G_school = $request->get('1G_school');
          $t1G_other = $request->get('1G_other');
          $t10G_Universities = $request->get('10G_Universities');
          $t10G_RI = $request->get('10G_RI');
          $t10G_govt = $request->get('10G_govt');
          $t10G_school = $request->get('10G_school');
          $t10G_other = $request->get('10G_other');
          $t40G_Universities = $request->get('40G_Universities');
          $t40G_RI = $request->get('40G_RI');
          $t40G_govt = $request->get('40G_govt');
          $t40G_school = $request->get('40G_school');
          $t40G_other = $request->get('40G_other');
          $hightest_throughput_Universities = $request->get('hightest_throughput_Universities');
          $hightest_throughput_RI = $request->get('hightest_throughput_RI');
          $hightest_throughput_govt = $request->get('hightest_throughput_govt');
          $hightest_throughput_school = $request->get('hightest_throughput_school');
          $hightest_throughput_other = $request->get('hightest_throughput_other');
          $average_throughput_Universities = $request->get('average_throughput_Universities');
          $average_throughput_RI = $request->get('average_throughput_RI');
          $average_throughput_govt = $request->get('average_throughput_govt');
          $average_throughput_school = $request->get('average_throughput_school');
          $average_throughput_other = $request->get('average_throughput_other');

       

          $contact_table = landscapeAndMarketShare::updateOrCreate(
                ['user_id'                         =>  Auth::user()->id],
                ['Institutions_category'                    =>  json_encode($Institutions_category),
                'Institutions_connected_Universities'                    =>  $Institutions_connected_Universities,
                'Institutions_connected_RI'                =>  $Institutions_connected_RI,
                'Institutions_connected_govt'                    =>  $Institutions_connected_govt,
                'Institutions_connected_school'                     =>  $Institutions_connected_school,
                'Institutions_connected_other'                   =>  $Institutions_connected_other,
                'Institutions_Target_Universities'             =>  $Institutions_Target_Universities,
                'Institutions_Target_RI'            =>  $Institutions_Target_RI,
                'Institutions_Target_govt'                =>  $Institutions_Target_govt,
                'Institutions_Target_school'                 =>  $Institutions_Target_school,
                'Institutions_Target_other'               =>  $Institutions_Target_other,
                'Institutions_Target_Universities_now'                  =>  $Institutions_Target_Universities_now,
                'Institutions_Target_RI_now'               =>  $Institutions_Target_RI_now,
                'Institutions_Target_govt_now'                 =>  $Institutions_Target_govt_now,
                'Institutions_Target_school_now'                   =>  $Institutions_Target_school_now,
                'Institutions_Target_other_now'                =>  $Institutions_Target_other_now,
                'user_Universities'             =>  $user_Universities,
                'user_RI'               =>  $user_RI,
                'user_govt'                 =>  $user_govt,
                'user_school'          =>  $user_school,
                'user_other'        =>  $user_other,
                'total_Universities'        =>  $total_Universities,
                'lessthan_FE_Universities'                 =>  $lessthan_FE_Universities,
                'lessthan_FE_RI'                =>  $lessthan_FE_RI,
                'lessthan_FE_govt'              =>  $lessthan_FE_govt,
                'lessthan_FE_school'            =>  $lessthan_FE_school,
                'lessthan_FE_other'              =>  $lessthan_FE_other,
                'FE_Universities'                   =>  $FE_Universities,
                'FE_RI'                     =>  $FE_RI,
                'FE_govt'                  => $FE_govt,
                'FE_school'                =>  $FE_school,
                'FE_other'                    =>  $FE_other,
                't1G_Universities'                   =>  $t1G_Universities,
                't1G_RI'                     =>  $t1G_RI,
                't1G_govt'                  => $t1G_govt,
                't1G_school'                =>  $t1G_school,
                't1G_other'                    =>  $t1G_other,
                't10G_Universities'                   =>  $t10G_Universities,
                't10G_RI'                     =>  $t10G_RI,
                't10G_govt'                  => $t10G_govt,
                't10G_school'                =>  $t10G_school,
                't10G_other'                    =>  $t10G_other,
                't40G_Universities'                   =>  $t40G_Universities,
                't40G_RI'                     =>  $t40G_RI,
                't40G_govt'                  => $t40G_govt,
                't40G_school'                =>  $t40G_school,
                't40G_other'                    =>  $t40G_other,
                'hightest_throughput_Universities'  =>  $hightest_throughput_Universities,
                'hightest_throughput_RI'                    =>  $hightest_throughput_RI,
                'hightest_throughput_govt'                  => $hightest_throughput_govt,
                'hightest_throughput_school'                =>  $hightest_throughput_school,
                'hightest_throughput_other'                 =>  $hightest_throughput_other,
                'average_throughput_Universities'           =>  $average_throughput_Universities,
                'average_throughput_RI'                     =>  $average_throughput_RI,
                'average_throughput_govt'                  => $average_throughput_govt,
                'average_throughput_school'                =>  $average_throughput_school,
                'average_throughput_other'                 =>  $average_throughput_other],
            );

                User::where('id','=', Auth::user()->id)
                    ->update([
                    'page_status' => 'page3'
                ]); 

         $check = network_trafficand_monitoring::where('user_id',Auth::id())->get();
         if ($check->isEmpty()) {
              return redirect()->route('page4');
            }else{
               return redirect()->route('page-4.index');
            }         

    }catch (\Exception $exception) {
            //dd($exception->getMessage());
            return back()->withError('Some Error Occured!  '.$exception->getMessage());
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\landscapeAndMarketShare  $landscapeAndMarketShare
     * @return \Illuminate\Http\Response
     */
    public function show(landscapeAndMarketShare $landscapeAndMarketShare)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\landscapeAndMarketShare  $landscapeAndMarketShare
     * @return \Illuminate\Http\Response
     */
    public function edit(landscapeAndMarketShare $landscapeAndMarketShare)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\landscapeAndMarketShare  $landscapeAndMarketShare
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, landscapeAndMarketShare $landscapeAndMarketShare)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\landscapeAndMarketShare  $landscapeAndMarketShare
     * @return \Illuminate\Http\Response
     */
    public function destroy(landscapeAndMarketShare $landscapeAndMarketShare)
    {
        //
    }
}
