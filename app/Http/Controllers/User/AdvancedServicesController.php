<?php

namespace App\Http\Controllers\User;

use App\Models\advanced_services;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\networkInfrastructureDarkFiber;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PDF;
use Response;
class AdvancedServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page6 = advanced_services::where('user_id',Auth::user()->id)
                   ->get();
                  
        return view('User.advanceServicesEdit',compact('page6'));
    }


      public function page6()
    {
        return view('User.advanceServices');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

          $service_demand = $request->get('service_demand');
          $service_implemented = $request->get('service_implemented');
          $service_pipeline = $request->get('service_pipeline');
          $DNS_Server = $request->get('DNS_Server');
          $web_Server = $request->get('web_Server');
          $mail_Server = $request->get('mail_Server');
          $dhcp_Server = $request->get('dhcp_Server');
          $routing_system = $request->get('routing_system');
          $switching_system = $request->get('switching_system');
          $ipv6_bgp_upstream = $request->get('ipv6_bgp_upstream');
          $ipv6_bgp_downstream = $request->get('ipv6_bgp_downstream');
          $ipv6_bgp_domestic = $request->get('ipv6_bgp_domestic');
          $ipv6_bgp_cdn = $request->get('ipv6_bgp_cdn');
          $upstram_peer_ipv4 = $request->get('upstram_peer_ipv4');
          $upstram_peer_ipv6 = $request->get('upstram_peer_ipv6');
          $parallei_peer_ipv4 = $request->get('parallei_peer_ipv4');
          $parallei_peer_ipv6 = $request->get('parallei_peer_ipv6');
          $downstream_peer_ipv4 = $request->get('downstream_peer_ipv4');
          $downstream_peer_ipv6 = $request->get('downstream_peer_ipv6');
          $ipv6_tein_upstream = $request->get('ipv6_tein_upstream');
           $application_server_ipv4 = $request->get('application_server_ipv4');
          $application_server_ipv6 = $request->get('application_server_ipv6');
          $config_network_ipv6 = $request->get('config_network_ipv6');
          $public_asn = $request->get('public_asn');
          $ipv4_24_blocks = $request->get('ipv4_24_blocks');
          $independent_ASN = $request->get('independent_ASN');
          $IPv6_Routed_ASN = $request->get('IPv6_Routed_ASN');
          $MANRS = $request->get('MANRS');
          
          $contact_table = advanced_services::updateOrCreate(
                ['user_id'                             =>  Auth::user()->id],
                ['service_demand'                  =>  json_encode($service_demand),
                'service_implemented'                  =>  json_encode($service_implemented),
                'service_pipeline'                  =>  json_encode($service_pipeline),
                'DNS_Server'            =>  $DNS_Server,
                'web_Server'                =>  $web_Server,
                'mail_Server'                 =>  $mail_Server,
                'dhcp_Server'               =>  $dhcp_Server,
                'routing_system'                  =>  $routing_system,
                'switching_system'               =>  $switching_system,
                'ipv6_bgp_upstream'                 =>  $ipv6_bgp_upstream,
                'ipv6_bgp_downstream'                   =>  $ipv6_bgp_downstream,
                'ipv6_bgp_domestic'               =>  $ipv6_bgp_domestic,
                'ipv6_bgp_cdn'                  =>  $ipv6_bgp_cdn,
                'upstram_peer_ipv4'               =>  $upstram_peer_ipv4,
                'upstram_peer_ipv6'                 =>  $upstram_peer_ipv6,
                'parallei_peer_ipv4'                   =>  $parallei_peer_ipv4,
                'parallei_peer_ipv6'                =>  $parallei_peer_ipv6,
                'downstream_peer_ipv4'               =>  $downstream_peer_ipv4,
                'downstream_peer_ipv6'                 =>  $downstream_peer_ipv6,
                'ipv6_tein_upstream'                   =>  $ipv6_tein_upstream,
                'application_server_ipv4'                =>  $application_server_ipv4,
                'application_server_ipv6'                =>  $application_server_ipv6,
                'config_network_ipv6'                =>  $config_network_ipv6,
                'public_asn'                     =>  $public_asn,
                'ipv4_24_blocks'                 =>  $ipv4_24_blocks,
                'independent_ASN'                =>  $independent_ASN,
                'IPv6_Routed_ASN'                =>  $IPv6_Routed_ASN,
                'MANRS'                =>  $MANRS],
            );

                User::where('id','=', Auth::user()->id)
                    ->update([
                    'page_status' => 'page6'
                ]);  

         $check = networkInfrastructureDarkFiber::where('user_id',Auth::id())->get();
         if ($check->isEmpty()) {
              return redirect()->route('page7');
            }else{
               return redirect()->route('page-7.index');
            }        

    }catch (\Exception $exception) {
            //dd($exception->getMessage());
            return back()->withError('Some Error Occured!  '.$exception->getMessage());
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\advanced_services  $advanced_services
     * @return \Illuminate\Http\Response
     */
    public function show(advanced_services $advanced_services)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\advanced_services  $advanced_services
     * @return \Illuminate\Http\Response
     */
    public function edit(advanced_services $advanced_services)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\advanced_services  $advanced_services
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, advanced_services $advanced_services)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\advanced_services  $advanced_services
     * @return \Illuminate\Http\Response
     */
    public function destroy(advanced_services $advanced_services)
    {
        //
    }
}
