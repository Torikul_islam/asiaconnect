<?php

namespace App\Http\Controllers\User;

use App\Models\eduroamServices;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\identityandTrustService;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PDF;
use Response;

class EduroamServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page8 = eduroamServices::where('user_id',Auth::user()->id)
                   ->get();
                  
        return view('User.eduroamServicesEdit',compact('page8'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

         public function page8()
    {
        return view('User.eduroamServices');
    }

    



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

          $eduroam = $request->get('eduroam');
          $Campus = $request->get('Campus');
          $Access_Points = $request->get('Access_Points');
          $Service_Location_Map = $request->get('Service_Location_Map');
          $F_ticks = $request->get('F_ticks');
          $CAT = $request->get('CAT');
          $IP_Trunk = $request->get('IP_Trunk');
          $govRoam = $request->get('govRoam');
          $eduRoam_Hotspot = $request->get('eduRoam_Hotspot');
          $Authentications_NRS_National_2018 = $request->get('Authentications_NRS_National_2018');
          $Authentications_NRS_International_2018 = $request->get('Authentications_NRS_International_2018');
          $Authentications_NRS_National_2019 = $request->get('Authentications_NRS_National_2019');
          $Authentications_NRS_International_2019 = $request->get('Authentications_NRS_International_2019');


          
          $contact_table = eduroamServices::updateOrCreate(
                ['user_id'                             =>  Auth::user()->id],
                ['eduroam'                  => $eduroam,
                'Campus'                  =>  $Campus,
                'Access_Points'                  =>  $Access_Points,
                'Service_Location_Map'            =>  $Service_Location_Map,
                'F_ticks'                =>  $F_ticks,
                'CAT'                 =>  $CAT,
                'govRoam'               =>  $govRoam,
                'eduRoam_Hotspot'                  =>  $eduRoam_Hotspot,
                'Authentications_NRS_National_2018'               =>  $Authentications_NRS_National_2018,
                'Authentications_NRS_International_2018'                 =>  $Authentications_NRS_International_2018,
                'Authentications_NRS_National_2019'                   =>  $Authentications_NRS_National_2019,
                'Authentications_NRS_International_2019'               =>  $Authentications_NRS_International_2019],
            );

                User::where('id','=', Auth::user()->id)
                    ->update([
                    'page_status' => 'page8'
                ]);

         $check = identityandTrustService::where('user_id',Auth::id())->get();
         if ($check->isEmpty()) {
              return redirect()->route('page9');
            }else{
               return redirect()->route('page-9.index');
            }  
    

    }catch (\Exception $exception) {
            //dd($exception->getMessage());
            return back()->withError('Some Error Occured!  '.$exception->getMessage());
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\eduroamServices  $eduroamServices
     * @return \Illuminate\Http\Response
     */
    public function show(eduroamServices $eduroamServices)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\eduroamServices  $eduroamServices
     * @return \Illuminate\Http\Response
     */
    public function edit(eduroamServices $eduroamServices)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\eduroamServices  $eduroamServices
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, eduroamServices $eduroamServices)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\eduroamServices  $eduroamServices
     * @return \Illuminate\Http\Response
     */
    public function destroy(eduroamServices $eduroamServices)
    {
        //
    }
}
