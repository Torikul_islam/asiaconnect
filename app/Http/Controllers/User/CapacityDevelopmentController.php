<?php

namespace App\Http\Controllers\User;

use App\Models\capacityDevelopment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\promotion_research;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PDF;
use Response;

class CapacityDevelopmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page11 = capacityDevelopment::where('user_id',Auth::user()->id)
                   ->get();
                  
        return view('User.capacityDevelopmentEdit',compact('page11'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function page11()
    {
        return view('User.capacityDevelopment');
    }

     


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

          $NREN_Engineers_expert = $request->get('NREN_Engineers_expert');
          $NREN_Engineers_intermediate = $request->get('NREN_Engineers_intermediate');
          $NREN_Engineers_beginner = $request->get('NREN_Engineers_beginner');
          $Member_Institutes_expert = $request->get('Member_Institutes_expert');
          $Member_Institutes_intermediate = $request->get('Member_Institutes_intermediate');
          $Member_Institutes_beginner = $request->get('Member_Institutes_beginner');
          $wp2andwp3_package = $request->get('wp2andwp3_package');
          $efficacy_WP2_WP3_packages = $request->get('efficacy_WP2_WP3_packages');
          $AsiaConnect_Financed_Training_International = $request->get('AsiaConnect_Financed_Training_International');
          $AsiaConnect_Financed_Training_National = $request->get('AsiaConnect_Financed_Training_National');
          $own_Financed_Training_International = $request->get('own_Financed_Training_International');
          $own_Financed_Training_National = $request->get('own_Financed_Training_National');
          
          
         


          
          $contact_table = capacityDevelopment::updateOrCreate(
                ['user_id'                             =>  Auth::user()->id],
                ['NREN_Engineers_expert'                  => $NREN_Engineers_expert,
                'NREN_Engineers_intermediate'                  => $NREN_Engineers_intermediate,
                'NREN_Engineers_beginner'                  =>  $NREN_Engineers_beginner,
                'Member_Institutes_expert'            =>  $Member_Institutes_expert,
                'Member_Institutes_intermediate'                => $Member_Institutes_intermediate,
                'Member_Institutes_beginner'                        =>  $Member_Institutes_beginner,
                'wp2andwp3_package'               =>  $wp2andwp3_package,
                'efficacy_WP2_WP3_packages'                  =>  $efficacy_WP2_WP3_packages,
                'AsiaConnect_Financed_Training_International'              =>  $AsiaConnect_Financed_Training_International,
                'AsiaConnect_Financed_Training_National' =>  $AsiaConnect_Financed_Training_National,
                'own_Financed_Training_International'  =>  $own_Financed_Training_International,
            'own_Financed_Training_National' =>  $own_Financed_Training_National],
            );

                User::where('id','=', Auth::user()->id)
                    ->update([
                    'page_status' => 'page11'
                ]);

         $check = promotion_research::where('user_id',Auth::id())->get();
         if ($check->isEmpty()) {
              return redirect()->route('page12');
            }else{
               return redirect()->route('page-12.index');
            }  


    }catch (\Exception $exception) {
            //dd($exception->getMessage());
            return back()->withError('Some Error Occured!  '.$exception->getMessage());
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\capacityDevelopment  $capacityDevelopment
     * @return \Illuminate\Http\Response
     */
    public function show(capacityDevelopment $capacityDevelopment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\capacityDevelopment  $capacityDevelopment
     * @return \Illuminate\Http\Response
     */
    public function edit(capacityDevelopment $capacityDevelopment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\capacityDevelopment  $capacityDevelopment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, capacityDevelopment $capacityDevelopment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\capacityDevelopment  $capacityDevelopment
     * @return \Illuminate\Http\Response
     */
    public function destroy(capacityDevelopment $capacityDevelopment)
    {
        //
    }
}
