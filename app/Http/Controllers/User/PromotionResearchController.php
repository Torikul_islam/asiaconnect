<?php

namespace App\Http\Controllers\User;

use App\Models\promotion_research;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\initiativesofTEINCC;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PDF;
use Response;

class PromotionResearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page12 = promotion_research::where('user_id',Auth::user()->id)
                   ->get();
                  
        return view('User.promotionOfResearchEdit',compact('page12'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function page12()
    {
        return view('User.promotionOfResearch');
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

          $NREN_promotional_newsletter = $request->get('NREN_promotional_newsletter');
          $NREN_promotional_leaflets = $request->get('NREN_promotional_leaflets');
          $NREN_promotional_tender = $request->get('NREN_promotional_tender');
          $NREN_promotional_gift = $request->get('NREN_promotional_gift');
          $NREN_virtual_seminarTEIN_Sponsored = $request->get('NREN_virtual_seminarTEIN_Sponsored');
          $NREN_physical_seminarTEIN_Sponsored = $request->get('NREN_physical_seminarTEIN_Sponsored');
          $NREN_virtual_seminar_NREN_Sponsored = $request->get('NREN_virtual_seminar_NREN_Sponsored');
          $NREN_Physical_seminar_NREN_Sponsored = $request->get('NREN_Physical_seminar_NREN_Sponsored');
          $efficacy_WP2_WP3_packages = $request->get('efficacy_WP2_WP3_packages');
          $number_registrants_apan47 = $request->get('number_registrants_apan47');
          $number_registrants_apan48 = $request->get('number_registrants_apan48');
          $communicationWithInstitutions = $request->get('communicationWithInstitutions');


               $contact_table = promotion_research::updateOrCreate(
                ['user_id'                             =>  Auth::user()->id],
                ['NREN_promotional_newsletter'                  => $NREN_promotional_newsletter,
                'NREN_promotional_leaflets'                  => $NREN_promotional_leaflets,
                'NREN_promotional_tender'                  =>  $NREN_promotional_tender,
                'NREN_promotional_gift'            =>  $NREN_promotional_gift,
                'NREN_virtual_seminarTEIN_Sponsored'                => $NREN_virtual_seminarTEIN_Sponsored,
                'NREN_physical_seminarTEIN_Sponsored'                        =>  $NREN_physical_seminarTEIN_Sponsored,
                'NREN_virtual_seminar_NREN_Sponsored'               =>  $NREN_virtual_seminar_NREN_Sponsored,
                'NREN_Physical_seminar_NREN_Sponsored'                  =>  $NREN_Physical_seminar_NREN_Sponsored,
                'number_registrants_apan47'              =>  $number_registrants_apan47,
                'number_registrants_apan48' =>  $number_registrants_apan48,
                'communicationWithInstitutions'  => json_encode($communicationWithInstitutions)],
            );

                User::where('id','=', Auth::user()->id)
                    ->update([
                    'page_status' => 'page12'
                ]);

         $check = initiativesofTEINCC::where('user_id',Auth::id())->get();
         if ($check->isEmpty()) {
              return redirect()->route('page13');
            }else{
               return redirect()->route('page-13.index');
            }  
   

    }catch (\Exception $exception) {
            //dd($exception->getMessage());
            return back()->withError('Some Error Occured!  '.$exception->getMessage());
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\promotion_research  $promotion_research
     * @return \Illuminate\Http\Response
     */
    public function show(promotion_research $promotion_research)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\promotion_research  $promotion_research
     * @return \Illuminate\Http\Response
     */
    public function edit(promotion_research $promotion_research)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\promotion_research  $promotion_research
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, promotion_research $promotion_research)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\promotion_research  $promotion_research
     * @return \Illuminate\Http\Response
     */
    public function destroy(promotion_research $promotion_research)
    {
        //
    }
}
