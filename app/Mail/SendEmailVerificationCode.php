<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEmailVerificationCode extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $email_verification_code;
    public $email_address;
    public function __construct($code,$email)
    {
        $this->email_verification_code=$code;
        $this->email_address=$email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $app_name = env("APP_NAME");
        $data['app_name']=$app_name;
        $data['email_address']=$this->email_address;
        $data['app_url']=env("APP_URL");
        $data['email_verification_code']=$this->email_verification_code;
        return $this->subject('Email verification for your registration on '.$app_name)
                ->view('email.UserEmailVerificationCodeSend', $data);
    }
}
