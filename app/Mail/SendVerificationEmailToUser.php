<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendVerificationEmailToUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $app_name = env("APP_NAME");
        $data['app_url']=env("APP_URL");
        $data['app_name']=env("APP_NAME");
        return $this->from(env("MAIL_FROM_ADDRESS"))
                ->subject('Your Account is Verified by BdREN at '.$app_name)
                ->view('email.UserAccountVerifiedEmail', $data);
    }
}
