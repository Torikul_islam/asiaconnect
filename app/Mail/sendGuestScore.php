<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class sendGuestScore extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $registred_email;
    public function __construct($maturity_level,$Organization)
    {
        $this->maturity_level=$maturity_level;
        $this->Organization=$Organization;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $app_name = env("APP_NAME");
        $data['app_url']=env("APP_URL");
        $data['maturity_level']=$this->maturity_level;
        $data['Organization']=$this->Organization;
        return $this->subject('NREN Maturity Score from '.$app_name)
                ->view('email.sendGuestScore', $data);
    }
}
