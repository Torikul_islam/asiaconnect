<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NRENsupporting_online_education extends Model
{
    use HasFactory;
    protected $guarded = [];
}
