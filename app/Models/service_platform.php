<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class service_platform extends Model
{
    use HasFactory;
    protected $guarded = [];
}
