<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class network_trafficand_monitoring extends Model
{
    use HasFactory;
    protected $guarded = [];
}
