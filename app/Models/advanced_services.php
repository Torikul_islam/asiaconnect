<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class advanced_services extends Model
{
    use HasFactory;
    protected $guarded = [];
}
