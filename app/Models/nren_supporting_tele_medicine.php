<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class nren_supporting_tele_medicine extends Model
{
    use HasFactory;
    protected $guarded = [];
}
