<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DataExport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $structures = include "../../../data/column.php";
        $datas = include "../../../data/question.php";


        $tr = 1;
        foreach ($datas as $data) {
            $dataCollection = collect();
            $user = User::where('name', $data[2])->first();
            if ($user) {

                $user->page_status = "page16";
                $user->save();

                foreach ($data as $key => $value) {
                    if ($structures['table'][$key] != '') {
                        if (!$dataCollection->has($structures['table'][$key]))
                            $dataCollection->put($structures['table'][$key], collect());

                        if (!$dataCollection->get($structures['table'][$key])->has('user_id'))
                            $dataCollection->get($structures['table'][$key])->put('user_id', $user->id);


                        if ($structures['column'][$key] != '') {
                            if ($structures['type'][$key] == '1') {
                                $value = json_encode(explode(', ', $value));
                            }
                            $dataCollection->get($structures['table'][$key])->put($structures['column'][$key], $value);
                        }

                        // created_at
                        if (!$dataCollection->get($structures['table'][$key])->has('created_at')) {
                            $dataCollection->get($structures['table'][$key])->put('created_at', now());
                            $dataCollection->get($structures['table'][$key])->put('updated_at', now());
                        }
                    }
                }


                if ($tr) {
                    //Insert in database
                    foreach ($dataCollection as $table => $data) {
                        DB::table($table)->truncate();
                    }
                    $tr = 0;
                }

                foreach ($dataCollection as $key => $value) {
                    DB::table($key)->insert($value->toArray());
                    echo $key . '<br>';
                }
            } else {
                echo "Not found user: " . $data[2] . '<br>';
            }
        }

        return Command::SUCCESS;
    }
}
