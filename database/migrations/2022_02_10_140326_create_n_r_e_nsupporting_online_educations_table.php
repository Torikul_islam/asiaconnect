<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNRENsupportingOnlineEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('n_r_e_nsupporting_online_educations', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('Video_Collaboration_Software')->nullable();
            $table->string('NumberofVirtual_Classes_jan_2020')->nullable();
            $table->string('NumberofVirtual_Classes_feb_2020')->nullable();
            $table->string('NumberofVirtual_Classes_march_2020')->nullable();
            $table->string('NumberofVirtual_Classes_april_2020')->nullable();
            $table->string('NumberofVirtual_Classes_may_2020')->nullable();
            $table->string('NumberofVirtual_Classes_june_2020')->nullable();
            $table->string('NumberofVirtual_Classes_july_2020')->nullable();
            $table->string('NumberofVirtual_Classes_august_2020')->nullable();
            $table->string('NumberofVirtual_Classes_september_2020')->nullable();
            $table->string('NumberofVirtual_Classes_october_2020')->nullable();
            $table->string('NumberofVirtual_Classes_url')->nullable();

            $table->string('Duration_Virtual_Classes_jan_2020')->nullable();
            $table->string('Duration_Virtual_Classes_feb_2020')->nullable();
            $table->string('Duration_Virtual_Classes_march_2020')->nullable();
            $table->string('Duration_Virtual_Classes_april_2020')->nullable();
            $table->string('Duration_Virtual_Classes_may_2020')->nullable();
            $table->string('Duration_Virtual_Classes_june_2020')->nullable();
            $table->string('Duration_Virtual_Classes_july_2020')->nullable();
            $table->string('Duration_Virtual_Classes_august_2020')->nullable();
            $table->string('Duration_Virtual_Classes_september_2020')->nullable();
            $table->string('Duration_Virtual_Classes_october_2020')->nullable();
            $table->string('Duration_Virtual_Classes_URL')->nullable();

            $table->string('Total_Participants_VirtualClasses_jan_2020')->nullable();
            $table->string('Total_Participants_VirtualClasses_feb_2020')->nullable();
            $table->string('Total_Participants_VirtualClasses_march_2020')->nullable();
            $table->string('Total_Participants_VirtualClasses_april_2020')->nullable();
            $table->string('Total_Participants_VirtualClasses_may_2020')->nullable();
            $table->string('Total_Participants_VirtualClasses_june_2020')->nullable();
            $table->string('Total_Participants_VirtualClasses_july_2020')->nullable();
            $table->string('Total_Participants_VirtualClasses_august_2020')->nullable();
            $table->string('Total_Participants_VirtualClasses_september_2020')->nullable();
            $table->string('Total_Participants_VirtualClasses_october_2020')->nullable();

            $table->string('Total_Participants_VirtualClasses_URL')->nullable();
            $table->string('Video_Collaboration_Software_run')->nullable();

            $table->string('Domestic_Internet_Traffic')->nullable();
            $table->string('DIT_maximum_IX_january')->nullable();

            $table->string('DIT_maximum_IX_june')->nullable();
            $table->string('Available_Software')->nullable();
            $table->string('Maximum_Software')->nullable();
            $table->string('quantity_available_Licenses')->nullable();

            $table->string('Clients_VCA_institutes')->nullable();
            $table->string('Clients_VCA_faculty')->nullable();
            $table->string('Clients_VCA_student')->nullable();

            $table->string('Challenges_faced_by_students')->nullable();
            $table->longText('Challenges_faced_by_faculty')->nullable();
            $table->string('Challenges_faced_by_nren')->nullable();

            $table->string('limitation_VCA')->nullable();
            $table->string('lack_of_hardware')->nullable();

            $table->string('high_bandwidth_cost')->nullable();
            $table->string('confidence_assessment')->nullable();

            $table->string('lack_lms_software')->nullable();
            $table->string('support_24')->nullable();

            $table->string('other_step')->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('n_r_e_nsupporting_online_educations');
    }
}
