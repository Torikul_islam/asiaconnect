<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapacityDevelopmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capacity_developments', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('NREN_Engineers_expert')->nullable();
            $table->string('NREN_Engineers_intermediate')->nullable();
            $table->string('NREN_Engineers_beginner')->nullable();
            $table->string('Member_Institutes_expert')->nullable();
            $table->string('Member_Institutes_intermediate')->nullable();
            $table->string('Member_Institutes_beginner')->nullable();
            $table->string('wp2andwp3_package')->nullable();
            $table->longText('efficacy_WP2_WP3_packages')->nullable();
            $table->string('AsiaConnect_Financed_Training_International')->nullable();
            $table->string('AsiaConnect_Financed_Training_National')->nullable();
            $table->string('own_Financed_Training_International')->nullable();
            $table->string('own_Financed_Training_National')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capacity_developments');
    }
}
