<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIdentityandTrustServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('identityand_trust_services', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('Identity_and_Trust_Services')->nullable();
            $table->string('Video_Collaboration')->nullable();
            $table->string('Digital_Library')->nullable();
            $table->string('Trusted_Digital_Certificate')->nullable();
            $table->string('Big_File_Sender')->nullable();
            $table->string('Cloud_Storage')->nullable();
            $table->string('Laptop_Backup_Services')->nullable();
            $table->string('Research_Repository')->nullable(); 
            $table->string('Federation_architecture')->nullable();
            $table->string('information_Federation_Services_IdPs')->nullable();
            $table->string('information_Federation_Services_SPs')->nullable();
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identityand_trust_services');
    }
}
