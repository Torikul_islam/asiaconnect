<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLandscapeAndMarketSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landscape_and_market_shares', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('Institutions_category')->nullable();
            $table->string('Institutions_connected_Universities')->nullable();
            $table->string('Institutions_connected_RI')->nullable();
            $table->string('Institutions_connected_govt')->nullable();
            $table->string('Institutions_connected_school')->nullable();
            $table->string('Institutions_connected_other')->nullable();
            $table->string('Institutions_Target_Universities')->nullable();
            $table->string('Institutions_Target_RI')->nullable(); 
            $table->string('Institutions_Target_govt')->nullable();
            $table->string('Institutions_Target_school')->nullable();
            $table->string('Institutions_Target_other')->nullable();
            $table->string('Institutions_Target_Universities_now')->nullable();
            $table->string('Institutions_Target_RI_now')->nullable();
            $table->string('Institutions_Target_govt_now')->nullable();
            $table->string('Institutions_Target_school_now')->nullable();
            $table->string('Institutions_Target_other_now')->nullable();
            $table->string('user_Universities')->nullable();
            $table->string('user_RI')->nullable();
            $table->string('user_govt')->nullable();
            $table->string('user_school')->nullable();
            $table->string('user_other')->nullable();
            $table->string('total_Universities')->nullable();
            $table->string('lessthan_FE_Universities')->nullable();
            $table->string('lessthan_FE_RI')->nullable();
            $table->string('lessthan_FE_govt')->nullable();
            $table->string('lessthan_FE_school')->nullable();
            $table->string('lessthan_FE_other')->nullable();
            $table->string('FE_Universities')->nullable();
            $table->string('FE_RI')->nullable();
            $table->string('FE_govt')->nullable();
            $table->string('FE_school')->nullable();
            $table->string('FE_other')->nullable();
            $table->string('t1G_Universities')->nullable();
            $table->string('t1G_RI')->nullable();
            $table->string('t1G_govt')->nullable();
            $table->string('t1G_school')->nullable();
            $table->string('t1G_other')->nullable();
            $table->string('t10G_Universities')->nullable();
            $table->string('t10G_RI')->nullable();
            $table->string('t10G_govt')->nullable();
            $table->string('t10G_school')->nullable();
            $table->string('t10G_other')->nullable();
            $table->string('t40G_Universities')->nullable();
            $table->string('t40G_RI')->nullable();
            $table->string('t40G_govt')->nullable();
            $table->string('t40G_school')->nullable();
            $table->string('t40G_other')->nullable();
            $table->string('hightest_throughput_Universities')->nullable();
            $table->string('hightest_throughput_RI')->nullable();
            $table->string('hightest_throughput_govt')->nullable();
            $table->string('hightest_throughput_school')->nullable();
            $table->string('hightest_throughput_other')->nullable();
            $table->string('average_throughput_Universities')->nullable();
            $table->string('average_throughput_RI')->nullable();
            $table->string('average_throughput_govt')->nullable();
            $table->string('average_throughput_school')->nullable();
            $table->string('average_throughput_other')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landscape_and_market_shares');
    }
}
