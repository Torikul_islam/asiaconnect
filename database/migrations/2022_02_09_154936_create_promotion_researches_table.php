<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionResearchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion_researches', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('NREN_promotional_newsletter')->nullable();
            $table->string('NREN_promotional_leaflets')->nullable();
            $table->string('NREN_promotional_tender')->nullable();
            $table->string('NREN_promotional_gift')->nullable();
            $table->string('NREN_virtual_seminarTEIN_Sponsored')->nullable();
            $table->string('NREN_physical_seminarTEIN_Sponsored')->nullable();
            $table->string('NREN_virtual_seminar_NREN_Sponsored')->nullable();
            $table->string('NREN_Physical_seminar_NREN_Sponsored')->nullable(); 
            $table->string('number_registrants_apan47')->nullable();
            $table->string('number_registrants_apan48')->nullable();
            $table->string('communicationWithInstitutions')->nullable();
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion_researches');
    }
}
