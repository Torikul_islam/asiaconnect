<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('ad_con_name');
            $table->string('ad_con_designation');
            $table->string('ad_con_email');
            $table->string('ad_con_phone');

            $table->string('tec_con_name');
            $table->string('tec_con_designation');
            $table->string('tec_con_email');
            $table->string('tec_con_phone');
            
            $table->string('visi_con_name');
            $table->string('visi_con_designation');
            $table->string('visi_con_email');
            $table->string('visi_con_phone');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
