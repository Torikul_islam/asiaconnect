<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInitiativesofTEINCCSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('initiativesof_t_e_i_n_c_c_s', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('efficacy_WP2_WP3_packages')->nullable();
            $table->string('efficacy_WP4_WP5_packages')->nullable();
            $table->string('satisfied_on_TEINCC')->nullable();
            $table->string('strengths_of_TEINCC')->nullable();
            $table->string('weaknesses_of_TEINCC')->nullable();
            $table->string('opportunities_TEINCC')->nullable();
            $table->string('looming_Threats_TEINCC')->nullable();
           

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('initiativesof_t_e_i_n_c_c_s');
    }
}
