<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvancedServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advanced_services', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('service_demand')->nullable();
            $table->string('service_implemented')->nullable();
            $table->string('service_pipeline')->nullable();
            $table->string('DNS_Server')->nullable();
            $table->string('web_Server')->nullable();
            $table->string('mail_Server')->nullable();
            $table->string('dhcp_Server')->nullable();
            $table->string('routing_system')->nullable(); 
            $table->string('switching_system')->nullable();
            $table->string('ipv6_bgp_upstream')->nullable();
            $table->string('ipv6_bgp_downstream')->nullable();
            $table->string('ipv6_bgp_domestic')->nullable();
            $table->string('ipv6_bgp_cdn')->nullable();
            $table->string('upstram_peer_ipv4')->nullable();
            $table->string('upstram_peer_ipv6')->nullable();
            $table->string('parallei_peer_ipv4')->nullable();
            $table->string('parallei_peer_ipv6')->nullable();
            $table->string('downstream_peer_ipv4')->nullable();
            $table->string('downstream_peer_ipv6')->nullable();
            $table->string('ipv6_tein_upstream')->nullable();
            $table->string('application_server_ipv4')->nullable();
            $table->string('application_server_ipv6')->nullable();
            $table->string('config_network_ipv6')->nullable();
            $table->string('public_asn')->nullable();
            $table->string('ipv4_24_blocks')->nullable();
            $table->string('independent_ASN')->nullable();
            $table->string('IPv6_Routed_ASN')->nullable();
            $table->string('MANRS')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advanced_services');
    }
}
