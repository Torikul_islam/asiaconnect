<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChallengesAndNrenAdaptibilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenges_and_nren_adaptibilities', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('Internet_jan_2020')->nullable();
            $table->string('Internet_feb_2020')->nullable();
            $table->string('Internet_march_2020')->nullable();
            $table->string('Internet_april_2020')->nullable();
            $table->string('Internet_may_2020')->nullable();
            $table->string('Internet_june_2020')->nullable();
            $table->string('Internet_july_2020')->nullable();
            $table->string('Internet_august_2020')->nullable(); 
            $table->string('Internet_september_2020')->nullable();
            $table->string('Internet_october_2020')->nullable();
            $table->string('Internet_MRTG')->nullable();
            $table->string('Research_Traffic_jan_2020')->nullable();
            $table->string('Research_Traffic_feb_2020')->nullable();
            $table->string('Research_Traffic_march_2020')->nullable();
            $table->string('Research_Traffic_april_2020')->nullable();
            $table->string('Research_Traffic_may_2020')->nullable();
            $table->string('Research_Traffic_june_2020')->nullable();
            $table->string('Research_Traffic_july_2020')->nullable();
            $table->string('Research_Traffic_august_2020')->nullable();
            $table->string('Research_Traffic_september_2020')->nullable();
            $table->string('Research_Traffic_october_2020')->nullable();
            $table->string('Research_Traffic_MRTG')->nullable();
            $table->string('Impact_Revenue')->nullable();
            $table->string('Major_Threat')->nullable();
            $table->string('Big_Opportunities')->nullable();
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenges_and_nren_adaptibilities');
    }
}
