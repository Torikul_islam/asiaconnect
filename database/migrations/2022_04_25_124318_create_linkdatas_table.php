<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkdatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linkdatas', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('link_id')->nullable();
            $table->foreign('link_id')->references('id')->on('links');

            $table->unsignedBigInteger('month_id')->nullable();
            $table->foreign('month_id')->references('id')->on('allmonths');

            $table->string('AvailabilitywithoutMaintenance')->nullable();
            $table->string('AvailabilitywithMaintenance')->nullable();
            $table->string('Traffic_in')->nullable();
            $table->string('Traffic_out')->nullable();
            $table->string('Average_in')->nullable();
            $table->string('Average_out')->nullable();
            $table->string('max_in')->nullable();
            $table->string('max_out')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('linkdatas');
    }
}
