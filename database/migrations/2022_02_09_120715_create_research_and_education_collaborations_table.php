<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResearchAndEducationCollaborationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research_and_education_collaborations', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('ResEdu_activities')->nullable();
            $table->string('Title_of_Research')->nullable();
            $table->string('research_url')->nullable();
            $table->string('Abstract')->nullable();
            $table->string('name_email_first_author')->nullable();
            $table->string('scope')->nullable();
            $table->string('Beneficiaries')->nullable();
            $table->string('Financing')->nullable(); 
            $table->string('NREN_contribute')->nullable();
            $table->string('ResEdu_activities_other')->nullable();
            $table->string('Title_of_Research_other')->nullable();
            $table->string('research_url_other')->nullable();
            $table->string('Abstract_other')->nullable();
            $table->string('name_email_first_author_other')->nullable();
            $table->string('scope_other')->nullable();
            $table->string('Beneficiaries_other')->nullable();
            $table->string('Financing_other')->nullable();
            $table->string('NREN_contribute_other')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('research_and_education_collaborations');
    }
}
