<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNrenSupportingTeleMedicinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nren_supporting_tele_medicines', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('Supporting_Tele_medicine')->nullable();
            $table->string('Challenges_Tele_medicine')->nullable();
            $table->string('Mitigating_Steps_Tele_medicine')->nullable();
            $table->string('step_supporting_CandV')->nullable();
            $table->string('challenges_supporting_CandV')->nullable();
            $table->string('Mitigating_Steps_CandV')->nullable();
            $table->string('anyOther_support_services')->nullable();
            $table->string('role_of_teincc')->nullable(); 
            $table->string('Looming_Threat')->nullable();
            $table->string('Future_Opportunity')->nullable();
            $table->string('availing_opportunity_dampening')->nullable();
            $table->string('CommentConductedSurvey')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nren_supporting_tele_medicines');
    }
}
