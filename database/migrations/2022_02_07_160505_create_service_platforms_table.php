<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicePlatformsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_platforms', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('network_service_SO')->nullable();
            $table->string('security_service_SO')->nullable();
            $table->string('identity_service_SO')->nullable();
            $table->string('collaboration_service_SO')->nullable();
            $table->string('multimedia_service_SO')->nullable();
            $table->string('storage_service_SO')->nullable();
            $table->string('professional_service_SO')->nullable();
            $table->string('flagship_1_61')->nullable(); 
            $table->string('flagship_2_61')->nullable();
            $table->string('flagship_3_61')->nullable();
            $table->string('flagship_4_61')->nullable();
            $table->string('flagship_5_61')->nullable();
            $table->string('flagship_1_62')->nullable();
            $table->string('flagship_2_62')->nullable();
            $table->string('flagship_3_62')->nullable();
            $table->string('flagship_4_62')->nullable();
            $table->string('flagship_5_62')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_platforms');
    }
}
