<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNetworkTrafficandMonitoringsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('network_trafficand_monitorings', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('type_of_traffic')->nullable();
            $table->string('total_trafficfor_RE')->nullable();
            $table->string('Upstream_commodity_2018')->nullable();
            $table->string('Upstream_RE_2018')->nullable();
            $table->string('Upstream_CDN_2018')->nullable();
            $table->string('Upstream_IXP_2018')->nullable();
            $table->string('Upstream_commodity_2019')->nullable();
            $table->string('Upstream_RE_2019')->nullable(); 
            $table->string('Upstream_CDN_2019')->nullable();
            $table->string('Upstream_IXP_2019')->nullable();
            $table->string('ratio_DU_commodity_traffic')->nullable();
            $table->string('ratio_DU_research_traffic')->nullable();
            $table->string('yearly_traffic_commodity')->nullable();
            $table->string('yearly_traffic_RE')->nullable();
            $table->string('yearly_traffic_CDN')->nullable();
            $table->string('yearly_traffic_IXP')->nullable();
            $table->string('MRTG_commodity')->nullable();
            $table->string('MRTG_credentilas')->nullable();
            $table->string('MRTG_ResearchTraffic')->nullable();
            $table->string('MRTG_CreAccess')->nullable();
            $table->string('MRTG_CDN')->nullable();
            $table->string('MRTG_credentilas_2')->nullable();
            $table->string('MRTG_domestic')->nullable();
            $table->string('MRTG_credentilas_3')->nullable();
            $table->string('ipv6_traffic')->nullable();
            $table->string('ipv4_bgp_upstream_2018')->nullable();
            $table->string('ipv4_bgp_downstream_2018')->nullable();
            $table->string('ipv4_bgp_domestic_2018')->nullable();
            $table->string('ipv4_bgp_cdn_2018')->nullable();
            $table->string('ipv4_bgp_upstream_2019')->nullable();
            $table->string('ipv4_bgp_downstream_2019')->nullable();
            $table->string('ipv4_bgp_domestic_2019')->nullable();
            $table->string('ipv4_bgp_cdn_2019')->nullable();
            $table->string('upstream_non_outage')->nullable();
            $table->string('upstream_outage')->nullable();
            $table->string('downstream_non_outage')->nullable();
            $table->string('downstream_outage')->nullable();
            $table->string('network_diagram_url')->nullable();
            $table->string('network_diagram_credentilas')->nullable();
            $table->string('NOC')->nullable();
            $table->string('tools_TMM')->nullable();
            $table->string('statistical_data')->nullable();
            $table->string('communication_way')->nullable();
            $table->string('SLA')->nullable();
            $table->string('option_of_compensation')->nullable();
    

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('network_trafficand_monitorings');
    }
}
