<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStructureAndFinancingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('structure_and_financings', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('Strapolicies')->nullable();
            $table->string('emp_technical')->nullable();
            $table->string('emp_non_technical')->nullable();
            $table->string('emp_permanent')->nullable();
            $table->string('emp_contract')->nullable();
            $table->string('emp_outsourced')->nullable();
            $table->string('emp_female_technical')->nullable();
            $table->string('emp_fem_non_technical')->nullable(); 
            $table->string('emp_fem_permanent')->nullable();
            $table->string('emp_fem_contract')->nullable();
            $table->string('emp_fem_outsourced')->nullable();
            $table->string('capital_ownfund')->nullable();
            $table->string('capital_government')->nullable();
            $table->string('capital_donation')->nullable();
            $table->string('capital_others')->nullable();
            $table->string('operating_ownfund')->nullable();
            $table->string('operating_government')->nullable();
            $table->string('operating_donation')->nullable();
            $table->string('operating_others')->nullable();
            $table->string('distribute_netoperation')->nullable();
            $table->string('distribute_netmaintenance')->nullable();
            $table->string('distribute_administration')->nullable();
            $table->string('distribute_other')->nullable();
            $table->string('network_operation')->nullable();
            $table->string('network_maintenance')->nullable();
            $table->string('backbone_connectivity')->nullable();
            $table->string('access_connectivity')->nullable();
            $table->string('backbone_fiber')->nullable();
            $table->string('access_fiber')->nullable();
            $table->string('membership_fees')->nullable();
            $table->string('bandwidth_service')->nullable();
            $table->string('other_service')->nullable();
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('structure_and_financings');
    }
}
