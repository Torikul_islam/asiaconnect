<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEduroamServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eduroam_services', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('eduroam')->nullable();
            $table->string('Campus')->nullable();
            $table->string('Access_Points')->nullable();
            $table->string('Service_Location_Map')->nullable();
            $table->string('F_ticks')->nullable();
            $table->string('CAT')->nullable();
            $table->string('govRoam')->nullable();
            $table->string('eduRoam_Hotspot')->nullable(); 
            $table->string('Authentications_NRS_National_2018')->nullable();
            $table->string('Authentications_NRS_International_2018')->nullable();
            $table->string('Authentications_NRS_National_2019')->nullable();
            $table->string('Authentications_NRS_International_2019')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eduroam_services');
    }
}
