<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNetworkInfrastructureDarkFibersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('network_infrastructure_dark_fibers', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('dark_fiber_domestic_usage')->nullable();
            $table->string('dark_fiber_domestic_long')->nullable();
            $table->string('dark_fiber_cross_border')->nullable();
            $table->string('dark_fiber_cross_border_long')->nullable();
            $table->string('Alien_Wave_technology')->nullable();
            $table->string('offer_services_Alien_Wave_technology')->nullable();
            $table->string('IP_Trunk')->nullable();
            $table->string('aggregate_link_capacity')->nullable(); 
            $table->string('sdn_pilot')->nullable();
            $table->string('sdn_production')->nullable();
            $table->string('sdn_tested')->nullable();
            $table->string('sdn_planned')->nullable();
            $table->string('sdn_not_planned')->nullable();
            $table->string('NFV')->nullable();
            $table->string('Server_Farm')->nullable();
            $table->string('TLS')->nullable();
            $table->string('spam_andfishing')->nullable();
            $table->string('NGN_Firewall')->nullable();
            $table->string('Sand_Boxing')->nullable();
            $table->string('DDOS_Protection')->nullable();
            $table->string('DPI')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('network_infrastructure_dark_fibers');
    }
}
